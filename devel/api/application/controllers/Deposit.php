<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Deposit extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertdeposit() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $im = $parameter['im'];
        $td = $parameter['td'];
        $kp = $parameter['kp'];
        $nm = $parameter['nm'];
        $kd = $parameter['kd'];
        $no_tiket = $parameter['tiket'];
        $td_real = $parameter['td_real'];
        $pin = $parameter['pin'];
        $fr = $parameter['fr'];
        $bk = $parameter['bk'];
        $no_tiket = 0;
        if ($td_real == '') {
            $td_real = $td;
        }

        if ($no_tiket == '') {
            $no_tiket = 0;
            $totalDeposit = $td + $no_tiket;
        } else {
            $no_tiket = $parameter['tiket'];
            $totalDeposit = $td;
        }

        //$kp = $_REQUEST['kp'];

        $authen_key = md5(hash('sha512', $this->general_lib->random_numbers(6) . date('YmdHis'))); //md5($this->general_lib->random_numbers(6) . date('Ymd'));
        $no_deposit = "91" . $this->general_lib->random_numbers(6);
        $expired = date("Y-m-d H:i:s", strtotime('+1 hours'));
        $noDeposit = 'TH' . $no_deposit;

        $sAddress = "SELECT * FROM tb_customer WHERE id_customer='" . $im . "' ";
        $qAddress = $this->db->query($sAddress);
        $rAddress = $qAddress->row_array();

        $nama = $rAddress['nm_customer'];
        $email = $rAddress['email'];
        $nohp = $rAddress['no_hp'];

        if ($fr == '') {
            $fr = 'SH';
        }

        if ($bk == '') {
            $bk = 'BCA';
        }


        if ($rAddress['real_pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah...";
            $this->general_lib->error($code,$status);
        }

        $sBank = "SELECT * FROM tb_general_setting WHERE bank='" . $bk . "' AND status = '1' AND no_rekening!='' AND bank!='' AND tipe=99 ORDER BY date_created DESC ";
        $qBank = $this->db->query($sBank);
        $rBank = $qBank->row_array();

        $nmBank = $rBank['bank'];
        $norek = $rBank['no_rekening'];
        $atasNama = $rBank['atas_nama'];
        $descBank = $rBank['desc_setting'];
        $bank = $descBank;

        if ($kd == '') {
            $kd = 'DPT' . substr($td, 0, -3);
        }

        $item = array();
        $item[] = array(
            'name' => 'Deposit Thoyyiban',
            'sku' => $kd,
            'qty' => '1',
            'unitPrice' => $totalDeposit,
            'desc' => 'Deposit Thoyyiban',
            'produkToken' => '',
            'totalPrice' => $totalDeposit
        );


        $url = $this->url_witami . 'winpaybayar_api.php?action=insert_payment&u=userPST&p=88ef2b397e8a18c5881d020d3e8c7fa3';
        $fields = array(
            'fr' => 'SH',
            'ia' => '937',
            'nt' => $noDeposit,
            'ip_add' => '119.82.225.33',
            'ip' => $kp,
            'am' => $totalDeposit,
            'ph' => $nohp,
            'em' => $email,
            'bn' => $nama,
            'qty' => '1',
            'de' => 'Deposit Thoyyiban',
            'item' => $item,
            'tp' => '1'
        );
        $resp = pushAPISSL($url, $fields);
        $encode = json_decode($resp, true);

        $link = $encode['result']['url_pembayaran'];
        $kdBayar = $encode['result']['payment_code'];

        if ($encode['code'] != 200) {
            $code = '06';
            $status = $encode['message'];
            $this->general_lib->error($code,$status);
        }

        if ($nm == '') {
            $url2 = $this->url_witami . 'winpayproduk_api.php?action=get_produkpayment&u=userPST&p=88ef2b397e8a18c5881d020d3e8c7fa3';
            $fields2 = array(
                'kd' => $kp,
                'fr' => 'SH'
            );
            $resp2 = pushAPISSL($url2, $fields2);
            $encode2 = json_decode($resp2, true);

            $nm = $encode2['result'][0]['nama'];
        }

        $tl = "Permintaan deposit berhasil";
        $tl_eng = "The deposit request was successful";
        $tl_ar = "طلب الإيداع كان ناجحا";
        if ($kp == 'bca' || $kp == 'epaybri' || $kp == 'cimb' || $kp == 'danamon' || $kp == 'muamalat' || $kp == 'btnonline' || $kp == 'bni' || $kp == 'mandirikp' || $kp == 'kkwp' || $kp == 'cimbpc' || $kp == 'finpay_code') {
            $pesan = "Deposit Anda sedang dalam proses, silahkan melakukan pembayaran sebesar Rp." . number_format($totalDeposit) . " melalui " . $nm
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Request for deposit is successful, please make a payment of IDR " . number_format($totalDeposit) . " through " . $nm
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "طلب الإيداع ناجح ، يرجى تسديد مبلغ IDR " . number_format($totalDeposit) . " عبر " . $nm
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'bri') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer melalui akun BRI Mocash Anda dengan kode pembayaran: " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer through your BRI Mocash account with payment code: " . $kdBayar . ", IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "إيداعك قيد التنفيذ ، يرجى التحويل من خلال حساب BRI Mocash الخاص بك مع رمز الدفع: " . $kdBayar . " ، IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'tcash') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Telkomsel Cash, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Telkomsel Cash, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Telkomsel Cash ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'mandiri') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Mandiri Payment Code, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Kode pembayaran Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Mandiri Payment Code, Please make payment of IDR " . number_format($totalDeposit) . ". Your payment code is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Mandiri Payment Code ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". رمز الدفع الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'xltunai') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Xl Tunai, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Xl Tunai, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Xl Tunai ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'atm_prima') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Atm Bca, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Atm Bca, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Atm Bca ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'bniva') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer ke akun virtual BNI Anda dengan no akun : " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer to your BNI virtual account with account no: " . $kdBayar . ", for IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "يرجى النقل إلى حساب BNI الافتراضي الخاص بك مع رقم الحساب: " . $kdBayar . " ، مقابل IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'briva') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer ke akun virtual BRI Anda dengan no akun : " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer to your BRI virtual account with account no: " . $kdBayar . ", for IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "يرجى النقل إلى حساب BRI الافتراضي الخاص بك مع رقم الحساب: " . $kdBayar . " ، مقابل IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'indomaret') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Indomaret, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Indomaret, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Indomaret ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'alfamart') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Alfamart, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Alfamart, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Alfamart ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        }

        if ($im != '' && $td != '') {
            $sqlInsertDeposit = "INSERT INTO tb_deposit (no_deposit, total_deposit, authentication_key,date_expired,bank,no_rekening,atas_nama,
                date_created, date_updated, tipe, status,id_customer,tiket,real_deposit,from_cross,keterangan,kd_payment,nm_payment,link_payment,kd_produk_deposit)
                VALUES ('" . $no_deposit . "', '" . $totalDeposit . "','" . $authen_key . "','" . $expired . "','" . $nmBank . "','" . $norek . "','" . $atasNama . "',
                 NOW(), NOW(), '91', '1','" . $im . "','" . $no_tiket . "','" . $td_real . "','" . $fr . "','" . $pesan . "','" . $kp . "','" . $nm . "','" . $link . "','" . $kd . "') ";
            $this->db->query($sqlInsertDeposit);

            $s1 = "INSERT INTO tb_notif
                (id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $im . "','" . $tl . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan_eng . "','" . $tl_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            if ($rAddress['tipe'] == 1) {
                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Terima kasih telah melakukan permintaan deposit.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>No Deposit </td><td>:</td><td> <b>' . $no_deposit . '</b> </td></tr>
                                                                        <tr><td>Total Deposit </td><td>:</td><td>Rp.' . number_format($totalDeposit) . ' </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        ' . $pesan . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                $addheader = "DEPOSIT";
                $subject = "DEPOSIT";
                pushEmailServer($email, $nama, $message, $addheader, $subject);
            } elseif ($rAddress['tipe'] == 2) {
                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Terima kasih telah melakukan permintaan deposit.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>No Deposit </td><td>:</td><td> <b>' . $no_deposit . '</b> </td></tr>
                                                                        <tr><td>Total Deposit </td><td>:</td><td>Rp.' . number_format($totalDeposit) . ' </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        ' . $pesan . '
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                $addheader = "DEPOSIT";
                $subject = "DEPOSIT";
                pushEmailHerbal($email, $nama, $message, $addheader, $subject);
            }
            

            $sql = "SELECT * FROM tb_deposit WHERE no_deposit='" . $no_deposit . "'";

            responseQuery($sql, $err, $this->msg_code, $this->action, $this->lk, $pesan);
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function depositukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $td = $parameter['td'];
        $kp = $parameter['kp'];
        $nm = $parameter['nm'];
        $kd = $parameter['kd'];
        $no_tiket = $parameter['tiket'];
        $td_real = $parameter['td_real'];
        $pin = $parameter['pin'];
        $fr = $parameter['fr'];
        $bk = $parameter['bk'];

        $no_tiket = 0;
        if ($td_real == '') {
            $td_real = $td;
        }

        if ($no_tiket == '') {
            $no_tiket = 0;
            $totalDeposit = $td + $no_tiket;
        } else {
            $no_tiket = $parameter['tiket'];
            $totalDeposit = $td;
        }

        //$kp = $_REQUEST['kp'];

        $authen_key = md5(hash('sha512', $this->general_lib->random_numbers(6) . date('YmdHis'))); //md5($this->general_lib->random_numbers(6) . date('Ymd'));
        $no_deposit = "92" . $this->general_lib->random_numbers(6);
        $expired = date("Y-m-d H:i:s", strtotime('+1 hours'));
        $noDeposit = 'TH' . $no_deposit;

        $sAddress = "SELECT * FROM tb_ukm WHERE id_ukm='" . $iu . "' ";
        $qAddress = $this->db->query($sAddress);
        $rAddress = $qAddress->row_array();

        $nama = $rAddress['nm_ukm'];
        $email = $rAddress['email'];
        $nohp = $rAddress['no_hp'];

        if ($fr == '') {
            $fr = 'SH';
        }

        if ($bk == '') {
            $bk = 'BCA';
        }


        if ($rAddress['real_pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah...";
            $this->general_lib->error($code,$status);
        }

        $sBank = "SELECT * FROM tb_general_setting WHERE bank='" . $bk . "' AND status = '1' AND no_rekening!='' AND bank!='' AND tipe=99 ORDER BY date_created DESC ";
        $qBank = $this->db->query($sBank);
        $rBank = $qBank->row_array();

        $nmBank = $rBank['bank'];
        $norek = $rBank['no_rekening'];
        $atasNama = $rBank['atas_nama'];
        $descBank = $rBank['desc_setting'];
        $bank = $descBank;

        $item = array();
        $item[] = array(
            'name' => 'Deposit Thoyyiban',
            'sku' => $kd,
            'qty' => '1',
            'unitPrice' => $totalDeposit,
            'desc' => 'Deposit Thoyyiban',
            'produkToken' => '',
            'totalPrice' => $totalDeposit
        );


        $url = $this->url_witami . 'winpaybayar_api.php?action=insert_payment&u=userPST&p=88ef2b397e8a18c5881d020d3e8c7fa3';
        $fields = array(
            'fr' => 'SH',
            'ia' => '937',
            'nt' => $noDeposit,
            'ip_add' => '119.82.225.33',
            'ip' => $kp,
            'am' => $totalDeposit,
            'ph' => $nohp,
            'em' => $email,
            'bn' => $nama,
            'qty' => '1',
            'de' => 'Deposit Thoyyiban',
            'item' => $item,
            'tp' => '1'
        );
        $resp = pushAPISSL($url, $fields);
        $encode = json_decode($resp, true);

        $link = $encode['result']['url_pembayaran'];
        $kdBayar = $encode['result']['payment_code'];

        if ($encode['code'] != 200) {
            $code = '06';
            $status = $encode['message'];
            $this->general_lib->error($code,$status);
        }

        if ($nm == '') {
            $url2 = $this->url_witami . 'winpayproduk_api.php?action=get_produkpayment&u=userPST&p=88ef2b397e8a18c5881d020d3e8c7fa3';
            $fields2 = array(
                'kd' => $kp,
                'fr' => 'SH'
            );
            $resp2 = pushAPISSL($url2, $fields2);
            $encode2 = json_decode($resp2, true);

            $nm = $encode2['result'][0]['nama'];
        }

        $tl = "Permintaan deposit berhasil";
        $tl_eng = "The deposit request was successful";
        $tl_ar = "طلب الإيداع كان ناجحا";
        if ($kp == 'bca' || $kp == 'epaybri' || $kp == 'cimb' || $kp == 'danamon' || $kp == 'muamalat' || $kp == 'btnonline' || $kp == 'bni' || $kp == 'mandirikp' || $kp == 'kkwp' || $kp == 'cimbpc' || $kp == 'finpay_code') {
            $pesan = "Deposit Anda sedang dalam proses, silahkan melakukan pembayaran sebesar Rp." . number_format($totalDeposit) . " melalui " . $nm
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Request for deposit is successful, please make a payment of IDR " . number_format($totalDeposit) . " through " . $nm
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "طلب الإيداع ناجح ، يرجى تسديد مبلغ IDR " . number_format($totalDeposit) . " عبر " . $nm
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'bri') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer melalui akun BRI Mocash Anda dengan kode pembayaran: " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer through your BRI Mocash account with payment code: " . $kdBayar . ", IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "إيداعك قيد التنفيذ ، يرجى التحويل من خلال حساب BRI Mocash الخاص بك مع رمز الدفع: " . $kdBayar . " ، IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'tcash') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Telkomsel Cash, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Telkomsel Cash, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Telkomsel Cash ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'mandiri') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Mandiri Payment Code, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Kode pembayaran Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Mandiri Payment Code, Please make payment of IDR " . number_format($totalDeposit) . ". Your payment code is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Mandiri Payment Code ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". رمز الدفع الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'xltunai') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Xl Tunai, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Xl Tunai, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Xl Tunai ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'atm_prima') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Atm Bca, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Atm Bca, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Atm Bca ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'bniva') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer ke akun virtual BNI Anda dengan no akun : " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer to your BNI virtual account with account no: " . $kdBayar . ", for IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "يرجى النقل إلى حساب BNI الافتراضي الخاص بك مع رقم الحساب: " . $kdBayar . " ، مقابل IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'briva') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer ke akun virtual BRI Anda dengan no akun : " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer to your BRI virtual account with account no: " . $kdBayar . ", for IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "يرجى النقل إلى حساب BRI الافتراضي الخاص بك مع رقم الحساب: " . $kdBayar . " ، مقابل IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'indomaret') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Indomaret, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Indomaret, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Indomaret ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'alfamart') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Alfamart, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Alfamart, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Alfamart ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        }

        if ($iu != '' && $td != '') {

            $sqlInsertDeposit = "INSERT INTO tb_deposit (no_deposit, total_deposit, authentication_key,date_expired,bank,no_rekening,atas_nama,
                date_created, date_updated, tipe, status,id_ukm,tiket,real_deposit,from_cross,keterangan,kd_payment,nm_payment,link_payment,kd_produk_deposit)
                VALUES ('" . $no_deposit . "', '" . $totalDeposit . "','" . $authen_key . "','" . $expired . "','" . $nmBank . "','" . $norek . "','" . $atasNama . "',
                 NOW(), NOW(), '92', '1','" . $iu . "','" . $no_tiket . "','" . $td_real . "','" . $fr . "','" . $pesan . "','" . $kp . "','" . $nm . "','" . $link . "','" . $kd . "') ";
            $this->db->query($sqlInsertDeposit);

            $s1 = "INSERT INTO tb_notif
                (id_ukm,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $iu . "','" . $tl . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan_eng . "','" . $tl_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Terima kasih telah melakukan permintaan deposit.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>No Deposit </td><td>:</td><td> <b>' . $no_deposit . '</b> </td></tr>
                                                                        <tr><td>Total Deposit </td><td>:</td><td>Rp.' . number_format($totalDeposit) . ' </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        ' . $pesan . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "DEPOSIT";
            $subject = "DEPOSIT";
            pushEmailServer($email, $nama, $message, $addheader, $subject);

            $sql = "SELECT * FROM tb_deposit WHERE no_deposit='" . $no_deposit . "'";

            responseQuery($sql, $err, $this->msg_code, $this->action, $this->lk, $pesan);
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function depositdepo() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $td = $parameter['td'];
        $kp = $parameter['kp'];
        $nm = $parameter['nm'];
        $kd = $parameter['kd'];
        $no_tiket = $parameter['tiket'];
        $td_real = $parameter['td_real'];
        $pin = $parameter['pin'];
        $fr = $parameter['fr'];
        $bk = $parameter['bk'];

        $no_tiket = 0;
        if ($td_real == '') {
            $td_real = $td;
        }

        if ($no_tiket == '') {
            $no_tiket = 0;
            $totalDeposit = $td + $no_tiket;
        } else {
            $no_tiket = $parameter['tiket'];
            $totalDeposit = $td;
        }

        //$kp = $_REQUEST['kp'];

        $authen_key = md5(hash('sha512', $this->general_lib->random_numbers(6) . date('YmdHis'))); //md5($this->general_lib->random_numbers(6) . date('Ymd'));
        $no_deposit = "94" . $this->general_lib->random_numbers(6);
        $expired = date("Y-m-d H:i:s", strtotime('+1 hours'));
        $noDeposit = 'TH' . $no_deposit;

        $sAddress = "SELECT * FROM tb_depo WHERE id_depo='" . $iu . "' ";
        $qAddress = $this->db->query($sAddress);
        $rAddress = $qAddress->row_array();

        $nama = $rAddress['nm_depo'];
        $email = $rAddress['email'];
        $nohp = $rAddress['no_hp'];

        if ($fr == '') {
            $fr = 'SH';
        }

        if ($bk == '') {
            $bk = 'BCA';
        }


        if ($rAddress['real_pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah...";
            $this->general_lib->error($code,$status);
        }

        $sBank = "SELECT * FROM tb_general_setting WHERE bank='" . $bk . "' AND status = '1' AND no_rekening!='' AND bank!='' AND tipe=99 ORDER BY date_created DESC ";
        $qBank = $this->db->query($sBank);
        $rBank = $qBank->row_array();

        $nmBank = $rBank['bank'];
        $norek = $rBank['no_rekening'];
        $atasNama = $rBank['atas_nama'];
        $descBank = $rBank['desc_setting'];
        $bank = $descBank;

        $item = array();
        $item[] = array(
            'name' => 'Deposit Thoyyiban',
            'sku' => $kd,
            'qty' => '1',
            'unitPrice' => $totalDeposit,
            'desc' => 'Deposit Thoyyiban',
            'produkToken' => '',
            'totalPrice' => $totalDeposit
        );


        $url = $this->url_witami . 'winpaybayar_api.php?action=insert_payment&u=userPST&p=88ef2b397e8a18c5881d020d3e8c7fa3';
        $fields = array(
            'fr' => 'SH',
            'ia' => '937',
            'nt' => $noDeposit,
            'ip_add' => '119.82.225.33',
            'ip' => $kp,
            'am' => $totalDeposit,
            'ph' => $nohp,
            'em' => $email,
            'bn' => $nama,
            'qty' => '1',
            'de' => 'Deposit Thoyyiban',
            'item' => $item,
            'tp' => '1'
        );
        $resp = pushAPISSL($url, $fields);
        $encode = json_decode($resp, true);

        $link = $encode['result']['url_pembayaran'];
        $kdBayar = $encode['result']['payment_code'];

        if ($encode['code'] != 200) {
            $code = '06';
            $status = $encode['message'];
            $this->general_lib->error($code,$status);
        }

        if ($nm == '') {
            $url2 = $this->url_witami . 'winpayproduk_api.php?action=get_produkpayment&u=userPST&p=88ef2b397e8a18c5881d020d3e8c7fa3';
            $fields2 = array(
                'kd' => $kp,
                'fr' => 'SH'
            );
            $resp2 = pushAPISSL($url2, $fields2);
            $encode2 = json_decode($resp2, true);

            $nm = $encode2['result'][0]['nama'];
        }

        $tl = "Permintaan deposit berhasil";
        $tl_eng = "The deposit request was successful";
        $tl_ar = "طلب الإيداع كان ناجحا";
        if ($kp == 'bca' || $kp == 'epaybri' || $kp == 'cimb' || $kp == 'danamon' || $kp == 'muamalat' || $kp == 'btnonline' || $kp == 'bni' || $kp == 'mandirikp' || $kp == 'kkwp' || $kp == 'cimbpc' || $kp == 'finpay_code') {
            $pesan = "Deposit Anda sedang dalam proses, silahkan melakukan pembayaran sebesar Rp." . number_format($totalDeposit) . " melalui " . $nm
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Request for deposit is successful, please make a payment of IDR " . number_format($totalDeposit) . " through " . $nm
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "طلب الإيداع ناجح ، يرجى تسديد مبلغ IDR " . number_format($totalDeposit) . " عبر " . $nm
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'bri') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer melalui akun BRI Mocash Anda dengan kode pembayaran: " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer through your BRI Mocash account with payment code: " . $kdBayar . ", IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "إيداعك قيد التنفيذ ، يرجى التحويل من خلال حساب BRI Mocash الخاص بك مع رمز الدفع: " . $kdBayar . " ، IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'tcash') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Telkomsel Cash, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Telkomsel Cash, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Telkomsel Cash ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'mandiri') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Mandiri Payment Code, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Kode pembayaran Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Mandiri Payment Code, Please make payment of IDR " . number_format($totalDeposit) . ". Your payment code is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Mandiri Payment Code ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". رمز الدفع الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'xltunai') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Xl Tunai, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Xl Tunai, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Xl Tunai ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'atm_prima') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Atm Bca, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Atm Bca, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Atm Bca ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'bniva') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer ke akun virtual BNI Anda dengan no akun : " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer to your BNI virtual account with account no: " . $kdBayar . ", for IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "يرجى النقل إلى حساب BNI الافتراضي الخاص بك مع رقم الحساب: " . $kdBayar . " ، مقابل IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'briva') {
            $pesan = "Deposit Anda sedang dalam proses, Silahkan transfer ke akun virtual BRI Anda dengan no akun : " . $kdBayar . ", sebesar Rp." . number_format($totalDeposit)
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, Please transfer to your BRI virtual account with account no: " . $kdBayar . ", for IDR " . number_format($totalDeposit)
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "يرجى النقل إلى حساب BRI الافتراضي الخاص بك مع رقم الحساب: " . $kdBayar . " ، مقابل IDR " . number_format($totalDeposit)
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'indomaret') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Indomaret, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Indomaret, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Indomaret ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        } elseif ($kp == 'alfamart') {
            $pesan = "Deposit Anda sedang dalam proses, Anda akan melakukan pembayaran menggunakan Alfamart, Silahkan melakukan pembayaran sejumlah Rp." . number_format($totalDeposit) . ". Order ID Anda adalah " . $kdBayar
                    . " <br>Deposit akan batal otomatis pada " . $expired . " apabila dalam kurun waktu 1 jam tidak melakukan pembayaran";
            $pesan_eng = "Your deposit is in process, You will make payment using Alfamart, Please make payment of IDR " . number_format($totalDeposit) . ". Your order ID is " . $kdBayar
                    . " <br>Deposit will be canceled automatically in " . $expired . " if within 1 hours of not making payments";
            $pesan_ar = "سوف تقوم بالدفع باستخدام Alfamart ، يرجى تسديد مبلغ الـ IDR " . number_format($totalDeposit) . ". معرّف الطلب الخاص بك هو " . $kdBayar
                    . " <br> سيتم الإلغاء تلقائيًا على " . $expired . " إذا لم يتم الدفع في غضون ساعة واحدة";
        }

        if ($iu != '' && $td != '') {

            $sqlInsertDeposit = "INSERT INTO tb_deposit (no_deposit, total_deposit, authentication_key,date_expired,bank,no_rekening,atas_nama,
                date_created, date_updated, tipe, status,id_depo,tiket,real_deposit,from_cross,keterangan,kd_payment,nm_payment,link_payment,kd_produk_deposit)
                VALUES ('" . $no_deposit . "', '" . $totalDeposit . "','" . $authen_key . "','" . $expired . "','" . $nmBank . "','" . $norek . "','" . $atasNama . "',
                 NOW(), NOW(), '94', '1','" . $iu . "','" . $no_tiket . "','" . $td_real . "','" . $fr . "','" . $pesan . "','" . $kp . "','" . $nm . "','" . $link . "','" . $kd . "') ";
            $this->db->query($sqlInsertDeposit);

            $s1 = "INSERT INTO tb_notif
                (id_depo,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $iu . "','" . $tl . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan_eng . "','" . $tl_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Terima kasih telah melakukan permintaan deposit.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>No Deposit </td><td>:</td><td> <b>' . $no_deposit . '</b> </td></tr>
                                                                        <tr><td>Total Deposit </td><td>:</td><td>Rp.' . number_format($totalDeposit) . ' </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        ' . $pesan . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "DEPOSIT";
            $subject = "DEPOSIT";
            pushEmailServer($email, $nama, $message, $addheader, $subject);

            $sql = "SELECT * FROM tb_deposit WHERE no_deposit='" . $no_deposit . "'";

            responseQuery($sql, $err, $this->msg_code, $this->action, $this->lk, $pesan);
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdeposit() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $im = $parameter['im'];
        $st = $parameter['st'];
        $lt = $parameter['lt'];
        if ($lt != '') {
            $lt = ' LIMIT ' . $lt;
        }

        if ($im != '') {
            $sql = "SELECT a.*,
        (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer) as nm_customer,
        (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer) as email,
        (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_customer) as no_hp
        FROM tb_deposit a
        WHERE a.id_customer = '" . $im . "'
        ORDER BY id_deposit DESC " . $lt;

            if ($st != '') {
                $sql = "SELECT a.*,
            (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer) as nm_customer,
            (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer) as email,
            (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_customer) as no_hp
            FROM tb_deposit a
            WHERE a.id_customer = '" . $im . "'
            AND a.status='" . $st . "'
            ORDER BY id_deposit DESC " . $lt;
            }

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdepositukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $st = $parameter['st'];

        if ($iu != '') {
            $sql = "SELECT a.*,
        (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_ukm,
        (SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_pemilik,
        (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as email,
        (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as no_hp
        FROM tb_deposit a
        WHERE a.id_ukm = '" . $iu . "'
        ORDER BY id_deposit DESC";

            if ($st != '') {
                $sql = "SELECT a.*,
            (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_ukm,
            (SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_pemilik,
            (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as email,
            (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as no_hp
            FROM tb_deposit a
            WHERE a.id_ukm = '" . $iu . "'
            AND a.status='" . $st . "'
            ORDER BY id_deposit DESC";
            }

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdepositdepo() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $st = $parameter['st'];

        if ($iu != '') {
            $sql = "SELECT a.*,
        (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_depo,
        (SELECT x.nm_pemilik FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_pemilik,
        (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_depo) as email,
        (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_depo) as no_hp
        FROM tb_deposit a
        WHERE a.id_depo = '" . $iu . "'
        ORDER BY id_deposit DESC";

            if ($st != '') {
                $sql = "SELECT a.*,
            (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_depo,
            (SELECT x.nm_pemilik FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_pemilik,
            (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_depo) as email,
            (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_depo) as no_hp
            FROM tb_deposit a
            WHERE a.id_depo = '" . $iu . "'
            AND a.status='" . $st . "'
            ORDER BY id_deposit DESC";
            }

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdepositcms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $st = $parameter['st'];

        $sql = "SELECT a.*,
            (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer) as nm_customer,
            (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer) as email,
            (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_customer) as no_hp
              FROM tb_deposit a
              WHERE a.tipe=91
              ORDER BY id_deposit DESC";

        if ($st != '') {
            $sql = "SELECT a.*,
                (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer) as nm_customer,
                (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer) as email,
                (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_customer) as no_hp
                  FROM tb_deposit a
                  WHERE a.status='" . $st . "'
                  AND a.tipe=91
                  ORDER BY id_deposit DESC";
        }


        $this->response->getresponse($sql,'success');
    }

    public function getdepositdepocms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $st = $parameter['st'];

        $sql = "SELECT a.*,
        (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_depo,
        (SELECT x.nm_pemilik FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_pemilik,
        (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_depo) as email,
        (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_depo) as no_hp
        FROM tb_deposit a
        WHERE a.tipe=94
        ORDER BY id_deposit DESC";

        if ($st != '') {
            $sql = "SELECT a.*,
            (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_depo,
            (SELECT x.nm_pemilik FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_pemilik,
            (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_depo) as email,
            (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_depo) as no_hp
            FROM tb_deposit a
            WHERE a.status='" . $st . "'
            AND a.tipe=94
            ORDER BY id_deposit DESC";
        }

        $this->response->getresponse($sql,'success');
    }

    public function getdepositukmcms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $st = $parameter['st'];

        $sql = "SELECT a.*,
        (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_ukm,
        (SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_pemilik,
        (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as email,
        (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as no_hp
        FROM tb_deposit a
        WHERE a.tipe=92
        ORDER BY id_deposit DESC";

        if ($st != '') {
            $sql = "SELECT a.*,
            (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_ukm,
            (SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_pemilik,
            (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as email,
            (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as no_hp
            FROM tb_deposit a
            WHERE a.status='" . $st . "'
            AND a.tipe=92
            ORDER BY id_deposit DESC";
        }


        $this->response->getresponse($sql,'success');
    }

    public function getbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
                (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer) as nm_customer,
                (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer) as email,
                (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_customer) as no_hp
                FROM tb_deposit a
                WHERE a.id_deposit = '" . $id . "'
                ORDER BY id_deposit DESC";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getukmbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
                (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_ukm,
                (SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as nm_pemilik,
                (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as email,
                (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_ukm) as no_hp
                FROM tb_deposit a
                WHERE a.id_deposit = '" . $id . "'
                ORDER BY id_deposit DESC";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdepobyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
                (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_depo,
                (SELECT x.nm_pemilik FROM tb_depo x WHERE x.id_depo = a.id_depo) as nm_pemilik,
                (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_depo) as email,
                (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_depo) as no_hp
                FROM tb_deposit a
                WHERE a.id_deposit = '" . $id . "'
                ORDER BY id_deposit DESC";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getagenbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
                (SELECT x.nm_agen FROM tb_agen x WHERE x.id_agen = a.id_agen) as nm_agen,
                (SELECT x.email FROM tb_agen x WHERE x.id_agen = a.id_agen) as email,
                (SELECT x.no_hp FROM tb_agen x WHERE x.id_agen = a.id_agen) as no_hp
                FROM tb_deposit a
                WHERE a.id_deposit = '" . $id . "'
                ORDER BY id_deposit DESC";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $td = $parameter['td'];
        $iu = $parameter['iu'];
        $pin = $parameter['pin'];
        $st = $parameter['st'];
        $ket = $parameter['ket'];

        if ($st == '') {
            $st = 3;
        }
        if ($id != '') {
            $sCek = "SELECT * FROM tb_deposit WHERE id_deposit = '" . $id . "'";
            $qCek = $this->db->query($sCek);
            $rCek = $qCek->row_array();

            $sAgen = "SELECT * FROM tb_user WHERE id_user='" . $iu . "'";
            $qAgen = $this->db->query($sAgen);
            $rAgen = $qAgen->row_array();

            /* if ($rAgen['status'] != 1) {
              $code = '205';
              $status = 'Akun Anda tidak aktif...';
              $this->general_lib->error($code,$status);
              }

              if ($rAgen['real_pin'] != $pin) {
              $code = '205';
              $status = 'PIN yang Anda masukan salah...';
              $this->general_lib->error($code,$status);
              }
             *
             */

            $total = $rCek['real_deposit'] + $rCek['tiket'];
            if ($td != '' && $td != $total) {
                $kurang = $total - $td;
                if ($kurang == 0) {
                    $noTiket = $rCek['tiket'];
                } else {
                    $noTiket = $rCek['tiket'] - $kurang;
                }
                if ($td < $total && $kurang > $rCek['tiket']) {
                    $noTiket = 0;
                    $saldo = $td;
                } else {
                    $saldo = $rCek['real_deposit'];
                }
            } else {
                $saldo = $rCek['real_deposit'];
                $noTiket = $rCek['tiket'];
            }

            $t_asli = $saldo + $noTiket;

            $sUpdateDeposit = "UPDATE tb_deposit SET date_updated = NOW(), total_deposit = '" . $t_asli . "',tiket='" . $noTiket . "',real_deposit='" . $saldo . "', status = '" . $st . "' WHERE id_deposit = '" . $id . "'";
            $this->db->query($sUpdateDeposit);

            if ($st == 3) {
                $sUpdateSado = "UPDATE tb_customer SET date_updated = NOW(), saldo = saldo+" . $t_asli . " WHERE id_customer = '" . $rCek['id_customer'] . "'";
                $this->db->query($sUpdateSado);
            }


            $sAnggota = "SELECT * FROM tb_customer WHERE id_customer='" . $rCek['id_customer'] . "'";
            $qAnggota = $this->db->query($sAnggota);
            $rAnggota = $qAnggota->row_array();

            if ($st == 3) {
                $nm = 'Top up deposit sukses';
                $pesan = 'Top up deposit dengan no deposit ' . $rCek['no_deposit'] . ' sebesar Rp.' . number_format($t_asli) . ' telah sukses';
                $nm_eng = 'Top up deposit successful';
                $pesan_eng = 'Top up deposit with no deposit of ' . $rCek['no_deposit'] . ' for Rp.' . number_format($t_asli) . ' has been successful';
                $nm_ar = 'الإيداع أعلى ناجحة';
                $pesan_ar = 'تم إيداع أعلى مع عدم وجود إيداع ' . $rCek['no_deposit'] . ' ل Rp.' . number_format($t_asli) . ' ناجحة';
            } elseif ($st == 8 || $st == 9) {
                $nm = 'Top up deposit batal';
                $pesan = 'Top up deposit dengan no deposit ' . $rCek['no_deposit'] . ' sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui';
                $nm_eng = 'Top Up deposit failed';
                $pesan_eng = 'Top up deposit with no deposit of ' . $rCek['no_deposit'] . ' for Rp.' . number_format($t_asli) . ' has been failed';
                $nm_ar = 'فشل إيداع أعلى';
                $pesan_ar = 'فشل إيداع أعلى مع عدم وجود إيداع ' . $rCek['no_deposit'] . ' من Rp.' . number_format($t_asli);
            }

            $s1 = "INSERT INTO tb_notif
                (id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $rAnggota['id_customer'] . "','" . $nm . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $nm_eng . "','" . $pesan_eng . "','" . $nm_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $email = $rAnggota['email'];
            $nama = $rAnggota['nm_customer'];

            if ($rAnggota['tipe'] == 1) {
                if ($st == 3) {
                    $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Top up deposit sebesar Rp.' . number_format($t_asli) . ' telah sukses
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>Total Saldo </td><td>:</td><td> Rp.<b>' . number_format($rAnggota['saldo']) . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                    $addheader = "DEPOSIT";
                    $subject = "DEPOSIT";
                } elseif ($st == 8 || $st == 9) {
                    $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Permintaan deposit sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Keterangan : <br>
                                                                        ' . $ket . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';

                    $addheader = "DEPOSIT";
                    $subject = "DEPOSIT";
                }
                pushEmailServer($email, $nama, $message, $addheader, $subject);
            } elseif ($rAnggota['tipe'] == 2) {
                if ($st == 3) {
                    $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Top up deposit sebesar Rp.' . number_format($t_asli) . ' telah sukses
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>Total Saldo </td><td>:</td><td> Rp.<b>' . number_format($rAnggota['saldo']) . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                    $addheader = "DEPOSIT";
                    $subject = "DEPOSIT";
                } elseif ($st == 8 || $st == 9) {
                    $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Permintaan deposit sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Keterangan : <br>
                                                                        ' . $ket . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';

                    $addheader = "DEPOSIT";
                    $subject = "DEPOSIT";
                }
                pushEmailHerbal($email, $nama, $message, $addheader, $subject);
            }
            

            $sql = "SELECT * FROM tb_deposit WHERE id_deposit = '" . $id . "'";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatusukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $td = $parameter['td'];
        $iu = $parameter['iu'];
        $pin = $parameter['pin'];
        $st = $parameter['st'];
        $ket = $parameter['ket'];

        if ($st == '') {
            $st = 3;
        }
        if ($id != '') {
            $sCek = "SELECT * FROM tb_deposit WHERE id_deposit = '" . $id . "'";
            $qCek = $this->db->query($sCek);
            $rCek = $qCek->row_array();

            $sUkm = "SELECT * FROM tb_ukm WHERE id_ukm='" . $rCek['id_ukm'] . "'";
            $qUkm = $this->db->query($sUkm);
            $rUkm = $qUkm->row_array();

            $sAgen = "SELECT * FROM tb_user WHERE id_user='" . $iu . "'";
            $qAgen = $this->db->query($sAgen);
            $rAgen = $qAgen->row_array();

            /* if ($rAgen['status'] != 1) {
              $code = '205';
              $status = 'Akun Anda tidak aktif...';
              $this->general_lib->error($code,$status);
              }

              if ($rAgen['real_pin'] != $pin) {
              $code = '205';
              $status = 'PIN yang Anda masukan salah...';
              $this->general_lib->error($code,$status);
              }
             *
             */

            $total = $rCek['real_deposit'] + $rCek['tiket'];
            if ($td != '' && $td != $total) {
                $kurang = $total - $td;
                if ($kurang == 0) {
                    $noTiket = $rCek['tiket'];
                } else {
                    $noTiket = $rCek['tiket'] - $kurang;
                }
                if ($td < $total && $kurang > $rCek['tiket']) {
                    $noTiket = 0;
                    $saldo = $td;
                } else {
                    $saldo = $rCek['real_deposit'];
                }
            } else {
                $saldo = $rCek['real_deposit'];
                $noTiket = $rCek['tiket'];
            }

            $t_asli = $saldo + $noTiket;

            $sUpdateDeposit = "UPDATE tb_deposit SET date_updated = NOW(), total_deposit = '" . $t_asli . "',tiket='" . $noTiket . "',real_deposit='" . $saldo . "', status = '" . $st . "' WHERE id_deposit = '" . $id . "'";
            $this->db->query($sUpdateDeposit);

            if ($st == 3) {
                $sUpdateSaldo = "UPDATE tb_ukm SET date_updated = NOW(), saldo = saldo+" . $t_asli . " WHERE id_ukm = '" . $rCek['id_ukm'] . "'";
                $this->db->query($sUpdateSaldo);
            }


            $sAnggota = "SELECT * FROM tb_ukm WHERE id_ukm='" . $rCek['id_ukm'] . "'";
            $qAnggota = $this->db->query($sAnggota);
            $rAnggota = $qAnggota->row_array();

            if ($st == 3) {
                $nm = 'Top up deposit sukses';
                $pesan = 'Top up deposit dengan no deposit ' . $rCek['no_deposit'] . ' sebesar Rp.' . number_format($t_asli) . ' telah sukses';
                $nm_eng = 'Top up deposit successful';
                $pesan_eng = 'Top up deposit with no deposit of ' . $rCek['no_deposit'] . ' for IDR ' . number_format($t_asli) . ' has been successful';
                $nm_ar = 'الإيداع أعلى ناجحة';
                $pesan_ar = 'تم إيداع أعلى مع عدم وجود إيداع ' . $rCek['no_deposit'] . ' ل IDR ' . number_format($t_asli) . ' ناجحة';
            } elseif ($st == 8 || $st == 9) {
                $nm = 'Top up deposit batal';
                $pesan = 'Top up deposit dengan no deposit ' . $rCek['no_deposit'] . ' sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui';
                $nm_eng = 'Top Up deposit failed';
                $pesan_eng = 'Top up deposit with no deposit of ' . $rCek['no_deposit'] . ' for IDR ' . number_format($t_asli) . ' has been failed';
                $nm_ar = 'فشل إيداع أعلى';
                $pesan_ar = 'فشل إيداع أعلى مع عدم وجود إيداع ' . $rCek['no_deposit'] . ' من IDR ' . number_format($t_asli);
            }

            $s1 = "INSERT INTO tb_notif
                (id_ukm,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $rAnggota['id_ukm'] . "','" . $nm . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $nm_eng . "','" . $pesan_eng . "','" . $nm_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $email = $rAnggota['email'];
            $nama = $rAnggota['nm_ukm'];

            if ($st == 3) {
                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Top up deposit sebesar Rp.' . number_format($t_asli) . ' telah sukses
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>Total Saldo </td><td>:</td><td> Rp.<b>' . number_format($rAnggota['saldo']) . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                $addheader = "DEPOSIT";
                $subject = "DEPOSIT";
            } elseif ($st == 8 || $st == 9) {
                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Permintaan deposit sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Keterangan : <br>
                                                                        ' . $ket . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';

                $addheader = "DEPOSIT";
                $subject = "DEPOSIT";
            }
            pushEmailServer($email, $nama, $message, $addheader, $subject);

            $sql = "SELECT * FROM tb_deposit WHERE id_deposit = '" . $id . "'";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatusdepo() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $td = $parameter['td'];
        $iu = $parameter['iu'];
        $pin = $parameter['pin'];
        $st = $parameter['st'];
        $ket = $parameter['ket'];

        if ($st == '') {
            $st = 3;
        }
        if ($id != '') {
            $sCek = "SELECT * FROM tb_deposit WHERE id_deposit = '" . $id . "'";
            $qCek = $this->db->query($sCek);
            $rCek = $qCek->row_array();

            $sUkm = "SELECT * FROM tb_depo WHERE id_depo='" . $rCek['id_depo'] . "'";
            $qUkm = $this->db->query($sUkm);
            $rUkm = $qUkm->row_array();

            $sAgen = "SELECT * FROM tb_user WHERE id_user='" . $iu . "'";
            $qAgen = $this->db->query($sAgen);
            $rAgen = $qAgen->row_array();

            /* if ($rAgen['status'] != 1) {
              $code = '205';
              $status = 'Akun Anda tidak aktif...';
              $this->general_lib->error($code,$status);
              }

              if ($rAgen['real_pin'] != $pin) {
              $code = '205';
              $status = 'PIN yang Anda masukan salah...';
              $this->general_lib->error($code,$status);
              }
             *
             */

            $total = $rCek['real_deposit'] + $rCek['tiket'];
            if ($td != '' && $td != $total) {
                $kurang = $total - $td;
                if ($kurang == 0) {
                    $noTiket = $rCek['tiket'];
                } else {
                    $noTiket = $rCek['tiket'] - $kurang;
                }
                if ($td < $total && $kurang > $rCek['tiket']) {
                    $noTiket = 0;
                    $saldo = $td;
                } else {
                    $saldo = $rCek['real_deposit'];
                }
            } else {
                $saldo = $rCek['real_deposit'];
                $noTiket = $rCek['tiket'];
            }

            $t_asli = $saldo + $noTiket;

            $sUpdateDeposit = "UPDATE tb_deposit SET date_updated = NOW(), total_deposit = '" . $t_asli . "',tiket='" . $noTiket . "',real_deposit='" . $saldo . "', status = '" . $st . "' WHERE id_deposit = '" . $id . "'";
            $this->db->query($sUpdateDeposit);

            if ($st == 3) {
                $sUpdateSaldo = "UPDATE tb_depo SET date_updated = NOW(), saldo = saldo+" . $t_asli . " WHERE id_depo = '" . $rCek['id_depo'] . "'";
                $this->db->query($sUpdateSaldo);
            }


            $sAnggota = "SELECT * FROM tb_depo WHERE id_depo='" . $rCek['id_depo'] . "'";
            $qAnggota = $this->db->query($sAnggota);
            $rAnggota = $qAnggota->row_array();

            if ($st == 3) {
                $nm = 'Top up deposit sukses';
                $pesan = 'Top up deposit dengan no deposit ' . $rCek['no_deposit'] . ' sebesar Rp.' . number_format($t_asli) . ' telah sukses';
                $nm_eng = 'Top up deposit successful';
                $pesan_eng = 'Top up deposit with no deposit of ' . $rCek['no_deposit'] . ' for IDR ' . number_format($t_asli) . ' has been successful';
                $nm_ar = 'الإيداع أعلى ناجحة';
                $pesan_ar = 'تم إيداع أعلى مع عدم وجود إيداع ' . $rCek['no_deposit'] . ' ل IDR ' . number_format($t_asli) . ' ناجحة';
            } elseif ($st == 8 || $st == 9) {
                $nm = 'Top up deposit batal';
                $pesan = 'Top up deposit dengan no deposit ' . $rCek['no_deposit'] . ' sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui';
                $nm_eng = 'Top Up deposit failed';
                $pesan_eng = 'Top up deposit with no deposit of ' . $rCek['no_deposit'] . ' for IDR ' . number_format($t_asli) . ' has been failed';
                $nm_ar = 'فشل إيداع أعلى';
                $pesan_ar = 'فشل إيداع أعلى مع عدم وجود إيداع ' . $rCek['no_deposit'] . ' من IDR ' . number_format($t_asli);
            }

            $s1 = "INSERT INTO tb_notif
                (id_depo,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $rAnggota['id_depo'] . "','" . $nm . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $nm_eng . "','" . $pesan_eng . "','" . $nm_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $email = $rAnggota['email'];
            $nama = $rAnggota['nm_depo'];

            if ($st == 3) {
                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Top up deposit sebesar Rp.' . number_format($t_asli) . ' telah sukses
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi deposit Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>Total Saldo </td><td>:</td><td> Rp.<b>' . number_format($rAnggota['saldo']) . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                $addheader = "DEPOSIT";
                $subject = "DEPOSIT";
            } elseif ($st == 8 || $st == 9) {
                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . headerEmail() . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Permintaan deposit sebesar Rp.' . number_format($t_asli) . ' tidak dapat kami setujui
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Keterangan : <br>
                                                                        ' . $ket . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . footerEmail() . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';

                $addheader = "DEPOSIT";
                $subject = "DEPOSIT";
            }
            pushEmailServer($email, $nama, $message, $addheader, $subject);

            $sql = "SELECT * FROM tb_deposit WHERE id_deposit = '" . $id . "'";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function transfer() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hpa = $parameter['hpa'];
        $hpt = $parameter['hpt'];
        $pin = $parameter['pin'];
        $nom = $parameter['nom'];

        $sAsal = "SELECT * FROM tb_customer WHERE no_hp='" . $hpa . "' AND tipe=2";
        $qAsal = $this->db->query($sAsal);
        $rAsal = $qAsal->row_array();

        $sTujuan = "SELECT * FROM tb_customer WHERE no_hp='" . $hpt . "' AND tipe=2";
        $qTujuan = $this->db->query($sTujuan);
        $rTujuan = $qTujuan->row_array();

        if ($rAsal['real_pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah.";
            $this->general_lib->error($code,$status);
        }

        if ($rAsal['saldo'] < $nom) {
            $code = '07';
            $status = "Saldo Anda tidak cukup.";
            $this->general_lib->error($code,$status);
        }

        if ($nom < 10000) {
            $code = '07';
            $status = "Transfer deposit minimal Rp.10.000";
            $this->general_lib->error($code,$status);
        }

        if ($rTujuan['id_customer'] == '') {
            $code = '07';
            $status = "No handphone tujuan transfer tidak terdaftar.";
            $this->general_lib->error($code,$status);
        }

        if ($rTujuan['status'] == 0) {
            $code = '07';
            $status = "Akun tujuan transfer tidak aktif.";
            $this->general_lib->error($code,$status);
        }

        if ($hpa == $hpt) {
            $code = '07';
            $status = "No handphone tujuan transfer tidak boleh sama dengan no Anda.";
            $this->general_lib->error($code,$status);
        }

        $sisaSaldo = $rAsal['saldo'] - $nom;
        $totalSaldo = $rTujuan['saldo'] + $nom;

        $noTrans = "43" . $this->general_lib->random_numbers(6);
        $tl = 'Transfer Deposit';
        $tl_eng = 'Transfer Deposit';
        $tl_ar = 'Transfer Deposit';

        $pesan = 'Transfer deposit kepada ' . $rTujuan['nm_customer'] . ' sebesar Rp.' . number_format($nom) . ' telah berhasil, sisa saldo Anda Rp.' . number_format($sisaSaldo);
        $pesan_eng = 'The deposit transfer to ' . $rTujuan['nm_customer'] . ' of IDR ' . number_format($nom) . ' has been successful, your remaining balance of IDR ' . number_format($sisaSaldo);
        $pesan_ar = 'لقد كان تحويل الوديعة إلى ' . $rTujuan['nm_customer'] . ' بقيمة ' . number_format($nom) . ' ريال عماني ناجحاً ، ورصيدك المتبقي من ' . number_format($sisaSaldo) . ' IDR';

        $pesan2 = 'Anda telah menerima transfer deposit dari ' . $rAsal['nm_customer'] . ' sebesar Rp.' . number_format($nom) . ' total saldo Anda Rp.' . number_format($totalSaldo);
        $pesan2_eng = 'You have received balance transfer from ' . $rAsal['nm_customer'] . ' of IDR ' . number_format($nom) . ', your total balance is IDR ' . number_format($totalSaldo);
        $pesan2_ar = 'لقد تلقيت تحويل الرصيد من ' . $rAsal['nm_customer'] . ' بقيمة ' . number_format($nom) . ' ريال عماني ، ورصيدك الإجمالي هو ' . number_format($totalSaldo) . ' IDR';
        if ($hpa != '' && $hpt != '' && $nom != '') {
            $insert = "INSERT INTO tb_transfer (no_transfer,no_hp_pengirim,no_hp_penerima,nominal,date_created,date_updated,tipe,status)"
                    . "VALUES('" . $noTrans . "','" . $hpa . "','" . $hpt . "','" . $nom . "',NOW(),NOW(),2,1)";
            $this->db->query($insert);

            $updateAsal = "UPDATE tb_customer SET saldo=saldo-" . $nom . " ,date_updated=NOW() WHERE no_hp='" . $hpa . "'";
            $this->db->query($updateAsal);

            $updateTujuan = "UPDATE tb_customer SET saldo=saldo+" . $nom . " ,date_updated=NOW() WHERE no_hp='" . $hpt . "'";
            $this->db->query($updateTujuan);

            $s1 = "INSERT INTO tb_notif
                (id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $rAsal['id_customer'] . "','" . $tl . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan_eng . "','" . $tl_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $s2 = "INSERT INTO tb_notif
                (id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $rTujuan['id_customer'] . "','" . $tl . "','" . $pesan2 . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan2_eng . "','" . $tl_ar . "','" . $pesan2_ar . "')";
            $this->db->query($s2);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $rAsal['nm_customer'] . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        ' . $pesan . '
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi transfer deposit :
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>No Handphone Tujuan Transfer</td><td>:</td><td> ' . $hpt . '</td></tr>
                                                                        <tr><td>Nama Tujuan Transfer</td><td>:</td><td>' . $rTujuan['nm_customer'] . '</td></tr>
                                                                        <tr><td>Nominal Transfer </td><td>:</td><td> Rp.' . number_format($nom) . '</td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "INFO";
            $subject = "TRANSFER SALDO";
            pushEmailHerbal($rAsal['email'], $rAsal['nm_customer'], $message, $addheader, $subject);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $rTujuan['nm_customer'] . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        ' . $pesan2 . '
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi transfer deposit :
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>No Handphone Pengirim</td><td>:</td><td> ' . $hpa . '</td></tr>
                                                                        <tr><td>Nama Pengirim</td><td>:</td><td>' . $rAsal['nm_customer'] . '</td></tr>
                                                                        <tr><td>Nominal Transfer </td><td>:</td><td> Rp.' . number_format($nom) . '</td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "INFO";
            $subject = "TRANSFER SALDO";
            pushEmailHerbal($rTujuan['email'], $rTujuan['nm_customer'], $message, $addheader, $subject);

            $sql = "SELECT * FROM tb_transfer WHERE no_transfer='" . $noTrans . "'";
            responseQuery($sql, $err, $this->msg_code, $this->action, $this->lk, $pesan);
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function gettransfer() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];

        if ($hp != '') {
            $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim AND x.tipe=2),'') as nm_pengirim,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim AND x.tipe=2),'') as email_pengirim,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim AND x.tipe=2),'') as image_pengirim,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima AND x.tipe=2),'') as nm_penerima,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima AND x.tipe=2),'') as email_penerima,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima AND x.tipe=2),'') as image_penerima
                    FROM tb_transfer a WHERE a.no_hp_pengirim='" . $hp . "' AND a.tipe=2 ORDER BY a.date_created DESC";
            $query = $this->db->query($sql);
            while ($row = mysqli_fetch_assoc($query)) {
                $result[] = $row;
            }

            $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim AND x.tipe=2),'') as nm_pengirim,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim AND x.tipe=2),'') as email_pengirim,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim AND x.tipe=2),'') as image_pengirim,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima AND x.tipe=2),'') as nm_penerima,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima AND x.tipe=2),'') as email_penerima,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima AND x.tipe=2),'') as image_penerima
                    FROM tb_transfer a WHERE a.no_hp_penerima='" . $hp . "' AND a.tipe=2 ORDER BY a.date_created DESC";
            $query = $this->db->query($sql);
            while ($row = mysqli_fetch_assoc($query)) {
                $result[] = $row;
            }

            if ($result == null) {
                $code = '01';
                $status = 'Data not found.';
                $this->general_lib->error($code,$status);
            }
            $code = '200';
            $status = "Proses berhasil.";
            $str = array(
                "result" => $result,
                "code" => $code,
                "message" => $status
            );

            $json = json_encode($str);
            header("Content-Type: application/json");
            ob_clean();
            flush();
            echo $json;
            exit(1);
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function gettransferbypengirim() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];

        if ($hp != '') {
            $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as nm_pengirim,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as email_pengirim,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as image_pengirim,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as nm_penerima,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as email_penerima,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as image_penerima
                    FROM tb_transfer a WHERE a.no_hp_pengirim='" . $hp . "' ORDER BY a.date_created DESC";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function gettransferbypenerima() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];

        if ($hp != '') {
            $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as nm_pengirim,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as email_pengirim,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as image_pengirim,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as nm_penerima,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as email_penerima,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as image_penerima
                    FROM tb_transfer a WHERE a.no_hp_penerima='" . $hp . "' ORDER BY a.date_created DESC";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function gettransfercms() {

        $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as nm_pengirim,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as email_pengirim,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as image_pengirim,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as nm_penerima,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as email_penerima,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as image_penerima
                    FROM tb_transfer a ORDER BY a.date_created DESC";
        $this->response->getresponse($sql,'success');
    }

    public function gettransferbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as nm_pengirim,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as email_pengirim,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_pengirim),'') as image_pengirim,
                    IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as nm_penerima,
                    IFNULL((SELECT x.email FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as email_penerima,
                    IFNULL((SELECT x.image FROM tb_customer x WHERE x.no_hp = a.no_hp_penerima),'') as image_penerima
                    FROM tb_transfer a WHERE a.id_transfer='" . $id . "'";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

}
