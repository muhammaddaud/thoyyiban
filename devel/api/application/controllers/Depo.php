<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Depo extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertdepo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $nmpm = addslashes($parameter['nmpm']);
        $kl = $parameter['kl'];
        $klu = $parameter['klu'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $posu = addslashes($parameter['posu']);
        $ktp = addslashes($parameter['ktp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $pv = $parameter['pv'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ju = $parameter['ju'];
        $bu = addslashes($parameter['bu']);
        $lt = $parameter['lt'];
        $ci = $parameter['ci'];
        $im = $parameter['im'];
        $ia = $parameter['ia'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = addslashes($parameter['npwp']);
        $siup = addslashes($parameter['siup']);
        $tdp = addslashes($parameter['tdp']);
        $ud = addslashes($parameter['ud']);
        $ip = $parameter['ip'];

        $password = $parameter['pass'];
        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }

        $pass = md5(hash('sha512', $password));

        $real_pin = $parameter['pin'];
        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        if ($tp == '') {
            $tp = 1;
        }

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        $sCekEmail = $this->db->query("SELECT id_depo FROM tb_depo WHERE email='" . $em . "'");
        $rCekEmail = $sCekEmail->row_array();

        $sCekHp = $this->db->query("SELECT id_depo FROM tb_depo WHERE no_hp='" . $ph . "'");
        $rCekHp = $sCekHp->row_array();

        if ($rCekEmail['id_depo'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_depo'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        $sCekEmailUkm = $this->db->query("SELECT id_ukm FROM tb_ukm WHERE email='" . $em . "'");
        $rCekEmailUkm = $sCekEmailUkm->row_array();

        $sCekHpUkm = $this->db->query("SELECT id_ukm FROM tb_ukm WHERE no_hp='" . $ph . "'");
        $rCekHpUkm = $sCekHpUkm->row_array();

        if ($rCekEmailUkm['id_ukm'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHpUkm['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($kl != '') {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $pv = $rKota['id_provinsi'];
            $kt = $rKota['id_kota'];
            $kc = $rKec['id_kecamatan'];
        }

        if ($klu != '') {
            $sqlKelu = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $klu . "' ");
            $rKelu = $sqlKelu->row_array();

            $sqlKecu = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKelu['id_kecamatan'] . "' ");
            $rKecu = $sqlKecu->row_array();

            $sqlKotau = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKecu ['id_kota'] . "' ");
            $rKotau = $sqlKotau->row_array();

            $pvu = $rKotau['id_provinsi'];
            $ktu = $rKotau['id_kota'];
            $kcu = $rKecu['id_kecamatan'];
        }

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $kd_depo = "TYD" . $this->general_lib->random_numbers(6);

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }


        if ($em != '' && $ph != '' && $nmp != '') {
            $sqlInsertUkm = "INSERT INTO tb_ukm (kd_ukm, nm_ukm, provinsi, kota, alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_anggota,id_rt,cabang_lpt,id_produsen,image_kk,image_keterangan_usaha,jenis_usaha,skala_ukm,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '1','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $id_anggota . "','937','1','" . $ip . "','" . $fotokk . "','" . $fotoku . "','" . $ju . "','" . $sk . "','ID','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "') ";
            $this->db->query($sqlInsertUkm);

            $sUkm = $this->db->query("SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'");
            $rUkm = $sUkm->row_array();

            $updateUkm = "UPDATE tb_ukm SET id_agen='" . $rUkm['id_ukm'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateUkm);

            $sqlInsert = "INSERT INTO tb_depo (id_mitra,kd_depo, nm_depo, provinsi, kota, alm_depo,no_ktp,nm_pemilik,desc_depo,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_produsen,id_rt,image_kk,image_keterangan_usaha,jenis_usaha,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha,id_ukm)
            VALUES ('" . $im . "','" . $kd_depo . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $almt . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '1','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $ip . "','937','" . $fotokk . "','" . $fotoku . "','" . $ju . "','ID','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "','" . $rUkm['id_ukm'] . "') ";
            $this->db->query($sqlInsert);

            $sDepo = $this->db->query("SELECT id_depo FROM tb_depo WHERE no_hp='" . $ph . "'");
            $rDepo = $sDepo->row_array();

            $updateDepo = "UPDATE tb_depo SET id_agen='" . $rDepo['id_depo'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateDepo);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_depo a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'insertdepo');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatedepo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $nmpm = addslashes($parameter['nmpm']);
        $pv = $parameter['pv'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $klu = $parameter['klu'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $posu = addslashes($parameter['posu']);
        $ktp = addslashes($parameter['ktp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $desc_eng = addslashes($parameter['desc_eng']);
        $desc_ar = addslashes($parameter['desc_ar']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ju = $parameter['ju'];
        $bu = addslashes($parameter['bu']);
        $lt = $parameter['lt'];
        $ma = $parameter['ma'];
        $ci = $parameter['ci'];
        $im = $parameter['im'];
        $id = $parameter['id'];

        $npwp = addslashes($parameter['npwp']);
        $siup = addslashes($parameter['siup']);
        $tdp = addslashes($parameter['tdp']);
        $ud = addslashes($parameter['ud']);

        $sCek = $this->db->query("SELECT * FROM tb_depo WHERE id_depo='" . $id . "'");
        $rCek = $sCek->row_array();

        if ($tp == '') {
            $tp = $rCek['tipe'];
        }

        if ($nmp == '') {
            $nmp = $rCek['nm_pemilik'];
        }

        if ($nmpm == '') {
            $nmpm = $rCek['nm_pemohon'];
        }

        if ($pv == '') {
            $pv = $rCek['provinsi'];
        }

        if ($kc == '') {
            $kc = $rCek['kecamatan'];
        }

        if ($kt == '') {
            $kt = $rCek['kota'];
        }

        if ($kl == '') {
            $kl = $rCek['kelurahan'];
        }

        if ($klu == '') {
            $klu = $rCek['kelurahan_usaha'];
        }

        if ($almt == '') {
            $almt = $rCek['alm_depo'];
        }

        if ($jk == '') {
            $jk = $rCek['jenis_kelamin'];
        }

        if ($tl == '') {
            $tl = $rCek['tempat_lahir'];
        }

        if ($tgl == '') {
            $tgl = $rCek['tgl_lahir'];
        }

        if ($em == '') {
            $em = $rCek['email'];
        }

        if ($ktp == '') {
            $ktp = $rCek['no_ktp'];
        }

        if ($npwp == '') {
            $npwp = $rCek['no_npwp'];
        }

        if ($siup == '') {
            $siup = $rCek['siup'];
        }

        if ($tdp == '') {
            $tdp = $rCek['tdp'];
        }

        if ($ud == '') {
            $ud = $rCek['ud'];
        }

        if ($nmu == '') {
            $nmu = $rCek['nm_depo'];
        }

        if ($desc == '') {
            $desc = $rCek['desc_depo'];
        }

        if ($desc_eng == '') {
            $desc_eng = $rCek['desc_depo_eng'];
        }

        if ($desc_ar == '') {
            $desc_ar = $rCek['desc_depo_ar'];
        }

        if ($au == '') {
            $au = $rCek['alm_usaha'];
        }

        if ($bk == '') {
            $bk = $rCek['bank'];
        }

        if ($nr == '') {
            $nr = $rCek['no_rekening'];
        }

        if ($an == '') {
            $an = $rCek['atas_nama'];
        }

        if ($pos == '') {
            $pos = $rCek['kode_pos'];
        }

        if ($posu == '') {
            $posu = $rCek['kode_pos_usaha'];
        }

        if ($ju == '') {
            $ju = $rCek['jenis_usaha'];
        }

        if ($bu == '') {
            $bu = $rCek['bentuk_usaha'];
        }

        if ($lt == '') {
            $lt = $rCek['latitude'];
        }

        if ($ma == '') {
            $ma = $rCek['max_agen'];
        }

        if ($ci == '') {
            $ci = $rCek['country_id'];
        }

        if ($im == '') {
            $im = $rCek['id_mitra'];
        }

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($kl != '') {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $pv = $rKota['id_provinsi'];
            $kt = $rKota['id_kota'];
            $kc = $rKec['id_kecamatan'];
        }

        if ($klu != '') {
            $sqlKelu = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $klu . "' ");
            $rKelu = $sqlKelu->row_array();

            $sqlKecu = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKelu['id_kecamatan'] . "' ");
            $rKecu = $sqlKecu->row_array();

            $sqlKotau = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKecu ['id_kota'] . "' ");
            $rKotau = $sqlKotau->row_array();

            $pvu = $rKotau['id_provinsi'];
            $ktu = $rKotau['id_kota'];
            $kcu = $rKecu['id_kecamatan'];
        }

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoktp);
        }

        if ($fotoktp == '') {
            $fotoktp = $rCek['image_ktp'];
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotousaha);
        }

        if ($fotousaha == '') {
            $fotousaha = $rCek['logo'];
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotokk);
        }

        if ($fotokk == '') {
            $fotokk = $rCek['image_kk'];
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoku);
        }

        if ($fotoku == '') {
            $fotoku = $rCek['image_keterangan_usaha'];
        }

        if ($em != '' && $ph != '' && $id != '' && $nmp != '') {
           $sqlUpdate = "UPDATE tb_depo SET nm_depo='" . $nmu . "', provinsi='" . $pv . "', kota='" . $kt . "',id_mitra='" . $im . "',alm_depo='" . $almt . "',no_ktp='" . $ktp . "',nm_pemilik='" . $nmp . "',desc_depo='" . $desc . "',alm_usaha='" . $au . "',email='" . $em . "',no_hp='" . $ph . "',foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "',country_id='" . $ci . "',kecamatan='" . $kc . "',kelurahan='" . $kl . "',bank='" . $bk . "',desc_depo_eng='" . $desc_eng . "',desc_depo_ar='" . $desc_ar . "',jenis_kelamin='" . $jk . "',tgl_lahir='" . $tgl . "',no_rekening='" . $nr . "',no_npwp='" . $npwp . "',siup='" . $siup . "',tdp='" . $tdp . "',ud='" . $ud . "',atas_nama='" . $an . "',tempat_lahir='" . $tl . "',kode_pos='" . $pos . "',image_kk='" . $fotokk . "',image_keterangan_usaha='" . $fotoku . "',jenis_usaha = '" . $ju . "',latitude = '" . $lt . "',max_agen = '" . $ma . "',nm_pemohon = '" . $nmpm . "',provinsi_usaha = '" . $pvu . "',kota_usaha = '" . $ktu . "',kecamatan_usaha = '" . $kcu . "',kelurahan_usaha = '" . $klu . "',kode_pos_usaha = '" . $posu . "',bentuk_usaha = '" . $bu . "' WHERE id_depo='" . $id . "' ";
            $this->db->query($sqlUpdate);

            if ($rCek['id_ukm'] != 0) {
                $sqlUpdateUkm = "UPDATE tb_ukm SET nm_ukm='" . $nmu . "', provinsi='" . $pv . "', kota='" . $kt . "',alm_ukm='" . $almt . "',no_ktp='" . $ktp . "',nm_pemilik='" . $nmp . "',desc_ukm='" . $desc . "',alm_usaha='" . $au . "',country_id='" . $ci . "',email='" . $em . "',no_hp='" . $ph . "',foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "',max_agen='" . $ma . "',kecamatan='" . $kc . "',kelurahan='" . $kl . "',bank='" . $bk . "',desc_ukm_eng='" . $desc_eng . "',desc_ukm_ar='" . $desc_ar . "',jenis_kelamin='" . $jk . "',tgl_lahir='" . $tgl . "',no_rekening='" . $nr . "',no_npwp='" . $npwp . "',siup='" . $siup . "',tdp='" . $tdp . "',ud='" . $ud . "',atas_nama='" . $an . "',tempat_lahir='" . $tl . "',kode_pos='" . $pos . "',image_kk='" . $fotokk . "',latitude = '" . $lt . "',image_keterangan_usaha='" . $fotoku . "',jenis_usaha='" . $ju . "',nm_pemohon = '" . $nmpm . "',provinsi_usaha = '" . $pvu . "',kota_usaha = '" . $ktu . "',kecamatan_usaha = '" . $kcu . "',kelurahan_usaha = '" . $klu . "',kode_pos_usaha = '" . $posu . "',bentuk_usaha = '" . $bu . "' WHERE id_ukm='" . $rCek['id_ukm'] . "' ";
                $this->db->query($sqlUpdateUkm);
            }

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_depo a
             WHERE a.id_depo='" . $id . "'";

            $this->response->getresponse($sql,'updatedepo');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdepo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $hp = $parameter['hp'];
        $ci = $parameter['ci'];
        $ipr = $parameter['ipr'];
        $lat = $parameter['lat'];
        $im = $parameter['im'];

        $produsen = "";
        if ($ipr != '') {
            $produsen = " AND a.id_produsen='" . $ipr . "' ";
        }

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $mitra = "";
        if ($im != '') {
            $mitra = " AND a.id_mitra='" . $im . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
        IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
        IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
        IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
        IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
        IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
        FROM tb_depo a
        WHERE a.status=1
        " . $mitra . $tipe . $prov . $kota . $kec . $kel . $produsen . $country . "
        ORDER BY a.id_depo DESC " . $limit;

        if ($hp != '') {
            $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
        IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
        IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
        IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
        IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
        IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
        FROM tb_depo a
        WHERE a.no_hp='" . $hp . "' ";
        }


        if ($lat != '') {
            $exp = explode(",", $lat);

            $latt = $exp[0];
            $long = $exp[1];

            $sql = "SELECT a.*,a.provinsi as id_provinsi,a.kota as id_kota,a.kecamatan as id_kecamatan,a.kelurahan as id_kelurahan,a.alm_usaha as alamat,
                IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
                IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
                IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
                IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
                (
                6371 * acos (
                cos ( radians(" . $latt . ") )
               * cos( radians( SUBSTRING_INDEX(a.latitude,',',1) ) )
               * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(a.latitude,',',2),',',-1) ) - radians(" . $long . ") )
                + sin ( radians(" . $latt . ") )
               * sin( radians( SUBSTRING_INDEX(a.latitude,',',1) ) )
                )
                ) AS distance
                FROM tb_depo a
                WHERE a.status=1 ORDER BY distance ASC ";
        }

        $this->response->getresponse($sql,'getdepo');
    }

    public function getdepocms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $ipr = $parameter['ipr'];
        $ci = $parameter['ci'];
        $im = $parameter['im'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $produsen = "";
        if ($ipr != '') {
            $produsen = " AND a.id_produsen='" . $ipr . "' ";
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $mitra = "";
        if ($im != '') {
            $mitra = " AND a.id_mitra='" . $im . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
        IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
        IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
        IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
        IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
        IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
        FROM tb_depo a WHERE id_depo!=''
        " . $mitra . $tipe . $prov . $kota . $kec . $kel . $produsen . $country . "
        ORDER BY a.id_depo DESC " . $limit;
        $this->response->getresponse($sql,'getdepocms');
    }

    public function getdepocmsnext() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $id = $parameter['id'];
        $ipr = $parameter['ipr'];
        $ci = $parameter['ci'];
        $im = $parameter['im'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $produsen = "";
        if ($ipr != '') {
            $produsen = " AND a.id_produsen='" . $ipr . "' ";
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $mitra = "";
        if ($im != '') {
            $mitra = " AND a.id_mitra='" . $im . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
        IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
        IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
        IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
        IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
        IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
        FROM tb_depo a WHERE id_depo<'" . $id . "'
        " . $mitra . $tipe . $prov . $kota . $kec . $kel . $produsen . $country . "
        ORDER BY a.id_depo DESC " . $limit;
        $this->response->getresponse($sql,'getdepocmsnext');
    }

    public function getdepocmsprev() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $id = $parameter['id'];
        $ipr = $parameter['ipr'];
        $ci = $parameter['ci'];
        $im = $parameter['im'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $produsen = "";
        if ($ipr != '') {
            $produsen = " AND a.id_produsen='" . $ipr . "' ";
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $mitra = "";
        if ($im != '') {
            $mitra = " AND a.id_mitra='" . $im . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
        IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
        IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
        IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
        IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
        IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
        FROM tb_depo a WHERE id_depo>'" . $id . "'
        " . $mitra . $tipe . $prov . $kota . $kec . $kel . $produsen . $country . "
        ORDER BY a.id_depo DESC " . $limit;
        $this->response->getresponse($sql,'getdepocmsnext');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
            IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
            IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
            IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
            IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
            IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
            IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
            IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
            FROM tb_depo a
            WHERE a.id_depo='" . $id . "'";

            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyhp() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];

        if($hp!=''){
            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
            IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
            IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
            IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
            IFNULL((SELECT x.status_tes FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as status_tes,
            IFNULL((SELECT x.is_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as is_zakat,
            IFNULL((SELECT x.total_zakat FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat,
            IFNULL((SELECT x.total_zakat_masuk FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as total_zakat_masuk
            FROM tb_depo a
            WHERE a.no_hp='" . $hp . "'";
            
            $this->response->getresponse($sql,'getbyhp');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyproduk() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $lat = $parameter['lat'];

        $sqlx = $this->db->query("SELECT DISTINCT(id_depo) as im FROM tb_produk_depo WHERE id_produk='" . $ip . "' AND status=1");
        $in = null;
        $a = 0;
        foreach ($sqlx->result_array() as $row) {
            $in .= $row['im'] . ',';
            $a++;
        }
        $az = strlen($in) - 1;
        $in_n = substr($in, 0, $az);
        $new_in = $in_n;
        if ($new_in == '') {
            $new_in = 0;
        }

        if($ip!=''){
            $sql = "SELECT a.*,a.provinsi as id_provinsi,a.kota as id_kota,a.kecamatan as id_kecamatan,a.kelurahan as id_kelurahan,a.alm_usaha as alamat,
                IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
                IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
                IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
                IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
                IFNULL((SELECT x.stok FROM tb_produk_depo x WHERE x.id_depo=a.id_depo AND x.id_produk='" . $ip . "'),'0') as stok,
                IFNULL((SELECT x.harga_jual FROM tb_produk x WHERE x.id_produk='" . $ip . "'),'0') as harga_jual,
                IFNULL((SELECT x.satuan FROM tb_produk x WHERE x.id_produk='" . $ip . "'),'0') as satuan,
                IFNULL((SELECT x.diskon FROM tb_produk x WHERE x.id_produk='" . $ip . "'),'0') as diskon,
                (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
                (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
                (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
                (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
                FROM tb_depo a
                WHERE a.id_depo IN(" . $new_in . ") AND a.status=1 ";

            if ($lat != '') {
                $exp = explode(",", $lat);

                $latt = $exp[0];
                $long = $exp[1];

                $sql = "SELECT a.*,a.provinsi as id_provinsi,a.kota as id_kota,a.kecamatan as id_kecamatan,a.kelurahan as id_kelurahan,a.alm_usaha as alamat,
                IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
                IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
                IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen,
                IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
                IFNULL((SELECT x.stok FROM tb_produk_depo x WHERE x.id_depo=a.id_depo AND x.id_produk='" . $ip . "'),'0') as stok,
                IFNULL((SELECT x.harga_jual FROM tb_produk x WHERE x.id_produk='" . $ip . "'),'0') as harga_jual,
                IFNULL((SELECT x.satuan FROM tb_produk x WHERE x.id_produk='" . $ip . "'),'0') as satuan,
                IFNULL((SELECT x.diskon FROM tb_produk x WHERE x.id_produk='" . $ip . "'),'0') as diskon,
                (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
                (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
                (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
                (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan,
                (
                6371 * acos (
                cos ( radians(" . $latt . ") )
               * cos( radians( SUBSTRING_INDEX(a.latitude,',',1) ) )
               * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(a.latitude,',',2),',',-1) ) - radians(" . $long . ") )
                + sin ( radians(" . $latt . ") )
               * sin( radians( SUBSTRING_INDEX(a.latitude,',',1) ) )
                )
                ) AS distance
                FROM tb_depo a
                WHERE a.id_depo IN(" . $new_in . ") AND a.status=1 ORDER BY distance ASC ";
            }
            
            $this->response->getresponse($sql,'getbyproduk');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function login() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);
        $password = addslashes($parameter['pass']);
        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT id_depo FROM tb_depo WHERE email='" . $em . "'");
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_depo'] != '') {
            $param_user = " a.email='" . $em . "' ";
        } else {
            $param_user = " a.no_hp='" . $em . "' ";
        }

        $sqlCek = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan
             FROM tb_depo a
             WHERE " . $param_user . "
             AND a.password='" . $pass . "'";
        $qCek = $this->db->query($sqlCek);
        $rCek = $qCek->row_array();

        if ($em != '' && $pass != '') {
            if ($rCek['id_depo'] == '') {
                $code = '03';
                $status = 'Username & password invalid...';
                $this->general_lib->error($code,$status);
            } else if ($rCek['status'] == '0') {
                $code = '04';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            } else {
                $sql = $sqlCek;
                $this->response->getresponse($sql,'login');
            }
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatefoto() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $hp = $parameter['hp'];

        if ($id != '') {
            $sCek = "SELECT * FROM tb_depo WHERE id_depo='" . $id . "'";
        } else {
            $sCek = "SELECT * FROM tb_depo WHERE no_hp='" . $hp . "'";
        }
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoktp);
        }

        if ($fotoktp == '') {
            $fotoktp = $rCek['image_ktp'];
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotousaha);
        }

        if ($fotousaha == '') {
            $fotousaha = $rCek['logo'];
        }

        if ($id == '') {
            $id = $rCek['id_depo'];
        }

        if ($id != '') {
            $sqlUpdate = "UPDATE tb_depo SET foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "' WHERE id_depo='" . $id . "'";
            $this->db->query($sqlUpdate);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_depo a
             WHERE a.id_depo='" . $id . "' ";

            $this->response->getresponse($sql,'updatefoto');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepassword() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $old_password = addslashes($parameter['old_pass']);
        $password = addslashes($parameter['pass']);
        $id = $parameter['iu'];
        
        $pass = md5(hash('sha512', $password));
        $s1 = "SELECT * FROM tb_depo WHERE id_depo='" . $id . "' AND real_password='" . $old_password . "'";
        $q1 = $this->db->query($s1);
        $r1 = $q1->row_array();

        if ($r1['id_depo'] == '') {
            $code = '04';
            $status = "Password lama yang Anda masukan salah...";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password tidak valid, minimal 6 digit huruf atau angka...";
            $this->general_lib->error($code,$status);
        }

        if ($password != '' && $id != '') {
            $sqlUpdate = "Update tb_depo set password='" . $pass . "', real_password ='" . $password . "',
                date_updated=NOW() where id_depo ='" . $id . "'";
            $this->db->query($sqlUpdate);

            $sqlUpdateUkm = "Update tb_ukm set password='" . $pass . "', real_password ='" . $password . "',
                date_updated=NOW() where id_ukm ='" . $r1['id_ukm'] . "'";
            $this->db->query($sqlUpdateUkm);

            $s1 = "SELECT * FROM tb_depo
                WHERE id_depo='" . $id . "'";
            $q1 = $this->db->query($s1);
            $r1 = $q1->row_array();
            $email = $r1['email'];
            $nama = $r1['nm_depo'];

            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan
             FROM tb_depo a
             WHERE a.id_depo='" . $id . "'";
            $this->response->getresponse($sql,'changepassword');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepin() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ia = $parameter['iu'];
        $old_pin = addslashes($parameter['old_pin']);
        $real_pin = addslashes($parameter['pin']);
        
        $pin = md5(hash('sha512', $real_pin));

        $s1 = "SELECT * FROM tb_depo WHERE id_depo='" . $ia . "' AND real_pin='" . $old_pin . "'";
        $q1 = $this->db->query($s1);
        $r1 = $q1->row_array();

        if ($r1['id_depo'] == '') {
            $code = "07";
            $status = "PIN lama salah...";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($real_pin) == FALSE) {
            $code = "07";
            $status = "PIN harus berupa angka...";
            $this->general_lib->error($code,$status);
        }

        if (strlen($real_pin) < 4) {
            $code = "07";
            $status = "PIN kurang dari 4 digit...";
            $this->general_lib->error($code,$status);
        }

        if (strlen($real_pin) > 10) {
            $code = "07";
            $status = "PIN lebih dari 10 digit...";
            $this->general_lib->error($code,$status);
        }

        if ($ia != '' && $real_pin != '') {
            $sUpdate = "UPDATE tb_depo SET pin='" . $pin . "',real_pin='" . $real_pin . "',date_updated=NOW() WHERE id_depo='" . $ia . "' ";
            $this->db->query($sUpdate);

            $sUpdateUkm = "UPDATE tb_ukm SET pin='" . $pin . "',real_pin='" . $real_pin . "',date_updated=NOW() WHERE id_ukm='" . $r1['id_ukm'] . "' ";
            $this->db->query($sUpdateUkm);

            $s1 = "SELECT * FROM tb_depo
                WHERE id_depo='" . $ia . "'";
            $q1 = $this->db->query($s1);
            $r1 = $q1->row_array();
            $email = $r1['email'];
            $nama = $r1['nm_depo'];

            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
            IFNULL((SELECT x.no_hp FROM tb_depo x WHERE x.tipe=0 LIMIT 1),'') as hp_agen
             FROM tb_depo a
             WHERE a.id_depo='" . $ia . "'";
            $this->response->getresponse($sql,'changepin');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sCek = $this->db->query("SELECT id_depo,id_ukm FROM sh_depo WHERE id_depo='".$id."'");
            $rCek = $sCek->row_array();

            $sUpdate = "UPDATE tb_depo SET status='" . $st . "',date_updated=NOW() WHERE id_depo='" . $id . "'";
            $this->db->query($sUpdate);

            $sUpdateUkm = "UPDATE sh_ukm SET status='" . $st . "',date_updated=NOW() WHERE id_ukm='" . $rCek['id_ukm'] . "'";
            $this->db->query($sUpdateUkm);

            $sql = "SELECT * FROM tb_depo WHERE id='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_depo WHERE id_depo='" . $id . "'";
            $this->db->query($sDelete);

            $sDeleteUkm = "DELETE FROM sh_ukm WHERE id_ukm='" . $rCek['id_ukm'] . "'";
            $this->db->query($sDeleteUkm);

            $sql = "SELECT * FROM tb_depo ORDER BY id_depo DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
