<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Country extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertcountry() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ci = $parameter['ci'];
        $nm = $parameter['nm'];
        $pc = $parameter['pc'];
        $cr = $parameter['cr'];
        $id = $parameter['id'];

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_country WHERE id='" . $id . "'");
        $rCek = $sCek->row_array();

        if ($id != '') {
            if ($ci == '') {
                $ci = $rCek['country_id'];
            }

            if ($nm == '') {
                $nm = $rCek['nama'];
            }

            if ($pc == '') {
                $pc = $rCek['phonecode'];
            }

            if ($cr == '') {
                $cr = $rCek['currency'];
            }
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($ci != '' && $nm != '') {
            if ($id == '') {
                $sInsert = "INSERT INTO tb_country (country_id,nama,phonecode,currency,image,date_created,date_updated,tipe,status)"
                        . "VALUES('" . $ci . "','" . $nm . "','" . $pc . "','" . $cr . "','" . $foto . "',NOW(),NOW(),1,1)";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_country ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_country SET country_id='" . $ci . "',nama='" . $nm . "',phonecode='" . $pc . "',currency='" . $cr . "',image='" . $foto . "',"
                        . "date_updated=NOW() WHERE id='" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_country WHERE id = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertcountry');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getcountry() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_country WHERE status='1' ORDER BY nama ASC ".$limit;
         $this->response->getresponse($sql,'getcountry');
    }

    public function getcountrycms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_country ORDER BY date_created DESC ".$limit;
        $this->response->getresponse($sql,'getcountrycms');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_country WHERE id='" . $id . "'";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_country SET status='" . $st . "' WHERE id='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_country WHERE id='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_country WHERE id='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_country ORDER BY id DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
