<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Webchat extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function send() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $ida = $parameter['ida'];
        $ps = addslashes($parameter['ps']);
        $tp = $parameter['tp'];

        if ($ps != '' && $tp != '') {
            if ($tp == '1') {
                $sInsert = "INSERT INTO tb_user_chat_message (id_user_u, message,tipe,status,date_created,date_updated)
                    VALUES ('" . $id . "', '" . $ps . "', '" . $tp . "','1',NOW(), NOW())  ";
                $this->db->query($sInsert);
            } else if ($tp == '2') {
                $sInsert = "INSERT INTO tb_user_chat_message (id_user_u,id_user_a, message,tipe,status,date_created,date_updated)
                    VALUES ('" . $id . "','" . $ida . "', '" . $ps . "', '" . $tp . "','1',NOW(), NOW())  ";
                $this->db->query($sInsert);
            }
            $sql = "SELECT a.*,"
                    . "IFNULL((SELECT x.email FROM tb_user_chat x WHERE x.id_user_chat=a.id_user_u),'') as email ,"
                    . "IFNULL((SELECT x.nama FROM tb_user_chat x WHERE x.id_user_chat=a.id_user_u),'') as nama "
                    . "FROM tb_user_chat_message a WHERE a.id_user_u='" . $id . "' ORDER BY a.id_message DESC LIMIT 1";
            $this->response->getresponse($sql,'send');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function login() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);
        $nm = addslashes($parameter['nm']);
        $tp = $parameter['tp'];

        $sCek = $this->db->query("SELECT * FROM tb_user_chat WHERE email='" . $em . "'");
        $rCek = $sCek->row_array();

        if ($tp == '') {
            $tp = 1;
        }

        if ($rCek['email'] == '') {
            $sInsert = "INSERT INTO tb_user_chat (nama, email,tipe,status,date_created,date_updated)
                    VALUES ('" . $nm . "', '" . $em . "', '" . $tp . "','1',NOW(), NOW())  ";
            $this->db->query($sInsert);
        }
        $sql="SELECT * FROM tb_user_chat WHERE email='" . $em . "'";
        $this->response->getresponse($sql,'login');
    }

    public function get() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $last = $parameter['last'];

        $sql = "SELECT a.*,"
                . "IFNULL((SELECT x.email FROM tb_user_chat x WHERE x.id_user_chat=a.id_user_u),'') as email ,"
                . "IFNULL((SELECT x.nama FROM tb_user_chat x WHERE x.id_user_chat=a.id_user_u),'') as nama "
                . "FROM tb_user_chat_message a WHERE a.id_user_u='" . $id . "' AND id_message > " . $last . "  ORDER BY a.id_message ASC";
        $this->response->getresponse($sql,'get');
    }

}
