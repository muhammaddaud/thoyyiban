<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertsetting() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $kd = $parameter['kd'];
        $de = addslashes($parameter['de']);
        $deg = addslashes($parameter['deg']);
        $de_ar = addslashes($parameter['de_ar']);
        $ni = $parameter['ni'];
        $nr = $parameter['nr'];
        $bk = $parameter['bk'];
        $an = $parameter['an'];
        $st = $parameter['st'];
        $id = $parameter['id'];
        $tp = $parameter['tp'];

        if ($deg == '') {
            $deg = $this->param['de_eng'];
        }

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_general_setting WHERE id_setting='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($kd != '') {
            if ($id == '') {
                $sInsertSetting = "INSERT INTO tb_general_setting
                         (kd_setting,desc_setting,desc_setting_eng,desc_setting_ar,date_created,date_updated,tipe,status,image,nilai_idr,no_rekening,bank,atas_nama)
                         VALUES
                         ('" . $kd . "','" . $de . "','" . $deg . "','" . $de_ar . "',NOW(),NOW(),'" . $tp . "','" . $st . "','" . $foto . "','" . $ni . "','" . $nr . "','" . $bk . "','" . $an . "')";
                $this->db->query($sInsertSetting);

                $sql = "SELECT * FROM tb_general_setting ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_general_setting SET kd_setting='" . $kd . "',desc_setting='" . $de . "',desc_setting_eng='" . $deg . "',nilai_idr='" . $ni . "',date_updated=NOW(),status='" . $st . "',image='" . $foto . "',tipe='" . $tp . "',desc_setting_ar='" . $de_ar . "' ,no_rekening='" . $nr . "',bank='" . $bk . "',atas_nama='" . $an . "'  WHERE id_setting='" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_general_setting WHERE id_setting = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertsetting');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getsetting() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $kd = $parameter['kd'];

        if($kd!=''){
            $sql = "SELECT * FROM tb_general_setting WHERE kd_setting='" . $kd . "' AND status = '1' ORDER BY date_created DESC ";
            $this->response->getresponse($sql,'getsetting');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getsettingcms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $tipe="";
        if($tp!=''){
            $kat=" WHERE tipe='".$tp."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_general_setting ".$tipe." ORDER BY date_created DESC ".$limit;
        $this->response->getresponse($sql,'getsettingcms');
    }

    public function getsettingall() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $tipe="";
        if($tp!=''){
            $kat=" AND tipe='".$tp."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_general_setting WHERE status=1 ".$tipe." ORDER BY date_created DESC ".$limit;
        $this->response->getresponse($sql,'getsettingall');
    }

    public function getbytipe() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];

        if($tp!=''){
            $sql = "SELECT * FROM tb_general_setting WHERE tipe='" . $tp . "' AND status=1 ORDER BY date_created DESC ";
            $this->response->getresponse($sql,'getbytipe');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbytipecms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];

        if($tp!=''){
            $sql = "SELECT * FROM tb_general_setting WHERE tipe='" . $tp . "' ORDER BY date_created DESC ";
            $this->response->getresponse($sql,'getbytipecms');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbylike() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $kd = $parameter['kd'];
        $tp = $parameter['tp'];

        if($kd!=''){
            $sql = "SELECT * FROM tb_general_setting WHERE kd_setting LIKE '" . $kd . "%' AND status=1 AND tipe=" . $tp . " ORDER BY date_created DESC";
            $this->response->getresponse($sql,'getbylike');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_general_setting WHERE id_setting='" . $id . "'";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_general_setting SET status='" . $st . "' WHERE id_setting='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_general_setting WHERE id_setting='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_general_setting WHERE id_setting='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_general_setting ORDER BY id_setting DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
