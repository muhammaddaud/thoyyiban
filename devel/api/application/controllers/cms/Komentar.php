<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Komentar extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertkomentar() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $nm = addslashes($parameter['nm']);
        $em = addslashes($parameter['em']);
        $id = $parameter['id'];
        $st = $parameter['st'];
        $tp = $parameter['tp'];
        $kom = addslashes($parameter['kom']);
        $web = $parameter['web'];
        $ii = $parameter['ii'];

        $sCek = $this->db->query("SELECT id_customer,nm_customer,email,no_hp FROM tb_customer WHERE id_customer='" . $ic . "'");
        $rCek = $sCek->row_array();

        if ($nm == '') {
            $nm = $rCek['nm_customer'];
        }

        if ($em == '') {
            $em = $rCek['email'];
        }

        if ($tp == '') {
            $tp = 1;
        }

        if ($st == '') {
            $st = 1;
        }

        if ($nm != '' && $kom != '') {
            if ($id == '') {
                $sInsert = "INSERT INTO tb_komentar (id_info,id_customer,nama,email,komentar,website,date_created,date_updated,status,tipe)
                    VALUES
                    ('" . $ii . "','" . $ic . "','" . $nm . "','" . $em . "','" . $kom . "','" . $web . "',NOW(),NOW(),'" . $st . "','" . $tp . "')";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_komentar ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_komentar SET nama = '" . $nm . "',email = '" . $em . "',komentar = '" . $kom . "',tipe = '" . $tp . "',id_info = '" . $ii . "',
                        website = '" . $web . "',status = '" . $st . "',id_customer = '" . $ic . "',date_updated=NOW() WHERE id_komentar = '" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_komentar WHERE id_komentar = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertkomentar');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertrating() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $nm = addslashes($parameter['nm']);
        $em = addslashes($parameter['em']);
        $rt = $parameter['rt'];
        $id = $parameter['id'];
        $st = $parameter['st'];
        $tp = $parameter['tp'];
        $kom = addslashes($parameter['kom']);
        $web = $parameter['web'];
        $ii = $parameter['ii'];

        $sCek = $this->db->query("SELECT id_customer,nm_customer,email,no_hp FROM tb_customer WHERE id_customer='" . $ic . "'");
        $rCek = $sCek->row_array();

        $sSum = "SELECT SUM(rating) as rating FROM tb_komentar WHERE id_info='" . $ii . "' AND rating>0";
        $rSum = $sSum->row_array();

        $sInfo = "SELECT id_info,total_rating FROM tb_info WHERE id_info='" . $ii . "'";
        $rInfo = $sInfo->row_array();

        $tr = $rInfo['total_rating'] + 1;
        $rate = ($rt + $rSum['rating']) / $tr;

        if ($nm == '') {
            $nm = $rCek['nm_customer'];
        }

        if ($em == '') {
            $em = $rCek['email'];
        }

        if ($tp == '') {
            $tp = 1;
        }

        if ($st == '') {
            $st = 1;
        }

        if ($ic != '' && $kom != '' && $rt != '' && $ii != '') {

            $sInsert = "INSERT INTO tb_komentar (id_info,id_customer,nama,email,komentar,website,rating,date_created,date_updated,status,tipe)
                VALUES
                ('" . $ii . "','" . $ic . "','" . $nm . "','" . $em . "','" . $kom . "','" . $web . "','" . $rt . "',NOW(),NOW(),'" . $st . "','" . $tp . "')";
            $this->db->query($sInsert);

            $update = "UPDATE tb_info SET total_rating='" . $tr . "',rating='".$rate."' WHERE id_info='".$ii."'";
            $this->db->query($update);

            $sql = "SELECT * FROM tb_komentar ORDER BY date_created DESC LIMIT 1";
            
            $this->response->getresponse($sql,'insertrating');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getkomentar() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ii = $parameter['ii'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT a.*,
                    IFNULL((SELECT x.title_info FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info,
                    IFNULL((SELECT x.title_info_eng FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_eng,
                    IFNULL((SELECT x.title_info_ar FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_ar
                    FROM tb_komentar a WHERE a.id_info='" . $ii . "' AND a.status=1 ORDER BY a.date_created DESC " . $limit;
         $this->response->getresponse($sql,'getkomentar');
    }

    public function getrating() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ii = $parameter['ii'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT a.*,
                    IFNULL((SELECT x.title_info FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info,
                    IFNULL((SELECT x.title_info_eng FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_eng,
                    IFNULL((SELECT x.title_info_ar FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_ar
                    FROM tb_komentar a WHERE a.id_info='" . $ii . "' AND a.status=1 AND rating>0 ORDER BY a.date_created DESC " . $limit;
         $this->response->getresponse($sql,'getkomentar');
    }

    public function getkomentarcms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ii = $parameter['ii'];
        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $info="";
        if($ii!=''){
            $info=" AND a.id_info='".$ii."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $sql = "SELECT a.*,
                    IFNULL((SELECT x.title_info FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info,
                    IFNULL((SELECT x.title_info_eng FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_eng,
                    IFNULL((SELECT x.title_info_ar FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_ar
                    FROM tb_komentar a WHERE a.id_komentar!='' " . $info . $tipe . " ORDER BY a.date_created DESC " . $limit;
        $this->response->getresponse($sql,'getkomentarcms');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT a.*,
                    IFNULL((SELECT x.title_info FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info,
                    IFNULL((SELECT x.title_info_eng FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_eng,
                    IFNULL((SELECT x.title_info_ar FROM tb_info x WHERE x.id_info=a.id_info),'') as title_info_ar
                    FROM tb_komentar a WHERE a.id_komentar='" . $id . "' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_komentar SET status='" . $st . "' WHERE id_komentar='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_komentar WHERE id_komentar='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_komentar WHERE id_komentar='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_komentar ORDER BY id_komentar DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
