<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ukm extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function getukm() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];

        if($iu==''){
            $iu = $this->uri->segment(3);
        }

        $url = $this->general_lib->url_thoyyiban() . 'api/ukm_api.php?action=get_byid'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'id' => $iu
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayUkm = array();
        $a = 0;
        foreach ($encode['result'] as $rowUkm) {
            $arrayUkm[$a] = $rowUkm;
            $a++;
        }

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => '10',
            'iu' => $iu
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProd = array();
        $b = 0;
        foreach ($encode['result'] as $rowProd) {
            $arrayProd[$b] = $rowProd;
            $b++;
        }

        $result[0]['ukm'] = $arrayUkm;
        $result[0]['produk'] = $arrayProd;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action getukm'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function register() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = $this->param['nmu'];
        $nmp = $this->param['nmp'];
        $nmpm = $this->param['nmpm'];
        $kl = $this->param['kl'];
        $almt = $this->param['almt'];
        $almtu = $this->param['almtu'];
        $pos = $this->param['pos'];
        $ktp = $this->param['ktp'];
        $hp = $this->param['hp'];
        $em = $this->param['em'];
        $jk = $this->param['jk'];
        $lat = $this->param['lat'];
        $pass = $this->param['pass'];

        $nm = $this->param['nm'];
        $kls = $this->param['kls'];
        $kms = $this->param['kms'];
        $qty = $this->param['qty'];
        $ct = $this->param['ct'];
        $tpu = $this->param['tpu'];
        $tb = $this->param['tb'];

        $url = $this->general_lib->url_thoyyiban() . 'api/ukm_api.php?action=register_bn'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'nmu' => $nmu,
            'nmp' => $nmp,
            'nmpm' => $nmpm,
            'kl' => $kl,
            'almt' => $almt,
            'almtu' => $almtu,
            'pos' => $pos,
            'ktp' => $ktp,
            'hp' => $hp,
            'em' => $em,
            'jk' => $jk,
            'lat' => $lat,
            'pass' => $pass,
            'nm' => $nm,
            'kls' => $kls,
            'kms' => $kms,
            'qty' => $qty,
            'ct' => $ct,
            'tpu' => $tpu,
            'tb' => $tb
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        die();
    }

}
