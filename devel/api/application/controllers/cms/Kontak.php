<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kontak extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function kirimpesan() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $em = addslashes($parameter['em']);
        $hp = addslashes($parameter['hp']);
        $ps = addslashes($parameter['ps']);
        $tp = addslashes($parameter['tp']);
        $ipaddr = $_SERVER['REMOTE_ADDR'];

        if ($tp == '') {
            $tp = 1;
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL) && $em!='') {
            $code = "07";
            $status = "Format email tidak valid";
            $this->general_lib->error($code,$status);
        }

        if ($nm != '' && $em != '' && $ps != '') {
            
            $sInsert = "INSERT INTO tb_kontak (nama, email, no_hp, pesan, date_created,date_updated,tipe, status, ip)
                     VALUES ('" . $nm . "', '" . $em . "','" . $hp . "','" . $ps . "', NOW(),NOW(),'" . $tp . "', '0', '" . $ipaddr . "') ";
            $this->db->query($sInsert);

            $sql = "SELECT * FROM tb_kontak ORDER BY date_created DESC LIMIT 1";
            $this->response->getresponse($sql,'kirimpesan');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function addfeedback() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = addslashes($parameter['ic']);
        $ik = addslashes($parameter['ik']);
        $ps = addslashes($parameter['ps']);
        $rt = addslashes($parameter['rt']);
        $uuid = addslashes($parameter['uuid']);
        $ipaddr = $_SERVER['REMOTE_ADDR'];

        $sCust = $this->db->query("SELECT id_customer,email,nm_customer,no_hp FROM tb_customer WHERE id_customer='" . $ic . "'");
        $rCust = $sCust->row_array();

        $em = $rCust['email'];
        $hp = $rCust['no_hp'];
        $nm = $rCust['nm_customer'];

        if ($ps != '') {
            $sqlInsert = "INSERT INTO tb_kontak (id_customer,id_kategori,nama, email, no_hp, pesan, date_created,date_updated,tipe, status, ip, rating)
                     VALUES ('" . $ic . "','" . $ik . "','" . $nm . "', '" . $em . "','" . $hp . "','" . $ps . "', NOW(),NOW(),3, 1, '" . $ipaddr . "', '" . $rt . "') ";
            $this->db->query($sqlInsert);

            $sql = "SELECT * FROM tb_kontak WHERE email='" . $em . "' ORDER BY id DESC LIMIT 1";
            $this->response->getresponse($sql,'addfeedback');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function chatting() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = addslashes($parameter['ic']);
        $ik = addslashes($parameter['ik']);
        $tp = addslashes($parameter['tp']); //1=greeting,2=kirim gambar,3=selesai
        $ps = addslashes($parameter['ps']);
        $uuid = addslashes($parameter['uuid']);
        $ipaddr = $_SERVER['REMOTE_ADDR'];

        $sCust = $this->db->query("SELECT id_customer,email,nm_customer,no_hp FROM tb_customer WHERE id_customer='" . $ic . "'");
        $rCust = $sCust->row_array();

        $em = $rCust['email'];
        $hp = $rCust['no_hp'];
        $nm = $rCust['nm_customer'];

        if ($tp == '') {
            $tp = 1;
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        $time = date('H');

        if ($time >= 00 && $time < 11) {
            $greeting = "Halo " . $nm . " selamat pagi";
            $greeting_eng = "Halo " . $nm . " good morning";
            $greeting_ar = "مرحبا " . $nm . " صباح الخير";
        } elseif ($time >= 11 && $time < 15) {
            $greeting = "Halo " . $nm . " selamat siang";
            $greeting_eng = "Halo " . $nm . " good afternoon";
            $greeting_ar = "مرحبا " . $nm . " بعد ظهر اليوم";
        } elseif ($time >= 15 && $time < 18) {
            $greeting = "Halo " . $nm . " selamat sore";
            $greeting_eng = "Halo " . $nm . " a good afternoon";
            $greeting_ar = "مرحبا " . $nm . " بعد ظهر اليوم";
        } elseif ($time >= 18 && $time < 25) {
            $greeting = "Halo " . $nm . " selamat malam";
            $greeting_eng = "Halo " . $nm . " a good night";
            $greeting_ar = "مرحبا " . $nm . " ليلة جيدة";
        }

        if ($tp == 1) {
            $rp = $greeting;
            $rp_eng = $greeting_eng;
            $rp_ar = $greeting_ar;
        } elseif ($tp == 2) {
            $rp = "Terima kasih pesan gambar Anda telah kami terima";
            $rp_eng = "Thank you for your picture message we received";
            $rp_ar = "شكرا لك على رسالتك المصورة التي تلقيناها";
        } elseif ($tp == 3) {
            $rp = "Terima kasih atas pesan yang ada sampaikan. Kami sangat menghargai pesan dari Anda untuk meningkatkan layanan kami.";
            $rp_eng = "Thank you for the message. We really appreciate you for improving our services.";
            $rp_ar = "شكرا على الرسالة. نحن نقدر كثيرا رسالتك لتحسين خدماتنا.";
        }

        if ($ps != '' && $ic != '') {
            $sqlInsert = "INSERT INTO tb_kontak (id_customer,id_kategori,nama, email, no_hp,pesan,image,reply,reply_eng,reply_ar, date_created,date_updated,tipe,tipe_chatting, status, ip)
                     VALUES ('" . $ic . "','" . $ik . "','" . $nm . "', '" . $em . "','" . $hp . "','" . $ps . "', '" . $foto . "', '" . $rp . "', '" . $rp_eng . "','" . $rp_ar . "', NOW(),NOW(),4,'" . $tp . "', '1', '" . $ipaddr . "') ";
            $this->db->query($sqlInsert);

            $sql = "SELECT * FROM tb_kontak WHERE email='" . $em . "' ORDER BY id DESC LIMIT 1";
            $this->response->getresponse($sql,'chatting');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function subscribe() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);

        if (!filter_var($em, FILTER_VALIDATE_EMAIL) && $em!='') {
            $code = "07";
            $status = "Format email tidak valid";
            $this->general_lib->error($code,$status);
        }
        if ($em != '') {
            $sCek = $this->db->query("SELECT id FROM tb_kontak WHERE email='" . $em . "' AND tipe=2");
            $rCek = $sCek->row_array();
            if ($rCek['id'] == '') {
                $sqlInsert = "INSERT INTO tb_kontak (nama, email, pesan, date_created,date_updated,tipe, status, ip)
                     VALUES ('Subscribe', '" . $em . "','', NOW(),NOW(),2, '1', '" . $ipaddr . "') ";
                $this->db->query($sqlInsert);
            }

            $sql = "SELECT * FROM tb_kontak WHERE email='" . $em . "' ORDER BY id DESC LIMIT 1";
            $this->response->getresponse($sql,'subscribe');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function replypesan() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = addslashes($parameter['id']);
        $rp = addslashes($parameter['rp']);
        if ($id != '' && $rp != '') {
            $sUpdate = "UPDATE tb_kontak SET reply='" . $rp . "',status=1,date_updated=NOW() WHERE id='" . $id . "'";
            $this->db->query($sUpdate);

            $sCek = $this->db->query("SELECT * FROM tb_kontak WHERE id='" . $id . "'");
            $rCek = $sCek->row_array();

            $email = $rCek['email'];
            $nama = $rCek['nama'];

            $sql = "SELECT * FROM tb_kontak WHERE id='" . $id . "'";
            $this->response->getresponse($sql,'replypesan');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getpesan() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $st = $parameter['st'];

        $sql = "SELECT * FROM tb_kontak WHERE tipe=1 ORDER BY id DESC";

        if ($st != '') {
            $sql = "SELECT * FROM tb_kontak WHERE tipe=1 AND status='" . $st . "' ORDER BY id DESC";
        }

        $cnt = $parameter['cnt'];
        if ($cnt != '') {
          $tp = $parameter['tp'];
          $sql = "SELECT COUNT(id_pesan) as cnt FROM " . $this->table . " ";
          // check aktif
          if ($tp == '1') {
            $sql = "SELECT COUNT(id_pesan) as cnt FROM " . $this->table . " WHERE status = 1 ";
          }
          // check on tidak aktif
          else if ($tp == '2') {
            $sql = "SELECT COUNT(id_pesan) as cnt FROM " . $this->table . " WHERE status!= 1 ";
          }
        }
        $this->response->getresponse($sql,'getpesan');
    }

    public function getfeedback() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $this->param['ic'];

        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as nm_customer,
                IFNULL((SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as email,
                IFNULL((SELECT x.image FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as image,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori = a.id_kategori),'') as nm_kategori
                FROM tb_kontak a WHERE a.tipe=3 ORDER BY a.id DESC";

        if ($ic != '') {
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as nm_customer,
                IFNULL((SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as email,
                IFNULL((SELECT x.image FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as image,
                FNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori = a.id_kategori),'') as nm_kategori
                FROM tb_kontak a WHERE a.tipe=3 AND a.id_customer='" . $ic . "' ORDER BY a.id DESC";
        }

        $this->response->getresponse($sql,'getfeedback');
    }

    public function getchatting() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $this->param['ic'];

        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as nm_customer,
                IFNULL((SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as email,
                IFNULL((SELECT x.image FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as image,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori = a.id_kategori),'') as nm_kategori
                FROM tb_kontak a WHERE a.tipe=4 ORDER BY a.id DESC";

        if ($ic != '') {
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as nm_customer,
                IFNULL((SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as email,
                IFNULL((SELECT x.image FROM tb_customer x WHERE x.id_customer = a.id_customer),'') as image,
                FNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori = a.id_kategori),'') as nm_kategori
                FROM tb_kontak a WHERE a.tipe=4 AND a.id_customer='" . $ic . "' ORDER BY a.id DESC";
        }

        $this->response->getresponse($sql,'getchatting');
    }

    public function getsubscribe() {
        $err = '';

        $sql = "SELECT * FROM tb_kontak WHERE tipe=2 ORDER BY id DESC";
        $this->response->getresponse($sql,'getSubscribe');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
             $sql = "SELECT * FROM tb_kontak WHERE id='" . $id . "'";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_kontak WHERE id='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_kontak ORDER BY id DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
