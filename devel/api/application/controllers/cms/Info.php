<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Info extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertinfo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $nm_eng = addslashes($parameter['nm_eng']);
        $nm_ar = addslashes($parameter['nm_ar']);
        $ti = addslashes($parameter['ti']);
        $ti_eng = addslashes($parameter['ti_eng']);
        $ti_ar = addslashes($parameter['ti_ar']);
        $de = addslashes($parameter['de']);
        $de_eng = addslashes($parameter['de_eng']);
        $de_ar = addslashes($parameter['de_ar']);
        $auth = addslashes($parameter['auth']);

        $tp = $parameter['tp'];
        $ln = $parameter['ln'];
        $vd = $parameter['vd'];
        $vd2 = $parameter['vd2'];
        $vd3 = $parameter['vd3'];
        $vd4 = $parameter['vd4'];
        $vd5 = $parameter['vd5'];
        $vid = $parameter['vid'];
        $st = $parameter['st'];
        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $id = $parameter['id'];
        $ih = $parameter['ih'];
        $ut = $parameter['ut'];
        $df = $parameter['df'];
        $lt = $parameter['lt'];
        $iu = $parameter['iu'];
        $tpk = $parameter['tpk'];
        $ci = $parameter['ci'];
        $dr = $parameter['dr'];
        $sz = $parameter['sz'];
        $ir = $parameter['ir'];
        $it = $parameter['it'];
        $kl = $parameter['kl'];
        $kt = $parameter['kt'];
        $almt = $parameter['almt'];

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_info WHERE id_info='" . $id . "'");
        $rCek = $sCek->row_array();

        if($kl==''){
            $kl=$rCek['id_kelurahan'];
        }

        if($kt==''){
            $kt=$rCek['id_kota'];
        }

        if(strlen($dr)==8){
            $exp=explode(":",$dr);
            if($exp[0]=='00'){
                $dr = substr($dr,3);
            }
        }

        if ($kl != 0) {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $sqlProv = $this->db->query("SELECT id_provinsi FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
            $rProv = $sqlProv->row_array();

            $kc = $rKec['id_kecamatan'];
            $kt = $rKota['id_kota'];
            $pv = $rProv['id_kota'];
        }elseif ($kt != 0) {
            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $kt . "' ");
            $rKota = $sqlKota->row_array();

            $kt = $rKota['id_kota'];
            $pv = $rKota['id_provinsi'];
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }

        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile2 = $parameter['image2'];
        $binary2 = base64_decode($datafile2);
        $namefile2 = $parameter['filename2'];
        if ($namefile2 != '') {
            $target_dir2 = $this->general_lib->path();

            if (!file_exists($target_dir2)) {
                mkdir($target_dir2, 0777, true);
            }

            $target_path2 = $target_dir2;
            $now2 = date('YmdHis');
            $rand2 = rand(1111, 9999);
            $generatefile2 = $now2 . $rand2;
            $namefile2 = $generatefile2 . ".jpeg";
            $target_path2 = $target_path2 . $namefile2;

            chmod($target_path2, 0777);
            $fh2 = fopen($target_path2, 'w') or die("can't open file");
            chmod($target_path2, 0777);
            fwrite($fh2, $binary2);
            fclose($fh2);

            sleep(1);

            $foto2 = "upload/" . $namefile2;
        }

        if ($namefile2 == '') {
            $foto2 = "";
        }

        if ($foto2 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto2,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto2);
        }

        if ($foto2 == '') {
            $foto2 = $rCek['image2'];
        }


        $datafile3 = $parameter['image3'];
        $binary3 = base64_decode($datafile3);
        $namefile3 = $parameter['filename3'];
        if ($namefile3 != '') {
            $target_dir3 = $this->general_lib->path();

            if (!file_exists($target_dir3)) {
                mkdir($target_dir3, 0777, true);
            }

            $target_path3 = $target_dir3;
            $now3 = date('YmdHis');
            $rand3 = rand(1111, 9999);
            $generatefile3 = $now3 . $rand3;
            $namefile3 = $generatefile3 . ".jpeg";
            $target_path3 = $target_path3 . $namefile3;

            chmod($target_path3, 0777);
            $fh3 = fopen($target_path3, 'w') or die("can't open file");
            chmod($target_path3, 0777);
            fwrite($fh3, $binary3);
            fclose($fh3);

            sleep(1);

            $foto3 = "upload/" . $namefile3;
        }

        if ($namefile3 == '') {
            $foto3 = "";
        }

        if ($foto3 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto3,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto3);
        }

        if ($foto3 == '') {
            $foto3 = $rCek['image3'];
        }

        $datafile4 = $parameter['image4'];
        $binary4 = base64_decode($datafile4);
        $namefile4 = $parameter['filename4'];
        if ($namefile4 != '') {
            $target_dir4 = $this->general_lib->path();

            if (!file_exists($target_dir4)) {
                mkdir($target_dir4, 0777, true);
            }

            $target_path4 = $target_dir4;
            $now4 = date('YmdHis');
            $rand4 = rand(1111, 9999);
            $generatefile4 = $now4 . $rand4;
            $namefile4 = $generatefile4 . ".jpeg";
            $target_path4 = $target_path4 . $namefile4;

            chmod($target_path4, 0777);
            $fh4 = fopen($target_path4, 'w') or die("can't open file");
            chmod($target_path4, 0777);
            fwrite($fh4, $binary4);
            fclose($fh4);

            sleep(1);

            $foto4 = "upload/" . $namefile4;
        }

        if ($namefile4 == '') {
            $foto4 = "";
        }

        if ($foto4 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto4,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto4);
        }

        if ($foto4 == '') {
            $foto4 = $rCek['image4'];
        }


        $datafile5 = $parameter['image5new'];
        $binary5 = base64_decode($datafile5);
        $namefile5 = $parameter['filename5new'];
        if ($namefile5 != '') {
            $target_dir5 = $this->general_lib->path();

            if (!file_exists($target_dir5)) {
                mkdir($target_dir5, 0777, true);
            }

            $target_path5 = $target_dir5;
            $now5 = date('YmdHis');
            $rand5 = rand(1111, 9999);
            $generatefile5 = $now5 . $rand5;
            $namefile5 = $generatefile5 . ".jpeg";
            $target_path5 = $target_path5 . $namefile5;

            chmod($target_path5, 0777);
            $fh5 = fopen($target_path5, 'w') or die("can't open file");
            chmod($target_path5, 0777);
            fwrite($fh5, $binary5);
            fclose($fh5);

            sleep(1);

            $foto5 = "upload/" . $namefile5;
        }

        if ($namefile5 == '') {
            $foto5 = "";
        }

        if ($foto5 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto5,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto5);
        }

        if ($foto5 == '') {
            $foto5 = $rCek['image5'];
        }

        $datafile_image5 = $parameter['image5'];
        $binary_image5 = base64_decode($datafile_image5);
        $namefile_image5 = $parameter['filename5'];
        if ($namefile_image5 != '') {
            $target_dir_image5 = $this->general_lib->path();

            if (!file_exists($target_dir_image5)) {
                mkdir($target_dir_image5, 0777, true);
            }

            $url_path_image5 = "upload/";

            $target_path_image5 = $target_dir_image5;
            $now_image5 = date('YmdHis');
            $rand_image5 = rand(1111, 9999);
            $generatefile_image5 = $now_image5 . $rand_image5;
            $namefile_image5 = $generatefile_image5 . ".jpeg";
            $target_path_image5 = $target_path_image5 . $namefile_image5;

            chmod($target_path_image5, 0777);
            $fh_image5 = fopen($target_path_image5, 'w') or die("can't open file");
            chmod($target_path_image5, 0777);
            fwrite($fh_image5, $binary_image5);
            fclose($fh_image5);

            sleep(1);

            $thumbnail = "upload/" . $namefile_image5;
        }
        if ($namefile_image5 == '') {
            $thumbnail = "";
        }

        if ($thumbnail != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $thumbnail,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $thumbnail);
        }

        if ($thumbnail == '') {
            $thumbnail = $rCek['thumbnail'];
        }

        $datafile6 = $parameter['image6'];
        $binary6 = base64_decode($datafile6);
        $namefile6 = $parameter['filename6'];
        if ($namefile6 != '') {
            $target_dir6 = $this->general_lib->path();

            if (!file_exists($target_dir6)) {
                mkdir($target_dir6, 0777, true);
            }

            $target_path6 = $target_dir6;
            $now6 = date('YmdHis');
            $rand6 = rand(1111, 9999);
            $generatefile6 = $now6 . $rand6;
            $namefile6 = $generatefile6 . ".jpeg";
            $target_path6 = $target_path6 . $namefile6;

            chmod($target_path6, 0777);
            $fh6 = fopen($target_path6, 'w') or die("can't open file");
            chmod($target_path6, 0777);
            fwrite($fh6, $binary6);
            fclose($fh6);

            sleep(1);

            $foto6 = "upload/" . $namefile6;
        }

        if ($namefile6 == '') {
            $foto6 = "";
        }

        if ($foto6 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto6,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto4);
        }

        if ($foto6 == '') {
            $foto6 = $rCek['thumbnail_small'];
        }

        if ($id == '') {

            $insert = "INSERT INTO tb_info
            (title_info,desc_info,date_created,date_updated,tipe,status,link,image,id_kategori,thumbnail,id_subkategori,video2,title_info_eng,desc_info_eng,title_info_ar,desc_info_ar,video,is_headline,urutan,data_file,latitude,is_utama,topik,title_image,title_image_eng,title_image_ar,video3,video4,video5,image2,image3,image4,image5,country_id,durasi,is_rekomendasi,id_tokoh,id_kelurahan,id_kecamatan,id_kota,id_provinsi,alamat,author,thumbnail_small,size,vid_youtube)
            VALUES
            ('" . $nm . "','" . $de . "',NOW(),NOW(),'" . $tp . "','" . $st . "','" . $ln . "','" . $foto . "','" . $ik . "','" . $thumbnail . "','" . $is . "','" . $vd2 . "', '" . $nm_eng . "','" . $de_eng . "','" . $nm_ar . "','" . $de_ar . "','" . $vd . "','" . $ih . "','" . $ut . "','" . $df . "','" . $lt . "','" . $iu . "','" . $tpk . "','" . $ti . "','" . $ti_eng . "','" . $ti_ar . "','" . $vd3 . "','" . $vd4 . "','" . $vd5 . "','" . $foto2 . "','" . $foto3 . "','" . $foto4 . "','" . $foto5 . "','" . $ci . "','" . $dr . "','" . $ir . "','" . $it . "','" . $kl . "','" . $kt . "','" . $kc . "','" . $pv . "','" . $almt . "','" . $auth . "','" . $foto6 . "','" . $sz . "','" . $vid . "')";
            $this->db->query($insert);

            $sql = "SELECT * FROM tb_info ORDER BY id_info DESC LIMIT 1";
        } else {
            $sUpdate = "UPDATE tb_info SET title_info='" . $nm . "',desc_info='" . $de . "',date_updated=NOW(),tipe='" . $tp . "',status='" . $st . "',link='" . $ln . "',id_subkategori='" . $is . "',image='" . $foto . "',title_info_eng='" . $nm_eng . "',desc_info_eng='" . $de_eng . "',data_file='" . $df . "',title_info_ar='" . $nm_ar . "',desc_info_ar='" . $de_ar . "',video='" . $vd . "',id_kategori='" . $ik . "',is_headline='" . $ih . "',urutan='" . $ut . "',thumbnail='" . $thumbnail . "',latitude='" . $lt . "',is_utama='" . $iu . "',title_image='" . $ti . "',video2 = '" . $vd2 . "',topik = '" . $tpk . "',title_image_eng='" . $ti_eng . "',title_image_ar='" . $ti_ar . "',video3='" . $vd3 . "',video4='" . $vd4 . "',video5='" . $vd5 . "',image2='" . $foto2 . "',image3='" . $foto3 . "',image4='" . $foto4 . "',image5='" . $foto5 . "',country_id='" . $ci . "',durasi='" . $dr . "',is_rekomendasi='" . $ir . "',id_tokoh='" . $it . "',id_kelurahan='" . $kl . "',id_kecamatan='" . $kc . "',id_kota='" . $kt . "',id_provinsi='" . $pv . "',alamat='" . $almt . "',author='" . $auth . "',thumbnail_small='" . $foto6 . "',size='" . $sz . "',vid_youtube='" . $vid . "' WHERE id_info = '" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info = '" . $id . "'";
        }

        $this->response->getresponse($sql,'insertinfo');
    }

    public function getinfocms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $ci = $parameter['ci'];
        $it = $parameter['it'];
        $lt = $parameter['lt'];

        $tipe = "";
        if ($tp != '') {
            $tipe = " WHERE a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $tokoh = "";
        if ($it != '') {
            $tokoh = " AND a.id_tokoh='" . $it . "' ";
        }

        if ($lt == '') {
            $lt = 100;
        }

        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                " . $tipe . $tokoh .$country . " ORDER BY a.id_info DESC ";
        $this->response->getresponse($sql,'getinfocms');
    }

    public function getinfo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $ir = $parameter['ir'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];
        $it = $parameter['it'];
        $ord = $parameter['ord'];

        if($tp==''){
            $tp=$this->uri->segment(3);
        }

        if($ik==''){
            $ik=$this->uri->segment(4);
        }

        if($is==''){
            $is=$this->uri->segment(5);
        }

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $sub = "";
        if ($is != '') {
            $sub = " AND a.id_subkategori='" . $is . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $tokoh = "";
        if ($it != '') {
            $tokoh = " AND a.id_tokoh='" . $it . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }

        $order=" a.id_info DESC ";
        if($ord==1){
            $order=" a.total_read DESC ";
        }

        $rek = "";
        if ($ir != '') {
            $rek = " AND a.is_rekomendasi=1 ";
            $order=" a.date_created DESC ";
        }

        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $sub . $tokoh . $rek . $country . $cari . " ORDER BY " .$order. $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getinfonew() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];
        $it = $parameter['it'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $tokoh = "";
        if ($it != '') {
            $tokoh = " AND a.id_tokoh='" . $it . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $tokoh . $country . $cari . " ORDER BY a.urutan ASC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getpromo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = 6;
        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $country . $cari . " ORDER BY a.id_info DESC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getberita() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = 1;
        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $country . $cari . " ORDER BY a.id_info DESC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getvideo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = 5;
        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $country . $cari . " ORDER BY a.id_info DESC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getinovasi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = 9;
        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $country . $cari . " ORDER BY a.id_info DESC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getpopuler() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe IN(1,5,9) " . $tipe . $kat . $country . $cari . " ORDER BY a.total_read DESC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getlatepost() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe IN(1,5,9) " . $tipe . $kat . $country . $cari . " ORDER BY a.id_info DESC " . $limit;
         $this->response->getresponse($sql,'getinfo');
    }

    public function getinfonext() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $lt = $parameter['lt'];
        $id = $parameter['id'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        if ($tp == '') {
            $tp = 1;
        }

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $sub = "";
        if ($is != '') {
            $sub = " AND a.id_subkategori='" . $is . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }

        if ($id != '') {
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.id_info>'" . $id . "' " . $tipe . $kat . $sub . $country . $cari . " ORDER BY a.id_info ASC " . $limit;

            $this->response->getresponse($sqlCek,'getinfonext');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getinfoprev() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $lt = $parameter['lt'];
        $id = $parameter['id'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        if ($tp == '') {
            $tp = 1;
        }

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $sub = "";
        if ($is != '') {
            $sub = " AND a.id_subkategori='" . $is . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }

        if ($id != '') {
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.id_info<'" . $id . "' " . $tipe . $kat . $sub . $country . $cari . " ORDER BY a.id_info DESC " . $limit;

            $this->response->getresponse($sqlCek,'getinfoprev');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getvideoinovasi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $lat = $parameter['lat'];

        if($lt==''){
            $lt=50;
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlSubkategori = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_subkategori=a.id_subkategori AND x.status=1) as jumlah_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar
                    FROM tb_subkategori a WHERE a.status=1 AND a.id_kategori=133  ORDER BY a.urutan ASC ".$limit);
        $arraySubkategori = array();
        $a = 0;
        foreach ($sqlSubkategori->result_array() as $rowSubkategori) {
            $arraySubkategori[$a] = $rowSubkategori;
            $a++;
        }

        $sqlSubkategori->free_result();

        $sqlInovasi = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_kategori=133 ORDER BY a.id_info DESC ".$limit);
        $arrayInovasi = array();
        $b = 0;
        foreach ($sqlInovasi->result_array() as $rowInovasi) {
            $arrayInovasi[$b] = $rowInovasi;
            $b++;
        }

        $sqlInovasi->free_result();

        $result[0]['subkategori'] = $arraySubkategori;
        $result[0]['inovasi'] = $arrayInovasi;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action getinovasi'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function getparenting() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $lat = $parameter['lat'];

        if($lt==''){
            $lt=50;
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlSubkategori = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_subkategori=a.id_subkategori AND x.status=1) as jumlah_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar
                    FROM tb_subkategori a WHERE a.status=1 AND a.id_kategori=129  ORDER BY a.urutan ASC ".$limit);
        $arraySubkategori = array();
        $a = 0;
        foreach ($sqlSubkategori->result_array() as $rowSubkategori) {
            $arraySubkategori[$a] = $rowSubkategori;
            $a++;
        }

        $sqlSubkategori->free_result();

        $sqlTerbaru = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_kategori=129 ORDER BY a.id_info DESC ".$limit);
        $arrayTerbaru = array();
        $b = 0;
        foreach ($sqlTerbaru->result_array() as $rowTerbaru) {
            $arrayTerbaru[$b] = $rowTerbaru;
            $b++;
        }

        $sqlTerbaru->free_result();

        $sqlPopuler = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_kategori=129 ORDER BY a.total_read DESC ".$limit);
        $arrayPopuler = array();
        $c = 0;
        foreach ($sqlPopuler->result_array() as $rowPopuler) {
            $arrayPopuler[$c] = $rowPopuler;
            $c++;
        }

        $sqlPopuler->free_result();

        $result[0]['subkategori'] = $arraySubkategori;
        $result[0]['terbaru'] = $arrayTerbaru;
        $result[0]['populer'] = $arrayPopuler;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action getparenting'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function getwanita() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $lat = $parameter['lat'];

        if($lt==''){
            $lt=50;
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlSubkategori = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_subkategori=a.id_subkategori AND x.status=1) as jumlah_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar
                    FROM tb_subkategori a WHERE a.status=1 AND a.id_kategori=128  ORDER BY a.urutan ASC ".$limit);
        $arraySubkategori = array();
        $a = 0;
        foreach ($sqlSubkategori->result_array() as $rowSubkategori) {
            $arraySubkategori[$a] = $rowSubkategori;
            $a++;
        }

        $sqlSubkategori->free_result();

        $sqlTerbaru = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_kategori=128 ORDER BY a.id_info DESC ".$limit);
        $arrayTerbaru = array();
        $b = 0;
        foreach ($sqlTerbaru->result_array() as $rowTerbaru) {
            $arrayTerbaru[$b] = $rowTerbaru;
            $b++;
        }

        $sqlTerbaru->free_result();

        $sqlPopuler = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_kategori=128 ORDER BY a.total_read DESC ".$limit);
        $arrayPopuler = array();
        $c = 0;
        foreach ($sqlPopuler->result_array() as $rowPopuler) {
            $arrayPopuler[$c] = $rowPopuler;
            $c++;
        }

        $sqlPopuler->free_result();

        $result[0]['subkategori'] = $arraySubkategori;
        $result[0]['terbaru'] = $arrayTerbaru;
        $result[0]['populer'] = $arrayPopuler;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action getparenting'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function gettokoh() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $lat = $parameter['lat'];

        if($lt==''){
            $lt=50;
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlSubkategori = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_subkategori=a.id_subkategori AND x.status=1) as jumlah_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar
                    FROM tb_subkategori a WHERE a.status=1 AND a.id_kategori=138  ORDER BY a.urutan ASC ".$limit);
        $arraySubkategori = array();
        $a = 0;
        foreach ($sqlSubkategori->result_array() as $rowSubkategori) {
            $arraySubkategori[$a] = $rowSubkategori;
            $a++;
        }

        $sqlSubkategori->free_result();

        $sqlTerbaru = $this->db->query("SELECT a.*,b.nm_tokoh,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                LEFT JOIN tb_tokoh b 
                ON a.id_tokoh=b.id_tokoh
                WHERE a.status=1 AND a.tipe=5 AND b.status=1 ORDER BY a.id_info DESC ".$limit);
        $arrayTerbaru = array();
        $b = 0;
        foreach ($sqlTerbaru->result_array() as $rowTerbaru) {
            $arrayTerbaru[$b] = $rowTerbaru;
            $b++;
        }

        $sqlTerbaru->free_result();

        $sqlPopuler = $this->db->query("SELECT a.*,b.nm_tokoh,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                LEFT JOIN tb_tokoh b 
                ON a.id_tokoh=b.id_tokoh
                WHERE a.status=1 AND a.tipe=5 AND b.status=1 ORDER BY a.total_read DESC ".$limit);
        $arrayPopuler = array();
        $c = 0;
        foreach ($sqlPopuler->result_array() as $rowPopuler) {
            $arrayPopuler[$c] = $rowPopuler;
            $c++;
        }

        $sqlPopuler->free_result();

        $sqlTokoh = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_tokoh=a.id_tokoh AND x.status=1) as total_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                    FROM tb_tokoh a WHERE a.status='1' ORDER BY a.urutan ASC ".$limit);
        $arrayTokoh = array();
        $d = 0;
        foreach ($sqlTokoh->result_array() as $rowTokoh) {
            $arrayTokoh[$d] = $rowTokoh;
            $d++;
        }

        $sqlTokoh->free_result();

        $result[0]['subkategori'] = $arraySubkategori;
        $result[0]['tokoh'] = $arrayTokoh;
        $result[0]['terbaru'] = $arrayTerbaru;
        $result[0]['populer'] = $arrayPopuler;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action getparenting'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function searchinfo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        if ($qy != '') {
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND tipe=1 " . $country . "
                AND (a.title_info LIKE '%" . $qy . "%' OR a.title_info_eng LIKE '%" . $qy . "%' OR a.title_info_ar LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' OR a.desc_info_eng LIKE '%" . $qy . "%' OR a.desc_info_ar LIKE '%" . $qy . "%' ) ORDER BY a.id_info DESC " . $limit;
            $this->response->getresponse($sql,'searchinfo');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function searchtv() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        if ($qy != '') {
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND tipe=5 " . $country . "
                AND (a.title_info LIKE '%" . $qy . "%' OR a.title_info_eng LIKE '%" . $qy . "%' OR a.title_info_ar LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' OR a.desc_info_eng LIKE '%" . $qy . "%' OR a.desc_info_ar LIKE '%" . $qy . "%' ) ORDER BY a.id_info DESC " . $limit;
            $this->response->getresponse($sql,'searchinfo');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbytipe() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $lt = $parameter['lt'];
        $it = $parameter['it'];
        $qy = $parameter['qy'];
        $ci = $parameter['ci'];

        $kat = "";
        if ($ik != '') {
            $kat = " AND a.id_kategori='" . $ik . "' ";
        }

        $sub = "";
        if ($is != '') {
            $sub = " AND a.id_subkategori='" . $is . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $cari = "";
        if ($qy != '') {
            $cari = " AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' ) ";
        }


        $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 " . $tipe . $kat . $sub . $country . $cari . " ORDER BY a.id_info DESC " . $limit;
        $this->response->getresponse($sql,'getbytipe');
    }

    public function getbytokoh() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $it = $parameter['it'];
        if($it==''){
            $it=$this->uri->segment(3);
        }

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        if($it!=''){
            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.id_tokoh='" . $id . "' AND a.status=1 ORDER BY a.date_created DESC ".$limit;
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            //$sUpdate = "UPDATE tb_info SET total_read=total_read+1 WHERE id_info='" . $id . "'";
            //$this->db->query($sUpdate);

            $sql = "SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.id_info='" . $id . "' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updateread() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sUpdate = "UPDATE tb_info SET total_read=total_read+1 WHERE id_info='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info='" . $id . "'";
            $this->response->getresponse($sql,'updateread');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatefavorite() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $lk = $parameter['lk'];

        if ($id != '') {
            $sCek = $this->db->query("SELECT id_info,total_favorite FROM tb_info WHERE id_info='" . $id . "'");
            $rCek = $sCek->row_array();

            $like="";
            if($lk==1){
                $like=" total_favorite=total_favorite+1 ";
            }elseif($lk==0){
                $like=" total_favorite=total_favorite-1 ";
                if($rCek['total_favorite']<=0){
                    $like = " total_favorite=0 ";
                }
            }

            $sUpdate = "UPDATE tb_info SET ".$like." WHERE id_info='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info='" . $id . "'";
            $this->response->getresponse($sql,'updateread');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatevid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $vid = $parameter['vid'];

        if ($vid != '' && $id != '') {
            $sUpdate = "UPDATE tb_info SET status='" . $st . "',vid_youtube='".$vid."' WHERE id_info='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }


    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_info SET status='" . $st . "' WHERE id_info='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function deleteimage() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $img=$parameter['img'];

        if ($img != '' && $id != '') {
            $sUpdate = "UPDATE tb_info SET ".$img."='' WHERE id_info='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function deletevideo() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sUpdate = "UPDATE tb_info SET video='' WHERE id_info='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_info WHERE id_info='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_info WHERE id_info='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_info ORDER BY id_info DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
