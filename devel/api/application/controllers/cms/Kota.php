<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kota extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }


    public function get() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sql="SELECT * FROM tb_kota WHERE id_kota='" . $id . "'";
        $this->response->getresponse($sql,'get');
    }

    public function getbyprovinsi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sql = "SELECT * FROM tb_kota WHERE id_provinsi='" . $id . "' AND status='1' order by nm_kota asc";
        $this->response->getresponse($sql,'getbyprovinsi');
    }

    public function getbyprovinsithoyyiban() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $url = $this->general_lib->url_thoyyiban() . 'api/kota_api.php?action=get_byprovinsi'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'id' => $id
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function getprovinsi() {

        $sql = "SELECT * FROM tb_provinsi WHERE status='1' order by nm_provinsi asc";
        $this->response->getresponse($sql,'getprovinsi');
    }

    public function getprovinsithoyyiban() {

        $url = $this->general_lib->url_thoyyiban() . 'api/kota_api.php?action=get_provinsi'.$this->general_lib->key_thoyyiban();
        $resp = $this->general_lib->general_http($url, '');
        echo $resp;
        exit();
    }

    public function getprovinsibyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sql = "SELECT * FROM tb_provinsi  WHERE id_provinsi='" . $id . "' order by nm_provinsi asc";
        $this->response->getresponse($sql,'getprovinsibyid');
    }

    public function getkota() {

        $sql = "SELECT * FROM tb_kota WHERE tipe='1' AND status='1' ORDER BY nm_kota ASC";
        $this->response->getresponse($sql,'getkota');
    }

}
