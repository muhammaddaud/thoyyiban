<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slide extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('cms',true);
        $this->load->model('responsecms');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertslide() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ut = $parameter['ut'];
        $tp = $parameter['tp'];
        $ts = addslashes($parameter['ts']);
        $ts_eng = addslashes($parameter['ts_eng']);
        $ts_ar = addslashes($parameter['ts_ar']);
        $st = $parameter['st'];
        $image = $parameter['img'];
        $ds = addslashes($parameter['ds']);
        $ds_eng = addslashes($parameter['ds_eng']);
        $ds_ar = addslashes($parameter['ds_ar']);
        $lk = $parameter['lk'];
        $bl = $parameter['bl'];
        $iw = $parameter['iw'];
        $id = $parameter['id'];

        $sCek = $this->db->query("SELECT * FROM tb_sitepage WHERE id_sitepage='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";

                //echo $namefile;
                // -------------------------------------------------------------------
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }

        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image1'];
        }

        $datafile2 = $parameter['image2'];
        $binary2 = base64_decode($datafile2);
        $namefile2 = $parameter['filename2'];
        if ($namefile2 != '') {
            $target_dir2 = $this->general_lib->path();

            if (!file_exists($target_dir2)) {
                mkdir($target_dir2, 0777, true);
            }

            $target_path2 = $target_dir2;
            $now2 = date('YmdHis');
            $rand2 = rand(1111, 9999);
            $generatefile2 = $now2 . $rand2;
            $namefile2 = $generatefile2 . ".jpeg";
            $target_path2 = $target_path2 . $namefile2;

            chmod($target_path2, 0777);
            $fh2 = fopen($target_path2, 'w') or die("can't open file");
            chmod($target_path2, 0777);
            fwrite($fh2, $binary2);
            fclose($fh2);

            sleep(1);

            $foto2 = "upload/" . $namefile2;
        }

        if ($namefile2 == '') {
            $foto2 = "";
        }

        if ($foto2 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto2,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto2);
        }

        if ($foto2 == '') {
            $foto2 = $rCek['thumbnail'];
        }


        $datafile3 = $parameter['image3'];
        $binary3 = base64_decode($datafile3);
        $namefile3 = $parameter['filename3'];
        if ($namefile3 != '') {
            $target_dir3 = $this->general_lib->path();

            if (!file_exists($target_dir3)) {
                mkdir($target_dir3, 0777, true);
            }

            $target_path3 = $target_dir3;
            $now3 = date('YmdHis');
            $rand3 = rand(1111, 9999);
            $generatefile3 = $now3 . $rand3;
            $namefile3 = $generatefile3 . ".jpeg";
            $target_path3 = $target_path3 . $namefile3;

            chmod($target_path3, 0777);
            $fh3 = fopen($target_path3, 'w') or die("can't open file");
            chmod($target_path3, 0777);
            fwrite($fh3, $binary3);
            fclose($fh3);

            sleep(1);

            $foto3 = "upload/" . $namefile3;
        }

        if ($namefile3 == '') {
            $foto3 = "";
        }

        if ($foto3 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto3,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto3);
        }

        if ($foto3 == '') {
            $foto3 = $rCek['thumbnail_small'];
        }


        if ($tp != '' && $ut != '') {

            if ($id == '') {
                $sInsertSlide = "INSERT INTO tb_sitepage
                (title_sitepage,urutan,desc_sitepage,title_sitepage_eng,desc_sitepage_eng,title_sitepage_ar,desc_sitepage_ar,image1,date_created,date_updated,tipe,status,link,button_link,is_web,thumbnail,thumbnail_small)
                VALUES
                ('" . $ts . "','" . $ut . "','" . $ds . "','" . $ts_eng . "','" . $ds_eng . "','" . $ts_ar . "','" . $ds_ar . "','" . $foto . "',NOW(),NOW(),'" . $tp . "','" . $st . "','" . $lk . "','" . $bl . "','" . $iw . "','" . $foto2 . "','" . $foto3 . "')";
                $this->db->query($sInsertSlide);

                $sql = "SELECT * FROM tb_sitepage ORDER BY id_sitepage DESC LIMIT 1";
            } else {
                $sInsertSlide = "UPDATE tb_sitepage SET title_sitepage='" . $ts . "',urutan='" . $ut . "',desc_sitepage='" . $ds . "',image1='" . $foto . "',link='" . $lk . "',title_sitepage_eng='" . $ts_eng . "',desc_sitepage_eng='" . $ds_eng . "',title_sitepage_ar='" . $ts_ar . "',desc_sitepage_ar='" . $ds_ar . "',date_updated=NOW(),tipe='" . $tp . "',status='" . $st . "',button_link='" . $bl . "',is_web='" . $iw . "',thumbnail='" . $foto2 . "',thumbnail_small='" . $foto3 . "' WHERE id_sitepage='" . $id . "'";
                $this->db->query($sInsertSlide);

                $sql = "SELECT * FROM tb_sitepage WHERE id_sitepage = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertslide');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getslide() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $tipe="";
        if($tp!=''){
            $tipe=" AND tipe='".$tp."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_sitepage WHERE id_sitepage!='' ".$tp." ORDER BY date_created DESC ".$limit;
         $this->response->getresponse($sql,'getslide');
    }

    public function slideweb() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_sitepage WHERE tipe='1' AND status ='1' ORDER BY urutan ASC LIMIT  " . $limit;
        $this->response->getresponse($sql,'slideweb');
    }

    public function slidemobile() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_sitepage WHERE tipe='2' AND status ='1' ORDER BY urutan ASC LIMIT  " . $limit;
        $this->response->getresponse($sql,'slideweb');
    }

    public function getbytipe() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $site="";
        if($sp!=''){
            $site=" AND title_sitepage='".$sp."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        if($tp!=''){
            $sql = "SELECT * FROM tb_sitepage WHERE tipe='" . $tp . "' AND status=1 ".$site." ORDER BY urutan ASC ".$limit;
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_sitepage WHERE id_sitepage='" . $id . "'";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function deleteimage() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $img=$parameter['img'];

        if ($img != '' && $id != '') {
            $sUpdate = "UPDATE tb_sitepage SET ".$img."='' WHERE id_sitepage='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_sitepage WHERE id_sitepage='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_sitepage SET status='" . $st . "' WHERE id_sitepage='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_sitepage WHERE id_sitepage='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_sitepage WHERE id_sitepage='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_sitepage ORDER BY id_sitepage DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
