<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertuser() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = $parameter['nm'];
        $hp = $parameter['hp'];
        $em = $parameter['em'];
        $st = $parameter['st'];
        $lv = $parameter['lv'];
        $tp = $parameter['tp'];
        $tg = $parameter['tg'];
        $ci = $parameter['ci'];
        $desc = $parameter['desc'];
        $password = $parameter['pass'];
        $real_pin = $parameter['pin'];
        $id = $parameter['id'];

        if ($tp == '') {
            $tp = 1;
        }

        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }
        $pass = md5(hash('sha512', $password));

        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        $sCek = $this->db->query("SELECT * FROM tb_user_login WHERE id_user='" . $id . "'");
        $rCek = $sCek->row_array();

        $sCekEmail = $this->db->query("SELECT * FROM tb_user_login WHERE email_user='" . $em . "'");
        $rCekEmail = $sCekEmail->row_array();

        $sCekHp = $this->db->query("SELECT * FROM tb_user_login WHERE msisdn='" . $hp . "'");
        $rCekHp = $sCekHp->row_array();
        if ($id == '') {
            if ($rCekEmail['id_user'] != '') {
                $code = '03';
                $status = 'Email sudah terdaftar';
                $this->general_lib->error($code,$status);
            }

            if ($rCekHp['id_user'] != '') {
                $code = '04';
                $status = 'No Handphone sudah terdaftar';
                $this->general_lib->error($code,$status);
            }
        } else {
            if ($rCek['email_user'] != $em && $rCekEmail['id_user'] != '') {
                $code = '03';
                $status = 'Email sudah terdaftar';
                $this->general_lib->error($code,$status);
            }

            if ($rCek['msisdn'] != $hp && $rCekHp['id_user'] != '') {
                $code = '04';
                $status = 'No Handphone sudah terdaftar';
                $this->general_lib->error($code,$status);
            }
        }

        if ($nm != '' && $hp != '' && $em != '' && $lv != '') {

            if ($id == '') {
                $sInsert = "INSERT INTO tb_user_login
                         (nm_user,email_user,msisdn,image,level_user,pass_user,real_pass_user,pin,real_pin,date_created,date_updated,
                         tipe,status,description,task,country_id)
                         VALUES
                         ('" . $nm . "','" . $em . "','" . $hp . "','" . $foto . "','" . $lv . "','" . $pass . "','" . $password . "','" . $pin . "','" . $real_pin . "',NOW(),NOW(),"
                        . "'" . $tp . "','" . $st . "','" . $desc . "','" . $tg . "','" . $ci . "')";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_user_login WHERE msisdn='" . $hp . "'";
            } else {

                if ($foto != '') {
                    $sUpdate = "UPDATE tb_user_login SET nm_user='" . $nm . "',email_user='" . $em . "',msisdn='" . $hp . "',image='" . $foto . "',level_user='" . $lv . "',
                    date_updated=NOW(),tipe='" . $tp . "',status='" . $st . "',description='" . $desc . "',task='" . $tg . "',country_id='" . $ci . "' WHERE id_user='" . $id . "'";
                    $this->db->query($sUpdate);
                } else {
                    $sUpdate = "UPDATE tb_user_login SET nm_user='" . $nm . "',email_user='" . $em . "',msisdn='" . $hp . "',level_user='" . $lv . "',country_id='" . $ci . "',
                    date_updated=NOW(),tipe='" . $tp . "',status='" . $st . "',description='" . $desc . "',task='" . $tg . "' WHERE id_user='" . $id . "'";
                    $this->db->query($sUpdate);
                }

                $sql = "SELECT * FROM tb_user_login WHERE id_user = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertuser');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function login() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = $parameter['em'];
        $password = $parameter['pass'];
        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT * FROM tb_user_login WHERE email_user='" . $em . "'");
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_user'] != '') {
            $param_user = " email_user='" . $em . "' ";
        } else {
            $param_user = " msisdn='" . $em . "' ";
        }

        $sqlCek = "SELECT a.*,
                 IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
                 FROM tb_user_login a WHERE " . $param_user . " AND a.pass_user='" . $pass . "'";
        $qCek = $this->db->query($sqlCek);
        $rCek = $qCek->row_array();

        if ($em != '' && $pass != '') {
            if ($rCek['id_user'] == '') {
                $code = '03';
                $status = 'Username & password invalid.';
                $this->general_lib->error($code,$status);
            } else if ($rCek['status'] == '0') {
                $code = '04';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            } else {
                $this->response->getresponse($sqlCek,'login');
            }
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getuser() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT a.*,
             IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
             FROM tb_user_login a ORDER BY a.date_created DESC ".$limit;
         $this->response->getresponse($sql,'getuser');
    }


    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT a.*,
                 IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
                 FROM tb_user_login a WHERE a." . $this->id . "='" . $id . "' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepassword() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $old_password = $parameter['old_pass'];
        $password = $parameter['pass'];
        $id = $parameter['id'];

        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT id_user FROM tb_user_login WHERE id_user='" . $id . "' AND real_pass_user='" . $old_password . "'");
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_user'] == '') {
            $code = '04';
            $status = "Password lama yang Anda masukan salah";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password tidak valid, minimal 6 digit huruf atau angka";
            $this->general_lib->error($code,$status);
        }

        if ($old_password != '' && $password != '' && $id != '') {
            $sqlUpdate = "Update tb_user_login set pass_user='" . $pass . "', real_pass_user ='" . $password . "',
                date_updated=NOW() where id_user ='" . $id . "'";
            $this->db->query($sqlUpdate);

            $sql = "SELECT * FROM tb_user_login WHERE id_user='" . $id . "'";
            $this->response->getresponse($sql,'changepassword');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepin() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $old_pin = $parameter['old_pin'];
        $real_pin = $parameter['pin'];
        $id = $parameter['id'];

        $pin = md5(hash('sha512', $real_pin));

        $sCekUser = $this->db->query("SELECT id_user FROM tb_user_login WHERE id_user='" . $id . "' AND real_pin='" . $old_pin . "'");
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_user'] == '') {
            $code = '04';
            $status = "PIN lama yang Anda masukan salah";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($real_pin) == FALSE) {
            $code = "07";
            $status = "PIN harus berupa angka";
            $this->general_lib->error($code,$status);
        }

        if (strlen($real_pin) != 6) {
            $code = "07";
            $status = "PIN minimal 6 digit angka";
            $this->general_lib->error($code,$status);
        }

        if ($old_pin != '' && $real_pin != '' && $id != '') {
            $sUpdate = "UPDATE tb_user_login SET pin='" . $pin . "',real_pin='" . $real_pin . "',date_updated=NOW() WHERE id_user='" . $id . "' ";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_user_login WHERE id_user='" . $id . "'";
            $this->response->getresponse($sql,'changepin');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_user_login SET status='" . $st . "' WHERE id_user='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_user_login WHERE id_user='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_user_login WHERE id_user='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_user_login ORDER BY id_user DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
