<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Withdrawal extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function withdrawalukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $nom = $parameter['nom'];
        $pin = $parameter['pin'];
       
        $sUkm = "SELECT * FROM tb_ukm WHERE id_ukm='" . $iu . "'";
        $qUkm = $this->db->query($sUkm);
        $rUkm = $qUkm->row_array();

        $bank = $rUkm['bank'];
        $noRekening = $rUkm['no_rekening'];
        $an = $rUkm['atas_nama'];

        if ($rUkm['real_pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah.";
            $this->general_lib->error($code,$status);
        }

        if ($rUkm['bank'] == '' || $rUkm['no_rekening'] == '' || $rUkm['atas_nama'] == '') {
            $code = '06';
            $status = "Untuk withdrawal silahkan lengkapi data akun bank Anda.";
            $this->general_lib->error($code,$status);
        }

        if ($nom > $rUkm['saldo_transaksi']) {
            $code = '06';
            $status = "Saldo transaksi Anda tidak cukup.";
            $this->general_lib->error($code,$status);
        }

        
        if ($iu != '' && $nom != '' && $pin != '') {
            
            $sInsert = "INSERT INTO tb_withdrawal(id_user,nominal,bank,no_rekening,atas_nama,date_created,date_updated,status,tipe)
                        VALUES
                        ('" . $iu . "','" . $nom . "','" . $bank . "','" . $noRekening . "','" . $an . "',NOW(),NOW(),'1','1')";
            $this->db->query($sInsert);

            $sql = "SELECT * FROM tb_withdrawal ORDER BY date_created DESC LIMIT 1";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function withdrawalprodusen() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['ip'];
        $nom = $parameter['nom'];
        $pin = $parameter['pin'];

        $sUkm = "SELECT * FROM tb_produsen WHERE id_produsen='" . $iu . "'";
        $qUkm = $this->db->query($sUkm);
        $rUkm = $qUkm->row_array();

        $bank = $rUkm['nm_bank'];
        $noRekening = $rUkm['no_rekening'];
        $an = $rUkm['atas_nama'];

        /* if ($rUkm['real_pin'] != $pin) {
          $code = '07';
          $status = "PIN yang Anda masukan salah.";
          $this->general_lib->error($code,$status);
          }
         * 
         */

        if ($rUkm['nm_bank'] == '' || $rUkm['no_rekening'] == '' || $rUkm['atas_nama'] == '') {
            $code = '06';
            $status = "Untuk withdrawal silahkan lengkapi data akun bank Anda.";
            $this->general_lib->error($code,$status);
        }

        if ($nom > $rUkm['saldo_transaksi']) {
            $code = '06';
            $status = "Saldo transaksi Anda tidak cukup.";
            $this->general_lib->error($code,$status);
        }


        if ($iu != '' && $nom != '') {

            $sInsert = "INSERT INTO tb_withdrawal(id_user,nominal,bank,no_rekening,atas_nama,date_created,date_updated,status,tipe)
                        VALUES
                        ('" . $iu . "','" . $nom . "','" . $bank . "','" . $noRekening . "','" . $an . "',NOW(),NOW(),'1','4')";
            $this->db->query($sInsert);

            $sql = "SELECT * FROM tb_withdrawal ORDER BY date_created DESC LIMIT 1";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function withdrawaldepo() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['id'];
        $nom = $parameter['nom'];
        $pin = $parameter['pin'];

        $sUkm = "SELECT * FROM tb_depo WHERE id_depo='" . $iu . "'";
        $qUkm = $this->db->query($sUkm);
        $rUkm = $qUkm->row_array();

        $bank = $rUkm['bank'];
        $noRekening = $rUkm['no_rekening'];
        $an = $rUkm['atas_nama'];

        if ($rUkm['real_pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah.";
            $this->general_lib->error($code,$status);
        }

        if ($rUkm['bank'] == '' || $rUkm['no_rekening'] == '' || $rUkm['atas_nama'] == '') {
            $code = '06';
            $status = "Untuk withdrawal silahkan lengkapi data akun bank Anda.";
            $this->general_lib->error($code,$status);
        }

        if ($nom > $rUkm['saldo_transaksi']) {
            $code = '06';
            $status = "Saldo transaksi Anda tidak cukup.";
            $this->general_lib->error($code,$status);
        }


        if ($iu != '' && $nom != '' && $pin != '') {

            $sInsert = "INSERT INTO tb_withdrawal(id_user,nominal,bank,no_rekening,atas_nama,date_created,date_updated,status,tipe)
                        VALUES
                        ('" . $iu . "','" . $nom . "','" . $bank . "','" . $noRekening . "','" . $an . "',NOW(),NOW(),'1','5')";
            $this->db->query($sInsert);

            $sql = "SELECT * FROM tb_withdrawal ORDER BY date_created DESC LIMIT 1";

            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function withdrawalmember() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $im = $parameter['im'];
        $nom = $parameter['nom'];
        $bank = $parameter['bk'];
        $noRekening = $parameter['nr'];
        $an = $parameter['an'];
        $real_pin = $parameter['pin'];

        $sMember = "SELECT * FROM tb_customer WHERE id_customer='" . $im . "'";
        $qMember = $this->db->query($sMember);
        $rMember = $qMember->row_array();

        if ($bank == '') {
            $bank = $rMember['bank'];
        }
        if ($noRekening == '') {
            $noRekening = $rMember['no_rekening'];
        }
        if ($an == '') {
            $an = $rMember['atas_nama'];
        }

        $pin = md5(hash('sha512', $real_pin));

        if ($rMember['pin'] != $pin) {
            $code = '07';
            $status = "PIN yang Anda masukan salah.";
            $this->general_lib->error($code,$status);
        }

        if ($bank == '' || $noRekening == '' || $an == '') {
            $code = '06';
            $status = "Untuk withdrawal silahkan lengkapi data akun bank Anda.";
            $this->general_lib->error($code,$status);
        }

        if ($nom > $rMember['saldo']) {
            $code = '06';
            $status = "Saldo Anda tidak cukup.";
            $this->general_lib->error($code,$status);
        }

        $tl = 'Tarik Tunai';
        $tl_eng = 'Withdrawal';
        $tl_ar = 'السحب النقدي';
        $pesan = 'Permintaan tarik tunai sukses, permintaan Anda akan segera kami proses<br/><br/>
                        Informasi Tarik Tunai : <br/><br/>
                        Nominal : Rp.' . number_format($nom) . '<br/>
                        Bank : ' . $bank . '<br/>
                        No Rekening : ' . $noRekening . '<br/>
                        Atas Nama : ' . $an;

        $pesan_eng = 'A successful withdrawal request, we will process your request immediately<br/><br/>
                        Withdrawal Informastion :<br/><br/>
                        Nominal : IDR.' . number_format($nom) . '<br/>
                        Bank : ' . $bank . '<br/>
                        Account Number : ' . $noRekening . '<br/>
                        Account Name : ' . $an;

        $pesan_ar = 'طلب سحب نقدي ناجح ، سنقوم بمعالجة طلبك على الفور<br/><br/>
                        معلومات السحب النقدي :<br/><br/>
                        اسمي : IDR.' . number_format($nom) . '<br/>
                        البنك : ' . $bank . '<br/>
                        رقم الحساب : ' . $noRekening . '<br/>
                        نيابة عن : ' . $an;

        if ($im != '' && $nom != '' && $pin != '') {

            $sInsert = "INSERT INTO tb_withdrawal(id_user,nominal,bank,no_rekening,atas_nama,date_created,date_updated,status,tipe)
                        VALUES
                        ('" . $im . "','" . $nom . "','" . $bank . "','" . $noRekening . "','" . $an . "',NOW(),NOW(),'1','6')";
            $this->db->query($sInsert);

            $s1 = "INSERT INTO tb_notif
                (id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $im . "','" . $tl . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan_eng . "','" . $tl_ar . "','" . $pesan_ar . "')";
            $this->db->query($s1);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $rMember['nm_customer'] . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Permintaan tarik tunai sukses, permintaan Anda akan segera kami proses.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Tarik Tunai:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nominal</td><td>:</td><td>Rp.' . number_format($nom) . '</td></tr>
                                                                        <tr><td>Bank </td><td>:</td><td> ' . $bank . '</td></tr>
                                                                        <tr><td>No Rekening </td><td>:</td><td> ' . $noRekening . '</td></tr>
                                                                        <tr><td>Atas Nama </td><td>:</td><td> <b>' . $an . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "Tarik Tunai";
            $subject = "Tarik Tunai";
            pushEmailHerbal($rMember['email'], $rMember['nm_customer'], $message, $addheader, $subject);

            $sql = "SELECT * FROM tb_withdrawal ORDER BY date_created DESC LIMIT 1";

            responseQuery($sql, $err, $this->msg_code, $this->action, $this->lk, $pesan);
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        if ($iu != '') {
            $sql = "SELECT a.*,
                (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_user) as nama,
                (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_user) as email,
                (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_user) as no_hp
                FROM tb_withdrawal a
                WHERE a.tipe=1
                AND a.id_user='" . $iu . "'
                ORDER BY a.id_withdrawal DESC";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyprodusen() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        if ($ip != '') {
            $sql = "SELECT a.*,
                (SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen = a.id_user) as nama,
                (SELECT x.email FROM tb_produsen x WHERE x.id_produsen = a.id_user) as email,
                (SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen = a.id_user) as no_hp
                FROM tb_withdrawal a
                WHERE a.tipe=4
                AND a.id_user='" . $ip . "'
                ORDER BY a.id_withdrawal DESC";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbydepo() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {
            $sql = "SELECT a.*,
                (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_user) as nama,
                (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_user) as email,
                (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_user) as no_hp
                FROM tb_withdrawal a
                WHERE a.tipe=5
                AND a.id_user='" . $id . "'
                ORDER BY a.id_withdrawal DESC";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbymember() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $im = $parameter['im'];
        if ($im != '') {
            $sql = "SELECT a.*,
                (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_user) as nm_customer,
                (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_user) as email,
                (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_user) as no_hp
                FROM tb_withdrawal a
                WHERE a.tipe=6
                AND a.id_user='" . $im . "'
                ORDER BY a.id_withdrawal DESC";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sCek = "SELECT * FROM tb_withdrawal WHERE id_withdrawal='" . $id . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($id != '') {
            if ($rCek['tipe'] == 1) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_user) as nama,
                    (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            } elseif ($rCek['tipe'] == 2) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_cabang FROM lpt_cabang x WHERE x.id_cabang = a.id_user) as nama,
                    (SELECT x.email FROM lpt_cabang x WHERE x.id_cabang = a.id_user) as email,
                    (SELECT x.no_hp FROM lpt_cabang x WHERE x.id_cabang = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            } elseif ($rCek['tipe'] == 3) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_perwakilan FROM lpt_perwakilan x WHERE x.id_perwakilan = a.id_user) as nama,
                    (SELECT x.email FROM lpt_perwakilan x WHERE x.id_perwakilan = a.id_user) as email,
                    (SELECT x.no_hp FROM lpt_perwakilan x WHERE x.id_perwakilan = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            } elseif ($rCek['tipe'] == 4) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen = a.id_user) as nama,
                    (SELECT x.email FROM tb_produsen x WHERE x.id_produsen = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            } elseif ($rCek['tipe'] == 5) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_user) as nama,
                    (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            } elseif ($rCek['tipe'] == 6) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_user) as nama,
                    (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            } elseif ($rCek['tipe'] == 7) {
                $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_agen FROM tb_agen x WHERE x.id_agen = a.id_agen),'') as nama,
                    IFNULL((SELECT x.email FROM tb_agen x WHERE x.id_agen = a.id_agen),'') as email,
                    IFNULL((SELECT x.no_hp FROM tb_agen x WHERE x.id_agen = a.id_agen),'') as no_hp
                    FROM tb_withdrawal a
                    WHERE a.id_withdrawal='" . $id . "'";
            }
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getwithdrawalcms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];

         $sql = "SELECT a.*,
                (SELECT x.nm_cabang FROM lpt_cabang x WHERE x.id_cabang = a.id_cabang) as nama,
                (SELECT x.email FROM lpt_cabang x WHERE x.id_cabang = a.id_cabang) as email,
                (SELECT x.no_hp FROM lpt_cabang x WHERE x.id_cabang = a.id_cabang) as no_hp
                FROM tb_withdrawal a
                ORDER BY a.id_withdrawal DESC";

        if ($tp != '') {
            if ($tp == 1) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm = a.id_user) as nama,
                    (SELECT x.email FROM tb_ukm x WHERE x.id_ukm = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.tipe='" . $tp . "' ORDER BY a.id_withdrawal DESC";
            } elseif ($tp == 2) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_cabang FROM lpt_cabang x WHERE x.id_cabang = a.id_user) as nama,
                    (SELECT x.email FROM lpt_cabang x WHERE x.id_cabang = a.id_user) as email,
                    (SELECT x.no_hp FROM lpt_cabang x WHERE x.id_cabang = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.tipe='" . $tp . "' ORDER BY a.id_withdrawal DESC";
            } elseif ($tp == 3) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_perwakilan FROM lpt_perwakilan x WHERE x.id_perwakilan = a.id_user) as nama,
                    (SELECT x.email FROM lpt_perwakilan x WHERE x.id_perwakilan = a.id_user) as email,
                    (SELECT x.no_hp FROM lpt_perwakilan x WHERE x.id_perwakilan = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.tipe='" . $tp . "' ORDER BY a.id_withdrawal DESC";
            } elseif ($tp == 4) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen = a.id_user) as nama,
                    (SELECT x.email FROM tb_produsen x WHERE x.id_produsen = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.tipe='" . $tp . "' ORDER BY a.id_withdrawal DESC";
            } elseif ($tp == 5) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_depo FROM tb_depo x WHERE x.id_depo = a.id_user) as nama,
                    (SELECT x.email FROM tb_depo x WHERE x.id_depo = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_depo x WHERE x.id_depo = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.tipe='" . $tp . "' ORDER BY a.id_withdrawal DESC";
            } elseif ($tp == 6) {
                $sql = "SELECT a.*,
                    (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer = a.id_user) as nama,
                    (SELECT x.email FROM tb_customer x WHERE x.id_customer = a.id_user) as email,
                    (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer = a.id_user) as no_hp
                    FROM tb_withdrawal a
                    WHERE a.tipe='" . $tp . "' ORDER BY a.id_withdrawal DESC";
            }
        }

        $this->response->getresponse($sql,'success');
    }

    public function updatestatus() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];
        $ket = $parameter['ket'];

        $sCek = "SELECT * FROM tb_withdrawal WHERE id_withdrawal='" . $id . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        $nom = $rCek['nominal'];

        if ($rCek['tipe'] == 1) {
            $sCekSaldo = "SELECT * FROM tb_ukm WHERE id_ukm='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            $saldo = $rCekSaldo['saldo_transaksi'];
        } elseif ($rCek['tipe'] == 2) {
            $sCekSaldo = "SELECT * FROM lpt_cabang WHERE id_cabang='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            $saldo = $rCekSaldo['saldo'];
        } elseif ($rCek['tipe'] == 3) {
            $sCekSaldo = "SELECT * FROM lpt_perwakilan WHERE id_perwakilan='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            $saldo = $rCekSaldo['saldo'];
        } elseif ($rCek['tipe'] == 4) {
            $sCekSaldo = "SELECT * FROM tb_produsen WHERE id_produsen='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            $saldo = $rCekSaldo['saldo_transaksi'];
        } elseif ($rCek['tipe'] == 5) {
            $sCekSaldo = "SELECT * FROM tb_depo WHERE id_depo='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            $saldo = $rCekSaldo['saldo_transaksi'];
        } elseif ($rCek['tipe'] == 6) {
            $sCekSaldo = "SELECT * FROM tb_customer WHERE id_customer='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            $saldo = $rCekSaldo['saldo'];
        } elseif ($rCek['tipe'] == 7) {
            $sCekSaldo = "SELECT * FROM tb_agen WHERE id_agen='" . $rCek['id_user'] . "'";
            $qCekSaldo = $this->db->query($sCekSaldo);
            $rCekSaldo = $qCekSaldo->row_array();

            if($rCek['tipe_withdrawal']==1){
                $saldo = $rCekSaldo['saldo'];
            }elseif($rCek['tipe_withdrawal']==2){
                $saldo = $rCekSaldo['total_komisi'];
            }
            
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->path;

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            unlink($this->u_path . $rCek['image']);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($nom > $saldo && $st == 2) {
            $code = '06';
            $status = "Saldo tidak cukup.";
            $this->general_lib->error($code,$status);
        }

        if ($id != '') {
            $sUpdate = "UPDATE tb_withdrawal SET status = '" . $st . "',keterangan='" . $ket . "',image='" . $foto . "',date_updated=NOW() WHERE id_withdrawal = '" . $id . "'";
            $this->db->query($sUpdate);

            if ($st == 3) {
                if ($rCek['tipe'] == 1) {
                    $updateSaldo = "UPDATE tb_ukm SET saldo_transaksi=saldo_transaksi-'" . $nom . "',date_updated=NOW() WHERE id_ukm='" . $rCek['id_user'] . "'";
                    $this->db->query($updateSaldo);
                } elseif ($rCek['tipe'] == 2) {
                    $updateSaldo = "UPDATE lpt_cabang SET saldo=saldo-'" . $nom . "',date_updated=NOW() WHERE id_cabang='" . $rCek['id_user'] . "'";
                    $this->db->query($updateSaldo);
                } elseif ($rCek['tipe'] == 3) {
                    $updateSaldo = "UPDATE lpt_perwakilan SET saldo=saldo-'" . $nom . "',date_updated=NOW() WHERE id_perwakilan='" . $rCek['id_user'] . "'";
                    $this->db->query($updateSaldo);
                } elseif ($rCek['tipe'] == 4) {
                    $updateSaldo = "UPDATE tb_produsen SET saldo_transaksi=saldo_transaksi-'" . $nom . "',date_updated=NOW() WHERE id_produsen='" . $rCek['id_user'] . "'";
                    $this->db->query($updateSaldo);
                } elseif ($rCek['tipe'] == 5) {
                    $updateSaldo = "UPDATE tb_depo SET saldo_transaksi=saldo_transaksi-'" . $nom . "',date_updated=NOW() WHERE id_depo='" . $rCek['id_user'] . "'";
                    $this->db->query($updateSaldo);
                } elseif ($rCek['tipe'] == 6) {
                    $updateSaldo = "UPDATE tb_customer SET saldo=saldo-'" . $nom . "',date_updated=NOW() WHERE id_customer='" . $rCek['id_user'] . "'";
                    $this->db->query($updateSaldo);
                } elseif ($rCek['tipe'] == 7) {
                    if($rCek['tipe_withdrawal']==1){
                        $updateSaldo = "UPDATE tb_agen SET saldo=saldo-'" . $nom . "',date_updated=NOW() WHERE id_agen='" . $rCek['id_user'] . "'";
                    }elseif($rCek['tipe_withdrawal']==2){
                        $updateSaldo = "UPDATE tb_agen SET total_komisi=total_komisi-'" . $nom . "',date_updated=NOW() WHERE id_agen='" . $rCek['id_user'] . "'";
                    }
                    
                    $this->db->query($updateSaldo);
                }
            }

            if ($rCek['tipe'] == 6) {

                $sMember = "SELECT * FROM tb_customer WHERE id_customer='" . $rCek['id_user'] . "'";
                $qMember = $this->db->query($sMember);
                $rMember = $qMember->row_array();

                if ($st == 3) {
                    $tl = 'Tarik Tunai Telah Diproses';
                    $tl_eng = 'Withdrawal Processed';
                    $tl_ar = 'السحب النقدي معالجتها';
                    $pesan = 'Permintaan tarik tunai telah kami proses, silahkan untuk dicek rekening Anda.<br/><br/>
                        Informasi Tarik Tunai : <br/><br/>
                        Nominal : Rp.' . number_format($rCek['nominal']) . '<br/>
                        Bank : ' . $rCek['bank'] . '<br/>
                        No Rekening : ' . $rCek['no_rekening'] . '<br/>
                        Atas Nama : ' . $rCek['atas_nama'];

                    $pesan_eng = 'We have processed the cash withdrawal request, please check your account.<br/><br/>
                        Withdrawal Informastion :<br/><br/>
                        Nominal : IDR.' . number_format($rCek['nominal']) . '<br/>
                        Bank : ' . $rCek['bank'] . '<br/>
                        Account Number : ' . $rCek['no_rekening'] . '<br/>
                        Account Name : ' . $rCek['atas_nama'];

                    $pesan_ar = 'لقد عالجنا طلب السحب النقدي ، يرجى التحقق من حسابك.<br/><br/>
                        معلومات السحب النقدي :<br/><br/>
                        اسمي : IDR.' . number_format($rCek['nominal']) . '<br/>
                        البنك : ' . $rCek['bank'] . '<br/>
                        رقم الحساب : ' . $rCek['no_rekening'] . '<br/>
                        نيابة عن : ' . $rCek['atas_nama'];

                    $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $rMember['nm_customer'] . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Permintaan tarik tunai telah kami proses, silahkan untuk dicek rekening Anda.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Tarik Tunai:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nominal</td><td>:</td><td>Rp.' . number_format($rCek['nominal']) . '</td></tr>
                                                                        <tr><td>Bank </td><td>:</td><td> ' . $rCek['bank'] . '</td></tr>
                                                                        <tr><td>No Rekening </td><td>:</td><td> ' . $rCek['no_rekening'] . '</td></tr>
                                                                        <tr><td>Atas Nama </td><td>:</td><td> <b>' . $rCek['atas_nama'] . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                    $addheader = "Tarik Tunai Selesai";
                    $subject = "Tarik Tunai Selesai";
                    pushEmailHerbal($rMember['email'], $rMember['nm_customer'], $message, $addheader, $subject);
                } elseif ($st == 4 || $st == 5) {
                    $tl = 'Tarik Tunai Gagal ';
                    $tl_eng = 'Withdrawal Failed';
                    $tl_ar = 'فشل سحب النقدية';
                    $pesan = 'Mohon maaf permintaan tarik tunai Anda tidak dapat kami proses.<br/><br/>
                        Informasi Tarik Tunai : <br/><br/>
                        Nominal : Rp.' . number_format($rCek['nominal']) . '<br/>
                        Bank : ' . $rCek['bank'] . '<br/>
                        No Rekening : ' . $rCek['no_rekening'] . '<br/>
                        Atas Nama : ' . rCek['atas_nama'];

                    $pesan_eng = 'Sorry, we cannot process your withdrawal.<br/><br/>
                        Withdrawal Informastion :<br/><br/>
                        Nominal : IDR.' . number_format($rCek['nominal']) . '<br/>
                        Bank : ' . $rCek['bank'] . '<br/>
                        Account Number : ' . $rCek['no_rekening'] . '<br/>
                        Account Name : ' . rCek['atas_nama'];

                    $pesan_ar = 'عذرًا ، لا يمكننا معالجة السحب النقدي<br/><br/>
                         السحب النقدي :<br/><br/>
                        اسمي : IDR.' . number_format($rCek['nominal']) . '<br/>
                        البنك : ' . $rCek['bank'] . '<br/>
                        رقم الحساب : ' . $rCek['no_rekening'] . '<br/>
                        نيابة عن : ' . rCek['atas_nama'];

                    $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                <tr>
                                                    <td align="center" valign="top" style="font-size:0; padding: 35px; font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;border-bottom:solid 3px #eeeeee;" bgcolor="#ffffff">
                                                        <h1 style="font-size: 36px; font-weight: 800; margin: 0; color: #ffffff;"><img src="https://thoyyiban.com/cms/assets/img/logoherbal.png" width="260px" height="100%"></h1>
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $rMember['nm_customer'] . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Mohon maaf permintaan tarik tunai Anda tidak dapat kami proses.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Tarik Tunai:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nominal</td><td>:</td><td>Rp.' . number_format($rCek['nominal']) . '</td></tr>
                                                                        <tr><td>Bank </td><td>:</td><td> ' . $rCek['bank'] . '</td></tr>
                                                                        <tr><td>No Rekening </td><td>:</td><td> ' . $rCek['no_rekening'] . '</td></tr>
                                                                        <tr><td>Atas Nama </td><td>:</td><td> <b>' . $rCek['atas_nama'] . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Keterangan : <br>
                                                                        ' . $ket . '
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Thoyyiban</p>
                                                                    <p style = "color:#777777;font-size: 13px;">
                                                                    Copyright © ' . date('Y') . ' Thoyyiban.<br>
                                                                    All rights reserved . </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style=" padding: 35px; background-color: #ffffff; border-bottom: 20px solid #F5F5F5;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.facebook.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/facebook.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://twitter.com/herbalthoyyiban" target="_blank"><img src="https://thoyyiban.com/upload/twitter.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://www.instagram.com/herbalthoyyiban/" target="_blank"><img src="https://thoyyiban.com/upload/instagram.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                            <td style="padding: 0 10px;">
                                                                                <a href="https://plus.google.com/u/0/102620397945300046595" target="_blank"><img src="https://thoyyiban.com/upload/google.png" width="35" height="29" style="display: block; border: 0px;" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                    $addheader = "Pembatalan Tarik Tunai";
                    $subject = "Pembatalan Tarik Tunai";
                    pushEmailHerbal($rMember['email'], $rMember['nm_customer'], $message, $addheader, $subject);
                }

                $s1 = "INSERT INTO tb_notif
                (id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,
                nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar)
                VALUES
                ('" . $rCek['id_user'] . "','" . $tl . "','" . $pesan . "',NOW(),NOW(),'1','1','','',
                '" . $tl_eng . "','" . $pesan_eng . "','" . $tl_ar . "','" . $pesan_ar . "')";
                $this->db->query($s1);
            }

            $sql = "SELECT * FROM tb_withdrawal WHERE id_withdrawal = '" . $id . "'";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);
        
        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_kategori WHERE id_kategori = '" . $id . "'";
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_kategori ORDER BY id_kategori DESC limit 1";
            $this->response->getresponse($sql,'success');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

}
