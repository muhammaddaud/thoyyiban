<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Iklan extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertiklan() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = $parameter['nm'];
        $de = $parameter['de'];
        $id = $parameter['id'];
        $st = $parameter['st'];
        $tp = $parameter['tp'];
        $ps = $parameter['ps'];
        $pg = $parameter['pg'];
        $lk = $parameter['lk'];
        $dp = $parameter['dp'];
        $dex = $parameter['dex'];

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_iklan WHERE id_iklan='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($nm != '' && $de != '') {
            if ($id == '') {
                $sInsert = "INSERT INTO tb_iklan (title_iklan,desc_iklan,posisi,page,date_created,date_updated,status,tipe,image,
                    date_published,date_expired,link)
                    VALUES
                    ('" . $nm . "','" . $de . "','" . $ps . "','" . $pg . "',NOW(),NOW(),'" . $st . "','" . $tp . "','" . $foto . "',"
                        . "'" . $dp . "','" . $dex . "','" . $lk . "')";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_iklan ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_iklan SET title_iklan = '" . $nm . "',desc_iklan = '" . $de . "',posisi = '" . $ps . "',tipe = '" . $tp . "',date_expired = '" . $dex . "',image = '" . $foto . "',
                        page = '" . $pg . "',status = '" . $st . "',date_published = '" . $dp . "',link = '" . $lk . "',date_updated=NOW() WHERE id_iklan = '" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_iklan WHERE id_iklan = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertiklan');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getiklan() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_iklan WHERE status='1' AND tipe='1' ORDER BY date_created DESC" . $limit;

        if ($tp != '') {
            $sql = "SELECT * FROM tb_iklan WHERE status='1' AND tipe='" . $tp . "' ORDER BY date_created DESC" . $limit;
        }
        $this->response->getresponse($sql,'getiklan');
    }

    public function getbanner() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $ps = $parameter['ps'];
        $pg = $parameter['pg'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $page = "";
        if ($pg != '') {
            $page = " AND page LIKE '%" . $pg . "%' ";
        }

        $posisi = "";
        if ($ps != '') {
            $posisi = " AND posisi='" . $ps . "' ";
        }

        $sql = "SELECT * FROM tb_iklan WHERE status='1' AND tipe='1' " . $page . $posisi . " ORDER BY date_created DESC" . $limit;
        
        $this->response->getresponse($sql,'getbanner');
    }

    public function getiklancms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_iklan ORDER BY date_created DESC";

        if ($tp != '') {
            $sql = "SELECT * FROM tb_iklan WHERE tipe='" . $tp . "' ORDER BY date_created DESC";
        }
        $this->response->getresponse($sql,'getiklancms');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_iklan WHERE id_iklan='" . $id . "' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_iklan SET status='" . $st . "' WHERE id_iklan='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_iklan WHERE id_iklan='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_iklan WHERE id_iklan='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_iklan ORDER BY id_iklan DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
