<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdashboard() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tgl = date('Y-m-d');
        $bl = $parameter['bl'];
        $th = $parameter['th'];

        if($bl==''){
            $bl = date('m');
        }

        if($th==''){
            $th = date('Y');
        }

        $sql = "SELECT (select count(id_info) FROM tb_info WHERE tipe=5) as jm_video,
                (select count(id_info) FROM tb_info WHERE tipe=5 AND status=1) as jm_video_aktif,
                (select count(id_info) FROM tb_info WHERE tipe=5 AND status=0) as jm_video_noaktif,
                (select count(id_customer) FROM tb_customer) as jm_member,
                (select count(id_customer) FROM tb_customer WHERE is_from='Android') as jm_member_andorid,
                (select count(id_customer) FROM tb_customer WHERE is_from='iOS') as jm_member_ios,
                (select count(id_customer) FROM tb_customer WHERE month(date_created)='" . $bl . "' AND year(date_created)='" . $th . "' ) as jm_member_bulan,
                (select count(id_customer) FROM tb_customer WHERE month(date_created)='" . $bl . "' AND year(date_created)='" . $th . "' AND YEARWEEK(date_created) = YEARWEEK(NOW()) ) as jm_member_minggu,
                (select count(id_customer) FROM tb_customer WHERE date(date_created)='" . $tgl . "' ) as jm_member_hari,
                (select count(id) FROM tb_log_install) as jm_install,
                (select count(id) FROM tb_log_install WHERE is_from='Android') as jm_install_andorid,
                (select count(id) FROM tb_log_install WHERE is_from='iOS') as jm_install_ios,
                (select count(id) FROM tb_log_install WHERE month(date_created)='" . $bl . "' AND year(date_created)='" . $th . "' ) as jm_install_bulan,
                (select count(id) FROM tb_log_install WHERE month(date_created)='" . $bl . "' AND year(date_created)='" . $th . "' AND YEARWEEK(date_created) = YEARWEEK(NOW()) ) as jm_install_minggu,
                (select count(id) FROM tb_log_install WHERE date(date_created)='" . $tgl . "' ) as jm_install_hari";
        
        $result = array();

        $code = "201";
        $status = 'Data Tidak ditemukan...';

        $sql = str_replace("\n", " ", $sql);
        $sql = str_replace("\t", " ", $sql);

        $query = $this->db->query($sql);
        $check = false;

        if ($err == '') {
            $a = 0;
            foreach ($query->result_array() as $row) {
                $result[$a] = $row;
                $url = $this->general_lib->url_thoyyiban() . 'api/dashboard_api.php?action=get_summary'.$this->general_lib->key_thoyyiban();
                $fields = array(
                    'id' => $row['id_produk']
                );
                $resp = $this->general_lib->general_http($url, $fields);
                $decode = json_decode($resp, true);
                $jmProdukNoaktif=$decode['result'][0]['jm_produk_all']-$decode['result'][0]['jm_produk_aktif'];
                $result[$a]['jm_ukm'] = $decode['result'][0]['jm_depo'];
                $result[$a]['jm_produk'] = $decode['result'][0]['jm_produk_all'];
                $result[$a]['jm_produk_aktif'] = $decode['result'][0]['jm_produk_aktif'];
                $result[$a]['jm_produk_noaktif'] = strval($jmProdukNoaktif);

                $code = "200";
                $status = "Succes action getdashboard";
                $check = true;
                $a++;
            }
        }

        $str = array(
            "result" => $result,
            "code" => $code,
            "message" => $status
        );
        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function grafikmemberv2() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $bl = $parameter['bl'];
        $th = $parameter['th'];

        if($bl==''){
            $bl = date('m');
        }

        if($th==''){
            $th = date('Y');
        }


        $sql = "SELECT COUNT(id_customer) as jm_member,DATE(date_created) AS tanggal FROM tb_customer WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' group by DATE(date_created) ORDER BY DATE(date_created) ASC";
        $this->response->getresponse($sql,'grafikmember');
    }

    public function grafikmember() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $bl = $parameter['bl'];
        $th = $parameter['th'];

        if($bl==''){
            $bl = date('m');
        }

        if($th==''){
            $th = date('Y');
        }


        $sqlAndroid = $this->db->query("SELECT COUNT(id_customer) as jm_member,DATE(date_created) AS tanggal FROM tb_customer WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' AND is_from='Android' group by DATE(date_created) ORDER BY DATE(date_created) ASC");
        $arrayAndroid = array();
        $a = 0;
        foreach ($sqlAndroid->result_array() as $rowAndorid) {
            $arrayAndroid[$a] = $rowAndorid;
            $a++;
        }

        $sqlIos = $this->db->query("SELECT COUNT(id_customer) as jm_member,DATE(date_created) AS tanggal FROM tb_customer WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' AND is_from='iOS' group by DATE(date_created) ORDER BY DATE(date_created) ASC");
        $arrayIos = array();
        $b = 0;
        foreach ($sqlIos->result_array() as $rowIos) {
            $arrayIos[$b] = $rowIos;
            $b++;
        }

        $sqlAll = $this->db->query("SELECT COUNT(id_customer) as jm_member,DATE(date_created) AS tanggal FROM tb_customer WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' group by DATE(date_created) ORDER BY DATE(date_created) ASC");
        $arrayAll = array();
        $c = 0;
        foreach ($sqlAll->result_array() as $rowAll) {
            $arrayAll[$c] = $rowAll;
            $c++;
        }

        $result['all'] = $arrayAll;
        $result['android'] = $arrayAndroid;
        $result['ios'] = $arrayIos;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action grafikmember'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function grafikmemberbulan() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $th = $parameter['th'];

        if($th==''){
            $th = date('Y');
        }


        $sqlAndroid = $this->db->query("SELECT COUNT(id_customer) as jm_member,MONTH(date_created) AS bulan FROM tb_customer WHERE YEAR(date_created)='".$th."' AND is_from='Android' group by MONTH(date_created) ORDER BY MONTH(date_created) ASC");
        $arrayAndroid = array();
        $a = 0;
        foreach ($sqlAndroid->result_array() as $rowAndorid) {
            $arrayAndroid[$a] = $rowAndorid;
            $a++;
        }

        $sqlIos = $this->db->query("SELECT COUNT(id_customer) as jm_member,MONTH(date_created) AS bulan FROM tb_customer WHERE YEAR(date_created)='".$th."' AND is_from='iOS' group by MONTH(date_created) ORDER BY MONTH(date_created) ASC");
        $arrayIos = array();
        $b = 0;
        foreach ($sqlIos->result_array() as $rowIos) {
            $arrayIos[$b] = $rowIos;
            $b++;
        }

        $sqlAll = $this->db->query("SELECT COUNT(id_customer) as jm_member,MONTH(date_created) AS bulan FROM tb_customer WHERE YEAR(date_created)='".$th."' group by MONTH(date_created) ORDER BY MONTH(date_created) ASC");
        $arrayAll = array();
        $c = 0;
        foreach ($sqlAll->result_array() as $rowAll) {
            $arrayAll[$c] = $rowAll;
            $c++;
        }

        $result['all'] = $arrayAll;
        $result['android'] = $arrayAndroid;
        $result['ios'] = $arrayIos;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action grafikmember'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function grafikinstallv2() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $bl = $parameter['bl'];
        $th = $parameter['th'];

        if($bl==''){
            $bl = date('m');
        }

        if($th==''){
            $th = date('Y');
        }


        $sql = "SELECT COUNT(id) as jm_install,DATE(date_created) AS tanggal FROM tb_log_install WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' group by DATE(date_created) ORDER BY DATE(date_created) ASC";
        $this->response->getresponse($sql,'grafikinstall');
    }

    public function grafikinstall() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $bl = $parameter['bl'];
        $th = $parameter['th'];

        if($bl==''){
            $bl = date('m');
        }

        if($th==''){
            $th = date('Y');
        }


        $sqlAndroid = $this->db->query("SELECT COUNT(id) as jm_install,DATE(date_created) AS tanggal FROM tb_log_install WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' AND is_from='Android' group by DATE(date_created) ORDER BY DATE(date_created) ASC");
        $arrayAndroid = array();
        $a = 0;
        foreach ($sqlAndroid->result_array() as $rowAndorid) {
            $arrayAndroid[$a] = $rowAndorid;
            $a++;
        }

        $sqlIos = $this->db->query("SELECT COUNT(id) as jm_install,DATE(date_created) AS tanggal FROM tb_log_install WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' AND is_from='iOS' group by DATE(date_created) ORDER BY DATE(date_created) ASC");
        $arrayIos = array();
        $b = 0;
        foreach ($sqlIos->result_array() as $rowIos) {
            $arrayIos[$b] = $rowIos;
            $b++;
        }

        $sqlAll = $this->db->query("SELECT COUNT(id) as jm_install,DATE(date_created) AS tanggal FROM tb_log_install WHERE YEAR(date_created)='".$th."' AND MONTH(date_created)='".$bl."' group by DATE(date_created) ORDER BY DATE(date_created) ASC");
        $arrayAll = array();
        $c = 0;
        foreach ($sqlAll->result_array() as $rowAll) {
            $arrayAll[$c] = $rowAll;
            $c++;
        }

        $result['all'] = $arrayAll;
        $result['android'] = $arrayAndroid;
        $result['ios'] = $arrayIos;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action grafikinstall'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function grafikinstallbulan() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $th = $parameter['th'];

        if($th==''){
            $th = date('Y');
        }


        $sqlAndroid = $this->db->query("SELECT COUNT(id) as jm_install,MONTH(date_created) AS bulan FROM tb_log_install WHERE YEAR(date_created)='".$th."' AND is_from='Android' group by MONTH(date_created) ORDER BY MONTH(date_created) ASC");
        $arrayAndroid = array();
        $a = 0;
        foreach ($sqlAndroid->result_array() as $rowAndorid) {
            $arrayAndroid[$a] = $rowAndorid;
            $a++;
        }

        $sqlIos = $this->db->query("SELECT COUNT(id) as jm_install,MONTH(date_created) AS bulan FROM tb_log_install WHERE YEAR(date_created)='".$th."' AND is_from='iOS' group by MONTH(date_created) ORDER BY MONTH(date_created) ASC");
        $arrayIos = array();
        $b = 0;
        foreach ($sqlIos->result_array() as $rowIos) {
            $arrayIos[$b] = $rowIos;
            $b++;
        }

        $sqlAll = $this->db->query("SELECT COUNT(id) as jm_install,MONTH(date_created) AS bulan FROM tb_log_install WHERE YEAR(date_created)='".$th."' group by MONTH(date_created) ORDER BY MONTH(date_created) ASC");
        $arrayAll = array();
        $c = 0;
        foreach ($sqlAll->result_array() as $rowAll) {
            $arrayAll[$c] = $rowAll;
            $c++;
        }

        $result['all'] = $arrayAll;
        $result['android'] = $arrayAndroid;
        $result['ios'] = $arrayIos;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action grafikinstall'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function getmapinstall() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);


        $sql = "SELECT a.*,
        IFNULL((SELECT x.id_customer FROM tb_customer x WHERE x.uuid=a.uuid),'') as id_customer,
        IFNULL((SELECT x.kd_customer FROM tb_customer x WHERE x.uuid=a.uuid),'') as kd_customer,
        IFNULL((SELECT x.nm_customer FROM tb_customer x WHERE x.uuid=a.uuid),'') as nm_customer,
        IFNULL((SELECT x.email FROM tb_customer x WHERE x.uuid=a.uuid),'') as email,
        IFNULL((SELECT x.no_hp FROM tb_customer x WHERE x.uuid=a.uuid),'') as no_hp,
        IFNULL((SELECT x.image FROM tb_customer x WHERE x.uuid=a.uuid),'') as image,
        IFNULL((SELECT z.nm_provinsi FROM tb_customer x left join tb_provinsi z on x.id_provinsi=z.id_provinsi WHERE x.uuid=a.uuid),'') as nm_provinsi,
        IFNULL((SELECT z.nm_kota FROM tb_customer x left join tb_kota z on x.id_kota=z.id_kota WHERE x.uuid=a.uuid),'') as nm_kota,
        IFNULL((SELECT z.nm_kecamatan FROM tb_customer x left join tb_kecamatan z on x.id_kecamatan=z.id_kecamatan WHERE x.uuid=a.uuid),'') as nm_kecamatan,
        IFNULL((SELECT z.nm_kelurahan FROM tb_customer x left join tb_kelurahan z on x.id_kelurahan=z.id_kelurahan WHERE x.uuid=a.uuid),'') as nm_kelurahan,
        IFNULL((SELECT x.alamat FROM tb_customer x WHERE x.uuid=a.uuid),'') as alamat,
        IFNULL((SELECT x.kode_pos FROM tb_customer x WHERE x.uuid=a.uuid),'') as kode_pos
        FROM tb_log_install a WHERE a.latitude!=''";
        $this->response->getresponse($sql,'getmapinstall');
    }

    public function getmapmember() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);


        $sql = "SELECT a.id_customer,a.kd_customer,a.email,a.nm_customer,a.id_provinsi,a.id_kota,a.id_kecamatan,a.id_kelurahan,a.no_hp,a.kode_pos,a.alamat,a.image,a.latitude,a.date_created,a.date_updated,a.tipe,a.status,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
        FROM tb_customer a WHERE a.latitude!=''";
        $this->response->getresponse($sql,'getmapmember');
    }

}
