<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ukm extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function getukmpra() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $ci = $parameter['ci'];
        $ipd = $parameter['ipd'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $produsen = "";
        if ($ipd != '') {
            $produsen = " AND a.id_produsen='" . $ipd . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
        IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
        IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen
        FROM tb_ukm_pra a WHERE id_ukm_pra!=''
        " . $tipe . $prov . $kota . $kec . $kel . $produsen . $country . "
        ORDER BY a.id_ukm_pra DESC " . $limit;

        $this->response->getresponse($sql,'getukmpra');
    }

    public function getukmcms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $ipr = $parameter['ipr'];
        $ipd = $parameter['ipd'];
        $cb = $parameter['cb'];
        $ci = $parameter['ci'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $cab = "";
        if ($cb != '' && $cb != '0') {
            $cab = " AND a.cabang_lpt='" . $cb . "' ";
        }
        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $pra = "";
        if ($ipr != '') {
            $pra = " AND a.is_pra='" . $ipr . "' ";
        }

        $produsen = "";
        if ($ipd != '') {
            $produsen = " AND a.id_produsen='" . $ipd . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha
        FROM tb_ukm a WHERE id_ukm!=''
        " . $tipe . $prov . $kota . $kec . $kel . $pra . $cab . $produsen . $country . "
        ORDER BY a.id_ukm DESC " . $limit;

        $this->response->getresponse($sql,'getukmcms');
    }

    public function getukmcmsnext() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $id = $parameter['id'];
        $ipr = $parameter['ipr'];
        $ipd = $parameter['ipd'];
        $cb = $parameter['cb'];
        $ci = $parameter['ci'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $cab = "";
        if ($cb != '' && $cb != '0') {
            $cab = " AND a.cabang_lpt='" . $cb . "' ";
        }
        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $pra = "";
        if ($ipr != '') {
            $pra = " AND a.is_pra='" . $ipr . "' ";
        }

        $produsen = "";
        if ($ipd != '') {
            $produsen = " AND a.id_produsen='" . $ipd . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha
        FROM tb_ukm a WHERE id_ukm<'" . $id . "'
        " . $tipe . $prov . $kota . $kec . $kel . $pra . $cab . $produsen . $country . "
        ORDER BY a.id_ukm DESC " . $limit;

        $this->response->getresponse($sql,'getukmcmsnext');
    }

    public function getukmcmsprev() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $id = $parameter['id'];
        $ipr = $parameter['ipr'];
        $ipd = $parameter['ipd'];
        $cb = $parameter['cb'];
        $ci = $parameter['ci'];

        $cab = "";
        if ($cb != '' && $cb != '0') {
            $cab = " AND a.cabang_lpt='" . $cb . "' ";
        }
        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $pra = "";
        if ($ipr != '') {
            $pra = " AND a.is_pra='" . $ipr . "' ";
        }

        $produsen = "";
        if ($ipd != '') {
            $produsen = " AND a.id_produsen='" . $ipd . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha
        FROM tb_ukm a WHERE id_ukm>'" . $id . "'
        " . $tipe . $prov . $kota . $kec . $kel . $pra . $cab . $produsen . $country . "
        ORDER BY a.id_ukm DESC " . $limit;

        $this->response->getresponse($sql,'getukmcmsprev');
    }

    public function getukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $hp = $parameter['hp'];
        $cb = $parameter['cb'];
        $iu = $parameter['iu'];
        $ci = $parameter['ci'];

        $cab = "";
        if ($cb != '' && $cb != '0') {
            $cab = " AND a.cabang_lpt='" . $cb . "' ";
        }
        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
        FROM tb_ukm a
        WHERE a.status=1
        " . $tipe . $prov . $kota . $kec . $kel . $cab . $country . "
        ORDER BY a.id_ukm DESC " . $limit;

        if ($hp != '') {
            $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
        FROM tb_ukm a
        WHERE a.no_hp='" . $hp . "' ";
        }
        if ($iu != '') {
            $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
        FROM tb_ukm a
        WHERE a.id_ukm='" . $iu . "' ";
        }

        $this->response->getresponse($sql,'getukm');
    }

    public function getukmzakat() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $tp = $parameter['tp'];
        $ci = $parameter['ci'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        $country = "";
        if ($ci != '') {
            $country = " AND a.country_id='" . $ci . "' ";
        }

        $sql = "SELECT a.*,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
        IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country
        FROM tb_ukm a
        WHERE a.status=1 AND is_zakat=1
        " . $tipe . $prov . $kota . $kec . $kel . $cab . $country . "
        ORDER BY a.id_ukm DESC " . $limit;

        $this->response->getresponse($sql,'getukmzakat');
    }

    public function getukmdetail() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $iu = $parameter['iu'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $ukm = "";
        if ($iu != '') {
            $ukm = " AND a.id_ukm='" . $iu . "' ";
        }


        $sql = "SELECT a.*,
        (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
        (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
        (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
        (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
        FROM tb_ukm_detail a
        WHERE a.status=1
        " . $ukm . $prov . $kota . $kec . $kel . "
        ORDER BY a.id_ukm_detail DESC " . $limit;

        $this->response->getresponse($sql,'getukmdetail');
    }

    public function getukmdetailcms() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $lt = $parameter['lt'];
        $iu = $parameter['iu'];

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }

        $prov = "";
        if ($ip != '') {
            $prov = " AND a.provinsi='" . $ip . "' ";
        }

        $kota = "";
        if ($kt != '') {
            $kota = " AND a.kota='" . $kt . "' ";
        }

        $kec = "";
        if ($kc != '') {
            $kec = " AND a.kecamatan='" . $kc . "' ";
        }

        $kel = "";
        if ($kl != '') {
            $kel = " AND a.kelurahan='" . $kl . "' ";
        }

        $ukm = "";
        if ($iu != '') {
            $ukm = " AND a.id_ukm='" . $iu . "' ";
        }

        $sql = "SELECT a.*,
        (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
        (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
        (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
        (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
        FROM tb_ukm_detail a
        WHERE a.id_ukm!=''
        " . $ukm . $prov . $kota . $kec . $kel . "
        ORDER BY a.id_ukm_detail DESC " . $limit;

        $this->response->getresponse($sql,'getukmdetailcms');
    }

    public function getbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
            IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
            (SELECT COUNT(x.id_produk) FROM tb_produk x WHERE x.id_ukm=a.id_ukm AND x.status=1 AND x.harga_jual > 0 AND x.is_publish=1 AND x.image!='') as total_produk
            FROM tb_ukm a
            WHERE a.id_ukm='" . $id . "'";

            $this->response->getresponse($sql,'getbyid');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getprabyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi_usaha),'') as nm_provinsi_usaha,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota_usaha),'') as nm_kota_usaha,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan_usaha),'') as nm_kecamatan_usaha,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan_usaha),'') as nm_kelurahan_usaha,
            IFNULL((SELECT x.nama FROM tb_country x WHERE x.country_id=a.country_id),'') as nm_country,
            IFNULL((SELECT x.nm_produsen FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as nm_produsen,
            IFNULL((SELECT x.email FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as email_produsen,
            IFNULL((SELECT x.no_hp FROM tb_produsen x WHERE x.id_produsen=a.id_produsen),'') as no_hp_produsen
            FROM tb_ukm_pra a
            WHERE a.id_ukm_pra='" . $id . "'";

            $this->response->getresponse($sql,'getprabyid');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdetailbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        if ($id != '') {

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
            FROM tb_ukm_detail a
            WHERE a.id_ukm_detail='" . $id . "'";

            $this->response->getresponse($sql,'getdetailbyid');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $kl = $parameter['kl'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $ktp = addslashes($parameter['ktp']);
        $npwp = addslashes($parameter['npwp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $desc_eng = addslashes($parameter['desc_eng']);
        $desc_ar = addslashes($parameter['desc_ar']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $kt = $parameter['kt'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ia = $parameter['ia'];
        $ci = $parameter['ci'];
        $lt = $parameter['lt'];
        $ipd = $parameter['ipd'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        if ($tp == '') {
            $tp = 1;
        }

        $sAnggota = "SELECT * FROM tb_anggota WHERE no_hp='" . $ph . "'";
        $qAnggota = $this->db->query($sAnggota);
        $rAnggota = $qAnggota->row_array();

        if ($rAnggota['id_anggota'] == '') {
            $code = '03';
            $status = 'No Handphone belum terdaftar sebagai anggota sibaweh lab...';
            $this->general_lib->error($code,$status);
        }

        $password = $rAnggota['real_password'];
        $pass = $rAnggota['password'];

        $real_pin = $rAnggota['real_pin'];
        $pin = $rAnggota['pin'];

        if ($nmp == '') {
            $nmp = $rAnggota['nm_anggota'];
        }

        if ($kl == '') {
            $rAnggota['id_kelurahan'];
        }

        if ($almt == '') {
            $almt = $rAnggota['alamat'];
        }

        if ($jk == '') {
            $jk = $rAnggota['jenis_kelamin'];
        }

        if ($tl == '') {
            $tl = $rAnggota['tempat_lahir'];
        }

        if ($tgl == '') {
            $tgl = $rAnggota['tgl_lahir'];
        }

        if ($em == '') {
            $em = $rAnggota['email'];
        }

        if ($ktp == '') {
            $ktp = $rAnggota['no_ktp'];
        }

        if ($npwp == '') {
            $npwp = $rAnggota['npwp'];
        }

        if ($ia == '') {
            $ia = $rAnggota['id_anggota'];
        }
        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
        $rKel = $sqlKel->row_array();

        $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
        $rKec = $sqlKec->row_array();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
        $rKota = $sqlKota->row_array();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
        $rProv = $sqlProv->row_array();

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $id_account = "42" . $bulan . $this->general_lib->random_numbers(8);

        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlCheck2 = $this->db->query("SELECT * FROM tb_ukm WHERE no_hp = '" . $ph . "'");
            $rCheck2 = $sqlProv->row_array();

            if ($rCheck2['id_ukm'] != '') {

                if ($nmp == '') {
                    $nmp = $rCheck2['nm_pemilik'];
                }

                if ($kl == '') {
                    $kl = $rCheck2['kelurahan'];
                }

                if ($almt == '') {
                    $almt = $rCheck2['alm_ukm'];
                }

                if ($jk == '') {
                    $jk = $rCheck2['jenis_kelamin'];
                }

                if ($tl == '') {
                    $tl = $rCheck2['tempat_lahir'];
                }

                if ($tgl == '') {
                    $tgl = $rCheck2['tgl_lahir'];
                }

                if ($em == '') {
                    $em = $rCheck2['email'];
                }

                if ($ktp == '') {
                    $ktp = $rCheck2['no_ktp'];
                }

                if ($npwp == '') {
                    $npwp = $rCheck2['no_npwp'];
                }

                if ($nmu == '') {
                    $nmu = $rCheck2['nm_ukm'];
                }

                if ($desc == '') {
                    $desc = $rCheck2['desc_ukm'];
                }

                if ($desc_eng == '') {
                    $desc_eng = $rCheck2['desc_ukm_eng'];
                }

                if ($desc_ar == '') {
                    $desc_ar = $rCheck2['desc_ukm_ar'];
                }

                if ($au == '') {
                    $au = $rCheck2['alm_usaha'];
                }

                if ($foto == '') {
                    $foto = $rCheck2['foto'];
                }

                if ($fotoktp == '') {
                    $fotoktp = $rCheck2['image_ktp'];
                }

                if ($fotousaha == '') {
                    $fotousaha = $rCheck2['logo'];
                }

                if ($bk == '') {
                    $bk = $rCheck2['bank'];
                }

                if ($nr == '') {
                    $nr = $rCheck2['no_rekening'];
                }

                if ($an == '') {
                    $an = $rCheck2['atas_nama'];
                }

                if ($pos == '') {
                    $pos = $rCheck2['kode_pos'];
                }

                if ($ipd == '') {
                    $ipd = $rCheck2['id_produsen'];
                }

                if ($ci == '') {
                    $ci = $rCheck2['country_id'];
                }

                if ($lt == '') {
                    $lt = $rCheck2['latitude'];
                }

                $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
                $rKel = $sqlKel->row_array();

                $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
                $rKec = $sqlKec->row_array();

                $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
                $rKota = $sqlKota->row_array();

                $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
                $rProv = $sqlProv->row_array();

                $sqlUpdate = "UPDATE tb_ukm SET nm_ukm='" . $nmu . "', provinsi='" . $rProv['id_provinsi'] . "', kota='" . $rKota['id_kota'] . "',alm_ukm='" . $almt . "',no_ktp='" . $ktp . "',nm_pemilik='" . $nmp . "',desc_ukm='" . $desc . "',alm_usaha='" . $au . "',country_id='" . $ci . "',email='" . $em . "',foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "',id_produsen='" . $ipd . "',kecamatan='" . $rKec['id_kecamatan'] . "',kelurahan='" . $kl . "',bank='" . $bk . "',desc_ukm_eng='" . $desc_eng . "',desc_ukm_ar='" . $desc_ar . "',jenis_kelamin='" . $jk . "',tgl_lahir='" . $tgl . "',no_rekening='" . $nr . "',no_npwp='" . $npwp . "',atas_nama='" . $an . "',tempat_lahir='" . $tl . "',kode_pos='" . $pos . "' WHERE no_hp='" . $ph . "' ";
                $this->db->query($sqlUpdate);
            } else {
                if ($ipd == '') {
                    $ipd = 1;
                }

                $sqlInsert = "INSERT INTO tb_ukm (kd_ukm,nm_ukm,provinsi,kota,alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,no_hp,email,foto,image_ktp,date_created,date_updated,tipe,status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,desc_ukm_eng,desc_ukm_ar,id_anggota,id_rt,id_produsen,country_id)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $rProv['id_provinsi'] . "', '" . $rKota['id_kota'] . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '1','" . $lt . "','" . $rKec['id_kecamatan'] . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $desc_eng . "','" . $desc_ar . "','" . $ia . "','937','" . $ipd . "','" . $ci . "') ";
                $this->db->query($sqlInsert);

                $sUkm = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
                $qUkm = $this->db->query($sUkm);
                $rUkm = $qUkm->row_array();

                $update = "UPDATE tb_ukm SET id_agen='" . $rUkm['id_ukm'] . "' WHERE no_hp='" . $ph . "'";
                $this->db->query($update);
            }

            $update = "UPDATE tb_anggota SET is_ukm=1,date_updated=NOW() WHERE no_hp='" . $ph . "'";
            $this->db->query($update);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'insertukm');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function register() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $kl = $parameter['kl'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $ktp = addslashes($parameter['ktp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $kt = $parameter['kt'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ipd = $parameter['ipd'];
        $ci = $parameter['ci'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = addslashes($parameter['npwp']);
        $siup = addslashes($parameter['siup']);
        $tdp = addslashes($parameter['tdp']);
        $ud = addslashes($parameter['ud']);

        if ($tp == '') {
            $tp = 1;
        }

        if ($kt == '') {
            $kt = $parameter['ik'];
        }

        $sCekEmail = "SELECT * FROM tb_ukm WHERE email='" . $em . "'";
        $qCekEmail = $this->db->query($sCekEmail);
        $rCekEmail = $qCekEmail->row_array();

        $sCekHp = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
        $qCekHp = $this->db->query($sCekHp);
        $rCekHp = $qCekHp->row_array();

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($rCekEmail['id_ukm'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }


        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
        $rKel = $sqlKel->row_array();

        $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
        $rKec = $sqlKec->row_array();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
        $rKota = $sqlKota->row_array();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
        $rProv = $sqlProv->row_array();

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $id_account = "42" . $bulan . $this->general_lib->random_numbers(8);

        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlInsert = "INSERT INTO tb_ukm (kd_ukm,nm_ukm,provinsi,kota,alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,
            no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,
            no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_anggota,id_rt,is_pra,id_produsen,country_id)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $rProv['id_provinsi'] . "', '" . $rKota['id_kota'] . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '1','" . $lt . "','" . $rKec['id_kecamatan'] . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $id_anggota . "','937','1','" . $ipd . "','" . $ci . "') ";
            $this->db->query($sqlInsert);

            $sUkm = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
            $qUkm = $this->db->query($sUkm);
            $rUkm = $qUkm->row_array();

            $updateUkm = "UPDATE tb_ukm SET id_agen='" . $rUkm['id_ukm'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateUkm);

            $update = "UPDATE tb_anggota SET is_ukm=1,date_updated=NOW() WHERE no_hp='" . $ph . "'";
            $this->db->query($update);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'register');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function registersibaweh() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = $parameter['nmu'];
        $nmp = $parameter['nmp'];
        $kl = $parameter['kl'];
        $almt = $parameter['alm'];
        $au = $parameter['au'];
        $pos = $parameter['pos'];
        $ktp = $parameter['ktp'];
        $ph = $parameter['ph'];
        $em = $parameter['em'];
        $desc = $parameter['desc'];
        $jk = $parameter['jk'];
        $tl = $parameter['tl'];
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $kt = $parameter['kt'];
        $bk = $parameter['bk'];
        $nr = $parameter['nr'];
        $an = $parameter['an'];
        $ipd = $parameter['ipd'];
        $ci = $parameter['ci'];
        $lt = $parameter['lt'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = $parameter['npwp'];
        $siup = $parameter['siup'];
        $tdp = $parameter['tdp'];
        $ud = $parameter['ud'];

        $password = $parameter['pass'];
        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }

        $pass = md5(hash('sha512', $password));

        $real_pin = $parameter['pin'];
        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        if ($tp == '') {
            $tp = 1;
        }

        if ($ipd == '') {
            $ipd = 1;
        }

        if ($kt == '') {
            $kt = $parameter['ik'];
        }

        $sCekEmail = "SELECT * FROM tb_ukm WHERE email='" . $em . "'";
        $qCekEmail = $this->db->query($sCekEmail);
        $rCekEmail = $qCekEmail->row_array();

        $sCekHp = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
        $qCekHp = $this->db->query($sCekHp);
        $rCekHp = $qCekHp->row_array();

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL) && $em != '') {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($rCekEmail['id_ukm'] != '' && $em != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        $sCekEmailDepo = "SELECT * FROM tb_depo WHERE email='" . $em . "'";
        $qCekEmailDepo = $this->db->query($sCekEmailDepo);
        $rCekEmailDepo = $qCekEmailDepo->row_array();

        $sCekHpDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
        $qCekHpDepo = $this->db->query($sCekHpDepo);
        $rCekHpDepo = $qCekHpDepo->row_array();

        if ($rCekEmailDepo['id_depo'] != '' && $em != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHpDepo['id_depo'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }


        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
        $rKel = $sqlKel->row_array();

        $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
        $rKec = $sqlKec->row_array();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
        $rKota = $sqlKota->row_array();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
        $rProv = $sqlProv->row_array();

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $kd_depo = "TYD" . $this->general_lib->random_numbers(6);

        if ($ph != '' && $nmu != '') {
            $err = '';
            $code = '200';

            $sqlInsert = "INSERT INTO tb_ukm (kd_ukm,nm_ukm,provinsi,kota,alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,
            no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,
            no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_anggota,id_rt,is_pra,id_produsen,country_id)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $rProv['id_provinsi'] . "', '" . $rKota['id_kota'] . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '1','" . $lt . "','" . $rKec['id_kecamatan'] . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','','937','1','" . $ipd . "','ID') ";
            $this->db->query($sqlInsert);

            $sUkm = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
            $qUkm = $this->db->query($sUkm);
            $rUkm = $qUkm->row_array();

            $updateUkm = "UPDATE tb_ukm SET id_agen='" . $rUkm['id_ukm'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateUkm);

            // Insert Depo
            $sqlInsertDepo = "INSERT INTO tb_depo (kd_depo,nm_depo,provinsi,kota,alm_depo,no_ktp,nm_pemilik,desc_depo,alm_usaha,no_hp,email,foto,image_ktp,date_created,date_updated,tipe,status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_produsen,id_rt,id_ukm,image_kk,image_keterangan_usaha,jenis_usaha,country_id)
            VALUES ('" . $kd_depo . "', '" . $nmu . "', '" . $rProv['id_provinsi'] . "', '" . $rKota['id_kota'] . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '0','" . $lt . "','" . $rKec['id_kecamatan'] . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $ipd . "','937','" . $rUkm['id_ukm'] . "','','','','ID') ";
            $this->db->query($sqlInsertDepo);

            $sDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
            $qDepo = $this->db->query($sDepo);
            $rDepo = $qDepo->row_array();

            $updateDepo = "UPDATE tb_depo SET id_agen='" . $rDepo['id_depo'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateDepo);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'registersibaweh');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function registerpra() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $nmpm = addslashes($parameter['nmpm']);
        $kl = $parameter['kl'];
        $klu = $parameter['klu'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $posu = addslashes($parameter['posu']);
        $ktp = addslashes($parameter['ktp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $pv = $parameter['pv'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ju = addslashes($parameter['ju']);
        $bu = addslashes($parameter['bu']);
        $np = addslashes($parameter['np']);
        $als = addslashes($parameter['als']);
        $ipd = $parameter['ipd'];
        $ci = $parameter['ci'];
        $lt = $parameter['lt'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = addslashes($parameter['npwp']);
        $siup = addslashes($parameter['siup']);
        $tdp = addslashes($parameter['tdp']);
        $ud = addslashes($parameter['ud']);
        $cab = $parameter['cab'];

        $password = $parameter['pass'];
        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }

        $pass = md5(hash('sha512', $password));

        $real_pin = $parameter['pin'];
        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        if ($tp == '') {
            $tp = 1;
        }

        $sCekEmail = "SELECT * FROM tb_ukm_pra WHERE email='" . $em . "'";
        $qCekEmail = $this->db->query($sCekEmail);
        $rCekEmail = $qCekEmail->row_array();

        $sCekHp = "SELECT * FROM tb_ukm_pra WHERE no_hp='" . $ph . "'";
        $qCekHp = $this->db->query($sCekHp);
        $rCekHp = $qCekHp->row_array();

        if (strlen($ph) < 10 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($rCekEmail['id_ukm'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        $sCekEmailUkm = "SELECT * FROM tb_ukm WHERE email='" . $em . "'";
        $qCekEmailUkm = $this->db->query($sCekEmailUkm);
        $rCekEmailUkm = $qCekEmailUkm->row_array();

        $sCekHpUkm = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
        $qCekHpUkm = $this->db->query($sCekHpUkm);
        $rCekHpUkm = $qCekHpUkm->row_array();

        if ($rCekEmailUkm['id_ukm'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar sebagai ukm.';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHpUkm['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar sebagai ukm.';
            $this->general_lib->error($code,$status);
        }

        $sCekEmailDepo = "SELECT * FROM tb_depo WHERE email='" . $em . "'";
        $qCekEmailDepo = $this->db->query($sCekEmailDepo);
        $rCekEmailDepo = $qCekEmailDepo->row_array();

        $sCekHpDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
        $qCekHpDepo = $this->db->query($sCekHpDepo);
        $rCekHpDepo = $qCekHpDepo->row_array();

        if ($rCekEmailDepo['id_depo'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar sebagai ukm.';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHpDepo['id_depo'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar sebagai ukm.';
            $this->general_lib->error($code,$status);
        }

        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        if ($kl != '') {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $pv = $rKota['id_provinsi'];
            $kt = $rKota['id_kota'];
            $kc = $rKec['id_kecamatan'];
        }

        if ($klu != '') {
            $sqlKelu = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $klu . "' ");
            $rKelu = $sqlKelu->row_array();

            $sqlKecu = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKelu['id_kecamatan'] . "' ");
            $rKecu = $sqlKecu->row_array();

            $sqlKotau = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKecu ['id_kota'] . "' ");
            $rKotau = $sqlKotau->row_array();

            $pvu = $rKotau['id_provinsi'];
            $ktu = $rKotau['id_kota'];
            $kcu = $rKecu['id_kecamatan'];
        }

        if($kt!='' && $kl==''){
            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $kt . "' ");
            $rKota = $sqlKota->row_array();

            $ktu = $rKota['id_kota'];
            $pv = $rKota['id_provinsi'];
            $pvu = $rKota['id_provinsi'];
        }

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);

        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlInsert = "INSERT INTO tb_ukm_pra (kd_ukm,nm_ukm,provinsi,kota,alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,from_cross,image_kk,image_keterangan_usaha,jenis_usaha,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha,produk,alasan)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '0','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','WEB','" . $fotokk . "','" . $fotoku . "','" . $ju . "','" . $ci . "','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "','" . $np . "','" . $als . "') ";
            $this->db->query($sqlInsert);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
                                    .button {
                                        background-color: #4CAF50; /* Green */
                                        border: none;
                                        color: white;
                                        padding: 15px 32px;
                                        text-align: center;
                                        text-decoration: none;
                                        display: inline-block;
                                        font-size: 16px;
                                        margin: 4px 2px;
                                        cursor: pointer;
                                        -webkit-transition-duration: 0.4s; /* Safari */
                                        transition-duration: 0.4s;
                                    }

                                    .button1:hover {
                                        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
                                    }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . /*headerEmail()*/ . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nmp . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Registrasi sebagai produsen Halal Thoyyiban telah berhasil, untuk selanjutnya mohon menunggu proses verifikasi dari tim Halal Thoyyiban.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Pendaftaran Anda:
                                                                        <br>
                                                                    <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama UKM</td><td>:</td><td>' . $nmu . '</td></tr>
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nmp . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $em . '</td></tr>
                                                                        <tr><td>No Hp </td><td>:</td><td> ' . $ph . '</td></tr>
                                                                        <tr><td>Alamat </td><td>:</td><td> ' . $au . '</td></tr>
                                                                    </table>
                                                                    </p>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . /*footerEmail()*/ . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "INFO";
            $subject = "REGISTER";
            //pushEmailServer($em, $nmp, $message, $addheader, $subject);


            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm_pra a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'registerpra');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function registerweb() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $nmpm = addslashes($parameter['nmpm']);
        $kl = $parameter['kl'];
        $klu = $parameter['klu'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $posu = addslashes($parameter['posu']);
        $ktp = addslashes($parameter['ktp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $pv = $parameter['pv'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ju = addslashes($parameter['ju']);
        $bu = $parameter['bu'];
        $ipd = $parameter['ipd'];
        $sk = $parameter['sk'];
        $lt = $parameter['lt'];
        $ci = $parameter['ci'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = addslashes($parameter['npwp']);
        $siup = addslashes($parameter['siup']);
        $tdp = addslashes($parameter['tdp']);
        $ud = addslashes($parameter['ud']);
        $cab = $parameter['cab'];

        $password = $parameter['pass'];
        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }

        $pass = md5(hash('sha512', $password));

        $real_pin = $parameter['pin'];
        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        if ($tp == '') {
            $tp = 1;
        }

        $sCekEmail = "SELECT * FROM tb_ukm WHERE email='" . $em . "'";
        $qCekEmail = $this->db->query($sCekEmail);
        $rCekEmail = $qCekEmail->row_array();

        $sCekHp = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
        $qCekHp = $this->db->query($sCekHp);
        $rCekHp = $qCekHp->row_array();

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($rCekEmail['id_ukm'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        $sCekEmailDepo = "SELECT * FROM tb_depo WHERE email='" . $em . "'";
        $qCekEmailDepo = $this->db->query($sCekEmailDepo);
        $rCekEmailDepo = $qCekEmailDepo->row_array();

        $sCekHpDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
        $qCekHpDepo = $this->db->query($sCekHpDepo);
        $rCekHpDepo = $qCekHpDepo->row_array();

        if ($rCekEmailDepo['id_depo'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHpDepo['id_depo'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }



        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }


        if ($kl != '') {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $pv = $rKota['id_provinsi'];
            $kt = $rKota['id_kota'];
            $kc = $rKec['id_kecamatan'];
        }

        if ($klu != '') {
            $sqlKelu = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $klu . "' ");
            $rKelu = $sqlKelu->row_array();

            $sqlKecu = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKelu['id_kecamatan'] . "' ");
            $rKecu = $sqlKecu->row_array();

            $sqlKotau = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKecu ['id_kota'] . "' ");
            $rKotau = $sqlKotau->row_array();

            $pvu = $rKotau['id_provinsi'];
            $ktu = $rKotau['id_kota'];
            $kcu = $rKecu['id_kecamatan'];
        }

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $id_account = "42" . $bulan . $this->general_lib->random_numbers(8);
        $kd_depo = "TYD" . $this->general_lib->random_numbers(6);

        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlInsert = "INSERT INTO tb_ukm (kd_ukm, nm_ukm, provinsi, kota, alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_anggota,id_rt,cabang_lpt,id_produsen,image_kk,image_keterangan_usaha,jenis_usaha,skala_ukm,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '0','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $id_anggota . "','937','" . $cab . "','" . $ipd . "','" . $fotokk . "','" . $fotoku . "','" . $ju . "','" . $sk . "','" . $ci . "','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "') ";
            $this->db->query($sqlInsert);

            $sUkm = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
            $qUkm = $this->db->query($sUkm);
            $rUkm = $qUkm->row_array();

            $updateUkm = "UPDATE tb_ukm SET id_agen='" . $rUkm['id_ukm'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateUkm);

            // Insert Depo
            $sqlInsertDepo = "INSERT INTO tb_depo (kd_depo, nm_depo, provinsi, kota, alm_depo,no_ktp,nm_pemilik,desc_depo,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_produsen,id_rt,id_ukm,image_kk,image_keterangan_usaha,jenis_usaha,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha)
            VALUES ('" . $kd_depo . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '0','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $ipd . "','937','" . $rUkm['id_ukm'] . "','" . $fotokk . "','" . $fotoku . "','" . $ju . "','" . $ci . "','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "') ";
            $this->db->query($sqlInsertDepo);

            $sDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
            $qDepo = $this->db->query($sDepo);
            $rDepo = $qDepo->row_array();

            $updateDepo = "UPDATE tb_depo SET id_agen='" . $rDepo['id_depo'] . "' WHERE no_hp='" . $ph . "'";
            $this->db->query($updateDepo);

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
                                    .button {
                                        background-color: #4CAF50; /* Green */
                                        border: none;
                                        color: white;
                                        padding: 15px 32px;
                                        text-align: center;
                                        text-decoration: none;
                                        display: inline-block;
                                        font-size: 16px;
                                        margin: 4px 2px;
                                        cursor: pointer;
                                        -webkit-transition-duration: 0.4s; /* Safari */
                                        transition-duration: 0.4s;
                                    }

                                    .button1:hover {
                                        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
                                    }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . /*headerEmail()*/ . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nmp . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Selamat Anda telah terdaftar sebagai produsen dilayanan aplikasi Halal Thoyyiban.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Anda:
                                                                        <br>
                                                                    <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nmp . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $em . '</td></tr>
                                                                        <tr><td>No Hp </td><td>:</td><td> ' . $ph . '</td></tr>
                                                                        <tr><td>Password </td><td>:</td><td> <b>' . $password . '</b> </td></tr>
                                                                        <tr><td>PIN </td><td>:</td><td> <b>' . $real_pin . '</b> </td></tr>
                                                                    </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Untuk login silahkan klik tombol dibawah.<br>
                                                                        <a href="https://halalthoyyiban.com/produsen/" target="_blank"><button class="button button1">Login</button></a>
                                                                    </p>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . /*footerEmail()*/ . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "INFO";
            $subject = "REGISTER";
            //pushEmailServer($em, $nmp, $message, $addheader, $subject);


            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'register');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function registerdetail() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = $parameter['nmu'];
        $nmp = $parameter['nmp'];
        $kl = $parameter['kl'];
        $almt = $parameter['alm'];
        $au = $parameter['au'];
        $pos = $parameter['pos'];
        $ktp = $parameter['ktp'];
        $ph = $parameter['ph'];
        $em = $parameter['em'];
        $desc = $parameter['desc'];
        $jk = $parameter['jk'];
        $tl = $parameter['tl'];
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $kt = $parameter['kt'];
        $bk = $parameter['bk'];
        $nr = $parameter['nr'];
        $an = $parameter['an'];
        $iu = $parameter['iu'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = $parameter['npwp'];
        $siup = $parameter['siup'];
        $tdp = $parameter['tdp'];
        $ud = $parameter['ud'];

        if ($tp == '') {
            $tp = 1;
        }

        if ($kt == '') {
            $kt = $parameter['ik'];
        }

        $sCekEmail = "SELECT * FROM tb_ukm_detail WHERE email='" . $em . "'";
        $qCekEmail = $this->db->query($sCekEmail);
        $rCekEmail = $qCekEmail->row_array();

        $sCekHp = "SELECT * FROM tb_ukm_detail WHERE no_hp='" . $ph . "'";
        $qCekHp = $this->db->query($sCekHp);
        $rCekHp = $qCekHp->row_array();

        if ($rCekEmail['id_ukm_detail'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_ukm_detail'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }


        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
        $rKel = $sqlKel->row_array();

        $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
        $rKec = $sqlKec->row_array();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
        $rKota = $sqlKota->row_array();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
        $rProv = $sqlProv->row_array();

        $bulan = date('m');
        $no_kartu = "42" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $id_account = "42" . $bulan . $this->general_lib->random_numbers(8);

        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlInsertDetail = "INSERT INTO tb_ukm_detail (id_ukm,kd_ukm, nm_ukm, provinsi, kota, alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,
            no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,
            no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,
            pin,real_pin,siup,tdp,ud)
            VALUES ('" . $iu . "'," . $kd_ukm . "', '" . $nmu . "', '" . $rProv['id_provinsi'] . "', '" . $rKota['id_kota'] . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "',
                   '" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '2', '1','" . $lt . "','" . $rKec['id_kecamatan'] . "','" . $kl . "','" . $npwp . "',
                   '" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "',
                   '" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "') ";
            $this->db->query($sqlInsertDetail);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm_detail a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'register');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertukmdetail() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = $parameter['nmu'];
        $nmp = $parameter['nmp'];
        $kl = $parameter['kl'];
        $almt = $parameter['alm'];
        $au = $parameter['au'];
        $pos = $parameter['pos'];
        $ktp = $parameter['ktp'];
        $ph = $parameter['ph'];
        $em = $parameter['em'];
        $desc = $parameter['desc'];
        $jk = $parameter['jk'];
        $tl = $parameter['tl'];
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $kt = $parameter['kt'];
        $bk = $parameter['bk'];
        $nr = $parameter['nr'];
        $an = $parameter['an'];
        $ju = $parameter['ju'];
        $iu = $parameter['iu'];
        $kd = "9" . $this->general_lib->random_numbers(7);

        $npwp = $parameter['npwp'];
        $siup = $parameter['siup'];
        $tdp = $parameter['tdp'];
        $ud = $parameter['ud'];

        $password = $parameter['pass'];
        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }

        $pass = md5(hash('sha512', $password));

        $real_pin = $parameter['pin'];
        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        if ($tp == '') {
            $tp = 1;
        }

        if ($kt == '') {
            $kt = $parameter['ik'];
        }

        $sCekEmail = "SELECT * FROM tb_ukm_detail WHERE email='" . $em . "'";
        $qCekEmail = $this->db->query($sCekEmail);
        $rCekEmail = $qCekEmail->row_array();

        $sCekHp = "SELECT * FROM tb_ukm_detail WHERE no_hp='" . $ph . "'";
        $qCekHp = $this->db->query($sCekHp);
        $rCekHp = $qCekHp->row_array();

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($rCekEmail['id_ukm'] != '') {
            $code = '03';
            $status = 'Email sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }

        if ($rCekHp['id_ukm'] != '') {
            $code = '04';
            $status = 'No Handphone sudah terdaftar...';
            $this->general_lib->error($code,$status);
        }


        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }
        }


        $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
        $rKel = $sqlKel->row_array();

        $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
        $rKec = $sqlKec->row_array();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
        $rKota = $sqlKota->row_array();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
        $rProv = $sqlProv->row_array();

        $bulan = date('m');
        $no_kartu = "43" . $bulan . $this->general_lib->random_numbers(12);
        $kd_ukm = "TY" . $this->general_lib->random_numbers(6);
        $id_account = "43" . $bulan . $this->general_lib->random_numbers(8);
        $kd_depo = "TYD" . $this->general_lib->random_numbers(6);

        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlInsert = "INSERT INTO tb_ukm_detail (kd_ukm,nm_ukm,provinsi,kota,alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,no_hp,email,foto,image_ktp,date_created,date_updated,tipe,status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_ukm,image_kk,image_keterangan_usaha,jenis_usaha)
            VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $rProv['id_provinsi'] . "', '" . $rKota['id_kota'] . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "',
                   '" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '1','" . $lt . "','" . $rKec['id_kecamatan'] . "','" . $kl . "','" . $npwp . "',
                   '" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "',
                   '" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $iu . "','" . $fotokk . "','" . $fotoku . "','" . $ju . "') ";
            $this->db->query($sqlInsert);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm_detail a
             WHERE a.no_hp='" . $ph . "' ";

            $this->response->getresponse($sql,'register');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updateukm() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = addslashes($parameter['nmu']);
        $nmp = addslashes($parameter['nmp']);
        $nmpm = addslashes($parameter['nmpm']);
        $pv = $parameter['pv'];
        $kt = $parameter['kt'];
        $kc = $parameter['kc'];
        $kl = $parameter['kl'];
        $klu = $parameter['klu'];
        $almt = addslashes($parameter['alm']);
        $au = addslashes($parameter['au']);
        $pos = addslashes($parameter['pos']);
        $posu = addslashes($parameter['posu']);
        $ktp = addslashes($parameter['ktp']);
        $ph = addslashes($parameter['ph']);
        $em = addslashes($parameter['em']);
        $desc = addslashes($parameter['desc']);
        $desc_eng = addslashes($parameter['desc_eng']);
        $desc_ar = addslashes($parameter['desc_ar']);
        $jk = $parameter['jk'];
        $tl = addslashes($parameter['tl']);
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $bk = addslashes($parameter['bk']);
        $nr = addslashes($parameter['nr']);
        $an = addslashes($parameter['an']);
        $ju = $parameter['ju'];
        $bu = addslashes($parameter['bu']);
        $ipr = $parameter['ipr'];
        $ipd = $parameter['ipd'];
        $sk = $parameter['sk'];
        $lt = $parameter['lt'];
        $ma = $parameter['ma'];
        $ci = $parameter['ci'];
        $id = $parameter['id'];

        $npwp = addslashes($parameter['npwp']);
        $siup = addslashes($parameter['siup']);
        $tdp = addslashes($parameter['tdp']);
        $ud = addslashes($parameter['ud']);
        $cab = $parameter['cab'];

        $sCek = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
        if ($id != '') {
            $sCek = "SELECT * FROM tb_ukm WHERE id_ukm='" . $id . "'";
        }
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($tp == '') {
            $tp = $rCek['tipe'];
        }

        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoktp);
        }

        if ($fotoktp == '') {
            $fotoktp = $rCek['image_ktp'];
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotousaha);
        }

        if ($fotousaha == '') {
            $fotousaha = $rCek['logo'];
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotokk);
        }

        if ($fotokk == '') {
            $fotokk = $rCek['image_kk'];
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoku);
        }

        if ($fotoku == '') {
            $fotoku = $rCek['image_keterangan_usaha'];
        }

        if ($ph == '') {
            $ph = $rCek['no_hp'];
        }

        if ($nmp == '') {
            $nmp = $rCek['nm_pemilik'];
        }

        if ($nmpm == '') {
            $nmpm = $rCek['nm_pemohon'];
        }

        if ($pv == '') {
            $pv = $rCek['provinsi'];
        }

        if ($kt == '') {
            $kt = $rCek['kota'];
        }

        if ($kc == '') {
            $kc = $rCek['kecamatan'];
        }

        if ($kl == '') {
            $kl = $rCek['kelurahan'];
        }

        if ($klu == '') {
            $klu = $rCek['kelurahan_usaha'];
        }

        if ($almt == '') {
            $almt = $rCek['alm_ukm'];
        }

        if ($jk == '') {
            $jk = $rCek['jenis_kelamin'];
        }

        if ($tl == '') {
            $tl = $rCek['tempat_lahir'];
        }

        if ($tgl == '') {
            $tgl = $rCek['tgl_lahir'];
        }

        if ($em == '') {
            $em = $rCek['email'];
        }

        if ($ktp == '') {
            $ktp = $rCek['no_ktp'];
        }

        if ($npwp == '') {
            $npwp = $rCek['no_npwp'];
        }

        if ($siup == '') {
            $siup = $rCek['siup'];
        }

        if ($tdp == '') {
            $tdp = $rCek['tdp'];
        }

        if ($ud == '') {
            $ud = $rCek['ud'];
        }

        if ($nmu == '') {
            $nmu = $rCek['nm_ukm'];
        }

        if ($desc == '') {
            $desc = $rCek['desc_ukm'];
        }

        if ($desc_eng == '') {
            $desc_eng = $rCek['desc_ukm_eng'];
        }

        if ($desc_ar == '') {
            $desc_ar = $rCek['desc_ukm_ar'];
        }

        if ($au == '') {
            $au = $rCek['alm_usaha'];
        }

        if ($bk == '') {
            $bk = $rCek['bank'];
        }

        if ($nr == '') {
            $nr = $rCek['no_rekening'];
        }

        if ($an == '') {
            $an = $rCek['atas_nama'];
        }

        if ($pos == '') {
            $pos = $rCek['kode_pos'];
        }

        if ($posu == '') {
            $posu = $rCek['kode_pos_usaha'];
        }

        if ($ipr == '') {
            $ipr = $rCek['is_pra'];
        }

        if ($ipd == '') {
            $ipd = $rCek['id_produsen'];
        }

        if ($ju == '') {
            $ju = $rCek['jenis_usaha'];
        }

        if ($bu == '') {
            $bu = $rCek['bentuk_usaha'];
        }

        if ($sk == '') {
            $sk = $rCek['skala_ukm'];
        }

        if ($lt == '') {
            $lt = $rCek['latitude'];
        }

        if ($ma == '') {
            $ma = $rCek['max_agen'];
        }

        if ($ci == '') {
            $ci = $rCek['country_id'];
        }

        if ($cab == '' || $cab == '0') {
            $cab = $rCek['cabang_lpt'];
        }

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if ($kl != '') {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $pv = $rKota['id_provinsi'];
            $kt = $rKota['id_kota'];
            $kc = $rKec['id_kecamatan'];
        }

        if ($klu != '') {
            $sqlKelu = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $klu . "' ");
            $rKelu = $sqlKelu->row_array();

            $sqlKecu = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKelu['id_kecamatan'] . "' ");
            $rKecu = $sqlKecu->row_array();

            $sqlKotau = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKecu ['id_kota'] . "' ");
            $rKotau = $sqlKotau->row_array();

            $pvu = $rKotau['id_provinsi'];
            $ktu = $rKotau['id_kota'];
            $kcu = $rKecu['id_kecamatan'];
        }


        if ($em != '' && $ph != '' && $nmp != '') {
            $err = '';
            $code = '200';

            $sqlUpdate = "UPDATE tb_ukm SET nm_ukm='" . $nmu . "', provinsi='" . $pv . "', kota='" . $kt . "',country_id='" . $ci . "',alm_ukm='" . $almt . "',no_ktp='" . $ktp . "',nm_pemilik='" . $nmp . "',desc_ukm='" . $desc . "',alm_usaha='" . $au . "',id_produsen='" . $ipd . "',email='" . $em . "',foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "',is_pra='" . $ipr . "',kecamatan='" . $kc . "',kelurahan='" . $kl . "',bank='" . $bk . "',desc_ukm_eng='" . $desc_eng . "',desc_ukm_ar='" . $desc_ar . "',jenis_kelamin='" . $jk . "',tgl_lahir='" . $tgl . "',no_rekening='" . $nr . "',no_npwp='" . $npwp . "',siup='" . $siup . "',tdp='" . $tdp . "',ud='" . $ud . "',atas_nama='" . $an . "',tempat_lahir='" . $tl . "',kode_pos='" . $pos . "',cabang_lpt='" . $cab . "',image_kk='" . $fotokk . "',max_agen='" . $ma . "',image_keterangan_usaha='" . $fotoku . "',jenis_usaha='" . $ju . "',skala_ukm='" . $sk . "',latitude = '" . $lt . "',nm_pemohon = '" . $nmpm . "',provinsi_usaha = '" . $pvu . "',kota_usaha = '" . $ktu . "',kecamatan_usaha = '" . $kcu . "',kelurahan_usaha = '" . $klu . "',kode_pos_usaha = '" . $posu . "',bentuk_usaha = '" . $bu . "' WHERE id_ukm='" . $id . "' ";
            $this->db->query($sqlUpdate);

            $sqlUpdateDepo = "UPDATE tb_depo SET nm_depo='" . $nmu . "', provinsi='" . $pv . "', kota='" . $kt . "',alm_depo='" . $almt . "',no_ktp='" . $ktp . "',nm_pemilik='" . $nmp . "',desc_depo='" . $desc . "',alm_usaha='" . $au . "',country_id='" . $ci . "',email='" . $em . "',foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "',max_agen='" . $ma . "',kecamatan='" . $kc . "',kelurahan='" . $kl . "',bank='" . $bk . "',desc_depo_eng='" . $desc_eng . "',desc_depo_ar='" . $desc_ar . "',jenis_kelamin='" . $jk . "',tgl_lahir='" . $tgl . "',no_rekening='" . $nr . "',no_npwp='" . $npwp . "',siup='" . $siup . "',tdp='" . $tdp . "',ud='" . $ud . "',atas_nama='" . $an . "',tempat_lahir='" . $tl . "',kode_pos='" . $pos . "',image_kk='" . $fotokk . "',image_keterangan_usaha='" . $fotoku . "',jenis_usaha = '" . $ju . "',latitude = '" . $lt . "',nm_pemohon = '" . $nmpm . "',provinsi_usaha = '" . $pvu . "',kota_usaha = '" . $ktu . "',kecamatan_usaha = '" . $kcu . "',kelurahan_usaha = '" . $klu . "',kode_pos_usaha = '" . $posu . "',bentuk_usaha = '" . $bu . "' WHERE id_ukm = '" . $id . "' ";
            $this->db->query($sqlUpdateDepo);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm a
             WHERE a.no_hp='" . $ph . "'";

            $this->response->getresponse($sql,'updateukm');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updateukmdetail() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nmu = $parameter['nmu'];
        $nmp = $parameter['nmp'];
        $kl = $parameter['kl'];
        $almt = $parameter['alm'];
        $au = $parameter['au'];
        $pos = $parameter['pos'];
        $ktp = $parameter['ktp'];
        $ph = $parameter['ph'];
        $em = $parameter['em'];
        $desc = $parameter['desc'];
        $jk = $parameter['jk'];
        $tl = $parameter['tl'];
        $tgl = $parameter['tgl'];
        $tp = $parameter['tp'];
        $bk = $parameter['bk'];
        $nr = $parameter['nr'];
        $an = $parameter['an'];
        $ju = $parameter['ju'];
        $iu = $parameter['iu'];
        $id = $parameter['id'];

        $npwp = $parameter['npwp'];
        $siup = $parameter['siup'];
        $tdp = $parameter['tdp'];
        $ud = $parameter['ud'];

        $sCek = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($tp == '') {
            $tp = $rCek['tipe'];
        }

        //Upload Foto -----------------------------------------------------------------------

        $datafile = $parameter['image_foto'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename_foto'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoktp);
        }

        if ($fotoktp == '') {
            $fotoktp = $rCek['image_ktp'];
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotousaha);
        }

        if ($fotousaha == '') {
            $fotousaha = $rCek['logo'];
        }

        //Upload KK -----------------------------------------------------------------------

        $datafile_kk = $parameter['image_kk'];
        $binary_kk = base64_decode($datafile_kk);
        $namefile_kk = $parameter['filename_kk'];
        if ($namefile_kk != '') {
            $target_dir_kk = $this->general_lib->path();

            if (!file_exists($target_dir_kk)) {
                mkdir($target_dir_kk, 0777, true);
            }

            $url_path_kk = "upload/";

            $target_path_kk = $target_dir_kk;
            $now_kk = date('YmdHis');
            $rand_kk = rand(1111, 9999);
            $generatefile_kk = $now_kk . $rand_kk;
            $namefile_kk = $generatefile_kk . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_kk = $target_path_kk . $namefile_kk;

            chmod($target_path_kk, 0777);
            $fh_kk = fopen($target_path_kk, 'w') or die("can't open file");
            chmod($target_path_kk, 0777);
            fwrite($fh_kk, $binary_kk);
            fclose($fh_kk);

            sleep(1);

            $fotokk = "upload/" . $namefile_kk;
        }

        if ($namefile_kk == '') {
            $fotokk = "";
        }

        if ($fotokk != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotokk,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotokk);
        }

        if ($fotokk == '') {
            $fotokk = $rCek['image_kk'];
        }

        //Upload Keterangan Usaha -----------------------------------------------------------------------

        $datafile_ku = $parameter['image_ku'];
        $binary_ku = base64_decode($datafile_ku);
        $namefile_ku = $parameter['filename_ku'];
        if ($namefile_ku != '') {
            $target_dir_ku = $this->general_lib->path();

            if (!file_exists($target_dir_ku)) {
                mkdir($target_dir_ku, 0777, true);
            }

            $url_path_ku = "upload/";

            $target_path_ku = $target_dir_ku;
            $now_ku = date('YmdHis');
            $rand_ku = rand(1111, 9999);
            $generatefile_ku = $now_ku . $rand_ku;
            $namefile_ku = $generatefile_ku . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ku = $target_path_ku . $namefile_ku;

            chmod($target_path_ku, 0777);
            $fh_ku = fopen($target_path_ku, 'w') or die("can't open file");
            chmod($target_path_ku, 0777);
            fwrite($fh_ku, $binary_ku);
            fclose($fh_ku);

            sleep(1);

            $fotoku = "upload/" . $namefile_ku;
        }

        if ($namefile_ku == '') {
            $fotoku = "";
        }

        if ($fotoku != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoku,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoku);
        }

        if ($fotoku == '') {
            $fotoku = $rCek['image_keterangan_usaha'];
        }

        if ($iu == '') {
            $iu = $rCek['id_ukm'];
        }

        if ($nmp == '') {
            $nmp = $rCek['nm_pemilik'];
        }

        if ($kl == '') {
            $rCek['kelurahan'];
        }

        if ($almt == '') {
            $almt = $rCek['alm_ukm'];
        }

        if ($jk == '') {
            $jk = $rCek['jenis_kelamin'];
        }

        if ($tl == '') {
            $tl = $rCek['tempat_lahir'];
        }

        if ($tgl == '') {
            $tgl = $rCek['tgl_lahir'];
        }

        if ($em == '') {
            $em = $rCek['email'];
        }

        if ($ktp == '') {
            $ktp = $rCek['no_ktp'];
        }

        if ($npwp == '') {
            $npwp = $rCek['no_npwp'];
        }

        if ($siup == '') {
            $siup = $rCek['siup'];
        }

        if ($tdp == '') {
            $tdp = $rCek['tdp'];
        }

        if ($ud == '') {
            $ud = $rCek['ud'];
        }

        if ($nmu == '') {
            $nmu = $rCek['nm_ukm'];
        }

        if ($desc == '') {
            $desc = $rCek['desc_ukm'];
        }

        if ($au == '') {
            $au = $rCek['alm_usaha'];
        }

        if ($bk == '') {
            $bk = $rCek['bank'];
        }

        if ($nr == '') {
            $nr = $rCek['no_rekening'];
        }

        if ($an == '') {
            $an = $rCek['atas_nama'];
        }

        if ($pos == '') {
            $pos = $rCek['kode_pos'];
        }

        if ($ju == '') {
            $ju = $rCek['jenis_usaha'];
        }

        if (strlen($ph) < 6 || strlen($ph) > 13) {
            $code = "06";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($ph) == FALSE) {
            $code = "07";
            $status = "No Handphone tidak valid.";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
            $code = "06";
            $status = "Email tidak valid.";
            $this->general_lib->error($code,$status);
        }

        $sqlKel = $this->db->query("SELECT * FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
        $rKel = $sqlKel->row_array();

        $sqlKec = $this->db->query("SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
        $rKec = $sqlKec->row_array();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
        $rKota = $sqlKota->row_array();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
        $rProv = $sqlProv->row_array();

        if ($em != '' && $ph != '' && $nmp != '' && $id != '') {
            $err = '';
            $code = '200';

            $sqlUpdate = "UPDATE tb_ukm_detail SET nm_ukm='" . $nmu . "', provinsi='" . $rProv['id_provinsi'] . "', kota='" . $rKota['id_kota'] . "',alm_ukm='" . $almt . "',no_ktp='" . $ktp . "',nm_pemilik='" . $nmp . "',desc_ukm='" . $desc . "',alm_usaha='" . $au . "',email='" . $em . "',foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "',kecamatan='" . $rKec['id_kecamatan'] . "',kelurahan='" . $kl . "',bank='" . $bk . "',id_ukm='" . $iu . "',jenis_kelamin='" . $jk . "',tgl_lahir='" . $tgl . "',no_rekening='" . $nr . "',no_npwp='" . $npwp . "',siup='" . $siup . "',tdp='" . $tdp . "',ud='" . $ud . "',atas_nama='" . $an . "',tempat_lahir='" . $tl . "',kode_pos='" . $pos . "',image_kk='" . $fotokk . "',image_keterangan_usaha='" . $fotoku . "',jenis_usaha='" . $ju . "' WHERE id_ukm_detail='" . $id . "' ";
            $this->db->query($sqlUpdate);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm_detail a
             WHERE a.id_ukm_detail='" . $id . "'";

            $this->response->getresponse($sql,'updateukmdetail');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function login() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);
        $password = addslashes($parameter['pass']);
        $pass = md5(hash('sha512', $password));

        $sCekUser = "SELECT id_ukm FROM tb_ukm WHERE email='" . $em . "'";
        $qCekUser = $this->db->query($sCekUser);
        $rCekUser = $qCekUser->row_array();

        if ($rCekUser['id_ukm'] != '') {
            $param_user = " a.email='" . $em . "' ";
        } else {
            $param_user = " a.no_hp='" . $em . "' ";
        }

        $sqlCek = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan
             FROM tb_ukm a
             WHERE " . $param_user . "
             AND a.password='" . $pass . "'";
        $qCek = $this->db->query($sqlCek);
        $rCek = $qCek->row_array();

        if ($em != '' && $pass != '') {
            if ($rCek['id_ukm'] == '') {
                $code = '03';
                $status = 'Username & password invalid...';
                $this->general_lib->error($code,$status);
            } else if ($rCek['status'] == '0') {
                $code = '04';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            } else {
                $sql = $sqlCek;
                $this->response->getresponse($sql,'login');
            }
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function loginsibaweh() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);
        $password = addslashes($parameter['pass']);
        $pass = md5(hash('sha512', $password));

        $sCekUser = "SELECT id_ukm FROM tb_ukm WHERE email='" . $em . "'";
        $qCekUser = $this->db->query($sCekUser);
        $rCekUser = $qCekUser->row_array();

        if ($rCekUser['id_ukm'] != '') {
            $param_user = " a.email='" . $em . "' ";
        } else {
            $param_user = " a.no_hp='" . $em . "' ";
        }

        $sqlCek = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan
             FROM tb_ukm a
             WHERE " . $param_user . "
             AND a.password='" . $pass . "'";
        $qCek = $this->db->query($sqlCek);
        $rCek = $qCek->row_array();

        if ($em != '' && $pass != '') {
            if ($rCek['id_ukm'] == '') {
                $code = '03';
                $status = 'Username & password invalid...';
                $this->general_lib->error($code,$status);
            } else if ($rCek['status'] == '0') {
                $code = '04';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            } else {
                $sql = $sqlCek;
                $this->response->getresponse($sql,'login');
            }
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatefoto() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $hp = $parameter['hp'];

        if ($id != '') {
            $sCek = "SELECT * FROM tb_ukm WHERE id_ukm='" . $id . "'";
        } else {
            $sCek = "SELECT * FROM tb_ukm WHERE no_hp='" . $hp . "'";
        }
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile_ktp = $parameter['image_ktp'];
        $binary_ktp = base64_decode($datafile_ktp);
        $namefile_ktp = $parameter['filename_ktp'];
        if ($namefile_ktp != '') {
            $target_dir_ktp = $this->general_lib->path();

            if (!file_exists($target_dir_ktp)) {
                mkdir($target_dir_ktp, 0777, true);
            }

            $url_path_ktp = "upload/";

            $target_path_ktp = $target_dir_ktp;
            $now_ktp = date('YmdHis');
            $rand_ktp = rand(1111, 9999);
            $generatefile_ktp = $now_ktp . $rand_ktp;
            $namefile_ktp = $generatefile_ktp . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_ktp = $target_path_ktp . $namefile_ktp;

            chmod($target_path_ktp, 0777);
            $fh_ktp = fopen($target_path_ktp, 'w') or die("can't open file");
            chmod($target_path_ktp, 0777);
            fwrite($fh_ktp, $binary_ktp);
            fclose($fh_ktp);

            sleep(1);

            $fotoktp = "upload/" . $namefile_ktp;
        }

        if ($namefile_ktp == '') {
            $fotoktp = "";
        }

        if ($fotoktp != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotoktp,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotoktp);
        }

        if ($fotoktp == '') {
            $fotoktp = $rCek['image_ktp'];
        }

        //Upload Foto Usaha -----------------------------------------------------------------------

        $datafile_usaha = $parameter['image_usaha'];
        $binary_usaha = base64_decode($datafile_usaha);
        $namefile_usaha = $parameter['filename_usaha'];
        if ($namefile_usaha != '') {
            $target_dir_usaha = $this->general_lib->path();

            if (!file_exists($target_dir_usaha)) {
                mkdir($target_dir_usaha, 0777, true);
            }

            $url_path_usaha = "upload/";

            $target_path_usaha = $target_dir_usaha;
            $now_usaha = date('YmdHis');
            $rand_usaha = rand(1111, 9999);
            $generatefile_usaha = $now_usaha . $rand_usaha;
            $namefile_usaha = $generatefile_usaha . ".jpeg";

            //echo $namefile;
            // -------------------------------------------------------------------
            $target_path_usaha = $target_path_usaha . $namefile_usaha;

            chmod($target_path_usaha, 0777);
            $fh_usaha = fopen($target_path_usaha, 'w') or die("can't open file");
            chmod($target_path_usaha, 0777);
            fwrite($fh_usaha, $binary_usaha);
            fclose($fh_usaha);

            sleep(1);

            $fotousaha = "upload/" . $namefile_usaha;
        }

        if ($namefile_usaha == '') {
            $fotousaha = "";
        }

        if ($fotousaha != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $fotousaha,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $fotousaha);
        }

        if ($fotousaha == '') {
            $fotousaha = $rCek['logo'];
        }

        if ($id == '') {
            $id = $rCek['id_ukm'];
        }

        if ($id != '') {
            $sqlUpdate = "UPDATE tb_ukm SET foto='" . $foto . "',image_ktp='" . $fotoktp . "',date_updated=NOW(),logo='" . $fotousaha . "' WHERE id_ukm='" . $id . "'";
            $this->db->query($sqlUpdate);

            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm a
             WHERE a.id_ukm='" . $id . "' ";

            $this->response->getresponse($sql,'insertinfo');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepassword() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $old_password = addslashes($parameter['old_pass']);
        $password = addslashes($parameter['pass']);
        $id = $parameter['iu'];
    
        $pass = md5(hash('sha512', $password));
        $s1 = "SELECT * FROM tb_ukm WHERE id_ukm='" . $id . "' AND real_password='" . $old_password . "'";
        $q1 = $this->db->query($s1);
        $r1 = $q1->row_array();

        if ($r1['id_ukm'] == '') {
            $code = '04';
            $status = "Password lama yang Anda masukan salah...";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password tidak valid, minimal 6 digit huruf atau angka...";
            $this->general_lib->error($code,$status);
        }

        if ($password != '' && $id != '') {
            $sqlUpdate = "Update tb_ukm set password='" . $pass . "', real_password ='" . $password . "',
                date_updated=NOW() where id_ukm ='" . $id . "'";
            $this->db->query($sqlUpdate);

            $sqlUpdateDepo = "Update tb_depo set password='" . $pass . "', real_password ='" . $password . "',
                date_updated=NOW() where id_ukm ='" . $id . "'";
            $this->db->query($sqlUpdateDepo);

            $s1 = "SELECT * FROM tb_ukm
                WHERE id_ukm='" . $id . "'";
            $q1 = $this->db->query($s1);
            $r1 = $q1->row_array();
            $email = $r1['email'];
            $nama = $r1['nm_ukm'];

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . /*headerEmail()*/ . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Selamat password Anda telah berhasil diganti
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi password Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>Password </td><td>:</td><td> <b>' . $password . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . /*footerEmail()*/ . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "INFO";
            $subject = "Pemberitahuan ganti password";
            //pushEmailServer($email, $nama, $message, $addheader, $subject);

            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan
             FROM tb_ukm a
             WHERE a.id_ukm='" . $id . "'";
            $this->response->getresponse($sql,'changepassword');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepin() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ia = $parameter['iu'];
        $old_pin = addslashes($parameter['old_pin']);
        $real_pin = addslashes($parameter['pin']);
        
        $pin = md5(hash('sha512', $real_pin));

        $s1 = "SELECT * FROM tb_ukm WHERE id_ukm='" . $ia . "' AND real_pin='" . $old_pin . "'";
        $q1 = $this->db->query($s1);
        $r1 = $q1->row_array();

        if ($r1['id_ukm'] == '') {
            $code = "07";
            $status = "PIN lama salah...";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($real_pin) == FALSE) {
            $code = "07";
            $status = "PIN harus berupa angka...";
            $this->general_lib->error($code,$status);
        }

        if (strlen($real_pin) < 4) {
            $code = "07";
            $status = "PIN kurang dari 4 digit...";
            $this->general_lib->error($code,$status);
        }

        if (strlen($real_pin) > 10) {
            $code = "07";
            $status = "PIN lebih dari 10 digit...";
            $this->general_lib->error($code,$status);
        }

        if ($ia != '' && $real_pin != '') {
            $sUpdate = "UPDATE tb_ukm SET pin='" . $pin . "',real_pin='" . $real_pin . "',date_updated=NOW() WHERE id_ukm='" . $ia . "' ";
            $this->db->query($sUpdate);

            $sUpdateDepo = "UPDATE tb_depo SET pin='" . $pin . "',real_pin='" . $real_pin . "',date_updated=NOW() WHERE id_ukm='" . $ia . "' ";
            $this->db->query($sUpdateDepo);

            $s1 = "SELECT * FROM tb_ukm
                WHERE id_ukm='" . $ia . "'";
            $q1 = $this->db->query($s1);
            $r1 = $q1->row_array();
            $email = $r1['email'];
            $nama = $r1['nm_ukm'];

            $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . /*headerEmail()*/ . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nama . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Selamat PIN Anda telah berhasil diganti
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi PIN Anda:
                                                                        <br>
                                                                        <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nama . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $email . '</td></tr>
                                                                        <tr><td>PIN </td><td>:</td><td> <b>' . $real_pin . '</b> </td></tr>
                                                                        </table>
                                                                    </p>
                                                                    <br>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                    Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . /*footerEmail()*/ . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
            $addheader = "INFO";
            $subject = "Pemberitahuan ganti PIN";
            //pushEmailServer($email, $nama, $message, $addheader, $subject);

            $sql = "SELECT a.*,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan),'') as nm_kelurahan
             FROM tb_ukm a
             WHERE a.id_ukm='" . $ia . "'";
            $this->response->getresponse($sql,'changepin');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatesaldo() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];
        $to = $parameter['to'];
        $ref = $parameter['ref'];

        if ($hp != '' && $to != '') {
            $update = "UPDATE tb_ukm SET saldo=saldo-'" . $to . "',date_updated=NOW() WHERE no_hp='" . $hp . "'";
            $this->db->query($update);

            $insert = "INSERT INTO tb_log_saldo (no_hp,saldo,reff,date_updated,date_created,tipe,status)"
                    . "VALUES('" . $hp . "','" . $to . "','" . $ref . "',NOW(),NOW(),1,1)";
            $this->db->query($insert);

            $sql = "SELECT * FROM tb_ukm WHERE no_hp='" . $hp . "'";
            $this->response->getresponse($sql,'updatesaldo');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatuspra() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $ip = $parameter['ip'];
        $st = $parameter['st'];
        $ket = $parameter['ket'];

        $sUkmPra = "SELECT * FROM tb_ukm_pra WHERE id_ukm_pra='" . $id . "'";
        $qUkmPra = $this->db->query($sUkmPra);
        $rUkmPra = $qUkmPra->row_array();

        $nmu = $rUkmPra['nm_ukm'];
        $nmp = $rUkmPra['nm_pemilik'];
        $nmpm = $rUkmPra['nm_pemohon'];
        $kl = $rUkmPra['kelurahan'];
        $kc = $rUkmPra['kecamatan'];
        $kt = $rUkmPra['kota'];
        $pv = $rUkmPra['provinsi'];
        $klu = $rUkmPra['kelurahan_usaha'];
        $kcu = $rUkmPra['kecamatan_usaha'];
        $ktu = $rUkmPra['kota_usaha'];
        $pvu = $rUkmPra['provinsi_usaha'];
        $almt = $rUkmPra['alm_ukm'];
        $au = $rUkmPra['alm_usaha'];
        $pos = $rUkmPra['kode_pos'];
        $posu = $rUkmPra['kode_pos_usaha'];
        $ktp = $rUkmPra['no_ktp'];
        $ph = $rUkmPra['no_hp'];
        $em = $rUkmPra['email'];
        $desc = $rUkmPra['desc_ukm'];
        $jk = $rUkmPra['jenis_kelamin'];
        $tl = $rUkmPra['tempat_lahir'];
        $tgl = $rUkmPra['tgl_lahir'];
        $tp = $rUkmPra['tipe'];
        $bk = $rUkmPra['bank'];
        $nr = $rUkmPra['no_rekening'];
        $an = $rUkmPra['atas_nama'];
        $ju = $rUkmPra['jenis_usaha'];
        $bu = $rUkmPra['bentuk_usaha'];
        $kd = $rUkmPra['kd_ukm'];
        $ci = $rUkmPra['country_id'];
        $lt = $rUkmPra['latitude'];
        $foto = $rUkmPra['foto'];
        $fotoktp = $rUkmPra['image_ktp'];
        $fotousaha = $rUkmPra['logo'];
        $fotokk = $rUkmPra['image_kk'];
        $fotoku = $rUkmPra['image_keterangan_usaha'];

        $npwp = $rUkmPra['no_npwp'];
        $siup = $rUkmPra['siup'];
        $tdp = $rUkmPra['tdp'];
        $cab = 1;

        $password = $rUkmPra['real_password'];
        if ($password == '') {
            $password = $this->general_lib->random_numbers(6);
        }

        $pass = md5(hash('sha512', $password));

        $real_pin = $rUkmPra['real_pin'];
        if ($real_pin == '') {
            $real_pin = $this->general_lib->random_numbers(6);
        }
        $pin = md5(hash('sha512', $real_pin));

        if ($st == 1) {
            $sCekEmail = "SELECT * FROM tb_ukm WHERE email='" . $em . "'";
            $qCekEmail = $this->db->query($sCekEmail);
            $rCekEmail = $qCekEmail->row_array();

            $sCekHp = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
            $qCekHp = $this->db->query($sCekHp);
            $rCekHp = $qCekHp->row_array();

            if ($rCekEmail['id_ukm'] != '') {
                $code = '03';
                $status = 'Email sudah terdaftar sebagai ukm.';
                $this->general_lib->error($code,$status);
            }

            if ($rCekHp['id_ukm'] != '') {
                $code = '04';
                $status = 'No Handphone sudah terdaftar sebagai ukm.';
                $this->general_lib->error($code,$status);
            }

            $sCekEmailDepo = "SELECT * FROM tb_depo WHERE email='" . $em . "'";
            $qCekEmailDepo = $this->db->query($sCekEmailDepo);
            $rCekEmailDepo = $qCekEmailDepo->row_array();

            $sCekHpDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
            $qCekHpDepo = $this->db->query($sCekHpDepo);
            $rCekHpDepo = $qCekHpDepo->row_array();

            if ($rCekEmailDepo['id_depo'] != '') {
                $code = '03';
                $status = 'Email sudah terdaftar sebagai ukm.';
                $this->general_lib->error($code,$status);
            }

            if ($rCekHpDepo['id_depo'] != '') {
                $code = '04';
                $status = 'No Handphone sudah terdaftar sebagai ukm.';
                $this->general_lib->error($code,$status);
            }

            if ($kl != '') {
                $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
                $rKel = $sqlKel->row_array();

                $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
                $rKec = $sqlKec->row_array();

                $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
                $rKota = $sqlKota->row_array();

                $pv = $rKota['id_provinsi'];
                $kt = $rKota['id_kota'];
                $kc = $rKec['id_kecamatan'];
            }

            $no_kartu = $rUkmPra['no_kartu'];
            $kd_ukm = $rUkmPra['kd_ukm'];
            $kd_depo = "TYD" . $this->general_lib->random_numbers(6);
        }

        if ($id != '' && $st != '') {
            $err = '';
            $code = '200';

            $updateStatus = "UPDATE tb_ukm_pra SET status='" . $st . "',keterangan='" . $ket . "',date_updated=NOW() WHERE id_ukm_pra='" . $id . "'";
            $this->db->query($updateStatus);

            if ($st == 1) {
                $sqlInsert = "INSERT INTO tb_ukm (kd_ukm, nm_ukm, provinsi, kota, alm_ukm,no_ktp,nm_pemilik,desc_ukm,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_anggota,id_rt,cabang_lpt,id_produsen,image_kk,image_keterangan_usaha,jenis_usaha,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha)
                VALUES ('" . $kd_ukm . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '0','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','','937','" . $cab . "','" . $ip . "','" . $fotokk . "','" . $fotoku . "','" . $ju . "','" . $ci . "','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "') ";
                $this->db->query($sqlInsert);

                $sUkm = "SELECT * FROM tb_ukm WHERE no_hp='" . $ph . "'";
                $qUkm = $this->db->query($sUkm);
                $rUkm = $qUkm->row_array();

                $updateUkm = "UPDATE tb_ukm SET id_agen='" . $rUkm['id_ukm'] . "' WHERE no_hp='" . $ph . "'";
                $this->db->query($updateUkm);

                // Insert Depo
                $sqlInsertDepo = "INSERT INTO tb_depo (kd_depo, nm_depo, provinsi, kota, alm_depo,no_ktp,nm_pemilik,desc_depo,alm_usaha,no_hp,email,foto,image_ktp,date_created, date_updated, tipe, status,latitude,kecamatan,kelurahan,no_npwp,no_kartu,password,real_password,jenis_kelamin,tgl_lahir,tempat_lahir,kode_pos,logo,bank,no_rekening,atas_nama,pin,real_pin,siup,tdp,ud,id_produsen,id_rt,id_ukm,image_kk,image_keterangan_usaha,jenis_usaha,country_id,nm_pemohon,provinsi_usaha,kota_usaha,kecamatan_usaha,kelurahan_usaha,kode_pos_usaha,bentuk_usaha)
                VALUES ('" . $kd_depo . "', '" . $nmu . "', '" . $pv . "', '" . $kt . "',  '" . $almt . "','" . $ktp . "','" . $nmp . "','" . $desc . "','" . $au . "','" . $ph . "','" . $em . "','" . $foto . "','" . $fotoktp . "',NOW(), NOW(), '" . $tp . "', '0','" . $lt . "','" . $kc . "','" . $kl . "','" . $npwp . "','" . $no_kartu . "','" . $pass . "','" . $password . "','" . $jk . "','" . $tgl . "','" . $tl . "','" . $pos . "','" . $fotousaha . "','" . $bk . "','" . $nr . "','" . $an . "','" . $pin . "','" . $real_pin . "','" . $siup . "','" . $tdp . "','" . $ud . "','" . $ip . "','937','" . $rUkm['id_ukm'] . "','" . $fotokk . "','" . $fotoku . "','" . $ju . "','" . $ci . "','" . $nmpm . "','" . $pvu . "','" . $ktu . "','" . $kcu . "','" . $klu . "','" . $posu . "','" . $bu . "') ";
                $this->db->query($sqlInsertDepo);

                $sDepo = "SELECT * FROM tb_depo WHERE no_hp='" . $ph . "'";
                $qDepo = $this->db->query($sDepo);
                $rDepo = $qDepo->row_array();

                $updateDepo = "UPDATE tb_depo SET id_agen='" . $rDepo['id_depo'] . "' WHERE no_hp='" . $ph . "'";
                $this->db->query($updateDepo);

                $message = '<html>
                            <head>
                                <title></title>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                <style type="text/css">
                                    /* CLIENT-SPECIFIC STYLES */
                                    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                    img { -ms-interpolation-mode: bicubic; }

                                    /* RESET STYLES */
                                    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                                    table { border-collapse: collapse !important; }
                                    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
                                    .button {
                                        background-color: #4CAF50; /* Green */
                                        border: none;
                                        color: white;
                                        padding: 15px 32px;
                                        text-align: center;
                                        text-decoration: none;
                                        display: inline-block;
                                        font-size: 16px;
                                        margin: 4px 2px;
                                        cursor: pointer;
                                        -webkit-transition-duration: 0.4s; /* Safari */
                                        transition-duration: 0.4s;
                                    }

                                    .button1:hover {
                                        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
                                    }

                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                        font-size: inherit !important;
                                        font-family: inherit !important;
                                        font-weight: inherit !important;
                                        line-height: inherit !important;
                                    }

                                    /* MEDIA QUERIES */
                                    @media screen and (max-width: 480px) {
                                        .mobile-hide {
                                            display: none !important;
                                        }
                                        .mobile-center {
                                            text-align: center !important;
                                        }
                                    }

                                    /* ANDROID CENTER FIX */
                                    div[style*="margin: 16px 0;"] { margin: 0 !important; }
                                </style>
                            <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                ' . /*headerEmail()*/ . '
                                                <tr style="border-bottom:solid 3px #eeeeee;">
                                                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                            <tr>
                                                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                    <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                                                        Halo ' . $nmp . ',
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Selamat akun Anda telah berhasil diverifikasi oleh tim Thoyyiban.
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Informasi Anda:
                                                                        <br>
                                                                    <table style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        <tr><td>Nama Lengkap</td><td>:</td><td>' . $nmp . '</td></tr>
                                                                        <tr><td>Email </td><td>:</td><td> ' . $em . '</td></tr>
                                                                        <tr><td>No Hp </td><td>:</td><td> ' . $ph . '</td></tr>
                                                                        <tr><td>Password </td><td>:</td><td> <b>' . $password . '</b> </td></tr>
                                                                        <tr><td>PIN </td><td>:</td><td> <b>' . $real_pin . '</b> </td></tr>
                                                                    </table>
                                                                    </p>
                                                                    <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;">
                                                                        Untuk login silahkan klik tombol dibawah.<br>
                                                                        <a href="https://halalthoyyiban.com/produsen/" target="_blank"><button class="button button1">Login</button></a>
                                                                    </p>
                                                                    <p style="color: #777777;">Salam hormat, <br>
                                                                       Halal Thoyyiban</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                ' . /*footerEmail()*/ . '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                        </html>';
                $addheader = "INFO";
                $subject = "REGISTER";
                //pushEmailServer($em, $nmp, $message, $addheader, $subject);
            }
            $sql = "SELECT a.*,
            (SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.provinsi) as nm_provinsi,
            (SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.kota) as nm_kota,
            (SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.kecamatan) as nm_kecamatan,
            (SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.kelurahan) as nm_kelurahan
             FROM tb_ukm_pra a
             WHERE a.id_ukm_pra='" . $id . "' ";

            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($id != '') {
            $sUpdate = "UPDATE tb_ukm SET status='" . $st . "',date_updated=NOW() WHERE id_ukm='" . $id . "'";
            $this->db->query($sUpdate);

            $updateDepo = "UPDATE tb_depo SET status='" . $st . "',date_updated=NOW() WHERE id_ukm='" . $id . "'";
            $this->db->query($updateDepo);

            $sql = "SELECT * FROM tb_ukm WHERE id_ukm = '" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatuszakat() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];
        $to = $parameter['to'];

        $sCek = "SELECT is_zakat,total_zakat FROM tb_ukm WHERE id_ukm='" . $id . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($st == '') {
            $st = $rCek['is_zakat'];
        }

        if ($to == '') {
            $to = $rCek['total_zakat'];
        }

        if ($id != '') {
            $sUpdate = "UPDATE tb_ukm SET is_zakat='" . $st . "',total_zakat='" . $to . "',date_updated=NOW() WHERE id_ukm='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_ukm WHERE id_ukm = '" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatustes() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        $sUkm = "SELECT * FROM tb_ukm WHERE id_ukm='" . $id . "'";
        $qUkm = $this->db->query($sUkm);
        $rUkm = $qUkm->row_array();

        if ($id != '') {
            $sUpdate = "UPDATE tb_ukm SET status_tes='" . $st . "',date_updated=NOW() WHERE id_ukm='" . $id . "'";
            $this->db->query($sUpdate);

            if ($st == 2) {
                $insertLog = "INSERT INTO tb_log_trans (url_api,req_api,res_api,tipe,status,date_created,date_updated)"
                        . "VALUES('" . $url . "','" . json_encode($fields) . "','" . $resp . "',1,1,NOW(),NOW())";
                $this->db->query($insertLog);

                $sUpdate = "UPDATE tb_ukm SET id_anggota='" . $id_anggota . "',status=1,date_updated=NOW() WHERE id_ukm='" . $id . "'";
                $this->db->query($sUpdate);

                $updateDepo = "UPDATE tb_depo SET status=1,date_updated=NOW() WHERE id_ukm='" . $id . "'";
                $this->db->query($updateDepo);
            }

            $sql = "SELECT * FROM tb_ukm WHERE id_ukm = '" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $sCek = "SELECT * FROM tb_ukm WHERE id_ukm='" . $id . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($id != '') {
            $sDelete = "DELETE FROM tb_ukm WHERE id_ukm='" . $id . "'";
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_ukm ORDER BY id_ukm DESC LIMIT 1";
            $this->response->getresponse($sql,'delete()');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatusdetail() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        $sCek = "SELECT * FROM tb_ukm_detail WHERE id_ukm_detail='" . $id . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($id != '') {
            $sUpdate = "UPDATE tb_ukm_detail SET status='" . $st . "' WHERE id_ukm_detail='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_ukm_detail WHERE id_ukm_detail = '" . $id . "'";
            $this->response->getresponse($sql,'updatestatusdetail');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function deletedetail() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $sCek = "SELECT * FROM tb_ukm_detail WHERE id_ukm_detail='" . $id . "'";
        $qCek = $this->db->query($sCek);
        $rCek = $qCek->row_array();

        if ($id != '') {
            $sDelete = "DELETE FROM tb_ukm_detail WHERE id_ukm_detail='" . $id . "'";
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_ukm_detail ORDER BY id_ukm_detail DESC LIMIT 1";
            $this->response->getresponse($sql,'deletedetail');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertdokumen() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $no = $parameter['no'];
        $nm = $parameter['nm'];
        $dk = $parameter['dk'];
        $ket = $parameter['ket'];
        $tp = $parameter['tp'];
        $id = $parameter['id'];

        if ($tp == '' && $id == '') {
            $tp = 1;
        }

        if ($id != '') {
            $sCek = "SELECT * FROM tb_ukm_dokumen WHERE id_dokumen='" . $id . "'";
            $qCek = $this->db->query($sCek);
            $rCek = $qCek->row_array();

            if ($iu == '') {
                $iu = $rCek['id_ukm'];
            }

            if ($no == '') {
                $no = $rCek['no_dokumen'];
            }

            if ($nm == '') {
                $nm = $rCek['nm_dokumen'];
            }

            if ($dk == '') {
                $dk = $rCek['dokumen'];
            }

            if ($ket == '') {
                $ket = $rCek['keterangan'];
            }

            if ($tp == '') {
                $tp = $rCek['tipe'];
            }
        }

        if ($iu != '' && $nm != '') {
            if ($id == '') {
                $insert = "INSERT INTO tb_ukm_dokumen (id_ukm,no_dokumen,nm_dokumen,dokumen,keterangan,date_created,date_updated,tipe,status) "
                        . "VALUES('" . $iu . "','" . $no . "','" . $nm . "','" . $dk . "','" . $ket . "',NOW(),NOW(),'" . $tp . "',1)";
                $this->db->query($insert);

                $sql = "SELECT * FROM tb_ukm_dokumen ORDER BY id_dokumen DESC LIMIT 1";
            } else {
                $update = "UPDATE tb_ukm_dokumen SET id_ukm='" . $iu . "',no_dokumen='" . $no . "',nm_dokumen='" . $nm . "',dokumen='" . $dk . "',keterangan='" . $ket . "',date_updated=NOW(),"
                        . "tipe='" . $tp . "' WHERE id_dokumen='" . $id . "'";
                $this->db->query($update);

                $sql = "SELECT * FROM tb_ukm_dokumen WHERE id_dokumen='" . $id . "'";
            }

            $this->response->getresponse($sql,'insertdokumen');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getdokumen() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $iu = $parameter['iu'];
        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $ukm = "";
        if ($iu != '') {
            $ukm = " AND a.id_ukm='" . $iu . "'";
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "'";
        }

        $limit = '';
        if ($lt != '') {
            $limit = ' LIMIT ' . $lt;
        }


        $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as nm_ukm,
                    IFNULL((SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as nm_pemilik,
                    IFNULL((SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as no_hp,
                    IFNULL((SELECT x.email FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as email
                    FROM tb_ukm_dokumen a WHERE a.id_dokumen!='' " . $ukm . $tipe . " ORDER BY a.date_created DESC " . $limit;

        $this->response->getresponse($sql,'getdokumen');
    }

    public function getdokumenbyid() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sql = "SELECT a.*,
                    IFNULL((SELECT x.nm_ukm FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as nm_ukm,
                    IFNULL((SELECT x.nm_pemilik FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as nm_pemilik,
                    IFNULL((SELECT x.no_hp FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as no_hp,
                    IFNULL((SELECT x.email FROM tb_ukm x WHERE x.id_ukm=a.id_ukm),'') as email
                    FROM tb_ukm_dokumen a WHERE a.id_dokumen='" . $id . "'";

            $this->response->getresponse($sql,'getdokumenbyid');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function deletedokumen() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $delete = "DELETE FROM tb_ukm_dokumen WHERE id_dokumen='" . $id . "'";
            $this->db->query($delete);

            $sql = "SELECT id_ukm FROM tb_ukm LIMIT 1";

            $this->response->getresponse($sql,'deletedokumen');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }


}
