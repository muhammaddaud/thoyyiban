<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{
    
    var $param;
    var $select;
    var $selecta;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }

        $select="id_customer, uuid, kd_customer, email, password, real_password, gelar, nm_korporasi, nm_customer, nm_toko, jenis_kelamin, tempat_lahir, tgl_lahir, id_provinsi, id_kota, id_kecamatan, id_kelurahan, kode_pos, telp, no_hp, pin, real_pin, saldo, last_saldo, komisi, no_kartu, alamat, deskripsi, nm_bank, no_rekening, atas_nama, image, image_toko, date_expired, date_created, date_updated, date_login, date_logout, tipe, status, latitude, is_from, is_login, is_ukm, is_register";

        $selecta="a.id_customer, a.uuid, a.kd_customer, a.email, a.password, a.real_password, a.gelar, a.nm_korporasi, a.nm_customer, a.nm_toko, a.jenis_kelamin, a.tempat_lahir, a.tgl_lahir, a.id_provinsi, a.id_kota, a.id_kecamatan, a.id_kelurahan, a.kode_pos, a.telp, a.no_hp, a.pin, a.real_pin, a.saldo, a.last_saldo, a.komisi, a.no_kartu, a.alamat, a.deskripsi, a.nm_bank, a.no_rekening, a.atas_nama, a.image, a.image_toko, a.date_expired, a.date_created, a.date_updated, a.date_login, a.date_logout, a.tipe, a.status, a.latitude, a.is_from, a.is_login, a.is_ukm, a.is_register";

        $this->select=$select;
        $this->selecta=$selecta;
    }

    public function loginstall() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $uuid = addslashes($parameter['uuid']);
        $fr = addslashes($parameter['fr']);
        $lat = addslashes($parameter['lat']);

        if ($fr == '') {
            $fr = 'Android';
        }

        if ($uuid != '' && $fr!='') {

            $sqlCheck1 = $this->db->query("SELECT id,latitude FROM tb_log_install WHERE uuid='" . $uuid . "'");
            $rCheck1 = $sqlCheck1->row_array();

            if($rCheck1['id']==''){
                    $sInsert = "INSERT INTO tb_log_install (uuid,is_from,latitude,date_created,date_updated, tipe, status)
                VALUES ('" . $uuid . "', '" . $fr . "','" . $lat . "',NOW(), NOW(), 1, 1)  ";
                $this->db->query($sInsert);
            }else{
                if($rCheck1['latitude']=='' && $lat!=''){
                    $update="UPDATE tb_log_install SET latitude='".$lat."',date_updated=NOW() WHERE uuid='".$uuid."'";
                }else{
                    $update="UPDATE tb_log_install SET date_updated=NOW() WHERE uuid='".$uuid."'";
                }
                
                $this->db->query($update);
            }
            
            $sql = "SELECT tb_log_install FROM tb_customer WHERE uuid='" . $uuid . "'";
            $this->response->getresponse($sql,'loginstall');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function register() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $hp = addslashes($parameter['hp']);
        $em = addslashes($parameter['em']);
        $almt = addslashes($parameter['almt']);
        $uuid = addslashes($parameter['uuid']);
        $fbt = addslashes($parameter['fbt']);
        $fr = addslashes($parameter['fr']);
        $lat = addslashes($parameter['lat']);
        $real_pin = addslashes($parameter['pin']);
        $password = addslashes($parameter['pass']);
        $de=date('Y-m-d', strtotime('+3 years'));

        if ($fr == '') {
            $fr = 'ANDROID';
        }

        if ($nm == '') {
            $code = '03';
            $status = 'Nama tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if ($hp == '') {
            $code = '03';
            $status = 'No handphone tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if ($password == '') {
            $code = '03';
            $status = 'Password tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if (strlen($nm) < 3) {
            $code = "07";
            $status = "Nama minimal tiga huruf";
            $this->general_lib->error($code,$status);
        }

        if (strlen($nm) > 50) {
            $code = "07";
            $status = "Nama maksimal 50 huruf";
            $this->general_lib->error($code,$status);
        }

        if(is_numeric($nm)==TRUE){
            $code = "07";
            $status = "Nama tidak boleh hanya angka";
            $this->general_lib->error($code,$status);
        }

        $pattern = "/([a-zA-Z].*?){3}/";
        if (preg_match($pattern, $nm)==FALSE) {
            $code = "07";
            $status = "Nama minimal tiga huruf";
            $this->general_lib->error($code,$status);
        }

        if(is_numeric($hp)==FALSE){
            $code = "07";
            $status = "No handphone hanya boleh angka";
            $this->general_lib->error($code,$status);
        }

        if (strlen($hp) < 10 || strlen($hp) > 14) {
            $code = "07";
            $status = "Format no handphone tidak valid";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password tidak valid, minimal 6 digit huruf atau angka";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) > 50) {
            $code = "07";
            $status = "Password tidak valid, maksimal 50 digit huruf atau angka";
            $this->general_lib->error($code,$status);
        }

        if ($hp != '' && $nm!='') {

            $sqlCheck1 = $this->db->query("SELECT id_customer,status,is_register FROM tb_customer WHERE email='" . $em . "'");
            $rCheck1 = $sqlCheck1->row_array();

            $sqlCheck2 = $this->db->query("SELECT id_customer,status,is_register FROM tb_customer WHERE no_hp='" . $hp . "'");
            $rCheck2 = $sqlCheck2->row_array();

            if ($rCheck2['id_customer'] != '' && $hp != '') {
                $code = "06";
                $status = "No Handphone sudah terdaftar";
                $this->general_lib->error($code,$status);
            }

            if ($rCheck1['id_customer'] != '' && $em != '') {
                $code = "06";
                $status = "Email sudah terdaftar";
                $this->general_lib->error($code,$status);
            }

            $cross = "BN";
            $kode_agen = $cross . $this->general_lib->random_numbers(6);

            $no_kartu = $this->general_lib->random_numbers(2).date('YmdHis');
            if($real_pin==''){
                $real_pin = $this->general_lib->random_numbers(6);
            }
            $pin = md5(hash('sha512', $real_pin));
            
            if($password==''){
                $password = $this->general_lib->random_numbers(6);
            }
            $pass = md5(hash('sha512', $password));

            
            $sInsert = "INSERT INTO tb_customer (kd_customer, no_hp,real_pin,pin,email,latitude,uuid,is_login,
            date_created,date_updated, tipe, status,password,real_password,nm_customer,no_kartu,is_from,is_register,alamat,date_expired,fb_token)
            VALUES ('" . $kode_agen . "', '" . $hp . "','" . $real_pin . "','" . $pin . "','" . $em . "','" . $lat . "','" . $uuid . "',1,NOW(), NOW(), 1, 1, '" . $pass . "', '" . $password . "', '" . $nm . "','" . $no_kartu . "','".$fr."',1,'".$almt."','".$de."','".$fbt."')  ";
            $this->db->query($sInsert);
            
            $sql = "SELECT ".$this->select." FROM tb_customer WHERE no_hp='" . $hp . "'";
            $this->response->getresponse($sql,'register');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatecustomer() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $hp = addslashes($parameter['hp']);
        $em = addslashes($parameter['em']);
        $jk = addslashes($parameter['jk']);
        $kl = addslashes($parameter['kl']);
        $almt = addslashes($parameter['almt']);
        $pos = addslashes($parameter['pos']);
        $fbt = addslashes($parameter['fbt']);
        $id = $parameter['id'];

        $sCek = $this->db->query("SELECT ".$this->select." FROM tb_customer WHERE id_customer='" . $id . "'");
        $rCek = $sCek->row_array();

        if ($rCek['email'] != $em && $em != '') {
            $sCekEmail = $this->db->query("SELECT id_customer FROM tb_customer WHERE email='" . $em . "'");
            $rCekEmail = $sCekEmail->row_array();

            if ($rCekEmail['id_customer'] != '') {
                $code = "04";
                $status = "Email sudah terdaftar";
                $this->general_lib->error($code,$status);
            }
        }

        if ($rCek['no_hp'] != $hp && $hp != '') {
            $sCekHp = $this->db->query("SELECT id_customer FROM tb_customer WHERE no_hp='" . $hp . "'");
            $rCekHp = $sCekHp->row_array();

            if ($rCekHp['id_customer'] != '') {
                $code = "05";
                $status = "No handphone sudah terdaftar";
                $this->general_lib->error($code,$status);
            }
        }

        if ($em == '') {
            $em = $rCek['email'];
        }

        if ($hp == '') {
            $hp = $rCek['no_hp'];
        }

        if ($jk == '') {
            $jk = $rCek['jenis_kelamin'];
        }

        if ($nm == '') {
            $nm = $rCek['nm_customer'];
        }

        if ($kl == '') {
            $kl = $rCek['id_kelurahan'];
        }

        if ($almt == '') {
            $almt = $rCek['alamat'];
        }

        if ($pos == '') {
            $pos = $rCek['kode_pos'];
        }

        if ($fbt == '') {
            $fbt = $rCek['fb_token'];
        }

        if ($kl != '') {
            $sqlKel = $this->db->query("SELECT id_kelurahan,id_kecamatan FROM tb_kelurahan WHERE id_kelurahan='" . $kl . "' ");
            $rKel = $sqlKel->row_array();

            $sqlKec = $this->db->query("SELECT id_kecamatan,id_kota FROM tb_kecamatan WHERE id_kecamatan='" . $rKel['id_kecamatan'] . "' ");
            $rKec = $sqlKec->row_array();

            $sqlKota = $this->db->query("SELECT id_kota,id_provinsi FROM tb_kota WHERE id_kota='" . $rKec ['id_kota'] . "' ");
            $rKota = $sqlKota->row_array();

            $sqlProv = $this->db->query("SELECT id_provinsi FROM tb_provinsi WHERE id_provinsi='" . $rKota['id_provinsi'] . "' ");
            $rProv = $sqlProv->row_array();

            $kc = $rKec['id_kecamatan'];
            $kt = $rKota['id_kota'];
            $pv = $rProv['id_kota'];
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if (strlen($nm) < 3) {
            $code = "07";
            $status = "Nama minimal tiga huruf";
            $this->general_lib->error($code,$status);
        }

        if (strlen($nm) > 50) {
            $code = "07";
            $status = "Nama maksimal 50 huruf";
            $this->general_lib->error($code,$status);
        }

        $pattern = "/([a-zA-Z].*?){3}/";
        if (preg_match($pattern, $nm)==FALSE) {
            $code = "07";
            $status = "Nama minimal tiga huruf";
            $this->general_lib->error($code,$status);
        }

        if(is_numeric($nm)==TRUE){
            $code = "07";
            $status = "Nama tidak boleh hanya angka";
            $this->general_lib->error($code,$status);
        }

        if(is_numeric($hp)==FALSE){
            $code = "07";
            $status = "No handphone hanya boleh angka";
            $this->general_lib->error($code,$status);
        }

        if (strlen($hp) < 10 || strlen($hp) > 14) {
            $code = "07";
            $status = "Format no handphone tidak valid";
            $this->general_lib->error($code,$status);
        }

        if (!filter_var($em, FILTER_VALIDATE_EMAIL) && $em!='') {
            $code = "07";
            $status = "Format email tidak valid";
            $this->general_lib->error($code,$status);
        }

        if ($id != '' && $nm!=''  && $hp!='') {
            
            $update = "UPDATE tb_customer SET nm_customer='".$nm."',no_hp='".$hp."',email='".$em."',jenis_kelamin='".$jk."',id_kelurahan='".$kl."',id_kecamatan='".$kc."',id_kota='".$kt."',id_provinsi='".$pv."',alamat='".$almt."',kode_pos='".$pos."',image='".$foto."',fb_token='".$fbt."',date_updated=NOW() WHERE id_customer='".$id."'";
            $this->db->query($update);
            
            $sql = "SELECT ".$this->select." FROM tb_customer WHERE id_customer='" . $id . "'";
            $this->response->getresponse($sql,'updatecustomer');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatefoto() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sCek = $this->db->query("SELECT ".$this->select." FROM tb_customer WHERE id_customer='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($id != '') {
            $update = "UPDATE tb_customer SET image='".$foto."',date_updated=NOW() WHERE id_customer='".$id."'";
            $this->db->query($update);
            
            $sql = "SELECT ".$this->select." FROM tb_customer WHERE id_customer='" . $id . "'";
            $this->response->getresponse($sql,'updatefoto');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function login() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);
        $password = addslashes($parameter['pass']);
        $lat = $parameter['lat'];
        $uuid = $parameter['uuid'];
        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT id_customer FROM tb_customer WHERE email='" . $em . "'");
        $rCekUser = $sCekUser->row_array();

        if($em==''){
            $code = '03';
            $status = 'No handphone tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if($password==''){
            $code = '03';
            $status = 'Password tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if (strlen($em) < 10 || strlen($em) > 14) {
            $code = "07";
            $status = "Format no handphone tidak valid";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password salah";
            $this->general_lib->error($code,$status);
        }

        if ($rCekUser['id_customer'] != '') {
            $param_user = " a.email='" . $em . "' ";
            $param_user2 = " email='" . $em . "' ";
        } else {
            $param_user = " a.no_hp='" . $em . "' ";
            $param_user2 = " no_hp='" . $em . "' ";
        }

        $sqlCekPass = "SELECT id_customer,real_password FROM tb_customer WHERE " . $param_user2 . " ";
        $qCekPass = $this->db->query($sqlCekPass);
        $rCekPass = $qCekPass->row_array();

        $sqlCek = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a
             WHERE " . $param_user . "
             AND a.password='" . $pass . "'";
        $qCek = $this->db->query($sqlCek);
        $rCek = $qCek->row_array();
        if ($uuid == '') {
            $uuid = $rCek['uuid'];
        }

        if ($lat == '') {
            $lat = $rCek['latitude'];
        }

        if ($em != '' && $pass != '') {
            if ($rCekPass['id_customer'] == '') {
                $code = '03';
                $status = 'No handphone belum terdaftar';
                $this->general_lib->error($code,$status);
            }else if ($rCekPass['real_password'] != $password) {
                $code = '03';
                $status = 'Password salah';
                $this->general_lib->error($code,$status);
            }else if ($rCek['id_customer'] == '') {
                $code = '03';
                $status = 'Username & password invalid.';
                $this->general_lib->error($code,$status);
            } else if ($rCek['status'] == '0') {
                $code = '04';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            } else if ($rCek['tipe'] != '1') {
                $code = '04';
                $status = 'Username & password invalid.';
                $this->general_lib->error($code,$status);
            } else {
                $update = "UPDATE tb_customer SET is_login=1,is_register=1,uuid='" . $uuid . "',latitude='" . $lat . "',date_updated=NOW(),date_login=NOW() WHERE id_customer='" . $rCek['id_customer'] . "' ";
                $this->db->query($update);

                $this->response->getresponse($sqlCek,'login');
            }
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function loginweb() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $em = addslashes($parameter['em']);
        $password = addslashes($parameter['pass']);
        $lat = $parameter['lat'];
        $uuid = $parameter['uuid'];
        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT id_customer FROM tb_customer WHERE email='" . $em . "'");
        $rCekUser = $sCekUser->row_array();

        if($em==''){
            $code = '03';
            $status = 'No handphone atau email tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if($password==''){
            $code = '03';
            $status = 'Password tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password salah";
            $this->general_lib->error($code,$status);
        }

        if ($rCekUser['id_customer'] != '') {
            $param_user = " a.email='" . $em . "' ";
            $param_user2 = " email='" . $em . "' ";
        } else {
            $param_user = " a.no_hp='" . $em . "' ";
            $param_user2 = " no_hp='" . $em . "' ";
        }

        $sqlCekPass = "SELECT id_customer,real_password FROM tb_customer WHERE " . $param_user2 . " ";
        $qCekPass = $this->db->query($sqlCekPass);
        $rCekPass = $qCekPass->row_array();

        $sqlCek = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a
             WHERE " . $param_user . "
             AND a.password='" . $pass . "'";
        $qCek = $this->db->query($sqlCek);
        $rCek = $qCek->row_array();
        if ($uuid == '') {
            $uuid = $rCek['uuid'];
        }

        if ($lat == '') {
            $lat = $rCek['latitude'];
        }

        if ($em != '' && $pass != '') {
            if ($rCekPass['id_customer'] == '') {
                $code = '03';
                $status = 'No handphone belum terdaftar';
                $this->general_lib->error($code,$status);
            }else if ($rCekPass['real_password'] != $password) {
                $code = '03';
                $status = 'Password salah';
                $this->general_lib->error($code,$status);
            }else if ($rCek['id_customer'] == '') {
                $code = '03';
                $status = 'Username & password invalid.';
                $this->general_lib->error($code,$status);
            } else if ($rCek['status'] == '0') {
                $code = '04';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            } else if ($rCek['tipe'] != '1') {
                $code = '04';
                $status = 'Username & password invalid.';
                $this->general_lib->error($code,$status);
            } else {
                $update = "UPDATE tb_customer SET is_login=1,is_register=1,uuid='" . $uuid . "',latitude='" . $lat . "',date_updated=NOW(),date_login=NOW() WHERE id_customer='" . $rCek['id_customer'] . "' ";
                $this->db->query($update);

                $this->response->getresponse($sqlCek,'login');
            }
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function logout() {
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $update = "UPDATE tb_customer SET is_login=0,date_updated=NOW(),date_logout=NOW() WHERE id_customer='" . $id . "' ";
            $this->db->query($update);

            $sql = "SELECT * FROM tb_customer WHERE id_customer='" . $id . "'";
            $this->response->getresponse($sql,'logout');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function getcustomercms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];
        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT ".$this->selecta.",
        IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
        IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
        IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
        IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
         FROM tb_customer a WHERE a.id_customer!='' ORDER BY a.date_created DESC ".$limit;

        $this->response->getresponse($sql,'getcustomercms');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = $this->db->query("SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a WHERE a.id_customer='" . $id . "'");
            $a = 0;
            foreach ($sql->result_array() as $row) {
                $sql2 = $this->db->query("SELECT id, id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status FROM tb_customer_detail WHERE id_customer='".$row['id_customer']."' AND tipe=1 ORDER BY id ASC ");
                $arrayAsal = array();
                $b = 0;
                foreach ($sql2->result_array() as $row2) {
                    $arrayAsal[$b] = $row2;
                    $b++;
                }

                $sql3 = $this->db->query("SELECT id, id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status FROM tb_customer_detail WHERE id_customer='".$row['id_customer']."' AND tipe=2 ORDER BY id ASC ");
                $arrayTuj = array();
                $c = 0;
                foreach ($sql3->result_array() as $row3) {
                    $arrayTuj[$c] = $row3;
                    $c++;
                }

                $result[$a] = $row;
                $result[$a]['alamat_asal'] = $arrayAsal;
                $result[$a]['alamat_tujuan'] = $arrayTuj;
                $a++;
            }

            $str = array(
                "result" => [],
                "code" => "201",
                "message" => 'Data not found.'
            );
            $rCek2 = $sql->row_array();
            if ($rCek2['id_customer'] != '') {
                $str = array(
                    "result" => $result,
                    "code" => "200",
                    "message" => 'Succes action getbyid'
                );
            }
            $json = json_encode($str);

            header("Content-Type: application/json");
            ob_clean();
            flush();
            echo $json;
            exit(1);
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyhp() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];

        if($hp!=''){
            $sql = $this->db->query("SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a WHERE a.no_hp='" . $hp . "'");
            $a = 0;
            foreach ($sql->result_array() as $row) {
                $sql2 = $this->db->query("SELECT id, id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status FROM tb_customer_detail WHERE id_customer='".$row['id_customer']."' AND tipe=1 ORDER BY id ASC ");
                $arrayAsal = array();
                $b = 0;
                foreach ($sql2->result_array() as $row2) {
                    $arrayAsal[$b] = $row2;
                    $b++;
                }

                $sql3 = $this->db->query("SELECT id, id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status FROM tb_customer_detail WHERE id_customer='".$row['id_customer']."' AND tipe=2 ORDER BY id ASC ");
                $arrayTuj = array();
                $c = 0;
                foreach ($sql3->result_array() as $row3) {
                    $arrayTuj[$c] = $row3;
                    $c++;
                }

                $result[$a] = $row;
                $result[$a]['alamat_asal'] = $arrayAsal;
                $result[$a]['alamat_tujuan'] = $arrayTuj;
                $a++;
            }

            $str = array(
                "result" => [],
                "code" => "201",
                "message" => 'Data not found.'
            );
            $rCek2 = $sql->row_array();
            if ($rCek2['id_customer'] != '') {
                $str = array(
                    "result" => $result,
                    "code" => "200",
                    "message" => 'Succes action getbyhp'
                );
            }
            $json = json_encode($str);

            header("Content-Type: application/json");
            ob_clean();
            flush();
            echo $json;
            exit(1);
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function setpassword() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $password = addslashes($parameter['pass']);
        $id = $parameter['id'];
        $hp = $parameter['hp'];

        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT id_customer,is_register FROM tb_customer WHERE id_customer='" . $id . "'");
        if($id=='' && $hp!=''){
            $sCekUser = $this->db->query("SELECT id_customer,is_register FROM tb_customer WHERE no_hp='" . $hp . "'");    
        }
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_customer'] == '') {
            $code = '04';
            $status = "Data customer tidak ditemukan";
            $this->general_lib->error($code,$status);
        }

        if ($rCekUser['is_register'] == 1) {
            $code = '04';
            $status = "Password telah disetting";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password tidak valid, minimal 6 digit huruf atau angka";
            $this->general_lib->error($code,$status);
        }

        if ($password != '' && $id != '') {
            $update = "UPDATE tb_customer set password='" . $pass . "', real_password ='" . $password . "',is_register=1,
                date_updated=NOW() WHERE id_customer ='" . $id . "'";
            $this->db->query($update);

            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a
             WHERE a.id_customer='" . $id . "'";
            $this->response->getresponse($sql,'setpassword');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepassword() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $old_password = addslashes($parameter['old_pass']);
        $password = addslashes($parameter['pass']);
        $id = $parameter['id'];

        $pass = md5(hash('sha512', $password));

        $sCekUser = $this->db->query("SELECT id_customer FROM tb_customer WHERE id_customer='" . $id . "' AND real_password='" . $old_password . "'");
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_customer'] == '') {
            $code = '04';
            $status = "Password lama yang Anda masukan salah";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) < 6) {
            $code = "07";
            $status = "Password tidak valid, minimal 6 digit huruf atau angka";
            $this->general_lib->error($code,$status);
        }

        if (strlen($password) > 50) {
            $code = "07";
            $status = "Password tidak valid, maksimal 50 digit huruf atau angka";
            $this->general_lib->error($code,$status);
        }

        if ($old_password != '' && $password != '' && $id != '') {
            $update = "UPDATE tb_customer set password='" . $pass . "', real_password ='" . $password . "',
                date_updated=NOW() WHERE id_customer ='" . $id . "'";
            $this->db->query($update);

            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a
             WHERE a.id_customer='" . $id . "'";
            $this->response->getresponse($sql,'changepassword');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function changepin() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $old_pin = $parameter['old_pin'];
        $real_pin = $parameter['pin'];
        $id = $parameter['id'];

        $pin = md5(hash('sha512', $real_pin));

        $sCekUser = $this->db->query("SELECT id_customer FROM tb_customer WHERE id_customer='" . $id . "' AND real_pin='" . $old_pin . "'");
        $rCekUser = $sCekUser->row_array();

        if ($rCekUser['id_customer'] == '') {
            $code = '04';
            $status = "PIN lama yang Anda masukan salah";
            $this->general_lib->error($code,$status);
        }

        if (is_numeric($real_pin) == FALSE) {
            $code = "07";
            $status = "PIN harus berupa angka";
            $this->general_lib->error($code,$status);
        }

        if (strlen($real_pin) != 6) {
            $code = "07";
            $status = "PIN minimal 6 digit angka";
            $this->general_lib->error($code,$status);
        }

        if ($old_pin != '' && $real_pin != '' && $id != '') {
            $update = "UPDATE tb_customer set pin='" . $pin . "', real_pin ='" . $real_pin . "',
                date_updated=NOW() WHERE id_customer ='" . $id . "'";
            $this->db->query($update);

            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
            IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
            IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
             FROM tb_customer a
             WHERE a.id_customer='" . $id . "'";
            $this->response->getresponse($sql,'changepin');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_customer SET status='" . $st . "' WHERE id_customer='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_customer WHERE id_customer='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_customer WHERE id_customer='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_customer ORDER BY id_customer DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
