<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Places extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->load->model('response');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function getplaces() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $qy = $parameter['qy'];

        if($qy!=''){
            $url ="https://maps.googleapis.com/maps/api/place/autocomplete/json?input=".$qy."&components=country:ID&key=AIzaSyCqvFeXeNMAHIHJK5Abq17UBN42V5ZYmWw";
            $resp = file_get_contents($url);
            echo $resp;
            exit();
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
