<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertkategori() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $nm_eng = addslashes($parameter['nm_eng']);
        $nm_ar = addslashes($parameter['nm_ar']);
        $de = addslashes($parameter['de']);
        $de_eng = addslashes($parameter['de_eng']);
        $de_ar = addslashes($parameter['de_ar']);
        $id = $parameter['id'];
        $st = $parameter['st'];
        $tp = $parameter['tp'];
        $ut = $parameter['ut'];
        $lk = $parameter['lk'];

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_kategori WHERE id_kategori='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile_image4 = $parameter['image_eng'];
        $binary_image4 = base64_decode($datafile_image4);
        $namefile_image4 = $parameter['filename_eng'];
        if ($namefile_image4 != '') {
            $target_dir_image4 = $this->general_lib->path();

            if (!file_exists($target_dir_image4)) {
                mkdir($target_dir_image4, 0777, true);
            }

            $url_path_image4 = "upload/";

            $target_path_image4 = $target_dir_image4;
            $now_image4 = date('YmdHis');
            $rand_image4 = rand(1111, 9999);
            $generatefile_image4 = $now_image4 . $rand_image4;
            $namefile_image4 = $generatefile_image4 . ".jpeg";
            $target_path_image4 = $target_path_image4 . $namefile_image4;

            chmod($target_path_image4, 0777);
            $fh_image4 = fopen($target_path_image4, 'w') or die("can't open file");
            chmod($target_path_image4, 0777);
            fwrite($fh_image4, $binary_image4);
            fclose($fh_image4);

            sleep(1);

            $foto_eng = "upload/" . $namefile_image4;
        }
        if ($namefile_image4 == '') {
            $foto_eng = "";
        }

        if ($foto_eng != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto_eng,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto_eng);
        }

        if ($foto_eng == '') {
            $foto_eng = $rCek['image_eng'];
        }

//Upload image5 -----------------------------------------------------------------------

        $datafile_image5 = $parameter['image_ar'];
        $binary_image5 = base64_decode($datafile_image5);
        $namefile_image5 = $parameter['filename_ar'];
        if ($namefile_image5 != '') {
            $target_dir_image5 = $this->general_lib->path();

            if (!file_exists($target_dir_image5)) {
                mkdir($target_dir_image5, 0777, true);
            }

            $url_path_image5 = "upload/";

            $target_path_image5 = $target_dir_image5;
            $now_image5 = date('YmdHis');
            $rand_image5 = rand(1111, 9999);
            $generatefile_image5 = $now_image5 . $rand_image5;
            $namefile_image5 = $generatefile_image5 . ".jpeg";
            $target_path_image5 = $target_path_image5 . $namefile_image5;

            chmod($target_path_image5, 0777);
            $fh_image5 = fopen($target_path_image5, 'w') or die("can't open file");
            chmod($target_path_image5, 0777);
            fwrite($fh_image5, $binary_image5);
            fclose($fh_image5);

            sleep(1);

            $foto_ar = "upload/" . $namefile_image5;
        }
        if ($namefile_image5 == '') {
            $foto_ar = "";
        }

        if ($foto_ar != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto_ar,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto_ar);
        }

        if ($foto_ar == '') {
            $foto_ar = $rCek['image_ar'];
        }

        $datafile2 = $parameter['image_bg'];
        $binary2 = base64_decode($datafile2);
        $namefile2 = $parameter['filename_bg'];
        if ($namefile2 != '') {
            $target_dir2 = $this->general_lib->path();

            if (!file_exists($target_dir2)) {
                mkdir($target_dir2, 0777, true);
            }

            $target_path2 = $target_dir2;
            $now2 = date('YmdHis');
            $rand2 = rand(1111, 9999);
            $generatefile2 = $now2 . $rand2;
            $namefile2 = $generatefile2 . ".jpeg";
            $target_path2 = $target_path2 . $namefile2;

            chmod($target_path2, 0777);
            $fh2 = fopen($target_path2, 'w') or die("can't open file");
            chmod($target_path2, 0777);
            fwrite($fh2, $binary2);
            fclose($fh2);

            sleep(1);

            $foto2 = "upload/" . $namefile2;
        }

        if ($namefile2 == '') {
            $foto2 = "";
        }

        if ($foto2 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto2,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto2);
        }

        if ($foto2 == '') {
            $foto2 = $rCek['image_background'];
        }

        if ($nm != '' && $de != '') {
            if ($id == '') {
                $sInsert = "INSERT INTO tb_kategori (nm_kategori,nm_kategori_eng,nm_kategori_ar,deskripsi,deskripsi_eng,deskripsi_ar,date_created,date_updated,status,tipe,
                    image,image_eng,image_ar,urutan,link,image_background)
                            VALUES
                            ('" . $nm . "','" . $nm_eng . "','" . $nm_ar . "','" . $de . "','" . $de_eng . "','" . $de_ar . "',NOW(),NOW(),'" . $st . "','" . $tp . "',"
                        . "'" . $foto . "','" . $foto_eng . "','" . $foto_ar . "','" . $ut . "','" . $lk . "','" . $foto2 . "')";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_kategori ORDER BY date_created DESC LIMIT 1";
            } else {
                
                $sUpdate = "UPDATE tb_kategori SET nm_kategori = '" . $nm . "',nm_kategori_eng = '" . $nm_eng . "',nm_kategori_ar = '" . $nm_ar . "',deskripsi = '" . $de . "',"
                        . "image = '" . $foto . "',tipe = '" . $tp . "',deskripsi_eng = '" . $de_eng . "',deskripsi_ar = '" . $de_ar . "',status = '" . $st . "',urutan = '" . $ut . "',"
                        . "link = '" . $lk . "',image_eng = '" . $foto_eng . "',image_ar = '" . $foto_ar . "',image_background='" . $foto2 . "',date_updated=NOW() WHERE id_kategori = '" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_kategori WHERE id_kategori = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertkategori');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getkategori() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_kategori=a.id_kategori) as jumlah_info
                    FROM tb_kategori a WHERE a.status='1' AND a.tipe='1' ORDER BY a.urutan ASC ".$limit;

        if ($tp != '') {
            $sql = "SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_kategori=a.id_kategori) as jumlah_info
                    FROM tb_kategori a WHERE a.status='1' AND a.tipe='" . $tp . "' ORDER BY a.urutan ASC ".$limit;
        }
         $this->response->getresponse($sql,'getkategori');
    }

    public function getkategoricms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_kategori ORDER BY date_created DESC ".$limit;

        if ($tp != '') {
            $sql = "SELECT * FROM tb_kategori WHERE tipe='" . $tp . "' ORDER BY date_created DESC ".$limit;
        }
        $this->response->getresponse($sql,'getkategoricms');
    }

    public function getbytipe() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];

        if($tp!=''){
            $sql = "SELECT * FROM tb_kategori WHERE tipe='" . $tp . "' ORDER BY date_created DESC ";
            $this->response->getresponse($sql,'getbytipe');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbytipeaktif() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];

        if($tp!=''){
            $sql = "SELECT a.*,
                    (SELECT COUNT(x.id_info) as total FROM tb_info x WHERE x.id_kategori=a.id_kategori AND x.status=1)  as total_info
                    FROM tb_kategori a WHERE a.tipe='" . $tp . "' AND a.status=1 ORDER BY a.urutan ASC ";
            $this->response->getresponse($sql,'getbytipeaktif');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_kategori WHERE id_kategori='" . $id . "' ORDER BY date_created DESC ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getklasifikasi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $url = $this->general_lib->url_thoyyiban() . 'api/kategori_api.php?action=get_kategori_klasifikasi'.$this->general_lib->key_thoyyiban();
        $resp = $this->general_lib->general_http($url, '');
        echo $resp;
        exit();
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_kategori SET status='" . $st . "' WHERE id_kategori='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_kategori WHERE id_kategori='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function deleteimage() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $img=$parameter['img'];

        if ($img != '' && $id != '') {
            $sUpdate = "UPDATE tb_kategori SET ".$img."='' WHERE id_kategori='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_kategori WHERE id_kategori='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_kategori WHERE id_kategori='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_kategori ORDER BY id_kategori DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
