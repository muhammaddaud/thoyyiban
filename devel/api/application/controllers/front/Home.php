<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function gethome(){

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $lt = $parameter['lt'];
        $lat = $parameter['lat'];
        $uuid = $parameter['uuid'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlSlide = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='7' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlide = array();
        $a = 0;
        foreach ($sqlSlide->result_array() as $rowSlide) {
            $arraySlide[$a] = $rowSlide;
            $a++;
        }

        $sqlSlide->free_result();

        $sqlSlideTeng = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='9' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlideTeng = array();
        $b = 0;
        foreach ($sqlSlideTeng->result_array() as $rowSlideTeng) {
            $arraySlideTeng[$b] = $rowSlideTeng;
            $b++;
        }

        $sqlSlideTeng->free_result();

        $sqlSlideBawah = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='10' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlideBawah = array();
        $c = 0;
        foreach ($sqlSlideBawah->result_array() as $rowSlideBawah) {
            $arraySlideBawah[$c] = $rowSlideBawah;
            $c++;
        }

        $sqlSlideBawah->free_result();

        $sqlMenu = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=2 AND status=1 ORDER BY urutan ASC");
        $arrayMenu = array();
        $d = 0;
        foreach ($sqlMenu->result_array() as $rowMenu) {
            $arrayMenu[$d] = $rowMenu;
            $d++;
        }

        $sqlMenu->free_result();

        $sqlKatProd = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=5 AND status=1 ORDER BY urutan ASC");
        $arrayKatProd = array();
        $e = 0;
        foreach ($sqlKatProd->result_array() as $rowKatProd) {
            $arrayKatProd[$e] = $rowKatProd;
            $e++;
        }

        $sqlKatProd->free_result();

        $sqlKatVideo = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=14 AND status=1 ORDER BY urutan ASC");
        $arrayKatVideo = array();
        $f = 0;
        foreach ($sqlKatVideo->result_array() as $rowKatVideo) {
            $arrayKatVideo[$f] = $rowKatVideo;
            $f++;
        }

        $sqlKatVideo->free_result();

        $sqlInbox = $this->db->query("SELECT a.*,
            IFNULL( (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as nm_customer,
            IFNULL( (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as no_hp,
            IFNULL( (SELECT x.email FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as email
            FROM tb_notif a WHERE a.status=1 AND (a.id_customer='" . $ic . "' OR a.id_customer=0) ORDER BY a.id_notif DESC " . $limit);
        $arrayInbox = array();
        $g = 0;
        foreach ($sqlInbox->result_array() as $rowInbox) {
            $arrayInbox[$g] = $rowInbox;
            $g++;
        }

        $sqlInbox->free_result();

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => '10',
            'fr' => 'SH'
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProd = array();
        $h = 0;
        foreach ($encode['result'] as $rowProd) {
            $arrayProd[$h] = $rowProd;
            $h++;
        }

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => '10',
            'ir' => '1',
            'fr' => 'SH'
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProdRek = array();
        $i = 0;
        foreach ($encode['result'] as $rowProdRek) {
            $arrayProdRek[$i] = $rowProdRek;
            $i++;
        }

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => '10',
            'ord' => '1',
            'fr' => 'SH'
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProdLow = array();
        $j = 0;
        foreach ($encode['result'] as $rowProdLow) {
            $arrayProdLow[$j] = $rowProdLow;
            $j++;
        }

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => '10',
            'ord' => '2',
            'fr' => 'SH'
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProdFav = array();
        $k = 0;
        foreach ($encode['result'] as $rowProdFav) {
            $arrayProdFav[$k] = $rowProdFav;
            $k++;
        }

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE status='1' order by nm_provinsi asc");
        $arrayProv = array();
        $l = 0;
        foreach ($sqlProv->result_array() as $rowProv) {
            $arrayProv[$l] = $rowProv;
            $l++;
        }

        $sqlProv->free_result();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE status='1' order by nm_kota asc");
        $arrayKota = array();
        $m = 0;
        foreach ($sqlKota->result_array() as $rowKota) {
            $arrayKota[$m] = $rowKota;
            $m++;
        }

        $sqlKota->free_result();

        $sqlHelp = $this->db->query("SELECT title_info,desc_info FROM tb_info WHERE status='1' AND tipe=4 order by urutan asc");
        $arrayHelp = array();
        $n = 0;
        foreach ($sqlHelp->result_array() as $rowHelp) {
            $arrayHelp[$n] = $rowHelp;
            $n++;
        }

        $sqlHelp->free_result();

        $sqlInovasi = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=9 ORDER BY a.id_info DESC LIMIT 5");
        $arrayInovasi = array();
        $o = 0;
        foreach ($sqlInovasi->result_array() as $rowInovasi) {
            $arrayInovasi[$o] = $rowInovasi;
            $o++;
        }

        $sqlInovasi->free_result();

        $sqlEdukasi = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=1 ORDER BY a.id_info DESC LIMIT 5");
        $arrayEdukasi = array();
        $p = 0;
        foreach ($sqlEdukasi->result_array() as $rowEdukasi) {
            $arrayEdukasi[$p] = $rowEdukasi;
            $p++;
        }

        $sqlEdukasi->free_result();

        $sqlVideo = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 ORDER BY a.id_info DESC LIMIT 5");
        $arrayVideo = array();
        $q = 0;
        foreach ($sqlVideo->result_array() as $rowVideo) {
            $arrayVideo[$q] = $rowVideo;
            $q++;
        }

        $sqlVideo->free_result();

        $sqlDownload = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.title_image='link_download_halalthoyyiban' ORDER BY a.id_info DESC ".$limit);
        $arrayDownload = array();
        $r = 0;
        foreach ($sqlDownload->result_array() as $rowDownload) {
            $arrayDownload[$r] = $rowDownload;
            $r++;
        }

        $sqlDownload->free_result();

        $result[0]['slide'] = $arraySlide;
        $result[0]['slide_tengah'] = $arraySlideTeng;
        $result[0]['slide_bawah'] = $arraySlideBawah;
        $result[0]['menu'] = $arrayMenu;
        $result[0]['kategori_produk'] = $arrayKatProd;
        $result[0]['kategori_video'] = $arrayKatVideo;
        $result[0]['produk'] = $arrayProd;
        $result[0]['produk_rekomendasi'] = $arrayProdRek;
        $result[0]['produk_favorite'] = $arrayProdFav;
        $result[0]['produk_low_price'] = $arrayProdLow;
        $result[0]['inbox'] = $arrayInbox;
        $result[0]['help'] = $arrayHelp;
        $result[0]['inovasi'] = $arrayInovasi;
        $result[0]['edukasi'] = $arrayEdukasi;
        $result[0]['video'] = $arrayVideo;
        $result[0]['download'] = $arrayDownload;
        $result[0]['provinsi'] = $arrayProv;
        $result[0]['kota'] = $arrayKota;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action get_home'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function gethometv(){

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $lt = $parameter['lt'];
        $lat = $parameter['lat'];
        $uuid = $parameter['uuid'];

        if($lt==''){
            $lt=20;
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlHeadline = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.is_headline=1 ORDER BY a.id_info DESC ".$limit);
        $arrayHeadline = array();
        $a = 0;
        foreach ($sqlHeadline->result_array() as $rowHeadline) {
            $arrayHeadline[$a] = $rowHeadline;
            $a++;
        }

        $sqlHeadline->free_result();

        $sqlTerbaru = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 ORDER BY a.id_info DESC ".$limit);
        $arrayTerbaru = array();
        $b = 0;
        foreach ($sqlTerbaru->result_array() as $rowTerbaru) {
            $arrayTerbaru[$b] = $rowTerbaru;
            $b++;
        }

        $sqlTerbaru->free_result();

        $sqlPopuler = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 ORDER BY a.total_read DESC ".$limit);
        $arrayPopuler = array();
        $c = 0;
        foreach ($sqlPopuler->result_array() as $rowPopuler) {
            $arrayPopuler[$c] = $rowPopuler;
            $c++;
        }

        $sqlPopuler->free_result();

        $sqlTokoh = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_tokoh=a.id_tokoh AND x.status=1) as total_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                    FROM tb_tokoh a WHERE a.status='1' ORDER BY a.urutan ASC ".$limit);
        $arrayTokoh = array();
        $d = 0;
        foreach ($sqlTokoh->result_array() as $rowTokoh) {
            $arrayTokoh[$d] = $rowTokoh;
            $d++;
        }

        $sqlTokoh->free_result();

        $sqlMenu = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=2 AND status=1 ORDER BY urutan ASC");
        $arrayMenu = array();
        $e = 0;
        foreach ($sqlMenu->result_array() as $rowMenu) {
            $arrayMenu[$e] = $rowMenu;
            $e++;
        }

        $sqlMenu->free_result();

        $sqlKatVideo = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=14 AND status=1 ORDER BY urutan ASC");
        $arrayKatVideo = array();
        $f = 0;
        foreach ($sqlKatVideo->result_array() as $rowKatVideo) {
            $arrayKatVideo[$f] = $rowKatVideo;
            $f++;
        }

        $sqlKatVideo->free_result();

        $suuid=" a.uuid='" . $uuid . "' ";
        if($ic!='' && $ic>0){
            $suuid=" (a.uuid='" . $uuid . "' OR a.id_customer='".$ic."' ) ";
        }

        $sqlInbox = $this->db->query("SELECT a.*,
            IFNULL( (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as nm_customer,
            IFNULL( (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as no_hp,
            IFNULL( (SELECT x.email FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as email
            FROM tb_notif a WHERE a.status=1 AND ".$suuid." AND a.status_read IN(0,1) ORDER BY a.id_notif DESC " . $limit);
        $arrayInbox = array();
        $g = 0;
        foreach ($sqlInbox->result_array() as $rowInbox) {
            $arrayInbox[$g] = $rowInbox;
            $g++;
        }

        $sqlInbox->free_result();

        $sqlProv = $this->db->query("SELECT * FROM tb_provinsi WHERE status='1' order by nm_provinsi asc");
        $arrayProv = array();
        $h = 0;
        foreach ($sqlProv->result_array() as $rowProv) {
            $arrayProv[$h] = $rowProv;
            $h++;
        }

        $sqlProv->free_result();

        $sqlKota = $this->db->query("SELECT * FROM tb_kota WHERE status='1' order by nm_kota asc");
        $arrayKota = array();
        $i = 0;
        foreach ($sqlKota->result_array() as $rowKota) {
            $arrayKota[$i] = $rowKota;
            $i++;
        }

        $sqlKota->free_result();

        $sqlSlide = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='12' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlide = array();
        $j = 0;
        foreach ($sqlSlide->result_array() as $rowSlide) {
            $arraySlide[$j] = $rowSlide;
            $j++;
        }

        $sqlSlide->free_result();

        $sqlRekomendasi = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.is_rekomendasi=1 ORDER BY a.date_created DESC ".$limit);
        $arrayRekomendasi = array();
        $k = 0;
        foreach ($sqlRekomendasi->result_array() as $rowRekomendasi) {
            $arrayRekomendasi[$k] = $rowRekomendasi;
            $k++;
        }

        $sqlRekomendasi->free_result();

        $sqlHelp = $this->db->query("SELECT title_info,desc_info FROM tb_info WHERE status='1' AND tipe=4 order by urutan asc");
        $arrayHelp = array();
        $l = 0;
        foreach ($sqlHelp->result_array() as $rowHelp) {
            $arrayHelp[$l] = $rowHelp;
            $l++;
        }

        $sqlHelp->free_result();

        $sqlSlideAkun = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='13' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlideAkun = array();
        $l = 0;
        foreach ($sqlSlideAkun->result_array() as $rowSlideAkun) {
            $arraySlideAkun[$l] = $rowSlideAkun;
            $l++;
        }

        $sqlSlideAkun->free_result();

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => '20',
            'fr' => 'SH'
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProd = array();
        $m = 0;
        foreach ($encode['result'] as $rowProd) {
            $arrayProd[$m] = $rowProd;
            $m++;
        }

        $sqlSlideIklan = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='4' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlideIklan = array();
        $n = 0;
        foreach ($sqlSlideIklan->result_array() as $rowSlideIklan) {
            $arraySlideIklan[$n] = $rowSlideIklan;
            $n++;
        }

        $sqlSlideIklan->free_result();

        $sqlParenting = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_subkategori=156 ORDER BY a.date_created DESC ".$limit);
        $arrayParenting = array();
        $o = 0;
        foreach ($sqlParenting->result_array() as $rowParenting) {
            $arrayParenting[$o] = $rowParenting;
            $o++;
        }

        $sqlParenting->free_result();

        $sqlMemasak = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.id_subkategori=172 ORDER BY a.date_created DESC ".$limit);
        $arrayMemasak = array();
        $p = 0;
        foreach ($sqlMemasak->result_array() as $rowMemasak) {
            $arrayMemasak[$p] = $rowMemasak;
            $p++;
        }

        $sqlMemasak->free_result();

        $sqlMenuLogistik = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=15 AND status=1 ORDER BY urutan ASC");
        $arrayMenuLogistik = array();
        $q = 0;
        foreach ($sqlMenuLogistik->result_array() as $rowMenuLogistik) {
            $arrayMenuLogistik[$q] = $rowMenuLogistik;
            $q++;
        }

        $sqlMenuLogistik->free_result();

        $sqlSlideParenting = $this->db->query("SELECT * FROM tb_sitepage WHERE tipe='14' AND status ='1' ORDER BY urutan ASC ".$limit);
        $arraySlideParenting = array();
        $r = 0;
        foreach ($sqlSlideParenting->result_array() as $rowSlideParenting) {
            $arraySlideParenting[$r] = $rowSlideParenting;
            $r++;
        }

        $sqlSlideParenting->free_result();

        $sqlFloating = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=11 ORDER BY a.date_created DESC LIMIT 1");
        $arrayFloating = array();
        $s = 0;
        foreach ($sqlFloating->result_array() as $rowFloating) {
            $arrayFloating[$s] = $rowFloating;
            $s++;
        }

        $sqlFloating->free_result();

        $result[0]['slide_login'] = $arraySlide;
        $result[0]['slide_akun'] = $arraySlideAkun;
        $result[0]['slide_iklan'] = $arraySlideIklan;
        $result[0]['slide_parenting'] = $arraySlideParenting;
        $result[0]['headline'] = $arrayHeadline;
        $result[0]['terbaru'] = $arrayTerbaru;
        $result[0]['populer'] = $arrayPopuler;
        $result[0]['rekomendasi'] = $arrayRekomendasi;
        $result[0]['parenting'] = $arrayParenting;
        $result[0]['memasak'] = $arrayMemasak;
        $result[0]['floating'] = $arrayFloating;
        $result[0]['tokoh'] = $arrayTokoh;
        $result[0]['menu'] = $arrayMenu;
        $result[0]['menu_logistik'] = $arrayMenuLogistik;
        $result[0]['kategori_video'] = $arrayKatVideo;
        $result[0]['produk'] = $arrayProd;
        $result[0]['help'] = $arrayHelp;
        $result[0]['inbox'] = $arrayInbox;
        $result[0]['provinsi'] = $arrayProv;
        $result[0]['kota'] = $arrayKota;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action gethometv'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function gethometvweb(){

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $lt = $parameter['lt'];
        $lat = $parameter['lat'];
        $uuid = $parameter['uuid'];

        if($lt==''){
            $lt=10;
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $sqlHeadline = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.is_headline=1 ORDER BY a.id_info DESC ".$limit);
        $arrayHeadline = array();
        $a = 0;
        foreach ($sqlHeadline->result_array() as $rowHeadline) {
            $arrayHeadline[$a] = $rowHeadline;
            $a++;
        }

        $sqlHeadline->free_result();

        $sqlTerbaru = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 ORDER BY a.id_info DESC ".$limit);
        $arrayTerbaru = array();
        $b = 0;
        foreach ($sqlTerbaru->result_array() as $rowTerbaru) {
            $arrayTerbaru[$b] = $rowTerbaru;
            $b++;
        }

        $sqlTerbaru->free_result();

        $sqlPopuler = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 ORDER BY a.total_read DESC ".$limit);
        $arrayPopuler = array();
        $c = 0;
        foreach ($sqlPopuler->result_array() as $rowPopuler) {
            $arrayPopuler[$c] = $rowPopuler;
            $c++;
        }

        $sqlPopuler->free_result();

        $sqlMenu = $this->db->query("SELECT * FROM tb_kategori WHERE tipe=2 AND status=1 ORDER BY urutan ASC");
        $arrayMenu = array();
        $e = 0;
        foreach ($sqlMenu->result_array() as $rowMenu) {
            $arrayMenu[$e] = $rowMenu;
            $e++;
        }

        $sqlMenu->free_result();

        $sqlKatVideo = $this->db->query("SELECT a.*,
        (SELECT count(x.id_info) FROM tb_info x WHERE x.id_kategori=a.id_kategori AND x.status=1) as total_video
        FROM tb_kategori a WHERE a.tipe=14 AND a.status=1 ORDER BY a.urutan ASC");
        $arrayKatVideo = array();
        $f = 0;
        foreach ($sqlKatVideo->result_array() as $rowKatVideo) {
            $sqlVideo = $this->db->query("SELECT a.*,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                    IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                    IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                    IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                    IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                    IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                    FROM tb_info a
                    WHERE a.status=1 AND a.tipe=5 AND a.id_kategori='".$rowKatVideo['id_kategori']."' ORDER BY a.id_info DESC ".$limit) ;
            $arrayVideo = array();
            $b = 0;
            foreach ($sqlVideo->result_array() as $rowVideo) {
                $arrayVideo[$b] = $rowVideo;
                $b++;
            }
            $sqlVideo->free_result();
            $arrayKatVideo[$f] = $rowKatVideo;
            $arrayKatVideo[$f]['video'] = $arrayVideo;
            $f++;
        }

        $sqlKatVideo->free_result();

        $sqlInbox = $this->db->query("SELECT a.*,
            IFNULL( (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as nm_customer,
            IFNULL( (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as no_hp,
            IFNULL( (SELECT x.email FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as email
            FROM tb_notif a WHERE a.status=1 AND (a.id_customer='" . $ic . "' OR a.id_customer=0) ORDER BY a.id_notif DESC " . $limit);
        $arrayInbox = array();
        $g = 0;
        foreach ($sqlInbox->result_array() as $rowInbox) {
            $arrayInbox[$g] = $rowInbox;
            $g++;
        }

        $sqlInbox->free_result();

        $sqlTokoh = $this->db->query("SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_tokoh=a.id_tokoh AND x.status=1) as total_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                    FROM tb_tokoh a WHERE a.status='1' ORDER BY a.urutan ASC ".$limit);
        $arrayTokoh = array();
        $j = 0;
        foreach ($sqlTokoh->result_array() as $rowTokoh) {
            $arrayTokoh[$j] = $rowTokoh;
            $j++;
        }

        $sqlTokoh->free_result();

        $sqlDownload = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.title_image='link_download_halalthoyyiban' ORDER BY a.id_info DESC ".$limit);
        $arrayDownload = array();
        $k = 0;
        foreach ($sqlDownload->result_array() as $rowDownload) {
            $arrayDownload[$k] = $rowDownload;
            $k++;
        }

        $sqlDownload->free_result();

        $sqlRekomendasi = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar,
                IFNULL((SELECT x.nm_tokoh FROM tb_tokoh x WHERE x.id_tokoh=a.id_tokoh),'') as nm_tokoh,
                IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi),'') as nm_provinsi,
                IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota),'') as nm_kota,
                IFNULL((SELECT x.nm_kecamatan FROM tb_kecamatan x WHERE x.id_kecamatan=a.id_kecamatan),'') as nm_kecamatan,
                IFNULL((SELECT x.nm_kelurahan FROM tb_kelurahan x WHERE x.id_kelurahan=a.id_kelurahan),'') as nm_kelurahan
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND a.is_rekomendasi=1 ORDER BY a.urutan ASC ".$limit);
        $arrayRekomendasi = array();
        $l = 0;
        foreach ($sqlRekomendasi->result_array() as $rowRekomendasi) {
            $arrayRekomendasi[$l] = $rowRekomendasi;
            $l++;
        }

        $sqlRekomendasi->free_result();

        $result[0]['headline'] = $arrayHeadline;
        $result[0]['terbaru'] = $arrayTerbaru;
        $result[0]['populer'] = $arrayPopuler;
        $result[0]['rekomendasi'] = $arrayRekomendasi;
        $result[0]['menu'] = $arrayMenu;
        $result[0]['kategori_video'] = $arrayKatVideo;
        $result[0]['tokoh'] = $arrayTokoh;
        $result[0]['download'] = $arrayDownload;
        $result[0]['inbox'] = $arrayInbox;

        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action gethometvweb'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function search(){

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $qy = $parameter['qy'];
        $lt = $parameter['lt'];
        $lat = $parameter['lat'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'lt' => $lt,
            'nm' => $qy,
            'lat' => $lat,
            'fr' => 'SH'
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProd = array();
        $d = 0;
        foreach ($encode['result'] as $rowProd) {
            $arrayProd[$d] = $rowProd;
            $d++;
        }

        $sqlPromo = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=6 AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' )  ORDER BY a.date_created DESC " . $limit);
        $arrayPromo = array();
        $a = 0;
        foreach ($sqlPromo->result_array() as $rowPromo) {
            $arrayPromo[$a] = $rowPromo;
            $a++;
        }

        $sqlPromo->free_result();

        $sqlVideo = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=5 AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' )  ORDER BY a.date_created DESC " . $limit);
        $arrayVideo = array();
        $b = 0;
        foreach ($sqlVideo->result_array() as $rowVideo) {
            $arrayVideo[$b] = $rowVideo;
            $b++;
        }

        $sqlVideo->free_result();

        $sqlBerita = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=1 AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' )  ORDER BY a.date_created DESC " . $limit);
        $arrayBerita = array();
        $c = 0;
        foreach ($sqlBerita->result_array() as $rowBerita) {
            $arrayBerita[$c] = $rowBerita;
            $c++;
        }

        $sqlBerita->free_result();

        $sqlInovasi = $this->db->query("SELECT a.*,
                IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                FROM tb_info a
                WHERE a.status=1 AND a.tipe=9 AND (a.title_info LIKE '%" . $qy . "%' OR a.desc_info LIKE '%" . $qy . "%' )  ORDER BY a.date_created DESC " . $limit);
        $arrayInovasi = array();
        $e = 0;
        foreach ($sqlInovasi->result_array() as $rowInovasi) {
            $arrayInovasi[$e] = $rowInovasi;
            $e++;
        }

        $sqlInovasi->free_result();

        $result[0]['produk'] = $arrayProd;
        $result[0]['promo'] = $arrayPromo;
        $result[0]['video'] = $arrayVideo;
        $result[0]['berita'] = $arrayBerita;
        $result[0]['inovasi'] = $arrayInovasi;
        $str = array(
            "result" => $result,
            "code" => "200",
            "message" => 'Succes action search'
        );

        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }
}
