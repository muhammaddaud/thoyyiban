<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Livechat extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function sendmessage() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id_u = $parameter['id_u'];
        $id_m = $parameter['id_m'];
        $msg = addslashes($parameter['msg']);

        if ($id_u != '' && $msg != '') {

            $sInsert = "INSERT INTO `tb_user_chat_message`(`id_user_u`, `id_user_a`, `message`, `tipe`, `status`, `date_created`, `date_updated`) "
                . "VALUES ('" . $id_u . "','0','" . $msg . "','2','1',NOW(),NOW())";
            $this->db->query($sInsert);
            
            $sql = "SELECT * FROM tb_user_chat_message WHERE id_user_u='" . $id_u . "' AND id_message > " . $id_m . " ORDER BY date_created asc  ";
            $this->response->getresponse($sql,'sendmessage');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getlistuser() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $sql = "SELECT a.id_user_chat as id,a.nama,a.email,count(b.id_user_u) as jmchat,MAX(b.date_created) as tgl,
                (SELECT c.message FROM tb_user_chat_message c WHERE c.date_created = MAX(b.date_created) limit 1) as message,
                (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.tipe = 1 limit 1) as noreply1,
                (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.tipe = 2 limit 1) as noreply2,
                (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.id_message > (SELECT MAX(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u  AND d.tipe = 2 limit 1) limit 1) as noreply,
                (CASE
                 WHEN (( (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.tipe = 1 limit 1) > 0)
                 &&   ((SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.tipe = 2 limit 1) = 0)) THEN 1
                 WHEN (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.id_message > (SELECT MAX(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u  AND d.tipe = 2 limit 1) limit 1) > 0 THEN 2
                 ELSE 0
                END) as urutan
                FROM tb_user_chat a
                LEFT JOIN tb_user_chat_message b
                ON(a.id_user_chat = b.id_user_u)
                GROUP BY a.id_user_chat
                ORDER BY (urutan = 0), (urutan) ASC,tgl DESC, a.id_user_chat DESC";
        $this->response->getresponse($sql,'getlistuser');
    }

    public function getlistchat() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_user_chat_message WHERE id_user_u='" . $id . "' ORDER BY date_created asc";
            $this->response->getresponse($sql,'getlistchat');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getlastmessage() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id_u = $parameter['id_u'];
        $id_m = intval($parameter['id_m']);

        if($id_u!=''){
            $sql = "SELECT * FROM tb_user_chat_message WHERE id_user_u='" . $id_u . "' AND id_message > " . $id_m . " ORDER BY date_created asc";
            $this->response->getresponse($sql,'getlastmessage');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getnotreply() {

        $sql = $this->db->query("SELECT (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.id_message > 
            (SELECT MAX(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u  AND d.tipe = 2 limit 1) limit 1) as jm
            FROM tb_user_chat a
            GROUP BY a.id_user_chat
            ORDER BY a.id_user_chat DESC ");

        $sql2 = $this->db->query("SELECT
            (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.tipe = 1 limit 1) as noreply1,
            (SELECT count(d.id_message) FROM tb_user_chat_message d WHERE a.id_user_chat = d.id_user_u AND d.tipe = 2 limit 1) as noreply2
            FROM tb_user_chat a
            GROUP BY a.id_user_chat
            ORDER BY (noreply1+noreply2 = 0), (noreply1+noreply2) ASC");

        $jm = 0;
        foreach ($sql->result_array() as $rHistory) {
            if ($rHistory['jm'] > 0) {
                $jm++;
            }
        }

        foreach ($sql2->result_array() as $rHistory2) {
            if (($rHistory2['noreply1'] > 0) && ($rHistory2['noreply2'] == 0)) {
                $jm++;
            }
        }

        $str = array(
            "jm" => $jm,
            "code" => 200,
            "message" => 'OKOK'
        );

        $json = json_encode($str);
        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

}
