<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tokoh extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function inserttokoh() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $de = addslashes($parameter['de']);
        $de_eng = addslashes($parameter['de_eng']);
        $de_ar = addslashes($parameter['de_ar']);
        $jb = addslashes($parameter['jb']);
        $id = $parameter['id'];
        $st = $parameter['st'];
        $tp = $parameter['tp'];
        $ut = $parameter['ut'];
        $ik = $parameter['ik'];
        $is = $parameter['is'];

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_tokoh WHERE id_tokoh='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($nm != '' && $de != '') {
            if ($id == '') {
                $sInsert = "INSERT INTO tb_tokoh (id_kategori,id_subkategori,nm_tokoh,deskripsi,deskripsi_eng,deskripsi_ar,date_created,date_updated,status,tipe,image,urutan,jabatan)
                            VALUES
                            ('" . $ik . "','" . $is . "','" . $nm . "','" . $de . "','" . $de_eng . "','" . $de_ar . "',NOW(),NOW(),'" . $st . "','" . $tp . "','" . $foto . "','" . $ut . "','" . $jb . "')";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_tokoh ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_tokoh SET nm_tokoh = '" . $nm . "',deskripsi = '" . $de . "',deskripsi_ar = '" . $de_ar . "',tipe = '" . $tp . "',deskripsi_eng = '" . $de_eng . "',status = '" . $st . "',urutan = '" . $ut . "',id_kategori = '" . $ik . "',image = '" . $foto . "',id_subkategori = '" . $is . "',jabatan = '" . $jb . "',date_updated=NOW() WHERE id_tokoh = '" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_tokoh WHERE id_tokoh = '" . $id . "'";
            }

            $this->response->getresponse($sql,'inserttokoh');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function gettokoh() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $lt = $parameter['lt'];

        if($ik==''){
            $ik = $this->uri->segment(3);
        }

        if($is==''){
            $is = $this->uri->segment(4);
        }

        $sub="";
        if($is!=''){
            $sub=" AND a.id_subkategori='".$is."' ";
        }

        $kat="";
        if($ik!=''){
            $kat=" AND a.id_kategori='".$ik."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_tokoh=a.id_tokoh AND x.status=1) as total_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                    FROM tb_tokoh a WHERE a.status='1' ".$kat.$sub."  ORDER BY a.urutan ASC ".$limit;
         $this->response->getresponse($sql,'gettokoh');
    }

    public function gettokohcms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $is = $parameter['is'];
        $lt = $parameter['lt'];

        if($ik==''){
            $ik = $this->uri->segment(3);
        }

        if($is==''){
            $is = $this->uri->segment(4);
        }

        $sub="";
        if($is!=''){
            $sub=" AND a.id_subkategori='".$is."' ";
        }

        $kat="";
        if($ik!=''){
            $kat=" AND a.id_kategori='".$ik."' ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_tokoh=a.id_tokoh AND x.status=1) as total_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                    FROM tb_tokoh a WHERE a.id_tokoh!='' ".$kat.$sub."  ORDER BY a.date_created DESC ".$limit;
        $this->response->getresponse($sql,'gettokohcms');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT a.*,
                    (SELECT COUNT(x.id_info) AS jumlah FROM tb_info x WHERE x.id_tokoh=a.id_tokoh AND x.status=1) as total_info,
                    IFNULL((SELECT x.nm_kategori FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori,
                    IFNULL((SELECT x.nm_kategori_eng FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_eng,
                    IFNULL((SELECT x.nm_kategori_ar FROM tb_kategori x WHERE x.id_kategori=a.id_kategori),'') as nm_kategori_ar,
                    IFNULL((SELECT x.nm_subkategori FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori,
                    IFNULL((SELECT x.nm_subkategori_eng FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_eng,
                    IFNULL((SELECT x.nm_subkategori_ar FROM tb_subkategori x WHERE x.id_subkategori=a.id_subkategori),'') as nm_subkategori_ar
                    FROM tb_tokoh a WHERE a.id_tokoh='".$id."' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_tokoh SET status='" . $st . "' WHERE id_tokoh='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_tokoh WHERE id_tokoh='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_tokoh WHERE id_tokoh='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_tokoh ORDER BY id_tokoh DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
