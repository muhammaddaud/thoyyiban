<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Testimoni extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function inserttestimoni() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = $parameter['nm'];
        $ts = $parameter['ts'];
        $pk = $parameter['pk'];
        $st = $parameter['st'];
        $tp = $parameter['tp'];
        $id = $parameter['id'];

        if ($tp == '') {
            $tp = 1;
        }

        $sCek = $this->db->query("SELECT * FROM tb_testimoni WHERE id_testimoni='" . $id . "'");
        $rCek = $sCek->row_array();

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        if ($nm != '') {
            if ($id == '') {
                $sInsert = "INSERT INTO tb_testimoni
                         (nama,pekerjaan,testimoni,image,date_created,date_updated,tipe,status)
                         VALUES
                         ('" . $nm . "','" . $pk . "','" . $ts . "','" . $foto . "',NOW(),NOW(),'" . $tp . "','" . $st . "')";
                $this->db->query($sInsert);

                $sql = "SELECT * FROM tb_testimoni ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_testimoni SET nama='" . $nm . "',pekerjaan='" . $pk . "',image='" . $foto . "',testimoni='" . $ts . "',
                date_updated=NOW(),tipe='" . $tp . "',status='" . $st . "' WHERE id_testimoni='" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_testimoni WHERE id_testimoni = '" . $id . "'";
            }

            $this->response->getresponse($sql,'inserttestimoni');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function gettestimoni() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_testimoni WHERE status=1 ORDER BY date_created DESC ".$limit;
         $this->response->getresponse($sql,'gettestimoni');
    }

    public function gettestimonicms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sql = "SELECT * FROM tb_testimoni ORDER BY date_created DESC ".$limit;
        $this->response->getresponse($sql,'gettestimonicms');
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            $sql = "SELECT * FROM tb_testimoni WHERE id_testimoni='" . $id . "' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_testimoni SET status='" . $st . "' WHERE id_testimoni='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_testimoni WHERE id_testimoni='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_testimoni WHERE id_testimoni='" . $id . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_testimoni ORDER BY id_testimoni DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
