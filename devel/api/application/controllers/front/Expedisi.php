<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Expedisi extends CI_Controller
{
    
    var $param;
    var $select;
    var $selecta;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }

        $select="id_trans, id_customer, id_layanan, id_provinsi_asal, id_provinsi_tujuan, id_kota_asal, id_kota_tujuan, latitude_asal, no_trans, no_resi, nm_barang, jumlah_barang, berat, volume, satuan, tipe_satuan, total_harga, harga_awal, diskon, image, image_terima, tipe_paket, tipe_pengiriman, tipe_pengiriman_kurir, tipe_pickup, tipe_payment, tipe_order, estimasi, nm_pengirim, no_hp_pengirim, email_pengirim, alamat_pengirim, nm_penerima, no_hp_penerima, email_penerima, alamat_penerima, kode_pos, keterangan, keterangan_batal, keterangan_pending, keterangan_rating, rating, date_created, date_updated, tipe, status";

        $selecta="a.id_trans, a.id_customer, a.id_layanan, a.id_provinsi_asal, a.id_provinsi_tujuan, a.id_kota_asal, a.id_kota_tujuan, a.latitude_asal, a.no_trans, a.no_resi, a.nm_barang, a.jumlah_barang, a.berat, a.volume, a.satuan, a.tipe_satuan, a.total_harga, a.harga_awal, a.diskon, a.image, a.image_terima, a.tipe_paket, a.tipe_pengiriman, a.tipe_pengiriman_kurir, a.tipe_pickup, a.tipe_payment, a.tipe_order, a.estimasi, a.nm_pengirim, a.no_hp_pengirim, a.email_pengirim, a.alamat_pengirim, a.nm_penerima, a.no_hp_penerima, a.email_penerima, a.alamat_penerima, a.kode_pos, a.keterangan, a.keterangan_batal, a.keterangan_pending, a.keterangan_rating, a.rating, a.date_created, a.date_updated, a.tipe, a.status";

        $this->select=$select;
        $this->selecta=$selecta;
    }

    public function cektarif() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ika = $parameter['ika'];
        $ikt = $parameter['ikt'];
        $pj = $parameter['pj'];
        $lb = $parameter['lb'];
        $tg = $parameter['tg'];
        $br = $parameter['br'];

        if($ika!='' && $ikt!='' && $br!=''){
            $url = $this->general_lib->url_api_baraka() . 'v2/partner/gettarif';
            $fields = array(
                'ika' => $ika,
                'ikt' => $ikt,
                'pj' => $pj,
                'lb' => $lb,
                'tg' => $tg,
                'br' => $br
            );
            $resp = $this->general_lib->push_http_baraka($url, $fields);
            echo $resp;
            exit();
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function order() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $noTrans = date('YmdHms').rand(111,999);
        $ic = $parameter['ic'];
        $nb = addslashes($parameter['nb']);
        $jb = $parameter['jb'];
        $to = $parameter['to'];
        $hr = $parameter['to'];
        $ds = $parameter['ds'];
        $tp = $parameter['tp'];
        $br = $parameter['br'];
        $pj = $parameter['pj'];
        $lb = $parameter['lb'];
        $tg = $parameter['tg'];
        $via = $parameter['via'];
        $ika = $parameter['ika'];
        $ikt = $parameter['ikt'];
        $kwt = $parameter['kwt'];
        $idk = $parameter['idk'];
        $ket = $parameter['ket'];
        $tpu = $parameter['tpu'];
        $nm_peng = addslashes($parameter['nm_peng']);
        $hp_peng = addslashes($parameter['hp_peng']);
        $em_peng = addslashes($parameter['em_peng']);
        $almt_peng = addslashes($parameter['almt_peng']);
        $nm_pene = addslashes($parameter['nm_pene']);
        $hp_pene = addslashes($parameter['hp_pene']);
        $em_pene = addslashes($parameter['em_pene']);
        $almt_pene = addslashes($parameter['almt_pene']);
        $stn = $parameter['stn'];
        $lat = $parameter['lat'];
        $fr = $parameter['fr'];
        $kali = ($pj*$lb*$tg)/4000;
        $vl = round($kali,2);

        if ($fr == '') {
            $fr = 'ANDROID';
        }

        if ($tp == '') {
            $tp = 'TUNAI';
        }

        if ($via == '') {
            $via = 'Darat';
        }

        if ($ds == '') {
            $ds = 0;
        }

        if ($stn == '') {
            $stn = 'KG';
        }

        $tpk=1;
        $tpn=1;
        if($via=='Laut'){
            $tpn=2;
        }elseif($via=='Udara'){
            $tpn=3;
        }

        $btkKiriman = 'Berat';
        if($vl>$br){
            $btkKiriman = 'Volume';
        }
        
        $sKotAsal = $this->db->query("SELECT a.id_kota,a.nm_kota,a.cabang_id,d.id_provinsi,d.nm_provinsi
                    FROM tb_kota a
                    LEFT JOIN tb_provinsi d
                    ON a.id_provinsi=d.id_provinsi
                    WHERE a.id_kota='" . $ika . "'");
        $rKotAsal = $sKotAsal->row_array();

        $sKotTuj = $this->db->query("SELECT a.id_kota,a.nm_kota,a.cabang_id,d.id_provinsi,d.nm_provinsi
                FROM tb_kota a
                LEFT JOIN tb_provinsi d
                ON a.id_provinsi=d.id_provinsi
                WHERE a.id_kota='" . $ikt . "'");
        $rKotTuj = $sKotTuj->row_array();

        $ipa = $rKotAsal['id_provinsi'];
        $ipt = $rKotTuj['id_provinsi'];

        if($ic!=''){
            $sCust = $this->db->query("SELECT id_customer,status FROM tb_customer WHERE id_customer='" . $ic . "'");
            $rCust = $sCust->row_array();

            if ($rCust['id_customer'] == '') {
                $code = '02';
                $status = 'Data customer tidak ditemukan';
                $this->general_lib->error($code,$status);
            }

            if ($rCust['status'] == 0) {
                $code = '02';
                $status = 'Customer tidak aktif';
                $this->general_lib->error($code,$status);
            }
        }elseif($hp_peng!=''){
            $sCust = $this->db->query("SELECT id_customer,status FROM tb_customer WHERE no_hp='" . $hp_peng . "'");
            $rCust = $sCust->row_array();

            $ic=$rCust['id_customer'];

            if($rCust['id_customer']=='' && $nm_peng!=''){
                $cross = "BN";
                $kode_agen = $cross . $this->general_lib->random_numbers(6);

                $no_kartu = $this->general_lib->random_numbers(12);
                $real_pin = $this->general_lib->random_numbers(6);
                $pin = md5(hash('sha512', $real_pin));
                
                $password = $this->general_lib->random_numbers(6);
                $pass = md5(hash('sha512', $password));

                $sInsert = "INSERT INTO tb_customer (kd_customer, no_hp,real_pin,pin,email,latitude,uuid,is_login,
                date_created,date_updated, tipe, status,password,real_password,nm_customer,no_kartu,is_from,is_register)
                VALUES ('" . $kode_agen . "', '" . $hp_peng . "','" . $real_pin . "','" . $pin . "','" . $em_peng . "','" . $lat . "','" . $uuid . "',1,NOW(), NOW(), 1, 1, '" . $pass . "', '" . $password . "', '" . $nm_peng . "','" . $no_kartu . "','".$fr."',0)  ";
                $this->db->query($sInsert);

                $sCust = $this->db->query("SELECT id_customer,status FROM tb_customer WHERE no_hp='" . $hp_peng . "'");
                $rCust = $sCust->row_array();

                $ic=$rCust['id_customer'];
            }
        }
        

        if ($to != '' && $nm_peng!='' && $hp_peng!='' && $almt_peng!='' && $nm_pene!='' && $hp_pene!='' && $almt_pene!='' && $ika!='' && $ikt!='' && $br!='') {
            $url = $this->general_lib->url_api_baraka() . 'v2/partner/inserttrans';
            $fields = array(
                'no' => $noTrans,
                'nb' => $nb,
                'jb' => $jb,
                'to' => $to,
                'tp' => $tp,
                'br' => $br,
                'pj' => $pj,
                'lb' => $lb,
                'tg' => $tg,
                'via' => $via,
                'ika' => $ika,
                'ikt' => $ikt,
                'kwt' => '',
                'idk' => '',
                'ket' => $ket,
                'tpu' => $tpu,
                'nm_peng' => $nm_peng,
                'hp_peng' => $hp_peng,
                'em_peng' => $em_peng,
                'almt_peng' => $almt_peng,
                'nm_pene' => $nm_pene,
                'hp_pene' => $hp_pene,
                'em_pene' => $em_pene,
                'almt_pene' => $almt_pene,
                'lat' => $lat,
            );
            $resp = $this->general_lib->push_http_baraka($url, $fields);
            $encode = json_decode($resp, true);
            $nores = $encode['result'][0]['no_resi'];
            $leadtime = $encode['result'][0]['etimasi'];

            if($encode['code']!=200){
                $code = '03';
                $status = 'Order pengiriman gagal, mohon ulangi beberapa saat lagi';
                $this->general_lib->error($code,$status);
            }

            $sInsertTrans = "INSERT INTO tb_trans (id_customer,id_paket,no_resi,berat,volume,satuan,total_harga,harga_awal,tipe_paket,tipe_pengiriman, nm_pengirim,no_hp_pengirim,email_pengirim,alamat_pengirim,nm_penerima,no_hp_penerima,email_penerima,alamat_penerima,date_created,date_updated, status,tipe,id_kota_asal,id_kota_tujuan,no_trans,diskon,estimasi,tipe_payment,tipe_satuan,tipe_order,latitude_asal,nm_barang,id_provinsi_asal,id_provinsi_tujuan,is_from,jumlah_barang,keterangan,tipe_pickup)
            VALUES ('" . $ic . "',1,'" . $nores . "','" . $br . "','" . $vl . "','" . $stn . "','" . $to . "','" . $hr . "',1,'".$tpn."','" . $nm_peng . "','" . $hp_peng . "','" . $em_peng . "','" . $almt_peng . "','" . $nm_pene . "','" . $hp_pene . "','" . $em_pene . "','" . $almt_pene . "', NOW(), NOW(),1,1,'" . $ika . "','" . $ikt . "','" . $noTrans . "','" . $ds . "','" . $leadtime . "','" . $tp . "','".$btkKiriman."',1,'" . $lat . "','" . $nb . "','" . $ipa . "','" . $ipt . "','" . $fr . "','" . $jb . "','" . $ket . "','".$tpu."') ";
            $this->db->query($sInsertTrans);

            $sAsal = $this->db->query("SELECT id FROM tb_customer_detail WHERE id_customer='" . $ic . "' AND id_kota='".$ika."' AND no_hp='".$hp_peng."' AND nama='".$nm_peng."' AND alamat='".$almt_peng."' AND tipe=1");
            $rAsal = $sAsal->row_array();

            if($rAsal['id']==''){
                $insertAsal = "INSERT INTO tb_customer_detail(id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status) VALUES ('".$ic."','".$ika."','".$nm_peng."','".$hp_peng."','".$em_peng."','".$almt_peng."',NOW(),NOW(),1,1)";
                $this->db->query($insertAsal);
            }

            $sTuj = $this->db->query("SELECT id FROM tb_customer_detail WHERE id_customer='" . $ic . "' AND id_kota='".$ikt."' AND no_hp='".$hp_pene."' AND nama='".$nm_pene."' AND alamat='".$almt_pene."' AND tipe=2");
            $rTuj = $sTuj->row_array();

            if($rTuj['id']==''){
                $insertTuj = "INSERT INTO tb_customer_detail(id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status) VALUES ('".$ic."','".$ikt."','".$nm_pene."','".$hp_pene."','".$em_pene."','".$almt_pene."',NOW(),NOW(),2,1)";
                $this->db->query($insertTuj);
            }

            $sql = "SELECT ".$this->select." FROM tb_trans WHERE no_trans='" . $nores . "'";
            $this->response->getresponse($sql,'order');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getriwayat() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $hp = $parameter['hp'];
        $lt = $parameter['lt'];

        if($hp!='' && $ic==''){
            $sCust = $this->db->query("SELECT id_customer,status,is_register FROM tb_customer WHERE no_hp='" . $hp . "'");
            $rCust = $sCust->row_array();

            $ic=$rCust['id_customer'];
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        if($ic!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_tujuan),'') as nm_kota_tujuan,
            (SELECT count(id_trans) FROM tb_trans WHERE id_customer='".$ic."') as total
            FROM tb_trans a WHERE a.id_customer='" . $ic . "' ORDER BY a.id_trans DESC ".$limit;
            $this->response->getresponse($sql,'getriwayat');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $no = $parameter['no'];

        if($id!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan
            FROM tb_trans a WHERE a.id_trans='" . $id . "'";
            $this->response->getresponse($sql,'getbyid');
        }elseif($no!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan
            FROM tb_trans a WHERE a.no_resi='" . $no . "'";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function cekresi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $no = $parameter['no'];

        if($no!=''){
            $url = $this->general_lib->url_api_baraka() . 'v2/partner/cekresi';
            $fields = array(
                'no_resi' => $no
            );
            $resp = $this->general_lib->push_http_baraka($url, $fields);
            echo $resp;
            exit();
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function batalbycustomer() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $it = $parameter['it'];
        $ket = $parameter['ket'];

        if($it!='' && $ic!=''){
            $sTrans = $this->db->query("SELECT id_trans,status,status_pickup,id_kurir,id_customer FROM tb_trans WHERE id_trans='" . $it . "'");
            $rTrans = $sTrans->row_array();

            if($rTrans['status']==3){
                $code = '03';
                $status = 'Kurir sedang menjemput paket Anda, harap menunggu';
                $this->general_lib->error($code,$status);
            }

            if($rTrans['status']>=8){
                $code = '03';
                $status = 'Resi sudah batal';
                $this->general_lib->error($code,$status);
            }

            if($rTrans['id_customer']!=$ic || $rTrans['status']>=4){
                $code = '03';
                $status = 'Resi tidak dapat dibatalkan';
                $this->general_lib->error($code,$status);
            }

            $update = "UPDATE tb_trans SET status=8,keterangan_batal='".$ket."',date_updated=NOW() WHERE id_trans='".$it."'";
            $this->db->query($update);

            $sql = "SELECT ".$this->select." FROM tb_trans WHERE id_trans='" . $it . "'";
            $this->response->getresponse($sql,'batalbycustomer');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
