<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function getprodukcms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $nm = $parameter['nm'];
        $lt = $parameter['lt'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_cms'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'tp' => $tp,
            'lt' => $lt,
            'nm' => $nm
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function getproduk2() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $lat = $parameter['lat'];
        $lt = $parameter['lt'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'ik' => $ik,
            'lt' => $lt,
            'lat' => $lat
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function getproduk() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $iu = $parameter['iu'];
        $qy = $parameter['qy'];
        $ord = $parameter['ord'];
        $ir = $parameter['ir'];
        $lat = $parameter['lat'];
        $lt = $parameter['lt'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'ik' => $ik,
            'iu' => $iu,
            'nm' => $qy,
            'ord' => $ord,
            'ir' => $ir,
            'lt' => $lt,
            'lat' => $lat
        );
        $resp = $this->general_lib->general_http($url, $fields);
        $encode = json_decode($resp, true);
        $arrayProd = array();
        $code = "201";
        $status = 'Data Tidak ditemukan...';
        $check = false;
        $a = 0;
        foreach ($encode['result'] as $rowProd) {
            $sKat = $this->db->query("SELECT nm_kategori,nm_kategori_eng,nm_kategori_ar,deskripsi,deskripsi_eng,deskripsi_ar,image_background FROM tb_kategori WHERE id_kategori='".$rowProd['id_kategori']."'");
            $rKat = $sKat->row_array();

            $arrayProd[$a] = $rowProd;
            $arrayProd[$a]['deskripsi_kategori'] = $rKat['deskripsi'];
            $arrayProd[$a]['deskripsi_kategori_eng'] = $rKat['deskripsi_eng'];
            $arrayProd[$a]['deskripsikategori_ar'] = $rKat['deskripsi_ar'];
            $arrayProd[$a]['image_background'] = $rKat['image_background'];

            $code = "200";
            $status = "Succes action getproduk";
            $check = true;
            $a++;
        }

        $str = array(
            "result" => $arrayProd,
            "code" => $code,
            "message" => $status
        );
        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function getpopuler() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $lat = $parameter['lat'];
        $lt = $parameter['lt'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'ik' => $ik,
            'lt' => $lt,
            'ord' => '2',
            'lat' => $lat
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function getrekomendasi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $lat = $parameter['lat'];
        $lt = $parameter['lt'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_produk_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'ik' => $ik,
            'lt' => $lt,
            'ir' => '1',
            'lat' => $lat
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $lat = $parameter['lat'];
        $lt = $parameter['lt'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_byid_neno'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'id' => $id
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function updateproduk() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $nm_eng = addslashes($parameter['nm_eng']);
        $nm_ar = addslashes($parameter['nm_ar']);
        $ket = addslashes($parameter['ket']);
        $ok = $parameter['ok'];
        $ns = $parameter['ns'];
        $ket_eng = addslashes($parameter['ket_eng']);
        $ket_ar = addslashes($parameter['ket_ar']);
        $hd = $parameter['hd'];
        $ds = $parameter['ds'];
        $hj = $parameter['hj'];
        $fee1 = $parameter['fee1'];
        $fee2 = $parameter['fee2'];
        $fee3 = $parameter['fee3'];
        $pjk = $parameter['pjk'];
        $dn = $parameter['dn'];
        $pn = $parameter['pn'];
        $hp = $parameter['hp'];
        $br = $parameter['br'];
        $bj = $parameter['bj'];
        $sat = strtoupper($parameter['sat']);
        $sto = $parameter['sto'];
        $dmn = $parameter['dmn'];
        $mo = $parameter['mo'];
        $mok = $parameter['mok'];
        $tp = $parameter['tp'];
        $st = $parameter['st'];
        $ik = $parameter['ik'];
        $ikp = $parameter['ikp'];
        $if = $parameter['if'];
        $is = $parameter['is'];
        $ia = $parameter['ia'];
        $iu = $parameter['iu'];
        $ip = $parameter['ip'];
        $ii = $parameter['ii'];
        $it = $parameter['it'];
        $tg = $parameter['tg'];
        $iam = $parameter['iam'];
        $iin = $parameter['iin'];
        $ipm = $parameter['ipm'];
        $hkm = $parameter['hkm'];
        $hqr = $parameter['hqr'];
        $hkmj = $parameter['hkmj'];
        $qr = $parameter['qr'];
        $bk = $parameter['bk'];

        $lat = $parameter['lat'];
        $vd = $parameter['vd'];
        $id = $parameter['id'];
        $dexa = $parameter['dexa'];

        $datafile = $parameter['image'];
        $namefile = $parameter['filename'];

        $datafile2 = $parameter['image2'];
        $namefile2 = $parameter['filename2'];

        $datafile3 = $parameter['image3'];
        $namefile3 = $parameter['filename3'];

        $datafile4 = $parameter['image4'];
        $namefile4 = $parameter['filename4'];

        $datafile5 = $parameter['image5'];
        $namefile5 = $parameter['filename5'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=update_produk'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'nm' => $nm,
            'nm_eng' => $nm_eng,
            'nm_ar' => $nm_ar,
            'ket' => $ket,
            'ok' => $ok,
            'ns' => $ns,
            'ket_eng' => $ket_eng,
            'ket_ar' => $ket_ar,
            'hd' => $hd,
            'ds' => $ds,
            'hj' => $hj,
            'fee1' => $fee1,
            'fee2' => $fee2,
            'fee3' => $fee3,
            'pjk' => $pjk,
            'dn' => $dn,
            'pn' => $pn,
            'hp' => $hp,
            'br' => $br,
            'bj' => $bj,
            'sat' => $sat,
            'sto' => $sto,
            'dmn' => $dmn,
            'mo' => $mo,
            'mok' => $mok,
            'tp' => $tp,
            'st' => $st,
            'ik' => $ik,
            'ikp' => $ikp,
            'if' => $if,
            'is' => $is,
            'ia' => $ia,
            'iu' => $iu,
            'ip' => $ip,
            'ii' => $ii,
            'it' => $it,
            'tg' => $tg,
            'iam' => $iam,
            'iin' => $iin,
            'ipm' => $ipm,
            'hkm' => $hkm,
            'hqr' => $hqr,
            'hkmj' => $hkmj,
            'qr' => $qr,
            'bk' => $bk,
            'lat' => $lat,
            'vd' => $vd,
            'id' => $id,
            'dexa' => $dexa,
            'datafile' => $datafile,
            'namefile' => $namefile,
            'datafile2' => $datafile2,
            'namefile2' => $namefile2,
            'datafile3' => $datafile3,
            'namefile3' => $namefile3,
            'datafile4' => $datafile4,
            'namefile4' => $namefile4,
            'datafile5' => $datafile5,
            'namefile5' => $namefile5
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function updateview() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=update_view'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'id' => $id
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

}
