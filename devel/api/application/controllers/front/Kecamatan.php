<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }


    public function getbykota() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sql = "SELECT * FROM tb_kecamatan WHERE id_kota='" . $id . "' AND status='1' order by nm_kecamatan asc";
        $this->response->getresponse($sql,'getbykota');
    }

    public function getbykotathoyyiban() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $url = $this->general_lib->url_thoyyiban() . 'api/kecamatan_api.php?action=get_bykota'.$this->general_lib->key_thoyyiban();
        $fields = array(
            'id' => $id
        );
        $resp = $this->general_lib->general_http($url, $fields);
        echo $resp;
        exit();
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sql = "SELECT * FROM tb_kecamatan WHERE id_kecamatan='" . $id . "'";
        $this->response->getresponse($sql,'getbyid');
    }

}
