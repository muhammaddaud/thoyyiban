<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trans extends CI_Controller
{
    
    var $param;
    var $select;
    var $selecta;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }

        $select="id_trans, id_customer, id_layanan, id_kurir, id_cabang_asal, id_cabang_tujuan, id_wilayah_tujuan, id_provinsi_asal, id_provinsi_tujuan, id_kota_asal, id_kota_tujuan, id_kecamatan_tujuan, nm_kecamatan_tujuan, latitude_asal, no_trans, no_resi, no_manifest, nm_barang, jumlah_barang, berat, volume, satuan, tipe_satuan, total_harga, harga_awal, biaya_via, biaya_ongkos, diskon, image, image_terima, tipe_paket, tipe_pengiriman, tipe_pengiriman_kurir, tipe_payment, tipe_order, estimasi, nm_pengirim, no_hp_pengirim, email_pengirim, alamat_pengirim, nm_penerima, no_hp_penerima, email_penerima, alamat_penerima, kode_pos, keterangan, keterangan_batal, keterangan_pending, keterangan_rating, rating, date_created, date_updated, tipe, status, status_pickup";

        $selecta="a.id_trans, a.id_customer, a.id_layanan, a.id_kurir, a.id_cabang_asal, a.id_cabang_tujuan, a.id_wilayah_tujuan, a.id_provinsi_asal, a.id_provinsi_tujuan, a.id_kota_asal, a.id_kota_tujuan, a.id_kecamatan_tujuan, a.nm_kecamatan_tujuan, a.latitude_asal, a.no_trans, a.no_resi, a.no_manifest, a.nm_barang, a.jumlah_barang, a.berat, a.volume, a.satuan, a.tipe_satuan, a.total_harga, a.harga_awal, a.biaya_via, a.biaya_ongkos, a.diskon, a.image, a.image_terima, a.tipe_paket, a.tipe_pengiriman, a.tipe_pengiriman_kurir, a.tipe_payment, a.tipe_order, a.estimasi, a.nm_pengirim, a.no_hp_pengirim, a.email_pengirim, a.alamat_pengirim, a.nm_penerima, a.no_hp_penerima, a.email_penerima, a.alamat_penerima, a.kode_pos, a.keterangan, a.keterangan_batal, a.keterangan_pending, a.keterangan_rating, a.rating, a.date_created, a.date_updated, a.tipe, a.status, a.status_pickup";

        $this->select=$select;
        $this->selecta=$selecta;
    }

    public function order() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $noTrans = date('YmdHms').rand(111,999);
        $ic = $parameter['ic'];
        $nb = addslashes($parameter['nb']);
        $jb = $parameter['jb'];
        $to = $parameter['to'];
        $hr = $parameter['to'];
        $ds = $parameter['ds'];
        $tp = $parameter['tp'];
        $br = $parameter['br'];
        $pj = $parameter['pj'];
        $lb = $parameter['lb'];
        $tg = $parameter['tg'];
        $via = $parameter['via'];
        $ika = $parameter['ika'];
        $ikt = $parameter['ikt'];
        $kwt = $parameter['kwt'];
        $idk = $parameter['idk'];
        $ket = $parameter['ket'];
        $nm_peng = addslashes($parameter['nm_peng']);
        $hp_peng = addslashes($parameter['hp_peng']);
        $em_peng = addslashes($parameter['em_peng']);
        $almt_peng = addslashes($parameter['almt_peng']);
        $nm_pene = addslashes($parameter['nm_pene']);
        $hp_pene = addslashes($parameter['hp_pene']);
        $em_pene = addslashes($parameter['em_pene']);
        $almt_pene = addslashes($parameter['almt_pene']);
        $stn = $parameter['stn'];
        $lat = $parameter['lat'];
        $fr = $parameter['fr'];
        $kali = ($pj*$lb*$tg)/4000;
        $vl = round($kali,2);

        if ($fr == '') {
            $fr = 'ANDROID';
        }

        if ($tp == '') {
            $tp = 'TUNAI';
        }

        if ($via == '') {
            $via = 'Darat';
        }

        if ($ds == '') {
            $ds = 0;
        }

        if ($stn == '') {
            $stn = 'KG';
        }

        $tpk=1;
        $tpn=1;
        if($via=='Laut'){
            $tpn=2;
        }elseif($via=='Udara'){
            $tpn=3;
        }

        $btkKiriman = 'Berat';
        if($vl>$br){
            $btkKiriman = 'Volume';
        }
        
        $sKotAsal = $this->db->query("SELECT a.id_kota,a.nm_kota,a.cabang_id,d.id_provinsi,d.nm_provinsi
                    FROM tb_kota a
                    LEFT JOIN tb_provinsi d
                    ON a.id_provinsi=d.id_provinsi
                    WHERE a.id_kota='" . $ika . "'");
        $rKotAsal = $sKotAsal->row_array();

        $sKotTuj = $this->db->query("SELECT a.id_kota,a.nm_kota,a.cabang_id,d.id_provinsi,d.nm_provinsi
                FROM tb_kota a
                LEFT JOIN tb_provinsi d
                ON a.id_provinsi=d.id_provinsi
                WHERE a.id_kota='" . $ikt . "'");
        $rKotTuj = $sKotTuj->row_array();

        $ipa = $rKotAsal['id_provinsi'];
        $ipt = $rKotTuj['id_provinsi'];
        $kca = $rKotAsal['cabang_id'];
        $kct = $rKotTuj['cabang_id'];

        if($idk!=''){
            $sKec = $this->db->query("SELECT ID,NM_KECAMATAN FROM prm_cover_kecamatan WHERE ID='".$idk."'");
            $rKec = $sKec->row_array();
            $nmk = $rKec['NM_KECAMATAN']; 
        }else{
            $nmk = 'AMBIL GUDANG';
            $tpk=2;
        }
        
        $sWilayah = $this->db->query("SELECT KODE_COVERAN FROM prm_cover_cabang WHERE KODE_CABANG='".$kwt."'");
        $rWilayah = $sWilayah->row_array();

        if($kca!=''){
            $getNores = $this->getnoresi('1',$kca);
            $decode = json_decode($getNores,true);
            $nores = $decode['no_resi'];
            if($decode['code']!='200'){
                $code = '03';
                $status = 'Order gagal, silahkan untuk diulang beberapa saat lagi';
                $this->general_lib->error($code,$status);
            }
        }

        $sTarif = $this->db->query("SELECT ID_HARGA,LEAD_TIME,HARGA FROM prm_harga WHERE cab_asal='" . $kca . "' AND cab_tujuan='" . $kct . "' AND via='".$via."'");
        $rTarif = $sTarif->row_array();

        $exp = explode('-', $rTarif['LEAD_TIME']);

        $est = date("Y-m-d", strtotime("+" . $exp[1] . " day", strtotime(date('Y-m-d'))));
        $estimasi = $rTarif['LEAD_TIME'] . ' Hari';

        if($ic!=''){
            $sCust = $this->db->query("SELECT id_customer,status FROM tb_customer WHERE id_customer='" . $ic . "'");
            $rCust = $sCust->row_array();

            if ($rCust['id_customer'] == '') {
                $code = '02';
                $status = 'Data customer tidak ditemukan';
                $this->general_lib->error($code,$status);
            }

            if ($rCust['status'] == 0) {
                $code = '02';
                $status = 'Customer tidak aktif';
                $this->general_lib->error($code,$status);
            }
        }elseif($hp_peng!=''){
            $sCust = $this->db->query("SELECT id_customer,status FROM tb_customer WHERE no_hp='" . $hp_peng . "'");
            $rCust = $sCust->row_array();

            $ic=$rCust['id_customer'];

            if($rCust['id_customer']=='' && $nm_peng!=''){
                $cross = "BL";
                $kode_agen = $cross . $this->general_lib->random_numbers(6);

                $no_kartu = $this->general_lib->random_numbers(12);
                $real_pin = $this->general_lib->random_numbers(6);
                $pin = md5(hash('sha512', $real_pin));
                
                $password = $this->general_lib->random_numbers(6);
                $pass = md5(hash('sha512', $password));

                $sInsert = "INSERT INTO tb_customer (kd_customer, no_hp,real_pin,pin,email,latitude,uuid,is_login,
                date_created,date_updated, tipe, status,password,real_password,nm_customer,no_kartu,is_from,is_register)
                VALUES ('" . $kode_agen . "', '" . $hp_peng . "','" . $real_pin . "','" . $pin . "','" . $em_peng . "','" . $lat . "','" . $uuid . "',1,NOW(), NOW(), 1, 1, '" . $pass . "', '" . $password . "', '" . $nm_peng . "','" . $no_kartu . "','".$fr."',0)  ";
                $this->db->query($sInsert);

                $sCust = $this->db->query("SELECT id_customer,status FROM tb_customer WHERE no_hp='" . $hp_peng . "'");
                $rCust = $sCust->row_array();

                $ic=$rCust['id_customer'];
            }
        }
        

        if ($to != '' && $nm_peng!='' && $hp_peng!='' && $almt_peng!='' && $nm_pene!='' && $hp_pene!='' && $almt_pene!='' && $ika!='' && $ikt!='' && $br!='' && $nores!='') {

            $sInsertTrans = "INSERT INTO tb_trans (id_customer,id_paket,no_resi,berat,volume,satuan,total_harga,harga_awal,tipe_paket,tipe_pengiriman, nm_pengirim,no_hp_pengirim,email_pengirim,alamat_pengirim,nm_penerima,no_hp_penerima,email_penerima,alamat_penerima,date_created,date_updated, status,tipe,id_cabang,id_kota_asal,id_kota_tujuan,no_trans,diskon,estimasi,tipe_payment,id_cabang_asal,id_cabang_tujuan,tipe_satuan,tipe_order,latitude_asal,nm_barang,id_provinsi_asal,id_provinsi_tujuan,is_from,jumlah_barang,keterangan,id_wilayah_tujuan,id_kecamatan_tujuan,nm_kecamatan_tujuan,tipe_pengiriman_kurir)
            VALUES ('" . $ic . "',1,'" . $nores . "','" . $br . "','" . $vl . "','" . $stn . "','" . $to . "','" . $hr . "',1,'".$tpn."','" . $nm_peng . "','" . $hp_peng . "','" . $em_peng . "','" . $almt_peng . "','" . $nm_pene . "','" . $hp_pene . "','" . $em_pene . "','" . $almt_pene . "', NOW(), NOW(),1,1,0,'" . $ika . "','" . $ikt . "','" . $noTrans . "','" . $ds . "','" . $rTarif['LEAD_TIME'] . "','" . $tp . "','" . $kca . "','" . $kct . "','".$btkKiriman."',1,'" . $lat . "','" . $nb . "','" . $ipa . "','" . $ipt . "','" . $fr . "','" . $jb . "','" . $ket . "','" . $kwt . "','" . $idk . "','" . $nmk . "','" . $tpk . "') ";
            $this->db->query($sInsertTrans);

            $sAsal = $this->db->query("SELECT id FROM tb_customer_detail WHERE id_customer='" . $ic . "' AND id_kota='".$ika."' AND no_hp='".$hp_peng."' AND nama='".$nm_peng."' AND alamat='".$almt_peng."' AND tipe=1");
            $rAsal = $sAsal->row_array();

            if($rAsal['id']==''){
                $insertAsal = "INSERT INTO tb_customer_detail(id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status) VALUES ('".$ic."','".$ika."','".$nm_peng."','".$hp_peng."','".$em_peng."','".$almt_peng."',NOW(),NOW(),1,1)";
                $this->db->query($insertAsal);
            }

            $sTuj = $this->db->query("SELECT id FROM tb_customer_detail WHERE id_customer='" . $ic . "' AND id_kota='".$ikt."' AND no_hp='".$hp_pene."' AND nama='".$nm_pene."' AND alamat='".$almt_pene."' AND tipe=2");
            $rTuj = $sTuj->row_array();

            if($rTuj['id']==''){
                $insertTuj = "INSERT INTO tb_customer_detail(id_customer, id_kota, nama, no_hp, email, alamat, date_created, date_updated, tipe, status) VALUES ('".$ic."','".$ikt."','".$nm_pene."','".$hp_pene."','".$em_pene."','".$almt_pene."',NOW(),NOW(),2,1)";
                $this->db->query($insertTuj);
            }

            $sql = "SELECT id_trans, id_customer, id_layanan, id_kurir, id_cabang_asal, id_cabang_tujuan, id_provinsi_asal, id_provinsi_tujuan, id_kota_asal, id_kota_tujuan, latitude_asal, no_trans, no_resi, no_manifest, nm_barang, jumlah_barang, berat, volume, satuan, tipe_satuan, total_harga, harga_awal, diskon, image, image_terima, tipe_paket, tipe_pengiriman, tipe_payment, tipe_order, estimasi, nm_pengirim, no_hp_pengirim, email_pengirim, alamat_pengirim, nm_penerima, no_hp_penerima, email_penerima, alamat_penerima, kode_pos, date_created, date_updated, tipe, status FROM tb_trans WHERE no_trans='" . $nores . "'";
            $this->response->getresponse($sql,'order');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updateorder(){
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $nb = addslashes($parameter['nb']);
        $jb = $parameter['jb'];
        $bv = $parameter['bv'];
        $bt = $parameter['bt'];
        $to = $parameter['to'];
        $hr = $parameter['to'];
        $tp = $parameter['tp'];
        $br = $parameter['br'];
        $pj = $parameter['pj'];
        $lb = $parameter['lb'];
        $tg = $parameter['tg'];
        $via = $parameter['via'];
        $ika = $parameter['ika'];
        $ikt = $parameter['ikt'];
        $kwt = $parameter['kwt'];
        $idk = $parameter['idk'];
        $ket = $parameter['ket'];
        $nm_peng = addslashes($parameter['nm_peng']);
        $hp_peng = addslashes($parameter['hp_peng']);
        $em_peng = addslashes($parameter['em_peng']);
        $almt_peng = addslashes($parameter['almt_peng']);
        $nm_pene = addslashes($parameter['nm_pene']);
        $hp_pene = addslashes($parameter['hp_pene']);
        $em_pene = addslashes($parameter['em_pene']);
        $almt_pene = addslashes($parameter['almt_pene']);
        $inpTgl = date("Y-m-d H:i:s");

        if($id!='' && $kwt!=''){
            $sCek = $this->db->query("SELECT ".$this->select." FROM tb_trans WHERE id_trans='".$id."'");
            $rCek = $sCek->row_array();
            $vl = $rCek['volume'];
            $tpn = $rCek['tipe_pengiriman'];
            $nores = $rCek['no_resi'];
            $tpk=$rCek['tipe_pengiriman_kurir'];

            if($nb==''){
                $nb=$rCek['nm_barang'];
            }

            if($jb==''){
                $jb=$rCek['jumlah_barang'];
            }

            if($to==''){
                $to=$rCek['total_harga'];
            }

            if($br==''){
                $br=$rCek['berat'];
            }

            $biaya = $bv+$bt;
            $hr=$to-$biaya;

            if($tp==''){
                $tp=$rCek['tipe_payment'];
            }

            if($pj!='' && $lb!='' && $tg!=''){
                $kali = ($pj*$lb*$tg)/4000;
                $vl = round($kali,2);
            }

            if($via!=''){
                $tpn=1;
                if($via=='Laut'){
                    $tpn=2;
                }elseif($via=='Udara'){
                    $tpn=3;
                }
            }

            if($via==''){
                if($tpn==1){
                    $via='Darat';
                }elseif($tpn==2){
                    $via='Laut';
                }elseif($tpn==3){
                    $via='Udara';
                }
            }

            if($ika==''){
                $ika=$rCek['id_kota_asal'];
            }

            if($ikt==''){
                $ikt=$rCek['id_kota_tujuan'];
            }

            if($kwt==''){
                $kwt=$rCek['id_wilayah_tujuan'];
            }

            if($idk==''){
                $idk=$rCek['id_kecamatan_tujuan'];
            }

            if($nm_peng==''){
                $nm_peng=$rCek['nm_pengirim'];
            }

            if($hp_peng==''){
                $hp_peng=$rCek['no_hp_pengirim'];
            }

            if($em_peng==''){
                $em_peng=$rCek['email_pengirim'];
            }

            if($almt_peng==''){
                $almt_peng=$rCek['alamat_pengirim'];
            }

            if($nm_pene==''){
                $nm_pene=$rCek['nm_penerima'];
            }

            if($hp_pene==''){
                $hp_pene=$rCek['no_hp_penerima'];
            }

            if($em_pene==''){
                $em_pene=$rCek['email_penerima'];
            }

            if($almt_pene==''){
                $almt_pene=$rCek['alamat_penerima'];
            }

            if($ket==''){
                $ket=$rCek['keterangan'];
            }

            $bi="";
            $cash=0;
            $bb=0;
            $bl=0;
            if($tp=='TUNAI'){
                $bi='Cash';
                $cash = $to;
            }elseif($tp=='TUJUAN'){
                $bi='BB';
                $bb = $to;
            }

            $btkKiriman = 'Berat';
            $servicechace=$br*500;
            if($vl>$br){
                $btkKiriman = 'Volume';
                $servicechace=$vl*500;
            }
            $omset = $to-$service;

            $sKotAsal = $this->db->query("SELECT a.id_kota,a.nm_kota,a.cabang_id,c.CABANG_DESC,d.id_provinsi,d.nm_provinsi
                    FROM tb_kota a
                    LEFT JOIN tb_provinsi d
                    ON a.id_provinsi=d.id_provinsi
                    LEFT JOIN prm_cabang c
                    ON a.cabang_id=c.cabang_id
                    WHERE a.id_kota='" . $ika . "'");
            $rKotAsal = $sKotAsal->row_array();

            $sKotTuj = $this->db->query("SELECT a.id_kota,a.nm_kota,a.cabang_id,d.id_provinsi,d.nm_provinsi
                    FROM tb_kota a
                    LEFT JOIN tb_provinsi d
                    ON a.id_provinsi=d.id_provinsi
                    WHERE a.id_kota='" . $ikt . "'");
            $rKotTuj = $sKotTuj->row_array();

            $ipa = $rKotAsal['id_provinsi'];
            $ipt = $rKotTuj['id_provinsi'];
            $kca = $rKotAsal['cabang_id'];
            $kct = $rKotTuj['cabang_id'];
            $nmca = $rKotAsal['CABANG_DESC'];

            if($idk!=''){
                $sKec = $this->db->query("SELECT ID,NM_KECAMATAN FROM prm_cover_kecamatan WHERE ID='".$idk."'");
                $rKec = $sKec->row_array();
                $nmk = $rKec['NM_KECAMATAN']; 
            }else{
                $nmk = 'AMBIL GUDANG';
                $tpk=2;
            }
            
            $sWilayah = $this->db->query("SELECT KODE_COVERAN FROM prm_cover_cabang WHERE KODE_CABANG='".$kwt."'");
            $rWilayah = $sWilayah->row_array();

            $sWilayahAsal = $this->db->query("SELECT KODE_COVERAN FROM prm_cover_cabang WHERE KODE_CABANG='".$kca."'");
            $rWilayahAsal = $sWilayahAsal->row_array();

            if($kca!='' && $kca!=$rCek['id_cabang_asal']){
                $getNores = $this->getnoresi('1',$kca);
                $decode = json_decode($getNores,true);
                $nores = $decode['no_resi'];
                if($decode['code']!='200'){
                    $code = '03';
                    $status = 'Update order gagal, silahkan untuk diulang beberapa saat lagi';
                    $this->general_lib->error($code,$status);
                }
            }

            $initRsCab = substr($nores,2,3);
            $initRsNo = (int)substr($nores,6,strlen($nores));

            $sTarif = $this->db->query("SELECT ID_HARGA,LEAD_TIME,HARGA FROM prm_harga WHERE cab_asal='" . $kca . "' AND cab_tujuan='" . $kct . "' AND via='".$via."'");
            $rTarif = $sTarif->row_array();

            $exp = explode('-', $rTarif['LEAD_TIME']);

            $est = date("Y-m-d", strtotime("+" . $exp[1] . " day", strtotime(date('Y-m-d'))));
            $estimasi = $rTarif['LEAD_TIME'] . ' Hari';


            $updateTrans = "UPDATE tb_trans SET no_resi='" . $nores . "',berat='" . $br . "',volume='" . $vl . "',total_harga='" . $to . "',harga_awal='" . $hr . "',tipe_pengiriman='".$tpn."',tipe_pengiriman_kurir='".$tpk."', nm_pengirim='" . $nm_peng . "',no_hp_pengirim='" . $hp_peng . "',email_pengirim='" . $em_peng . "',alamat_pengirim='" . $almt_peng . "',nm_penerima='" . $nm_pene . "',no_hp_penerima='" . $hp_pene . "',email_penerima='" . $em_pene . "',alamat_penerima='" . $almt_pene . "',tipe_satuan='" . $btkKiriman . "',date_updated=NOW(),id_kota_asal='" . $ika . "',id_kota_tujuan='" . $ikt . "',estimasi='" . $rTarif['LEAD_TIME'] . "',tipe_payment='" . $tp . "',id_cabang_asal='" . $kca . "',id_cabang_tujuan='" . $kct . "',nm_barang='" . $nb . "',jumlah_barang='" . $jb . "',id_provinsi_asal='" . $ipa . "',id_provinsi_tujuan='" . $ipt . "',id_wilayah_tujuan='" . $kwt . "',id_kecamatan_tujuan='" . $idk . "',nm_kecamatan_tujuan='" . $nmk . "',keterangan='" . $ket . "',status=12,status_pickup=2 WHERE id_trans='".$id."' ";
            $this->db->query($updateTrans);

            $insertTrx = "INSERT INTO trx_resi(TANGGAL, NO_RESI, NM_PENGIRIM, ALT_PENGIRIM, TLP_PENGIRIM, MAIL_PENGIRIM, NM_PENERIMA, ALT_PENERIMA, TLP_PENERIMA, MAIL_PENERIMA, BAYAR_ID, CAB_ASAL, CAB_TUJUAN, COV_ASAL, COV_TUJUAN, CAB_WILAYAH, KECAMATAN, BERAT, KOLI, MOTOR, VOLUME, HARGA, SUBTOTAL, SURCHARGE, BIAYA_VIA, TOTAL, JNS_KIRIMAN, BTK_KIRIMAN, KODE_JASA, PETUGAS, KETERANGAN, JNS_BARANG, JML_BARANG, LEAD_TIME, KURIR_RESI, VIA, KODE_NEGARA, SIMBOL, NILAI_TUKAR, FLAG_VENDOR_MANIFEST, TIPE_RESI, STATUS_TEKS, TIPE_USER, TIPE_PENGIRIMAN, status, KODE_INISIAL, FLAG_ONLINE) VALUES (NOW(),'".$nores."','".$nm_peng."','".$almt_peng."','".$hp_peng."','".$em_peng."','".$nm_pene."','".$almt_pene."','".$hp_pene."','".$em_pene."','".$bi."','".$kca."','".$kwt."','".$rWilayahAsal['KODE_COVERAN']."','".$rWilayah['KODE_COVERAN']."','".$kwt."','".$nmk."','".$br."',0,0,'".$vl."','".$rTarif['HARGA']."','".$hr."','".$bt."','".$bv."','".$to."','Paket','".$btkKiriman."','1','AGENONLINE','".$ket."','".$nb."','".$jb."','".$rTarif['LEAD_TIME']."','AGENONLINE','".$via."','ID','Rp',1,1,1,'SIAP MUAT !!!',3,'".$tpk."',2,'".$rWilayahAsal['KODE_COVERAN']."',1)";
            $this->db->query($insertTrx);

            $qri2 = "INSERT INTO trx_waitlist (NO_RESI,PARTAI,WAIT,CABANG,STATUS,TANGGAL) VALUES('".$nores."','".$jb."','".$jb."','".$rWilayahAsal['KODE_COVERAN']."','1',NOW())";
            $this->db->query($qri2);

            if(strtoupper($tp) === 'TUNAI'){
                $var1 = $this->callresiinputtunai('AGENONLINE','1',$initRsCab,$initRsNo,$inpTgl,$nores,'AGENONLINE',$bi);
            }else{
                $var1 = $this->callresiinput('AGENONLINE','1',$initRsCab,$initRsNo,$inpTgl,$nores,'AGENONLINE',$bi);
            }

            $insertData = "INSERT INTO data_mart_omset2 
                (NO_RESI,ASAL,KODE_JASA,FISIK_TUJUAN,FISIK_ASAL,COV_ASL,COV_TUJ,TANGGAL,NM_PENGIRIM,NM_PENERIMA,BAYAR_ID,BERAT,VOLUME,HARGA,DISKON,PPN,SUBTOTAL,KARANTINA,SURCHARGE,PACKING,ASURANSI,BIAYA_VIA,TOTAL_TRX,JNS_KIRIMAN,BTK_KIRIMAN,PETUGAS,NO_MNL_RESI,ID_PELANGGAN,JNS_BARANG,SURAT,JML_BARANG,LEAD_TIME,KURIR,PEMILIK_RESI,VIA,CASH,BB,BL,DARAT,LAUT,UDARA,PRIORITAS,CITY,COOL,TRUCK,CAB_TUJ_ATR,TUJUAN,ONGKIR,SERV_CHARGE,OMSET,STATUS_BATAL,SST,KODE_NEGARA) 
                VALUES
                ('".$nores."','".$nmca."','1','".$rWilayah['KODE_COVERAN']."','".$rWilayahAsal['KODE_COVERAN']."','".$kca."','".$kwt."','".$inpTgl."','".$nm_peng."','".$nm_peng."','".$bi."','".$br."','".$vl."','".$rTarif['HARGA']."','0','0','".$hr."',0,'".$bt."',0,0,'".$bv."','".$to."','Paket','".$btkKiriman."','AGENONLINE','','0','".$nb."','0','".$jb."','".$rTarif['LEAD_TIME']."','AGENONLINE','','".$via."','".$cash."','".$bb."','".$bl."','','','','','','','','".$rWilayah['KODE_COVERAN']."','','".$to."','".$servicechace."','".$omset."','0','0','ID')";
            $this->db->query($insertData);

            $sql = "SELECT ".$this->select." FROM tb_trans WHERE id_trans='".$id."'";
            $this->response->getresponse($sql,'updateorder');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function pickup(){
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $detail = $parameter['detail'];
        
        if($ik!='' && $detail!=''){
            $sKurir = $this->db->query("SELECT id_kurir,status,is_kirim FROM tb_kurir WHERE id_kurir='" . $ik . "'");
            $rKurir = $sKurir->row_array();

            if($rKurir['id_kurir']==''){
                $code = '03';
                $status = 'Data kurir tidak ditemukan';
                $this->general_lib->error($code,$status);
            }

            if($rKurir['status']!='1'){
                $code = '03';
                $status = 'Akun Anda tidak aktif';
                $this->general_lib->error($code,$status);
            }

            foreach ($detail as $value) {
                $it=$value['it'];

                $sTrans = $this->db->query("SELECT id_trans,id_kurir,status,status_pickup FROM tb_trans WHERE id_trans='" . $it . "'");
                $rTrans = $sTrans->row_array();

                if($rTrans['status']>=3){
                    $code = '03';
                    $status = 'Paket telah dipickup';
                    $this->general_lib->error($code,$status);
                }

                if($rTrans['status_pickup']>=2){
                    $code = '04';
                    $status = 'Paket telah dipickup';
                    $this->general_lib->error($code,$status);
                }

                if($rTrans['id_kurir']!='0'){
                    $code = '05';
                    $status = 'Paket telah dipickup';
                    $this->general_lib->error($code,$status);
                }

                /*if($rKurir['is_kirim']!='1'){
                    $code = '03';
                    $status = 'Harap menyelesaikan penjemputan paket sebelumnya terlebih dahulu sebelum pickup order kembali';
                    $this->general_lib->error($code,$status);
                }*/

                $update="UPDATE tb_trans SET id_kurir='".$ik."',status=3,status_pickup=2,date_updated=NOW() WHERE id_trans='".$it."'";
                $this->db->query($update);

                /*$updateKurir="UPDATE tb_kurir SET is_kirim=1,date_updated=NOW() WHERE id_kurir='".$ik."'";
                $this->db->query($updateKurir);*/
            }

            $sql="SELECT ".$this->select." FROM tb_trans WHERE id_kurir='".$ik."' ORDER BY date_updated DESC LIMIT 1";
            $this->response->getresponse($sql,'pickup');

        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getriwayat() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $hp = $parameter['hp'];
        $lt = $parameter['lt'];

        if($hp!='' && $ic==''){
            $sCust = $this->db->query("SELECT id_customer,status,is_register FROM tb_customer WHERE no_hp='" . $hp . "'");
            $rCust = $sCust->row_array();

            $ic=$rCust['id_customer'];
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        if($ic!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_asal),'') as nm_cabang_asal,
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_tujuan),'') as nm_cabang_tujuan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_tujuan),'') as nm_kota_tujuan,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan,
            (SELECT count(id_trans) FROM tb_trans WHERE id_customer='".$ic."') as total
            FROM tb_trans a WHERE a.id_customer='" . $ic . "' ORDER BY a.id_trans DESC ".$limit;
            $this->response->getresponse($sql,'getriwayat');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getnearby() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $lat = $parameter['lat'];
        $kc = $parameter['kc'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $exp = explode(",", $lat);

        $latt = $exp[0];
        $long = $exp[1];

        if($latt!='' && $long!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_asal),'') as nm_cabang_asal,
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_tujuan),'') as nm_cabang_tujuan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_tujuan),'') as nm_kota_tujuan,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan,
            (
                6371 * acos (
                  cos ( radians(" . $latt . ") )
                  * cos( radians( SUBSTRING_INDEX(latitude_asal,',',1) ) )
                  * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(latitude_asal,',',2),',',-1) ) - radians(" . $long . ") )
                  + sin ( radians(" . $latt . ") )
                  * sin( radians( SUBSTRING_INDEX(latitude_asal,',',1) ) )
                )
              ) AS distance
            FROM tb_trans a WHERE a.status=1 AND a.id_kurir=0 AND distance <=10 ORDER BY distance ASC ".$limit;
            $this->response->getresponse($sql,'getnearby');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbykurir() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $hp = $parameter['hp'];
        $st = $parameter['st'];
        $lt = $parameter['lt'];

        if($hp!='' && $ik==''){
            $sqlKurir = $this->db->query("SELECT id_kurir,status FROM tb_kurir WHERE no_hp='" . $hp . "'");
            $rKurir = $sqlKurir->row_array();

            $ik = $rKurir['id_kurir'];
        }

        $status = "";
        $status2 = "";
        if ($st != '') {
            $status = " AND a.status IN(" . $st . ") ";
            $status2 = " AND status IN(" . $st . ") ";
        }

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        if($ik!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_asal),'') as nm_cabang_asal,
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_tujuan),'') as nm_cabang_tujuan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_tujuan),'') as nm_kota_tujuan,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan,
            (SELECT count(id_trans) FROM tb_trans WHERE id_kurir='".$ik."' ".$status2.") as total
            FROM tb_trans a WHERE a.id_kurir='" . $ik . "' ".$status." ORDER BY a.id_trans DESC ".$limit;
            $this->response->getresponse($sql,'getbykurir');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getorderantar() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sqlKurir = $this->db->query("SELECT id_kurir,status,userid FROM tb_kurir WHERE no_hp='" . $hp . "'");
        $rKurir = $sqlKurir->row_array();

        if($hp!=''){
            $sql = "SELECT a.TANGGAL,a.NO_MANIFEST,a.JML_ANTAR,a.KURIR_ANTAR,a.NO_POLISI,a.FLAG_SAMPAI,b.TANGGAL AS 'TANGGAL_RESI', b.NO_RESI, b.NM_PENGIRIM, b.ALT_PENGIRIM, b.TLP_PENGIRIM, b.MAIL_PENGIRIM, b.NM_PENERIMA, b.ALT_PENERIMA, b.TLP_PENERIMA, b.MAIL_PENERIMA, b.BAYAR_ID, b.CAB_ASAL, b.CAB_TUJUAN, b.COV_ASAL, b.COV_TUJUAN, b.CAB_WILAYAH, b.KECAMATAN, b.BERAT, b.KOLI, b.MOTOR, b.VOLUME, b.HARGA, b.DISKON_ID, b.SST, b.PPN, b.SUBTOTAL, b.KARANTINA, b.SURCHARGE, b.BIAYA_VIA, b.PACKING, b.ASURANSI, b.ASURANSI_MOTOR, b.TOTAL, b.JNS_KIRIMAN, b.BTK_KIRIMAN, b.KODE_JASA, b.PETUGAS, b.KETERANGAN, b.JNS_BARANG AS NM_BARANG, b.JML_BARANG, b.JML_PENDING, b.LEAD_TIME, b.VIA, b.KODE_NEGARA, b.SIMBOL, b.NILAI_TUKAR, b.STATUS_TEKS, b.TGL_BERANGKAT, b.TIPE_PENGIRIMAN, b.status, b.TAGIHAN, b.KETERANGAN_PENDING,b.IMAGE,
                IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=b.CAB_ASAL),'') as NM_CABANG_ASAL,
                IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=b.CAB_TUJUAN),'') as NM_CABANG_TUJUAN,
                (SELECT count(TANGGAL) FROM trx_manifest_antar WHERE KURIR_ANTAR='".$rKurir['userid']."' AND a.FLAG_SAMPAI=0) as total_query
                FROM trx_manifest_antar a left join trx_resi b on a.NO_RESI=b.NO_RESI WHERE a.KURIR_ANTAR='" . $rKurir['userid'] . "' AND a.FLAG_SAMPAI=0 ORDER BY a.TANGGAL ASC ".$limit;
            $this->response->getresponse($sql,'getorderantar');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getriwayatantar() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $hp = $parameter['hp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $sqlKurir = $this->db->query("SELECT id_kurir,status,userid FROM tb_kurir WHERE no_hp='" . $hp . "'");
        $rKurir = $sqlKurir->row_array();

        if($hp!=''){
            $sql = "SELECT a.TANGGAL,a.NO_MANIFEST,a.JML_ANTAR,a.KURIR_ANTAR,a.NO_POLISI,a.FLAG_SAMPAI,b.TANGGAL AS 'TANGGAL_RESI', b.NO_RESI, b.NM_PENGIRIM, b.ALT_PENGIRIM, b.TLP_PENGIRIM, b.MAIL_PENGIRIM, b.NM_PENERIMA, b.ALT_PENERIMA, b.TLP_PENERIMA, b.MAIL_PENERIMA, b.BAYAR_ID, b.CAB_ASAL, b.CAB_TUJUAN, b.COV_ASAL, b.COV_TUJUAN, b.CAB_WILAYAH, b.KECAMATAN, b.BERAT, b.KOLI, b.MOTOR, b.VOLUME, b.HARGA, b.DISKON_ID, b.SST, b.PPN, b.SUBTOTAL, b.KARANTINA, b.SURCHARGE, b.BIAYA_VIA, b.PACKING, b.ASURANSI, b.ASURANSI_MOTOR, b.TOTAL, b.JNS_KIRIMAN, b.BTK_KIRIMAN, b.KODE_JASA, b.PETUGAS, b.KETERANGAN, b.JNS_BARANG AS NM_BARANG, b.JML_BARANG, b.JML_PENDING, b.LEAD_TIME, b.VIA, b.KODE_NEGARA, b.SIMBOL, b.NILAI_TUKAR, b.STATUS_TEKS, b.TGL_BERANGKAT, b.TIPE_PENGIRIMAN, b.status, b.TAGIHAN, b.KETERANGAN_PENDING, b.IMAGE,
                IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=b.CAB_ASAL),'') as NM_CABANG_ASAL,
                IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=b.CAB_TUJUAN),'') as NM_CABANG_TUJUAN,
                (SELECT count(TANGGAL) FROM trx_manifest_antar WHERE KURIR_ANTAR='".$rKurir['userid']."' AND a.FLAG_SAMPAI=1) as total_query
                FROM trx_manifest_antar a left join trx_resi b on a.NO_RESI=b.NO_RESI WHERE a.KURIR_ANTAR='" . $rKurir['userid'] . "' AND a.FLAG_SAMPAI=1 ORDER BY a.TANGGAL ASC ".$limit;
            $this->response->getresponse($sql,'getriwayatantar');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $no = $parameter['no'];

        if($id!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_asal),'') as nm_cabang_asal,
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_tujuan),'') as nm_cabang_tujuan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_tujuan),'') as nm_kota_tujuan,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan
            FROM tb_trans a WHERE a.id_trans='" . $id . "'";
            $this->response->getresponse($sql,'getbyid');
        }elseif($no!=''){
            $sql = "SELECT ".$this->selecta.",
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_asal),'') as nm_cabang_asal,
            IFNULL((SELECT x.cabang_desc FROM prm_cabang x WHERE x.cabang_id=a.id_cabang_tujuan),'') as nm_cabang_tujuan,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_asal),'') as nm_provinsi_asal,
            IFNULL((SELECT x.nm_provinsi FROM tb_provinsi x WHERE x.id_provinsi=a.id_provinsi_tujuan),'') as nm_provinsi_tujuan,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_asal),'') as nm_kota_asal,
            IFNULL((SELECT x.nm_kota FROM tb_kota x WHERE x.id_kota=a.id_kota_tujuan),'') as nm_kota_tujuan,
            IFNULL((SELECT x.CABANG_DESC FROM prm_cabang x WHERE x.CABANG_ID=a.id_wilayah_tujuan),'') as nm_wilayah_tujuan
            FROM tb_trans a WHERE a.no_resi='" . $no . "'";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function cekresi() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $no = $parameter['no'];

        if($no!=''){
            $sql = "SELECT a.Tanggal AS 'tanggal',a.No_Manifest AS 'no_manifest',
                    a.vendor AS 'vendor',a.No_Resi AS 'no_resi',
                    a.No_Polisi AS 'no_polisi',a.Kurir AS 'kurir',
                    IFNULL((SELECT x.USERNAME FROM prm_user x WHERE x.USERID=a.Kurir LIMIT 1),'') as 'nama_kurir',
                    a.Supir_Id AS 'supir', a.Status AS 'status'
                    FROM (SELECT DATE_FORMAT(a.TANGGAL,'%d-%m-%Y %H:%i:%s') TANGGAL,
                    '' AS NO_MANIFEST,
                    '' AS VENDOR,
                    a.NO_RESI,
                    '' AS NO_POLISI,
                    '' AS SUPIR_ID,
                    a.KURIR_RESI AS 'Kurir',
                    CONCAT('Gudang Asal - ',
                    (SELECT nama_coveran FROM prm_coveran WHERE kode_coveran = (SELECT kode_coveran FROM prm_cover_cabang WHERE kode_jasa = a.kode_jasa AND kode_cabang = a.cab_asal) AND kode_jasa = a.kode_jasa)) AS 'Status' FROM trx_resi a WHERE a.no_resi = '".$no."' UNION SELECT DATE_FORMAT(TANGGAL,'%d-%m-%Y %H:%i:%s') TANGGAL,NO_MANIFEST,VENDOR,NO_RESI,NO_POLISI,SUPIR_ID,PETUGAS AS 'Kurir','Manivest Asal' AS 'Status' FROM trx_manifest WHERE NO_RESI = '".$no."' UNION SELECT DATE_FORMAT(a.TANGGAL,'%d-%m-%Y %H:%i:%s') TANGGAL,a.NO_MANIVEST AS 'NO_MANIFEST','' AS VENDOR,b.NO_RESI,a.NOPOL AS 'NO_POLISI',a.SUPIR AS 'SUPIR_ID',a.PETUGAS AS 'Kurir', CONCAT('Gudang Transit - ',(select cabang_desc from prm_cabang where cabang_id = a.cab_tujuan)) as 'Status' from trx_gudang a left join trx_manifest b on a.no_manivest = b.no_manifest where b.no_resi = '".$no."' and a.flag_transit = 1 UNION SELECT DATE_FORMAT(a.TANGGAL,'%d-%m-%Y %H:%i:%s') TANGGAL,a.NO_MANIVEST AS 'NO_MANIFEST','' AS VENDOR,b.NO_RESI,a.NOPOL AS 'NO_POLISI',a.SUPIR AS 'SUPIR_ID',a.PETUGAS AS 'Kurir', CONCAT('Gudang Tujuan - ',(SELECT cabang_desc FROM prm_cabang WHERE cabang_id = c.cabang)) AS 'Status' FROM trx_gudang a LEFT JOIN trx_manifest b ON a.no_manivest = b.no_manifest LEFT JOIN prm_user c ON a.petugas = c.userid WHERE b.no_resi = '".$no."' AND a.flag_tujuan = 1 UNION SELECT DATE_FORMAT(TANGGAL,'%d-%m-%Y %H:%i:%s') TANGGAL,NO_MANIFEST,'' AS VENDOR,NO_RESI,NO_POLISI,SUPIR_ID,KURIR_ANTAR AS 'Kurir','Manivest Antar' AS 'Status' FROM trx_manifest_antar WHERE NO_RESI = '".$no."' UNION SELECT DATE_FORMAT(a.TANGGAL,'%d-%m-%Y %H:%i:%s') TANGGAL,b.NO_MANIFEST,'' AS VENDOR,a.NO_RESI,b.NO_POLISI,b.SUPIR_ID,a.PETUGAS AS 'Kurir',CONCAT('Barang Diterima Oleh - ',a.nm_penerima_brg) AS 'Status'
                    FROM trx_pengantaran a
                    LEFT JOIN trx_manifest_antar b ON a.no_resi = b.no_resi
                    WHERE a.NO_RESI = '".$no."') a";
            $this->response->getresponseresi($sql,'cekresi');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatekirim(){
        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $no = strtoupper(trim($parameter['no']));
        $nm = trim($parameter['nm']);
        $hp = trim($parameter['hp']);
        $ik = trim($parameter['ik']);
        $st = trim($parameter['st']);
        $sb = trim($parameter['sb']);
        $ket = trim($parameter['ket']);
        $tgl = date('Y-m-d H:i:s');

        $sKurir = $this->db->query("SELECT id_kurir,userid FROM tb_kurir WHERE id_kurir='" . $ik . "'");
        $rKurir = $sKurir->row_array();

        $ui=$rKurir['userid'];

        if($ui == ''){
            $code = '03';
            $status = 'Kurir tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        if($no == ''){
            $code = '03';
            $status = 'Nomor Resi tidak boleh kosong';
            $this->general_lib->error($code,$status);
        }

        $qrijml = $this->db->query("SELECT JML_BARANG,TLP_PENGIRIM,NM_PENGIRIM,TLP_PENERIMA,FLAG_ONLINE FROM trx_resi WHERE NO_RESI = '".$no."' LIMIT 1 ");
        $recjml = $qrijml->row_array();
        $jmlbrg = $recjml['JML_BARANG'];
        $tlppng = $recjml['TLP_PENGIRIM'];
        $nmpng = $recjml['NM_PENGIRIM'];

        if($hp==''){
            $hp=$recjml['TLP_PENERIMA'];
        }

        if(intval($jmlbrg) == 0){
            $code = '03';
            $status = 'Jumlah Barang yang tertera Pada Resi Samadengan 0 (Nol), Coba Cek Kembali';
            $this->general_lib->error($code,$status);
        }

        $qriAtr = $this->db->query("SELECT KURIR_ANTAR,NO_MANIFEST FROM trx_manifest_antar WHERE NO_RESI = '".$no."' LIMIT 1 ");
        $recAtr = $qriAtr->row_array();
        $kurAtr = $recAtr['KURIR_ANTAR'];
        $noMnv = $recAtr['NO_MANIFEST'];

        if($ui != $kurAtr){
            $code = '03';
            $status = 'Kurir Pengantaran Tidak sama dengan yang tertera pada Manifest Antar';
            $this->general_lib->error($code,$status);
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        
        if ($no != '' && $ui != '' && $st != '') {
            if($st==1){
                if($nm == ''){
                    $code = '03';
                    $status = 'Nama Penerima harus di isi';
                    $this->general_lib->error($code,$status);
                }

                $data = $this->terimabarang($ui,$no,'',$nm,$hp,$jmlbrg,$foto,$sb);
                if($data['code']!='200'){
                    $code = '03';
                    $status = $data['message'];
                    $this->general_lib->error($code,$status);
                }

                if($recjml['FLAG_ONLINE']==1){
                    $updateTrans = "UPDATE tb_trans SET status=5,nm_terima='".$nm."',image_terima='".$foto."',date_updated=NOW() WHERE no_resi='".$no."'";
                    $this->db->query($updateTrans);
                }
                
                $qriAtrUpd = "UPDATE trx_pengantaran SET NO_MANIVEST_ANTAR = '".$noMnv."',IMAGE='".$foto."' WHERE NO_RESI = '".$no."' AND PETUGAS = '".$ui."' ";
                $this->db->query($qriAtrUpd);

                $updateManifest = "UPDATE trx_manifest_antar SET FLAG_SAMPAI = 1 WHERE NO_RESI = '".$no."' AND KURIR_ANTAR='".$ui."'";
                $this->db->query($updateManifest);

                $updateKurir = "UPDATE tb_kurir set total_terima=total_terima+1 WHERE userid='".$ui."'";
                $this->db->query($updateKurir);
            }elseif($st==2){
                if($recjml['FLAG_ONLINE']==1){
                    $updateTrans = "UPDATE tb_trans SET status=11,keterangan_pending='".$ket."',date_updated=NOW() WHERE no_resi='".$no."'";
                    $this->db->query($updateTrans);
                }

                $updateTrx = "UPDATE trx_resi SET status=8,KETERANGAN_PENDING='".$ket."' WHERE NO_RESI='".$no."'";
                $this->db->query($updateTrx);
            }

            $sql = "SELECT ".$this->select." FROM trx_resi WHERE NO_RESI = '" . $no . "'";
            $this->response->getresponseresi($sql,'updatekirim');
        } else {
            $code = '02';
            $status = 'Required data parameter...';
            $this->general_lib->error($code,$status);
        }
    }

    public function rating() {
        $err = '';

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $it = $parameter['it'];
        $rt = $parameter['rt'];
        $ket = $parameter['ket'];

        $sTrans = $this->db->query("SELECT id_trans,status,status_pickup,id_kurir FROM tb_trans WHERE id_trans='" . $it . "'");
        $rTrans = $sTrans->row_array();

        $sSum = "SELECT SUM(rating) as rating FROM tb_trans WHERE id_kurir='" . $rTrans['id_kurir'] . "' AND status=5";
        $rSum = $sSum->row_array();

        $sKurir = "SELECT id_kurir,total_rating FROM tb_kurir WHERE id_kurir='" . $rTrans['id_kurir'] . "'";
        $rKurir = $sKurir->row_array();

        $tr = $rKurir['total_rating'] + 1;
        $rate = ($rt + $rSum['rating']) / $tr;

        if($rTrans['id_trans']==''){
            $code = '03';
            $status = 'Data order tidak ditemukan';
            $this->general_lib->error($code,$status);
        }

        if($rTrans['status']!=5){
            $code = '03';
            $status = 'Order belum selesai';
            $this->general_lib->error($code,$status);
        }

        if ($it != '' && $rt != '') {

            $update = "UPDATE tb_trans SET rating='" . $rt . "',keterangan_rating='".$ket."',date_updated=NOW() WHERE id_trans='" . $it . "'";
            $this->db->query($update);

            $updateKurir = "UPDATE tb_kurir SET total_rating='" . $tr . "',rating='".$rate."',date_updated=NOW() WHERE id_kurir='" . $ik . "'";
            $this->db->query($updateKurir);

            $sql = "SELECT ".$this->select." FROM tb_trans WHERE id_trans='".$it."'";
            $this->response->getresponseresi($sql,'rating');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function batalbykurir() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ik = $parameter['ik'];
        $hp = $parameter['hp'];
        $it = $parameter['it'];
        $ket = $parameter['ket'];

        if($hp!='' && $ik==''){
            $sqlKurir = $this->db->query("SELECT id_kurir,status FROM tb_kurir WHERE no_hp='" . $hp . "'");
            $rKurir = $sqlKurir->row_array();

            $ik = $rKurir['id_kurir'];
        }

        if($it!='' && $ik!=''){
            $sTrans = $this->db->query("SELECT id_trans,status,status_pickup,id_kurir FROM tb_trans WHERE id_trans='" . $it . "'");
            $rTrans = $sTrans->row_array();

            if($rTrans['status']>=8){
                $code = '03';
                $status = 'Resi sudah batal';
                $this->general_lib->error($code,$status);
            }

            if($rTrans['id_kurir']!=$ik || $rTrans['status']>=4){
                $code = '03';
                $status = 'Resi tidak dapat dibatalkan';
                $this->general_lib->error($code,$status);
            }

            $update = "UPDATE tb_trans SET status=10,status_pickup=4,keterangan_batal='".$ket."',date_updated=NOW() WHERE id_trans='".$it."'";
            $this->db->query($update);

             $sql = "SELECT ".$this->select." FROM tb_trans WHERE id_trans='" . $it . "'";
            $this->response->getresponse($sql,'batalbykurir');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function batalbycustomer() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $it = $parameter['it'];
        $ket = $parameter['ket'];

        if($it!='' && $ic!=''){
            $sTrans = $this->db->query("SELECT id_trans,status,status_pickup,id_kurir,id_customer FROM tb_trans WHERE id_trans='" . $it . "'");
            $rTrans = $sTrans->row_array();

            if($rTrans['status']==3){
                $code = '03';
                $status = 'Kurir sedang menjemput paket Anda, harap menunggu';
                $this->general_lib->error($code,$status);
            }

            if($rTrans['status']>=8){
                $code = '03';
                $status = 'Resi sudah batal';
                $this->general_lib->error($code,$status);
            }

            if($rTrans['id_customer']!=$ic || $rTrans['status']>=4){
                $code = '03';
                $status = 'Resi tidak dapat dibatalkan';
                $this->general_lib->error($code,$status);
            }

            $update = "UPDATE tb_trans SET status=8,keterangan_batal='".$ket."',date_updated=NOW() WHERE id_trans='".$it."'";
            $this->db->query($update);

            $sql = "SELECT ".$this->select." FROM tb_trans WHERE id_trans='" . $it . "'";
            $this->response->getresponse($sql,'batalbycustomer');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    function getnoresi($kd,$kc){

        $div = $kd;
        $cab = $kc;

        $qri = "SELECT KODE_INISIAL FROM PRM_USER WHERE KODE_JASA = '$div' AND CABANG = '$cab'";

        $query = $this->db->query($qri);
        $row = $query->row_array();

        $data = array('code' => 201 ,'message' => 'terjadi kesalahan', 'result' => array());

        if ($row) {

            $dtaNmCov = $row['KODE_INISIAL'];

            $qri2 = "SELECT KODE_RESI FROM prm_coveran WHERE KODE_COVERAN = '$dtaNmCov' LIMIT 1 ";
            $res2 = $this->db->query($qri2);
            $rec2 = $res2->row_array();
            $kdrs = $rec2['KODE_RESI'];

            $resi = $this->getCounterNew($div,$dtaNmCov);
            $gtCntr = str_pad($resi,5,"0",STR_PAD_LEFT);
            $noresi_ = $dtaNmCov.date('y').date('m').date('d').$gtCntr;

            $data = array('code' => 200 ,'message' => 'sukses', 'no_resi' => $noresi_);
        }

        return json_encode($data);

    }

    function getCounterNew($div,$cab){
        $qry = $this->db->query("SELECT COUNTER_EL FROM PRM_COVER_CABANG WHERE KODE_COVERAN = '$cab' AND KODE_JASA = '$div'");
        $row = $qry->row_array();
        $cnt = $row['COUNTER_EL']+1;
        $this->db->query("UPDATE PRM_COVER_CABANG SET COUNTER_EL = $cnt WHERE KODE_COVERAN ='$cab' AND KODE_JASA = '$div'");
        return $cnt;
    }

    function callresiinput($inpKurirRes,$initRsJs,$initRsCab,$initRsNo,$inpTgl,$inpResi,$petugas,$inpBayar){
        $qri = "CALL RESI_INPUT_EL('$inpKurirRes','$initRsJs','$initRsCab',$initRsNo,'$inpTgl','$inpResi','$petugas','$inpBayar',@OUT1)";
        $result = $this->db->query($qri);
        $query = "SELECT @OUT1 as var";
        $res = $this->db->query($query);
        $row = $res->row();
        return $row->var;
    }

    function callresiinputtunai($inpKurirRes,$initRsJs,$initRsCab,$initRsNo,$inpTgl,$inpResi,$petugas,$inpBayar){
        $qri = "CALL RESI_INPUT_EL_TUNAI('$inpKurirRes','$initRsJs','$initRsCab',$initRsNo,'$inpTgl','$inpResi','$petugas','$inpBayar',@OUT1)";
        $result = $this->db->query($qri);
        $query = "SELECT @OUT1 as var";
        $res = $this->db->query($query);
        $row = $res->row();
        return $row->var;
    }

    function terimabarang($petugas = null,$inpResi = null,$inpMnvAtr = null,$inpNmTerima = null,$inpTlpTerima = null,$inpJmlBrg = null,$image = null,$tagihan = null){

        $qri = "CALL RESI_TERIMA('$inpResi','$inpMnvAtr','$inpNmTerima','$inpTlpTerima',$inpJmlBrg,'$petugas',@OUT1)";
        $result = $this->db->query($qri);
        $query = "SELECT @OUT1 as out1";
        $res = $this->db->query($query);
        $row = $res->row();
        $var1 = $row->out1;

        $code = '201';

        if(!empty($var1))
        {
            switch($var1)
            {
                case "EXISTS" : $errMsg = "No. Resi $inpResi Sudah Teregistrasi Sebelumnya";$code = '202'; break;
                case "REMAIN" : $errMsg = "No. Resi $inpResi Sukses Disimpan";$code = '200'; break;
                case "COMPLETE" : $errMsg = "No. Resi $inpResi <br> Sudah Lengkap Tidak Memiliki Pending";$code = '200'; break;
                case "EMPTY" : $errMsg = "No. Resi $inpResi Tidak Ditemukan";$code = '203';break;
            }
        } else {
            $errMsg = "Penerimaan Resi Bermasalah";
            $code = '204';
        }

        if($var1 == 'COMPLETE'){
            $qri = "UPDATE trx_resi SET status = '6',IMAGE = '".$image."',TAGIHAN = '".$tagihan."',STATUS_TEKS = 'SUDAG DITERIMA ".$inpNmTerima."' WHERE no_resi  = '$inpResi' ";
            $result = $this->db->query($qri);
            $qri = "UPDATE trx_waitlist SET status = '4' WHERE no_resi  = '$inpResi' ";
            $result = $this->db->query($qri);
        }

        $data = array('message'=>$errMsg, 'message2'=>$var1, 'code' => $code);
        return $data;

    }

}
