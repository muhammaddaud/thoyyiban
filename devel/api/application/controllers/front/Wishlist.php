<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wishlist extends CI_Controller
{
    
    var $param;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insert() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ip = $parameter['ip'];
        $ic = $parameter['ic'];
        $uuid = $parameter['uuid'];

        if ($ip != '') {
            if ($ic=='' && $uuid=='') {
                $code = '02';
                $status = 'Required data parameter';
                $this->general_lib->error($code,$status);
            }
            
            if($ic!=''){
                $sCek = $this->db->query("SELECT * FROM tb_wishlist WHERE id_customer='" . $ic . "' AND id_produk='" . $ip . "'");
            }else{
                $sCek = $this->db->query("SELECT * FROM tb_wishlist WHERE uuid='" . $uuid . "' AND id_produk='" . $ip . "'");
            }
            $rCek = $sCek->row_array();
            if ($rCek['id_wishlist'] == '') {
                $insert = "INSERT INTO tb_wishlist
                (id_customer,uuid,id_produk,date_created,date_updated,tipe,status)
                VALUES
                ('" . $ic . "','" . $uuid . "','" . $ip . "',NOW(),NOW(),'1','1')";
                $this->db->query($insert);
            }

            $this->response->getresponse($sCek,'insert');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getwishlist() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $uuid = $parameter['uuid'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        if ($ic=='' && $uuid=='') {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }

        if($ic!=''){
            $sql = "SELECT * FROM tb_wishlist WHERE id_customer='".$ic."' ORDER BY id_wishlist DESC ".$limit;
        }else{
            $sql = "SELECT * FROM tb_wishlist WHERE uuid='".$uuid."' ORDER BY id_wishlist DESC ".$limit;
        }
        
        $result = array();

        $code = "201";
        $status = 'Data Tidak ditemukan...';

        $sql = str_replace("\n", " ", $sql);
        $sql = str_replace("\t", " ", $sql);

        $query = $this->db->query($sql);
        $check = false;

        if ($err == '') {
            $a = 0;
            foreach ($query->result_array() as $row) {
                $result[$a]['id_wishlist'] = $row['id_wishlist'];
                $result[$a]['id_customer'] = $row['id_customer'];
                $result[$a]['uuid'] = $row['uuid'];
                $result[$a]['id_produk'] = $row['id_produk'];
                $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_byid_neno'.$this->general_lib->key_thoyyiban();
                $fields = array(
                    'id' => $row['id_produk']
                );
                $resp = $this->general_lib->general_http($url, $fields);
                $decode = json_decode($resp, true);
                $result[$a]['nm_produk'] = $decode['result'][0]['nm_produk'];
                $result[$a]['kd_produk'] = $decode['result'][0]['kd_produk'];
                $result[$a]['no_sertifikat'] = $decode['result'][0]['no_sertifikat'];
                $result[$a]['qrcode'] = $decode['result'][0]['qrcode'];
                $result[$a]['ket_produk'] = $decode['result'][0]['ket_produk'];
                $result[$a]['image'] = $decode['result'][0]['image'];
                $result[$a]['image2'] = $decode['result'][0]['image2'];
                $result[$a]['image3'] = $decode['result'][0]['image3'];
                $result[$a]['image4'] = $decode['result'][0]['image4'];
                $result[$a]['image5'] = $decode['result'][0]['image5'];
                $result[$a]['video'] = $decode['result'][0]['video'];
                $result[$a]['diskon'] = $decode['result'][0]['diskon'];
                $result[$a]['harga_dasar'] = $decode['result'][0]['harga_dasar'];
                $result[$a]['harga_jual'] = $decode['result'][0]['harga_jual'];
                $result[$a]['stok'] = $decode['result'][0]['stok'];
                $result[$a]['total_jual'] = $decode['result'][0]['total_jual'];
                $result[$a]['berat'] = $decode['result'][0]['berat'];
                $result[$a]['satuan'] = $decode['result'][0]['satuan'];
                $result[$a]['minimal_order'] = $decode['result'][0]['minimal_order'];
                $result[$a]['total_view'] = $decode['result'][0]['total_view'];
                $result[$a]['total_wishlist'] = $decode['result'][0]['total_wishlist'];
                $result[$a]['total_rating'] = $decode['result'][0]['total_rating'];
                $result[$a]['rating'] = $decode['result'][0]['rating'];
                $result[$a]['latitude'] = $decode['result'][0]['latitude'];
                $result[$a]['nm_ukm'] = $decode['result'][0]['nm_ukm'];
                $result[$a]['no_hp'] = $decode['result'][0]['no_hp'];
                $result[$a]['nm_kategori'] = $decode['result'][0]['nm_kategori'];
                $result[$a]['nm_subkategori'] = $decode['result'][0]['nm_subkategori'];
                $result[$a]['nm_provinsi'] = $decode['result'][0]['nm_provinsi'];
                $result[$a]['nm_kota'] = $decode['result'][0]['nm_kota'];
                $result[$a]['nm_kecamatan'] = $decode['result'][0]['nm_kecamatan'];
                $result[$a]['nm_kelurahan'] = $decode['result'][0]['nm_kelurahan'];
                $result[$a]['date_created'] = $row['date_created'];
                $result[$a]['date_updated'] = $row['date_updated'];
                $result[$a]['tipe'] = $row['tipe'];
                $result[$a]['status'] = $row['status'];

                $code = "200";
                $status = "Succes action getwishlist";
                $check = true;
                $a++;
            }
        }

        $str = array(
            "result" => $result,
            "code" => $code,
            "message" => $status
        );
        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        if($id!=''){
            
            $sql = "SELECT * FROM tb_wishlist WHERE id_wishlist='".$id."' ";
        
            $result = array();

            $code = "201";
            $status = 'Data Tidak ditemukan...';

            $sql = str_replace("\n", " ", $sql);
            $sql = str_replace("\t", " ", $sql);

            $query = $this->db->query($sql);
            $check = false;

            if ($err == '') {
                $a = 0;
                foreach ($query->result_array() as $row) {
                    $result[$a]['id_wishlist'] = $row['id_wishlist'];
                    $result[$a]['id_customer'] = $row['id_customer'];
                    $result[$a]['uuid'] = $row['uuid'];
                    $result[$a]['id_produk'] = $row['id_produk'];
                    $url = $this->general_lib->url_thoyyiban() . 'api/produk_api.php?action=get_byid_neno'.$this->general_lib->key_thoyyiban();
                    $fields = array(
                        'id' => $row['id_produk']
                    );
                    $resp = $this->general_lib->general_http($url, $fields);
                    $decode = json_decode($resp, true);
                    $result[$a]['nm_produk'] = $decode['result'][0]['nm_produk'];
                    $result[$a]['kd_produk'] = $decode['result'][0]['kd_produk'];
                    $result[$a]['no_sertifikat'] = $decode['result'][0]['no_sertifikat'];
                    $result[$a]['qrcode'] = $decode['result'][0]['qrcode'];
                    $result[$a]['ket_produk'] = $decode['result'][0]['ket_produk'];
                    $result[$a]['image'] = $decode['result'][0]['image'];
                    $result[$a]['image2'] = $decode['result'][0]['image2'];
                    $result[$a]['image3'] = $decode['result'][0]['image3'];
                    $result[$a]['image4'] = $decode['result'][0]['image4'];
                    $result[$a]['image5'] = $decode['result'][0]['image5'];
                    $result[$a]['video'] = $decode['result'][0]['video'];
                    $result[$a]['diskon'] = $decode['result'][0]['diskon'];
                    $result[$a]['harga_dasar'] = $decode['result'][0]['harga_dasar'];
                    $result[$a]['harga_jual'] = $decode['result'][0]['harga_jual'];
                    $result[$a]['stok'] = $decode['result'][0]['stok'];
                    $result[$a]['total_jual'] = $decode['result'][0]['total_jual'];
                    $result[$a]['berat'] = $decode['result'][0]['berat'];
                    $result[$a]['satuan'] = $decode['result'][0]['satuan'];
                    $result[$a]['minimal_order'] = $decode['result'][0]['minimal_order'];
                    $result[$a]['total_view'] = $decode['result'][0]['total_view'];
                    $result[$a]['total_wishlist'] = $decode['result'][0]['total_wishlist'];
                    $result[$a]['total_rating'] = $decode['result'][0]['total_rating'];
                    $result[$a]['rating'] = $decode['result'][0]['rating'];
                    $result[$a]['latitude'] = $decode['result'][0]['latitude'];
                    $result[$a]['nm_ukm'] = $decode['result'][0]['nm_ukm'];
                    $result[$a]['no_hp'] = $decode['result'][0]['no_hp'];
                    $result[$a]['nm_kategori'] = $decode['result'][0]['nm_kategori'];
                    $result[$a]['nm_subkategori'] = $decode['result'][0]['nm_subkategori'];
                    $result[$a]['nm_provinsi'] = $decode['result'][0]['nm_provinsi'];
                    $result[$a]['nm_kota'] = $decode['result'][0]['nm_kota'];
                    $result[$a]['nm_kecamatan'] = $decode['result'][0]['nm_kecamatan'];
                    $result[$a]['nm_kelurahan'] = $decode['result'][0]['nm_kelurahan'];
                    $result[$a]['date_created'] = $row['date_created'];
                    $result[$a]['date_updated'] = $row['date_updated'];
                    $result[$a]['tipe'] = $row['tipe'];
                    $result[$a]['status'] = $row['status'];

                    $code = "200";
                    $status = "Succes action getbyid";
                    $check = true;
                    $a++;
                }
            }

            $str = array(
                "result" => $result,
                "code" => $code,
                "message" => $status
            );
            $json = json_encode($str);

            header("Content-Type: application/json");
            ob_clean();
            flush();
            echo $json;
            exit(1);
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_wishlist SET status='" . $st . "' WHERE id_wishlist='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_wishlist WHERE id_wishlist='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $ip = $parameter['ip'];
        $ic = $parameter['ic'];
        $uuid = $parameter['uuid'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_wishlist WHERE id_wishlist='" . $id . "'";
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_wishlist ORDER BY id_wishlist DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } elseif ($ip != '' && $ic!='') {
            $sDelete = "DELETE FROM tb_wishlist WHERE id_produk='" . $ip . "' AND id_customer='".$ic."'";
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_wishlist ORDER BY id_wishlist DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } elseif ($ip != '' && $uuid!='') {
            $sDelete = "DELETE FROM tb_wishlist WHERE id_produk='" . $ip . "' AND uuid='".$uuid."'";
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_wishlist ORDER BY id_wishlist DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
