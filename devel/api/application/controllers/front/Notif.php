<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notif extends CI_Controller
{
    
    var $param;
    var $req;

    function __construct() {
        parent::__construct();
        $this->db=$this->load->database('front',true);
        $this->load->model('responsefront');

        $param = file_get_contents('php://input');
        $param_json = json_decode($param, true);
        $this->param=$param_json;
        $this->req= $_REQUEST;

        $u = $_SERVER['PHP_AUTH_USER'];
        $p = $_SERVER['PHP_AUTH_PW'];
        $ipAdd = $_SERVER['REMOTE_ADDR'];

        $query = $this->db->query("SELECT id,status,ip_address FROM tb_user_api WHERE username='".$u."' AND password='".$p."' AND status=1");
        $row = $query->row_array();

        if($u=='' || $p=='' || $row['id']==''){
            $code = '08';
            $status = 'Failed Authentication';
            $this->general_lib->error($code,$status);
        }
    }

    public function insertnotif() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $nm = addslashes($parameter['nm']);
        $de = addslashes($parameter['de']);
        $nm_eng = addslashes($parameter['nm_eng']);
        $de_eng = addslashes($parameter['de_eng']);
        $nm_ar = addslashes($parameter['nm_ar']);
        $de_ar = addslashes($parameter['de_ar']);
        $is = $parameter['is'];
        $tp = $parameter['tp'];
        $st = $parameter['st'];
        $id = $parameter['id'];

        $sCek = $this->db->query("SELECT * FROM tb_notif WHERE id_notif='" . $id . "'");
        $rCek = $sCek->row_array();

        if ($st == '' && $id == '') {
            $st = 1;
        }

        $datafile = $parameter['image'];
        $binary = base64_decode($datafile);
        $namefile = $parameter['filename'];
        if ($namefile != '') {
            $target_dir = $this->general_lib->path();

            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }

            $url_path = "upload/";

            $target_path = $target_dir;
            $now = date('YmdHis');
            $rand = rand(1111, 9999);
            $generatefile = $now . $rand;
            $namefile = $generatefile . ".jpeg";
            $target_path = $target_path . $namefile;

            chmod($target_path, 0777);
            $fh = fopen($target_path, 'w') or die("can't open file");
            chmod($target_path, 0777);
            fwrite($fh, $binary);
            fclose($fh);

            sleep(1);

            $foto = "upload/" . $namefile;
        }
        if ($namefile == '') {
            $foto = "";
        }

        if ($foto != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto);
        }

        if ($foto == '') {
            $foto = $rCek['image'];
        }

        $datafile2 = $parameter['image2'];
        $binary2 = base64_decode($datafile2);
        $namefile2 = $parameter['filename2'];
        if ($namefile2 != '') {
            $target_dir2 = $this->general_lib->path();

            if (!file_exists($target_dir2)) {
                mkdir($target_dir2, 0777, true);
            }

            $target_path2 = $target_dir2;
            $now2 = date('YmdHis');
            $rand2 = rand(1111, 9999);
            $generatefile2 = $now2 . $rand2;
            $namefile2 = $generatefile2 . ".jpeg";
            $target_path2 = $target_path2 . $namefile2;

            chmod($target_path2, 0777);
            $fh2 = fopen($target_path2, 'w') or die("can't open file");
            chmod($target_path2, 0777);
            fwrite($fh2, $binary2);
            fclose($fh2);

            sleep(1);

            $foto2 = "upload/" . $namefile2;
        }

        if ($namefile2 == '') {
            $foto2 = "";
        }

        if ($foto2 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto2,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto2);
        }

        if ($foto2 == '') {
            $foto2 = $rCek['thumbnail'];
        }


        $datafile3 = $parameter['image3'];
        $binary3 = base64_decode($datafile3);
        $namefile3 = $parameter['filename3'];
        if ($namefile3 != '') {
            $target_dir3 = $this->general_lib->path();

            if (!file_exists($target_dir3)) {
                mkdir($target_dir3, 0777, true);
            }

            $target_path3 = $target_dir3;
            $now3 = date('YmdHis');
            $rand3 = rand(1111, 9999);
            $generatefile3 = $now3 . $rand3;
            $namefile3 = $generatefile3 . ".jpeg";
            $target_path3 = $target_path3 . $namefile3;

            chmod($target_path3, 0777);
            $fh3 = fopen($target_path3, 'w') or die("can't open file");
            chmod($target_path3, 0777);
            fwrite($fh3, $binary3);
            fclose($fh3);

            sleep(1);

            $foto3 = "upload/" . $namefile3;
        }

        if ($namefile3 == '') {
            $foto3 = "";
        }

        if ($foto3 != '') {
            $url = $this->general_lib->base_url() . 'api/uploader.php?action=upload';
            $fields = array(
                'nf' => $foto3,
                'fr' => 'SH'
            );
            $resp = $this->general_lib->general_http($url, $fields);
            $encode = json_decode($resp, true);

            if ($encode['code'] != 200) {
                $code = '03';
                $status = 'Upload image failed, please try again.';
                $this->general_lib->error($code,$status);
            }

            unlink($this->general_lib->upath() . $foto3);
        }

        if ($foto3 == '') {
            $foto3 = $rCek['thumbnail_small'];
        }

        if ($nm != '') {
            if ($id == '') {
                $sMax = $this->db->query("SELECT MAX(kelompok) AS ID  FROM tb_notif");
                $rMax = $sMax->row_array();

                $kelompok = $rMax['ID'] + 1;

                if($foto==''){
                    $foto='upload/defaultnotif.jpg';
                }

                if($foto2==''){
                    $foto2='upload/defaultnotif.jpg';
                }

                if($foto3==''){
                    $foto3='upload/defaultnotif.jpg';
                }

                if ($tp == 1) {
                    $sAnggota = $this->db->query("SELECT uuid,id_customer FROM tb_customer WHERE status=1 ");
                }elseif ($tp == 2) {
                    $sAnggota = $this->db->query("SELECT a.uuid,IFNULL(b.id_customer,'') as id_customer FROM tb_log_install a LEFT JOIN tb_customer b ON a.uuid=b.uuid WHERE a.status=1 AND b.id_customer IS NULL GROUP BY a.uuid
                        UNION
                        SELECT uuid,id_customer FROM tb_customer WHERE status=1");
                }
                $a = 0;
                foreach ($sAnggota->result_array() as $rAnggota) {
                    $s1 = "INSERT INTO tb_notif
                (uuid,id_customer,nm_notif,deskripsi,date_created,date_updated,tipe,status,image,kelompok,nm_notif_eng,deskripsi_eng,nm_notif_ar,deskripsi_ar,thumbnail,thumbnail_small)
                VALUES
                ('" . $rAnggota['uuid'] . "','" . $rAnggota['id_customer'] . "','" . $nm . "','" . $de . "',NOW(),NOW(),1,'" . $st . "','" . $foto . "','" . $kelompok . "','" . $nm_eng . "','" . $de_eng . "','" . $nm_ar . "','" . $de_ar . "','" . $foto2 . "','" . $foto3 . "')";
                    $this->db->query($s1);
                    $a++;
                }

                $sql = "SELECT * FROM tb_notif ORDER BY date_created DESC LIMIT 1";
            } else {
                $sUpdate = "UPDATE tb_notif SET nm_notif='" . $nm . "',deskripsi='" . $de . "',nm_notif_eng='" . $nm_eng . "',deskripsi_eng='" . $de_eng . "',nm_notif_ar='" . $nm_ar . "',deskripsi_ar='" . $de_ar . "',date_updated=NOW(),status='" . $st . "',image='" . $foto . "',thumbnail='" . $foto2 . "',thumbnail_small='" . $foto3 . "' WHERE kelompok='" . $rCek['kelompok'] . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_notif WHERE id_notif = '" . $id . "'";
            }

            $this->response->getresponse($sql,'insertnotif');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getnotifss() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $requestData = json_decode($decrypt,true);

        $columns = array(
            0 => 'id_notif',
            1 => 'image',
            2 => 'nm_notif',
            3 => 'deskripsi',
            4 => 'kelompok',
            6 => 'status_read',
            7 => 'date_created',
            8 => 'status',
            9 => 'action'
        );

        // getting total number records without any search

        $sql = "SELECT * FROM tb_notif";
        $query = $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


        if (!empty($requestData['search']['value'])) {
            // if there is a search parameter
            $sql = "SELECT *";
            $sql .= " FROM tb_notif";
            $sql .= " WHERE id_notif LIKE '" . $requestData['search']['value'] . "%' ";    // $requestData['search']['value'] contains search parameter
            $sql .= " OR nm_notif LIKE '" . $requestData['search']['value'] . "%' ";
            $sql .= " OR deskripsi LIKE '" . $requestData['search']['value'] . "%' ";
            $sql .= " OR kelompok LIKE '" . $requestData['search']['value'] . "%' ";
            $sql .= " OR status_read LIKE '" . $requestData['search']['value'] . "%' ";
            $sql .= " OR date_created LIKE '" . $requestData['search']['value'] . "%' ";
            $sql .= " OR status LIKE '" . $requestData['search']['value'] . "%' ";

            $query = $this->db->query($sql);
            $totalFiltered = $query->num_rows(); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

            $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.


            $query = $this->db->query($sql); // again run query with limit
        } else {
            $sql = "SELECT * ";
            $sql .= " FROM tb_notif";
            $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
            $query = $this->db->query($sql);
        }

        $data = array();
        $a = 0;
        foreach ($query->result_array() as $row) {  // preparing an array
            $nestedData = array();

            $desc = null;

            $nestedData[] = '<input type="checkbox" name="ik[]" id="ik[]" value="' . $row["id_notif"] . '">';
            $img = 'https://bundaneno.com/cms/assets/img/nogambar.jpg';
            if ($row['image'] != '') {
                $img = 'https://bundaneno.com/' . $row['image'];
            }
            $nestedData[] = '<img src="' . $img . '" width="40px">';
            $nestedData[] = $row["nm_notif"];
            $nestedData[] = $row["deskripsi"];
            $nestedData[] = $row["kelompok"];

            

            $str = null;
            if ($row['status_read'] == '0') {
                $str = '<button class="btn btn-danger">Belum Dibaca</button>';
            } else {
                $str = '<button class="btn btn-success">Sudah Dibaca</button>';
            }
            $nestedData[] = $str;
            $nestedData[] = $row["date_created"];


            $btn_ = '<a href="#" onclick="javascript:detilData(\'' . urlencode($desc) . '\', \'' . $img . '\');" data-toggle="tooltip" title="lihat" class="btn btn-sm btn-warning"> <img src="https://bundaneno.com/cms/assets/img/detail.png"></a>';
            $btn_ .= '<a href="insert_broadcast/' . $row['id_notif'] . '" data-toggle="tooltip" title="edit" class="btn btn-sm btn-primary"> <img src="https://bundaneno.com/cms/assets/img/edit.png"></a>';
            if ($row["status"] == '1') {
                $nestedData[] = '<span class="label label-success">active</span>';
                $btn_ .= '<a href="#" onclick="javascript:showdelete(\'' . $row['id_notif'] . '\');" data-toggle="tooltip" title="non aktifkan" class="btn btn-sm btn-danger"> <img src="https://bundaneno.com/cms/assets/img/delete.png"></a>';
            } else {
                $nestedData[] = '<span class="label label-danger">not active</span>';
                $btn_ .= '<a href="#" onclick="javascript:showactive(\'' . $row['id_notif'] . '\');" data-toggle="tooltip" title="aktifkan" class="btn btn-sm btn-success"> <img src="https://bundaneno.com/cms/assets/img/chek.png" style="width:16px;"></a>';
            }


            $nestedData[] = '<td>' . $btn . $btn_ . ''
                    . '<a href="#" onclick="showhapus(' . $row['id_notif'] . ')" data-toggle="tooltip" title="Hapus" class="btn btn-sm btn-danger"> <img src="https://bundaneno.com/cms/assets/img/delete1.png"> </a>
                     </td>';

            $data[] = $nestedData;
            $a++;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );
        echo json_encode($json_data);
        exit();
    }

    public function getnotifcms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

         $sql = "SELECT * FROM tb_notif WHERE date_created!='' " . $tipe . " GROUP BY kelompok ORDER BY id_notif DESC ".$limit;
        $this->response->getresponse($sql,'getnotifcms');
    }

    public function getnotif() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $ic = $parameter['ic'];
        $uuid = $parameter['uuid'];
        $tp = $parameter['tp'];
        $lt = $parameter['lt'];

        $limit = "";
        if ($lt != '') {
            $limit = " LIMIT " . $lt;
        }

        $tipe = "";
        if ($tp != '') {
            $tipe = " AND a.tipe='" . $tp . "' ";
        }

        if($ic!='' && $uuid!=''){
            $sql = "SELECT a.*,
                IFNULL( (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as nm_customer,
                IFNULL( (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as no_hp,
                IFNULL( (SELECT x.email FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as email
                FROM tb_notif a WHERE (a.id_customer='" . $ic . "' OR a.uuid='" . $uuid . "') AND a.status=1 AND a.status_read IN(0,1) ".$tipe." ORDER BY a.id_notif DESC ".$limit;
            $this->response->getresponse($sql,'getnotif');
        }elseif($ic!='' && $uuid==''){
            $sql = "SELECT a.*,
                IFNULL( (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as nm_customer,
                IFNULL( (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as no_hp,
                IFNULL( (SELECT x.email FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as email
                FROM tb_notif a WHERE a.id_customer='" . $ic . "' AND a.status=1 AND a.status_read IN(0,1) ".$tipe." ORDER BY a.id_notif DESC ".$limit;
            $this->response->getresponse($sql,'getnotif');
        }elseif($ic=='' && $uuid!=''){
            $sql = "SELECT a.*,
                IFNULL( (SELECT x.nm_customer FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as nm_customer,
                IFNULL( (SELECT x.no_hp FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as no_hp,
                IFNULL( (SELECT x.email FROM tb_customer x WHERE x.id_customer=a.id_customer), '') as email
                FROM tb_notif a WHERE a.uuid='" . $uuid . "' AND a.status=1 AND a.status_read IN(0,1) ".$tipe." ORDER BY a.id_notif DESC ".$limit;
            $this->response->getresponse($sql,'getnotif');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function getbyid() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sCek = $this->db->query("SELECT * FROM tb_notif WHERE id_notif='" . $id . "'");
        $rCek = $sCek->row_array();

        $kl = $rCek['kelompok'];

        if($id!=''){
            $sql = "SELECT * FROM tb_notif WHERE kelompok='" . $kl . "' ";
            $this->response->getresponse($sql,'getbyid');
        }else{
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updateread() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];
        $uuid = $parameter['uuid'];

        if($st==''){
            $st=1;
        }

        if ($st != '') {
            if($st=='all'){
                $sUpdate = "UPDATE tb_notif SET status_read=4 WHERE uuid='".$uuid."'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_notif WHERE uuid='" . $uuid . "' LIMIT 1";
            }else{
                $sUpdate = "UPDATE tb_notif SET status_read='".$st."' WHERE id_notif='" . $id . "'";
                $this->db->query($sUpdate);

                $sql = "SELECT * FROM tb_notif WHERE id_notif='" . $id . "'";
            }
            
            $this->response->getresponse($sql,'updateread');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatuscms() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        $sCek = $this->db->query("SELECT * FROM tb_notif WHERE id_notif='" . $id . "'");
        $rCek = $sCek->row_array();

        $kl = $rCek['kelompok'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_notif SET status='" . $st . "' WHERE kelompok='" . $kl . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_notif WHERE id_notif='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function updatestatus() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];
        $st = $parameter['st'];

        if ($st != '' && $id != '') {
            $sUpdate = "UPDATE tb_notif SET status='" . $st . "' WHERE id_notif='" . $id . "'";
            $this->db->query($sUpdate);

            $sql = "SELECT * FROM tb_notif WHERE id_notif='" . $id . "'";
            $this->response->getresponse($sql,'updatestatus');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

    public function delete() {

        $data = $this->param['data'];
        $decrypt = $this->general_lib->decryptData($data);
        $parameter = json_decode($decrypt,true);

        $id = $parameter['id'];

        $sCek = $this->db->query("SELECT * FROM tb_notif WHERE id_notif='" . $id . "'");
        $rCek = $sCek->row_array();

        $kl = $rCek['kelompok'];

        if ($id != '') {
            $sDelete = "DELETE FROM tb_notif WHERE kelompok='" . $kl . "'";;
            $this->db->query($sDelete);

            $sql = "SELECT * FROM tb_notif ORDER BY id_notif DESC LIMIT 1";
            $this->response->getresponse($sql,'delete');
        } else {
            $code = '02';
            $status = 'Required data parameter';
            $this->general_lib->error($code,$status);
        }
    }

}
