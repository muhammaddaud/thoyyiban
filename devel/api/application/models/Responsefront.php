<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Responsefront extends CI_Model
{

    public function getresponse($sql,$action,$pesan='',$err='',$message_code=''){
    	$result = array();

	    $code = "201";
	    $status = 'Data Tidak ditemukan...';

	    $str = array(
	        "result" => $result,
	        "code" => "206",
	        "message" => "Failed, transaction not allowed",
	    );

	    $sql = str_replace("\n", " ", $sql);
	    $sql = str_replace("\t", " ", $sql);

	//die($sql);
	// -------------------------------------------------------------------------------------------
	    $query = $this->db->query($sql);
	    $check = false;

	    if ($err == '') {
	        $a = 0;
        	foreach ($query->result_array() as $row) {
	            $result[] = $row;

	            $code = "200";
	            if ($pesan == '') {
	                $status = "Succes action " . $action;
	            } else {
	                $status = $pesan;
	            }
	            $check = true;
	            $a++;
	        }
	    }

	    $query->free_result();

	    $str = array(
	        "result" => $result,
	        "code" => $code,
	        "message" => $status
	    );


	// --------------------------------------------------------------
	    $json = json_encode($str);

	    header("Content-Type: application/json");
	    ob_clean();
	    flush();
        echo $json;
    	exit(1);
    }

    public function getresponseresi($sql,$action,$pesan='',$err='',$message_code=''){
    	$result = array();

	    $code = "201";
	    $status = 'No resi tidak valid';

	    $str = array(
	        "result" => $result,
	        "code" => "206",
	        "message" => "Failed, transaction not allowed",
	    );

	    $sql = str_replace("\n", " ", $sql);
	    $sql = str_replace("\t", " ", $sql);

	//die($sql);
	// -------------------------------------------------------------------------------------------
	    $query = $this->db->query($sql);
	    $check = false;

	    if ($err == '') {
	        $a = 0;
        	foreach ($query->result_array() as $row) {
	            $result[] = $row;

	            $code = "200";
	            if ($pesan == '') {
	                $status = "Succes action " . $action;
	            } else {
	                $status = $pesan;
	            }
	            $check = true;
	            $a++;
	        }
	    }

	    $query->free_result();

	    $str = array(
	        "result" => $result,
	        "code" => $code,
	        "message" => $status
	    );


	// --------------------------------------------------------------
	    $json = json_encode($str);

	    header("Content-Type: application/json");
	    ob_clean();
	    flush();
        echo $json;
    	exit(1);
    }


}