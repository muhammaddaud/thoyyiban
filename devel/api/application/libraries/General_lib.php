<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class General_lib {

    function key() {
        return "%^$%^&%*098765431255677787555TYWEBSITE";
    }

    function path() {
        return "/home/thoyyiban/web/api.thoyyiban.com/public_html/upload/";
    }

    function upath() {
        return "/home/thoyyiban/web/api.thoyyiban.com/public_html/";
    }

    function base_url() {
        return "https://thoyyiban.com/";
    }

    function url_thoyyiban() {
        return "https://thoyyiban.com/";
    }

    function key_thoyyiban() {
        return "&u=apiPST&p=024f77abbce3f5f34329ab186bd0668d";
    }

    function url_api_baraka() {
        return "http://api.barakalogistik.com/";
    }

    function crypt_url_lib( $string = null, $action = null,$key1 = null,$key2 =null) {
        $secret_key = $key1;
        $secret_iv = $key2;

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }

        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }


        return $output;
    }

    function decryptData($data) {
        $KEY_AES = 'sirotolmustakkim'; //combination of 16 character  1245714587458745
        $IV_AES = 'pHWZMBt_zZ6[<6Xm';  //combination of 16 character Initialtization Vector

        //To decrypt in PHP
        $method = 'aes-128-cbc';
        //$decryptedString = openssl_decrypt("encryptedString", $method, "SameKeyUsedInFlutter", 0, "SameIvUsedInFlutter");
        $decryptedString = openssl_decrypt($data, $method, $KEY_AES, 0, $IV_AES);
        return $decryptedString;
    }

    function random_numbers($digits) {
        $min = pow(10, $digits - 1);
        $max = pow(10, $digits) - 1;
        return mt_rand($min, $max);
    }  

    function general_http($path, $array) {
        $post = json_encode($array);
        $opts = array('http' =>
        array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $post
            ), "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )
        );
        $context = stream_context_create($opts);
        $datane = file_get_contents($path, false, $context);
        return $datane;
    }

    function push_http_baraka($path, $ar) {

        $array = array('data' => $this->encryptDataBaraka(json_encode($ar,true)));  
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array));

        $headers = array();
        $headers[] = 'Authorization: Basic YnVuZGFuZW5vOmE4NjJiOTc4NmUxNjAzYTJhMGI2ZDYxZmQ1N2MxOTA1';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $result = 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }


    function general_post_api($url, $data) {
        $post_data = http_build_query($data, '', '&');
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    function encryptDataBaraka($data) {
        $KEY_AES = 'S3OyuGt82VX8DtG8'; //combination of 16 character  1245714587458745
        $IV_AES = 'oYGSTPs_uV8[<8Xm';  //combination of 16 character Initialtization Vector

        //To decrypt in PHP
        $method = 'aes-128-cbc';
        //$decryptedString = openssl_decrypt("encryptedString", $method, "SameKeyUsedInFlutter", 0, "SameIvUsedInFlutter");
        //$decryptedString = openssl_decrypt($data, $method, $KEY_AES, 0, $IV_AES);
        $encryptedString = openssl_encrypt($data, $method, $KEY_AES, 0, $IV_AES);
        return $encryptedString;
    }

    function error($code, $status) {
        if ($code == "00") {
            $kode = "200";
        } else if ($code == "01") {
            $kode = "201";
        } else if ($code == "02") {
            $kode = "202";
        } else if ($code == "03") {
            $kode = "203";
        } else if ($code == "04") {
            $kode = "204";
        } else if ($code == "05") {
            $kode = "205";
        } else if ($code == "06") {
            $kode = "206";
        } else if ($code == "07") {
            $kode = "207";
        } else if ($code == "08") {
            $kode = "208";
        } else if ($code == "09") {
            $kode = "209";
        }


        $result = array();
        $str = array(
            "result" => $result,
            "code" => $kode,
            "message" => $status
        );

    // --------------------------------------------------------------
        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

    function response($str) {
        $json = json_encode($str);

        header("Content-Type: application/json");
        ob_clean();
        flush();
        echo $json;
        exit(1);
    }

}

?>